## :rocket: Unidad Mínima Rentable

![aaandrades](https://img.shields.io/badge/-Frontend-orange)
![Build](https://img.shields.io/badge/-Working-brightgreen)

### :memo: Descripción
Aplicación web para el cálculo y selección de modelos forestales. 
### :sparkles: Features
- Selección de modelos.
- Edición de información según cada modelo.
- Calculo de información.
- Descarga en formato PDF.
### :construction: Tecnologías
- Angular 12.
- Rxjs 6.
- Scss.
- Html.
- Typescript 4.
- Karma 6.
- Jasmine 4.

### :bulb: Ejecutar la aplicación
```
git clone
npm install
ng serve --open
```
