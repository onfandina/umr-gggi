import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { PFCComponent } from './pages/pfc/pfc.component';
import { RestauracionComponent } from './pages/restauracion/restauracion.component';
import { SilvopastorilComponent } from './pages/silvopastoril/silvopastoril.component';
import { BosqueNaturalComponent } from './pages/bosque-natural/bosque-natural.component';
import { AgroforestalComponent } from './pages/agroforestal/agroforestal.component';
import {MfsComponent} from "./pages/mfs/mfs.component";

const routes: Routes = [
  {
    path: 'modelos',
    component: MainPageComponent,
  },
  {
    path: 'pfc',
    component: PFCComponent,
    data: { animation: 'FilterPage' },
  },
  {
    path: 'mfs',
    component: MfsComponent,
    data: { animation: 'FilterPage' },
  },
  {
    path: 'restauracion',
    component: RestauracionComponent,
  },
  {
    path: 'silvopastoril',
    component: SilvopastorilComponent,
  },
  {
    path: 'bosque-natural',
    component: BosqueNaturalComponent,
  },
  {
    path: 'agroforestal',
    component: AgroforestalComponent,
  },
  { path: '**', redirectTo: 'modelos' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
