import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PFCComponent } from './pages/pfc/pfc.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterContainer } from './components/footer/footer-container.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CardFormComponent } from './components/card-form/card-form.component';
import { PfcModule } from './pages/pfc/pfc.module';
import { RestauracionComponent } from './pages/restauracion/restauracion.component';
import { SilvopastorilComponent } from './pages/silvopastoril/silvopastoril.component';
import { BosqueNaturalComponent } from './pages/bosque-natural/bosque-natural.component';
import { AgroforestalComponent } from './pages/agroforestal/agroforestal.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CommonLibsModule } from './components/common.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MsfModule} from "./pages/mfs/msf.module";
import {MatButtonModule} from "@angular/material/button";
import {MfsComponent} from "./pages/mfs/mfs.component";
import { CurrencyPipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    PFCComponent,
    MfsComponent,
    NavbarComponent,
    FooterContainer,
    SidebarComponent,
    CardFormComponent,
    MainPageComponent,
    RestauracionComponent,
    SilvopastorilComponent,
    BosqueNaturalComponent,
    AgroforestalComponent
  ],
  imports: [
    PfcModule,
    MsfModule,
    FormsModule,
    BrowserModule,
    MatIconModule,
    AppRoutingModule,
    MatStepperModule,
    CommonLibsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
  ],
  providers: [ CurrencyPipe ],
  bootstrap: [AppComponent],
})
export class AppModule {}
