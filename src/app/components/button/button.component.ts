import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'button-component',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  @Input() type: string = '';
  @Input() title: string = '';
  @Input() disable: boolean = true;
  
  @Output() HandleClick = new EventEmitter<Event>();
  @Output() HandleCancel = new EventEmitter<Event>();
  constructor() {}

  ngOnInit(): void {}

  handleClick() {
    this.HandleClick.emit();
  }

  handleCancel() {
    this.HandleCancel.emit();
  }
}
