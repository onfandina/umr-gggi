import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Costo } from 'src/app/core/entities/Costos/Costo';
import { Mantenimiento } from 'src/app/core/entities/Costos/Mantenimiento';
import { Poda } from 'src/app/core/entities/Costos/Poda';
import { ResumenCostos } from 'src/app/core/entities/Costos/ResumenCostos';
import { TransporteMayor } from 'src/app/core/entities/Costos/TransporteMayor';
import { TransporteMenor } from 'src/app/core/entities/Costos/TransporteMenor';
import { TipoTransporteMenor } from 'src/app/core/entities/Enumeradores/TipoTransporteMenor';
import { TipoVehiculo } from 'src/app/core/entities/Enumeradores/TipoVehiculo';
import { CustomDataTable } from 'src/app/core/helpers/DataTableEspecie';
import { modosCobroAdmin } from 'src/app/models/modosCobroAdmin';
import { modosCobroAsistencia } from 'src/app/models/modosCobroAsistencia';
import { modosCobroPoda } from 'src/app/models/modosCobroPoda';
import { tiposCostoTierra } from 'src/app/models/tiposCostoTierra';
import { EspecificacionCostoDialogComponent } from '../../dialogos/especificacion-costo-dialog/especificacion-costo-dialog.component';
import { ResumenCostosService } from '../services/resumenCostos/resumen-costos.service';

@Component({
  selector: 'app-costos',
  templateUrl: './costos.component.html',
  styleUrls: ['./costos.component.scss']
})
export class CostosComponent implements OnInit {

  //Forms
  fgroupCostos = new FormGroup({
    //Asistencia
    fcontrolModoCobroAsistencia: new FormControl('', Validators.required),
    fcontrolCostoAsistencia: new FormControl('', Validators.required),
    
    //Terreno
    fcontrolTipoCostoTierra: new FormControl('', Validators.required),
    fcontrolCostoTierra: new FormControl('', Validators.required),

    //Caminos
    fcontrolConstCaminos: new FormControl('', Validators.required),
    fcontrolKmConstCaminos: new FormControl('', Validators.required),
    fcontrolMantCaminos: new FormControl('', Validators.required),
    fcontrolKmMantCaminos: new FormControl('', Validators.required), 

    //Establecimiento
    fcontrolCostoEstablecimiento: new FormControl('', Validators.required),
    
    //Transporte menor
    fcontrolFuerzaHumana: new FormControl(),
    fcontrolMecanizado: new FormControl(),
    fcontrolFuerzaAnimal: new FormControl(),

    fcontrolPorcVolumenHumana: new FormControl(),
    fcontrolCostoM3Humana: new FormControl(),
    
    fcontrolPorcVolumenMecanizado: new FormControl(), 
    fcontrolCostoM3Mecanizado: new FormControl(),
    
    fcontrolPorcVolumenAnimal: new FormControl(),
    fcontrolCostoM3Animal: new FormControl(),
  
    //Transporte mayor
    fcontrolCamionSencillo: new FormControl(),
    fcontrolDobleTroque: new FormControl(),
    fcontrolTractoMula: new FormControl(),

    fcontrolPorcPrincipalSencillo: new FormControl(),
    fcontrolPorcIntermediaSencillo: new FormControl(),
    fcontrolTotalViajePrincipalSencillo: new FormControl(),
    fcontrolTotalViajeIntermediaSencillo: new FormControl(),
    fcontrolSalvoconductoPrincipalSencillo: new FormControl(),
    fcontrolSalvoconductoIntermediaSencillo: new FormControl(),
    fcontrolCarguePrincipalSencillo: new FormControl(),
    fcontrolCargueIntermediaSencillo: new FormControl(),

    fcontrolPorcPrincipalDT: new FormControl(),
    fcontrolPorcIntermediaDT: new FormControl(),
    fcontrolTotalViajePrincipalDT: new FormControl(),
    fcontrolTotalViajeIntermediaDT: new FormControl(),
    fcontrolSalvoconductoPrincipalDT: new FormControl(),
    fcontrolSalvoconductoIntermediaDT: new FormControl(),
    fcontrolCarguePrincipalDT: new FormControl(),
    fcontrolCargueIntermediaDT: new FormControl(),

    fcontrolPorcPrincipalTractomula: new FormControl(),
    fcontrolPorcIntermediaTractomula: new FormControl(),
    fcontrolTotalViajePrincipalTractomula: new FormControl(),
    fcontrolTotalViajeIntermediaTractomula: new FormControl(),
    fcontrolSalvoconductoPrincipalTractomula: new FormControl(),
    fcontrolSalvoconductoIntermediaTractomula: new FormControl(),
    fcontrolCarguePrincipalTractomula: new FormControl(),
    fcontrolCargueIntermediaTractomula: new FormControl(),

    //Costos Adicionales
    fcontrolCostoCif: new FormControl('', Validators.required),
    fcontrolModoCobroAdmin: new FormControl('', Validators.required),
    fcontrolCostoAdmin: new FormControl('', Validators.required),

    //Poda
    fcontrolModoCobroPoda: new FormControl('', Validators.required),
    
    fgroupPoda: new FormGroup({
      fcontrolAnioPoda: new FormControl(),
      fcontrolValorUnitarioAnio: new FormControl(),
    }),

    //Mantenimientos
    fgroupMantenimiento: new FormGroup({
      fcontrolCostoMantenimientoAnio: new FormControl(),
      fcontrolAnioMantenimiento: new FormControl(),
    }),

    fgroupCarbono: new FormGroup({
      fcontrolCostoCarbonoAnio: new FormControl(),
      fcontrolCostoCarbono: new FormControl(),
    }),

    //Otros costos
    fgroupOtroCosto: new FormGroup({
      fcontrolDescrOtroCosto: new FormControl(''),
      fcontroValorOtroCosto: new FormControl(),
      fcontrolAnioOtroCosto: new FormControl()
    }),

  });

  //Conditionals
  verFormCosto = false;
  modoValorFijo = true;
  verFormPoda = false;
  verFormInsumo = false;
  verFormMantenimiento = false;
  verFormCarbono = false;
  verFormOtrosCostos = false;
  verFormHumana = false;
  verFormAnimal = false;
  verFormMecanizado = false;
  verFormSencillo = false;
  verFormDobleTroque = false;
  verFormTractoMula = false;
  modoValorFijoAdmin = false;
  verCostosTerreno = true;
  aplicaPoda = true;

  //Data
  listaModosCobro =  modosCobroAsistencia;
  listaModosCobroPoda = modosCobroPoda;
  listaModosCobroAdmin = modosCobroAdmin;
  listaCostoTierra = tiposCostoTierra;
  listaPodas: Poda[] = [];
  listaInsumos: Costo[] = [];
  listaMantenimientos: Mantenimiento[] = [];
  listaTransportesMenor: TransporteMenor[] = [];
  listaTransportesMayor: TransporteMayor[] = [];
  listaCostosCarbono: Costo[] = [];
  listaOtrosCostos: Costo[] = [];

  //Tabla Podas
  dataPodas = new CustomDataTable<Poda>(this.listaPodas);
  podasColumns: string[] = ['anio', 'valor', 'accion'];

  //Tabla Insumos
  dataInsumos = new CustomDataTable<Costo>(this.listaInsumos);
  insumosColumns: string[] = ['nombre', 'valor', 'accion'];

  //Tabla Mantenimientos
  dataMantenimientos = new CustomDataTable<Mantenimiento>(this.listaMantenimientos);
  mantenimientosColumns: string[] = ['anio', 'valor', 'accion'];

  //Tabla Costos Carbono
  dataCostosCarbono = new CustomDataTable<Costo>(this.listaCostosCarbono);
  costosCarbonoColumns: string[] = ['anio', 'valor', 'accion'];

  //Tabla Otros Costos
  dataOtrosCostos = new CustomDataTable<Costo>(this.listaOtrosCostos);
  otrosCostosColumns: string[] = ['nombre', 'valor', 'accion']; 

  //Resumen costos
  resumenCostos = new ResumenCostos();


  constructor(public dialog: MatDialog,
              private servicioResumenCostos: ResumenCostosService) {
   }
 

  ngOnInit(): void {
  }

  //Elimina las validaciones a los controles indicados
  eliminarValidadores(controles: FormControl[]){
    controles.forEach(control => {
      control.clearValidators();
      control.updateValueAndValidity();
    });
  }

  //Agrega las validaciones a los controles indicados
  agregarValidadores(controles: FormControl[]){
    controles.forEach(control => {
      control.setValidators(Validators.required);
      control.updateValueAndValidity();
    });
  }

  //Event handlers
  
  //Establecimiento
  onEspecificarCostoEstablecimiento(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      titulo: 'Costos de Establecimiento',
      unidades: 'ha',
      conceptos:[
        {concepto: 'Mano de obra', valor: 'Limpia ha'},
        {concepto: 'Mano de obra', valor: 'Ahoyado'},
        {concepto: 'Mano de obra', valor: 'Trazado'},
        {concepto: 'Mano de obra', valor: 'Plateo'},
        {concepto: 'Mano de obra', valor: 'Plantación'},
        {concepto: 'Mano de obra', valor: 'Fertilización/Rustificación'},
      ]};

    let dialogRef = this.dialog.open(EspecificacionCostoDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.fgroupCostos.get('fcontrolCostoEstablecimiento')?.setValue(result);
      console.log("Dialog result: " +  result)
    });
  }

  onSelectModoPoda(){
    const modo = this.fgroupCostos.get('fcontrolModoCobroPoda')?.value;
    this.resumenCostos.modoCobroPodas = modo;
    if (modo == 'No Aplica') {
      this.aplicaPoda = false;
    }
    else {
      this.aplicaPoda = true;
    }
  } 

  //Manejador del evento al agregar una poda
  onAddPoda(){
    this.verFormPoda = true;
    let controles: FormControl[] = [];
    controles.push( this.fgroupCostos.controls.fgroupPoda.get('fcontrolAnioPoda') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupPoda.get('fcontrolValorUnitarioAnio') as FormControl);
    this.agregarValidadores(controles);
  }

  //Manejador del evento al cancelar una poda
  onCancelarPoda(){
    this.verFormPoda = false;
    let controles: FormControl[] = [];
    controles.push( this.fgroupCostos.controls.fgroupPoda.get('fcontrolAnioPoda') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupPoda.get('fcontrolValorUnitarioAnio') as FormControl);
    this.eliminarValidadores(controles);
  }

  //Guarda la poda
  onSavePoda(){
    this.verFormPoda = false;
    let poda = new Poda();
    poda.Anio = this.fgroupCostos.controls.fgroupPoda.get('fcontrolAnioPoda')?.value;
    poda.Valor = this.fgroupCostos.controls.fgroupPoda.get('fcontrolValorUnitarioAnio')?.value;
    this.listaPodas.push(poda);
    this.dataPodas.setData(this.listaPodas);
  }

  //Manejador del evento al eliminar una poda
  onDeletePoda(element: Poda){
      this.listaPodas = this.listaPodas.filter(p => p.Anio != element.Anio);
      this.dataPodas.setData(this.listaPodas); 
      this.verFormPoda = false; 
  }

  //Mantenimiento
  onAddMantenimiento(){
    this.verFormMantenimiento = true;
    let controles: FormControl[] = [];
    controles.push( this.fgroupCostos.controls.fgroupMantenimiento.get('fcontrolCostoMantenimientoAnio') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupMantenimiento.get('fcontrolAnioMantenimiento') as FormControl);
    this.agregarValidadores(controles);
  }

  //Manejador del evento al cancelar un mantenimiento
  onCancelarMantenimiento(){
    this.verFormMantenimiento = false;
    let controles: FormControl[] = [];
    controles.push( this.fgroupCostos.controls.fgroupMantenimiento.get('fcontrolCostoMantenimientoAnio') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupMantenimiento.get('fcontrolAnioMantenimiento') as FormControl);
    this.eliminarValidadores(controles);
  }

  //Guarda el mantenimiento
  onSaveMantenimiento(){
    this.verFormMantenimiento = false;
    let mantenimiento = new Mantenimiento();
    mantenimiento.Valor = this.fgroupCostos.controls.fgroupMantenimiento.get('fcontrolCostoMantenimientoAnio')?.value;
    mantenimiento.Anio = this.fgroupCostos.controls.fgroupMantenimiento.get('fcontrolAnioMantenimiento')?.value;
    this.listaMantenimientos.push(mantenimiento);
    this.dataMantenimientos.setData(this.listaMantenimientos);
  }

  //Manejador del evento al borrar el mantenimiento
  onDeleteMantenimiento(element: Mantenimiento){
    this.listaMantenimientos = this.listaMantenimientos.filter(p => p.Anio != element.Anio);
    this.dataMantenimientos.setData(this.listaMantenimientos); 
    this.verFormMantenimiento = false; 
  }

  //Manejador del evento al especificar el costo de mantenimiento
  onEspecificarCostoMantenimiento(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      titulo: 'Costos de Mantenimiento',
      unidades: 'ha',
      conceptos:[
        {concepto: 'Mano de obra', valor: 'Limpia ha'},
        {concepto: 'Mano de obra', valor: 'Fertilización'},
      ]};

    let dialogRef = this.dialog.open(EspecificacionCostoDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.fgroupCostos.controls.fgroupMantenimiento.get('fcontrolCostoMantenimientoAnio')?.setValue(result);
      console.log("Dialog result: " +  result)
    });
  }

  //Costos carbono

  onAddCarbono(){
    this.verFormCarbono = true;
    let controles: FormControl[] = [];
    controles.push( this.fgroupCostos.controls.fgroupCarbono.get('fcontrolCostoCarbonoAnio') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupCarbono.get('fcontrolCostoCarbono') as FormControl);
    this.agregarValidadores(controles);
  }
  onCancelarCarbono(){
    this.verFormCarbono = false;
    let controles: FormControl[] = [];
    controles.push(this.fgroupCostos.controls.fgroupCarbono.get('fcontrolCostoCarbonoAnio') as FormControl);
    controles.push(this.fgroupCostos.controls.fgroupCarbono.get('fcontrolCostoCarbono') as FormControl);
    this.eliminarValidadores(controles);
  }
  onSaveCarbono(){
    this.verFormCarbono = false;
    let costoCarbono = new Costo();
    costoCarbono.Anio = this.fgroupCostos.controls.fgroupCarbono.get('fcontrolCostoCarbonoAnio')?.value;
    costoCarbono.Valor = this.fgroupCostos.controls.fgroupCarbono.get('fcontrolCostoCarbono')?.value;
    this.listaCostosCarbono.push(costoCarbono);
    this.dataCostosCarbono.setData(this.listaCostosCarbono);
  }
  onDeleteCarbono(element: Costo){
    this.listaCostosCarbono = this.listaCostosCarbono.filter(p => p.Anio != element.Anio);
    this.dataCostosCarbono.setData(this.listaCostosCarbono); 
    this.verFormCarbono = false; 
  }
  

  //Otros costos

  //Manejador del evento al agregar otro costo
  onAddOtroCosto(){
    this.verFormOtrosCostos = true;
    let controles: FormControl[] = [];
    controles.push( this.fgroupCostos.controls.fgroupOtroCosto.get('fcontrolDescrOtroCosto') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupOtroCosto.get('fcontroValorOtroCosto') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupOtroCosto.get('fcontrolAnioOtroCosto') as FormControl);
    this.agregarValidadores(controles);
  }

   //Manejador del evento al cancelar otro costo
   onCancelarOtroCosto(){
    this.verFormOtrosCostos = false;
    let controles: FormControl[] = [];
    controles.push( this.fgroupCostos.controls.fgroupOtroCosto.get('fcontrolDescrOtroCosto') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupOtroCosto.get('fcontroValorOtroCosto') as FormControl);
    controles.push( this.fgroupCostos.controls.fgroupOtroCosto.get('fcontrolAnioOtroCosto') as FormControl);
    this.eliminarValidadores(controles);
  }

  //Manejador del evento al borrar otro costo
  onDeleteOtroCosto(element: Costo){
    this.listaOtrosCostos = this.listaOtrosCostos.filter(p => p.Nombre != element.Nombre);
    this.dataOtrosCostos.setData(this.listaOtrosCostos); 
    this.verFormOtrosCostos = false; 
  }

  //Manejador del evento al guardar otro costo
  onSaveOtroCosto(){
    this.verFormOtrosCostos = false;
    let otroCosto = new Costo();
    otroCosto.Nombre = this.fgroupCostos.controls.fgroupOtroCosto.get('fcontrolDescrOtroCosto')?.value;
    otroCosto.Valor = this.fgroupCostos.controls.fgroupOtroCosto.get('fcontroValorOtroCosto')?.value;
    otroCosto.Anio = this.fgroupCostos.controls.fgroupOtroCosto.get('fcontrolAnioOtroCosto')?.value;
    this.listaOtrosCostos.push(otroCosto);
    this.dataOtrosCostos.setData(this.listaOtrosCostos);
  }

  //Transporte menor
  
  //Abre la tabla de especificación de transporte menor
  onEspecificarCostoTranspMenor(tipoTransporte:TipoTransporteMenor){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      titulo: 'Costos de Transporte Menor',
      unidades: 'm3',
      conceptos:[
        {concepto: '', valor: ''},
      ]};

    let dialogRef = this.dialog.open(EspecificacionCostoDialogComponent, dialogConfig); 
    dialogRef.afterClosed().subscribe(result => {
      switch (tipoTransporte) {
        case TipoTransporteMenor.FuerzaHumana:
          this.fgroupCostos.get('fcontrolCostoM3Humana')?.setValue(result);
          break;
        case TipoTransporteMenor.Mecanizado:
          this.fgroupCostos.get('fcontrolCostoM3Mecanizado')?.setValue(result);
          break;
        case TipoTransporteMenor.FuerzaAnimal:
          this.fgroupCostos.get('fcontrolCostoM3Animal')?.setValue(result);
          break;
        default:
          break;
      }
      
      console.log("Dialog result: " +  result)
    });
  }

  //Manejador del evento al cambiar el check de fuerza humana
  cambioCheckFHumana(){
    let controles: FormControl[] = [];
    controles.push(this.fgroupCostos.controls.fcontrolPorcVolumenHumana as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCostoM3Humana as FormControl);

    if (this.fgroupCostos.get('fcontrolFuerzaHumana')?.value === true) {
      this.verFormHumana = true;     
      this.agregarValidadores(controles);
    }
    else{
      this.verFormHumana = false;
      this.eliminarValidadores(controles);
    }
  }

  //Manejador del evento al cambiar el check de transporte mecanizado
  cambioCheckMecanizado(){
    let controles: FormControl[] = [];
    controles.push(this.fgroupCostos.controls.fcontrolPorcVolumenMecanizado as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCostoM3Mecanizado as FormControl);

    if (this.fgroupCostos.get('fcontrolMecanizado')?.value === true){
      this.verFormMecanizado = true;
      this.agregarValidadores(controles);
    }
    else{
      this.verFormMecanizado = false;
      this.eliminarValidadores(controles);
    }
  }
  
  //Manejador del evento al cambiar el check de transporte animal
  cambioCheckAnimal(){
    let controles: FormControl[] = [];
    controles.push(this.fgroupCostos.controls.fcontrolPorcVolumenAnimal as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCostoM3Animal as FormControl);

    if (this.fgroupCostos.get('fcontrolFuerzaAnimal')?.value === true){
      this.verFormAnimal = true;
      this.agregarValidadores(controles);
    }
    else{
      this.verFormAnimal = false;
      this.eliminarValidadores(controles);
    }
    
  }

  //Manejador del evento al cambiar el checkbox de camión sencillo
  cambioCheckSencillo(){
    let controles: FormControl[] = [];
    controles.push(this.fgroupCostos.controls.fcontrolPorcPrincipalSencillo as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolPorcIntermediaSencillo as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolTotalViajePrincipalSencillo as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolTotalViajeIntermediaSencillo as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolSalvoconductoPrincipalSencillo as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolSalvoconductoIntermediaSencillo as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCarguePrincipalSencillo as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCargueIntermediaSencillo as FormControl);
 

    if (this.fgroupCostos.get('fcontrolCamionSencillo')?.value === true){
      this.verFormSencillo = true;
      this.agregarValidadores(controles);
    }
    else{
      this.verFormSencillo = false;
      this.eliminarValidadores(controles);
    }
  }

  //Manejador del evento al cambiar el checkbox de doble troque
  cambioCheckDobleTroque(){

    let controles: FormControl[] = [];
    controles.push(this.fgroupCostos.controls.fcontrolPorcPrincipalDT as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolPorcIntermediaDT as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolTotalViajePrincipalDT as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolTotalViajeIntermediaDT as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolSalvoconductoPrincipalDT as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolSalvoconductoIntermediaDT as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCarguePrincipalDT as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCargueIntermediaDT as FormControl);


    if (this.fgroupCostos.get('fcontrolDobleTroque')?.value === true){
      this.verFormDobleTroque = true;
      this.agregarValidadores(controles);
    }
    else{
      this.verFormDobleTroque = false;
      this.eliminarValidadores(controles);
    }
  }

  //Manejador del evento al cambiar el checkbox de tracto mula
  cambioCheckTractoMula(){

    let controles: FormControl[] = [];
    controles.push(this.fgroupCostos.controls.fcontrolPorcPrincipalTractomula as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolPorcIntermediaTractomula as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolTotalViajePrincipalTractomula as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolTotalViajeIntermediaTractomula as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolSalvoconductoPrincipalTractomula as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolSalvoconductoIntermediaTractomula as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCarguePrincipalTractomula as FormControl);
    controles.push(this.fgroupCostos.controls.fcontrolCargueIntermediaTractomula as FormControl);


    if (this.fgroupCostos.get('fcontrolTractoMula')?.value === true){
      this.verFormTractoMula = true;
      this.agregarValidadores(controles);
    }
    else{
      this.verFormTractoMula = false;
      this.eliminarValidadores(controles);
    }
  }

  //Otros costos
  onSelectModoAdmin(){
    const modo = this.fgroupCostos.get('fcontrolModoCobroAdmin')?.value;
    if(modo === 'Valor fijo anual'){
      this.modoValorFijoAdmin = true;
    }
    else{
      this.modoValorFijoAdmin = false;
    }
  }

  //Cambio en controles
  onSelectModoAsistencia(){
    const modo = this.fgroupCostos.get('fcontrolModoCobroAsistencia')?.value;
    if(modo === 'Valor fijo anual'){
      this.modoValorFijo = true;
    }
    else{
      this.modoValorFijo = false;
    }

    this.resumenCostos.modoCobroAsistencia = modo;
  }

  //Guarda los costos y envía resumen
  guardarCostos(){
    this.resumenCostos = new ResumenCostos();
    this.listaTransportesMayor = [];
    this.listaTransportesMenor = [];
    
    this.agregarCostosTransporteMenor();
    this.agregarCostosTransporteMayor();

    this.resumenCostos.valorJornal = this.obtenerControl('fcontrolValorJornal')?.value;
    this.resumenCostos.modoCobroAsistencia = this.obtenerControl('fcontrolModoCobroAsistencia')?.value;
    this.resumenCostos.costoAsistencia.Valor = this.obtenerControl('fcontrolCostoAsistencia')?.value;
    this.resumenCostos.costoTierra.Valor = this.obtenerControl('fcontrolCostoTierra')?.value;
    this.resumenCostos.costoConstCaminos.Valor = this.obtenerControl('fcontrolConstCaminos')?.value;
    this.resumenCostos.kmAConstruir = this.obtenerControl('fcontrolKmConstCaminos')?.value;
    this.resumenCostos.costoMantCaminos.Valor = this.obtenerControl('fcontrolMantCaminos')?.value;
    this.resumenCostos.kmAMantener = this.obtenerControl('fcontrolKmMantCaminos')?.value;
    this.resumenCostos.modoCobroPodas = this.obtenerControl('fcontrolModoCobroPoda')?.value;
    this.resumenCostos.podas = this.listaPodas;
    this.resumenCostos.insumos = this.listaInsumos;
    this.resumenCostos.costoEstablecimiento.Valor = this.obtenerControl('fcontrolCostoEstablecimiento')?.value;
    this.resumenCostos.mantenimientos = this.listaMantenimientos;
    this.resumenCostos.transportesMayor = this.listaTransportesMayor;
    this.resumenCostos.transportesMenor = this.listaTransportesMenor;
    this.resumenCostos.costosCarbono = this.listaCostosCarbono;
    this.resumenCostos.costoCIF.Valor = this.obtenerControl('fcontrolCostoCif')?.value;
    this.resumenCostos.modoCobroAdmin = this.obtenerControl('fcontrolModoCobroAdmin')?.value;
    this.resumenCostos.costoAdmin.Valor = this.obtenerControl('fcontrolCostoAdmin')?.value;
    this.resumenCostos.otrosCostos = this.listaOtrosCostos;

    this.servicioResumenCostos.enviarResumenCostos(this.resumenCostos);

  }

  //Agrega los costos de transporte menor
  agregarCostosTransporteMenor(){
    if(this.verFormHumana){
      let transporteHumano = new TransporteMenor();
      transporteHumano.Tecnologia = "Fuerza Humana";
      transporteHumano.porcentajeVolumen = this.fgroupCostos.get('fcontrolPorcVolumenHumana')?.value;
      transporteHumano.CostoM3 = this.fgroupCostos.get('fcontrolCostoM3Humana')?.value;
      this.listaTransportesMenor.push(transporteHumano);
    }
    if(this.verFormMecanizado){
      let transporteMecan = new TransporteMenor();
      transporteMecan.Tecnologia = "Mecanizado";
      transporteMecan.porcentajeVolumen = this.fgroupCostos.get('fcontrolPorcVolumenMecanizado')?.value;
      transporteMecan.CostoM3 = this.fgroupCostos.get('fcontrolCostoM3Mecanizado')?.value;
      this.listaTransportesMenor.push(transporteMecan);
    }
    if(this.verFormAnimal){
      let transporteAnimal= new TransporteMenor();
      transporteAnimal.Tecnologia = "Fuerza Animal";
      transporteAnimal.porcentajeVolumen = this.fgroupCostos.get('fcontrolPorcVolumenAnimal')?.value;
      transporteAnimal.CostoM3 = this.fgroupCostos.get('fcontrolCostoM3Animal')?.value;
      this.listaTransportesMenor.push(transporteAnimal);
    }
  }

  //Agrega los costos de transporte mayor
  agregarCostosTransporteMayor(){ 
    if(this.verFormSencillo){
      let camionSencillo = new TransporteMayor();
      camionSencillo.TipoVehiculo = TipoVehiculo.CamionSencillo;
      camionSencillo.porcentajeCiudadPrincipal = this.fgroupCostos.get('fcontrolPorcPrincipalSencillo')?.value;
      camionSencillo.porcentajeCiudadIntermedia = this.fgroupCostos.get('fcontrolPorcIntermediaSencillo')?.value;
      camionSencillo.costoViajeCiudadPrincipal = this.fgroupCostos.get('fcontrolTotalViajePrincipalSencillo')?.value;
      camionSencillo.costoViajeCiudadIntermedia = this.fgroupCostos.get('fcontrolTotalViajeIntermediaSencillo')?.value;
      camionSencillo.costoSalvoconductoCiudadPrincipal = this.fgroupCostos.get('fcontrolSalvoconductoPrincipalSencillo')?.value;
      camionSencillo.costoSalvoconductoCiudadIntermedia = this.fgroupCostos.get('fcontrolSalvoconductoIntermediaSencillo')?.value;
      camionSencillo.costoCargueCiudadPrincipal = this.fgroupCostos.get('fcontrolCarguePrincipalSencillo')?.value;
      camionSencillo.costoCargueCiudadIntermedia = this.fgroupCostos.get('fcontrolCargueIntermediaSencillo')?.value;
      this.listaTransportesMayor.push(camionSencillo);
    }
    if(this.verFormDobleTroque){
      let camionDT = new TransporteMayor();
      camionDT.TipoVehiculo = TipoVehiculo.DobleTroque;
      camionDT.porcentajeCiudadPrincipal = this.fgroupCostos.get('fcontrolPorcPrincipalDT')?.value;
      camionDT.porcentajeCiudadIntermedia = this.fgroupCostos.get('fcontrolPorcIntermediaDT')?.value;
      camionDT.costoViajeCiudadPrincipal = this.fgroupCostos.get('fcontrolTotalViajePrincipalDT')?.value;
      camionDT.costoViajeCiudadIntermedia = this.fgroupCostos.get('fcontrolTotalViajeIntermediaDT')?.value;
      camionDT.costoSalvoconductoCiudadPrincipal = this.fgroupCostos.get('fcontrolSalvoconductoPrincipalDT')?.value;
      camionDT.costoSalvoconductoCiudadIntermedia = this.fgroupCostos.get('fcontrolSalvoconductoIntermediaDT')?.value;
      camionDT.costoCargueCiudadPrincipal = this.fgroupCostos.get('fcontrolCarguePrincipalDT')?.value;
      camionDT.costoCargueCiudadIntermedia = this.fgroupCostos.get('fcontrolCargueIntermediaDT')?.value;
      this.listaTransportesMayor.push(camionDT);
    }
    if(this.verFormTractoMula){
      let tractomula = new TransporteMayor();
      tractomula.TipoVehiculo = TipoVehiculo.TractoMula;
      tractomula.porcentajeCiudadPrincipal = this.fgroupCostos.get('fcontrolPorcPrincipalTractomula')?.value;
      tractomula.porcentajeCiudadIntermedia = this.fgroupCostos.get('fcontrolPorcIntermediaTractomula')?.value;
      tractomula.costoViajeCiudadPrincipal = this.fgroupCostos.get('fcontrolTotalViajePrincipalTractomula')?.value;
      tractomula.costoViajeCiudadIntermedia = this.fgroupCostos.get('fcontrolTotalViajeIntermediaTractomula')?.value;
      tractomula.costoSalvoconductoCiudadPrincipal = this.fgroupCostos.get('fcontrolSalvoconductoPrincipalTractomula')?.value;
      tractomula.costoSalvoconductoCiudadIntermedia = this.fgroupCostos.get('fcontrolSalvoconductoIntermediaTractomula')?.value;
      tractomula.costoCargueCiudadPrincipal = this.fgroupCostos.get('fcontrolCarguePrincipalTractomula')?.value;
      tractomula.costoCargueCiudadIntermedia = this.fgroupCostos.get('fcontrolCargueIntermediaTractomula')?.value;
      this.listaTransportesMayor.push(tractomula);
    }
  }

  //helpers
  obtenerControl(nombreControl: string){
    return this.fgroupCostos.get(nombreControl);
  }
}
