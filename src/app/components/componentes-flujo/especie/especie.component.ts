import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Especie } from 'src/app/core/entities/Especie';
import { CustomDataTable } from '../../../core/helpers/DataTableEspecie';
import { Producto } from 'src/app/core/entities/Producto';
import { especies } from 'src/app/models/especies';
import { especiesRestauracion } from 'src/app/models/especiesRestauracion';
import { productos } from 'src/app/models/productos';
import { VolumenXProducto } from 'src/app/core/entities/VolumenProducto';
import { Entresaca } from 'src/app/core/entities/Entresaca';
import { Establecimiento } from 'src/app/core/entities/Establecimiento';
import { EspecieService } from '../services/especie/especie.service';
import { InformacionGeneralService } from '../services/informacionGeneral/informacion-general.service';
import { sistemasTransPrimaria } from 'src/app/models/sistemasTransPrimaria';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { EspecificacionCostoDialogComponent } from '../../dialogos/especificacion-costo-dialog/especificacion-costo-dialog.component';
import { UbicacionService } from '../services/ubicacion/ubicacion.service';
import { Ubicacion } from 'src/app/core/entities/Ubicacion';
import { CostoAprovechamientoDialogComponent } from '../../dialogos/costo-aprovechamiento-dialog/costo-aprovechamiento-dialog.component';
import { ModeloEspecie } from 'src/app/core/entities/ModeloEspecie';
import { Concepto } from 'src/app/pages/mfs/models/concepto.model';

@Component({
  selector: 'app-especie',
  templateUrl: './especie.component.html',
  styleUrls: ['./especie.component.scss'],
})


export class EspecieComponent implements OnInit {

  //Flujo
  flujoActual = '';

  // FormControls
  fgroupEspecie = new FormGroup({
    fcontrolNombre: new FormControl('',Validators.required),
    fcontrolNombreOtra: new FormControl(''),
    fcontrolTurno: new FormControl('', Validators.required),
    fcontrolRendimiento: new FormControl('', Validators.required),
    fcontrolConoceRendimiento: new FormControl(''),
    
    //Establecimientos
    fgroupEstablecimiento: new FormGroup({
        fcontrolAnioEstablecimiento: new FormControl(''),
        fcontrolAreaEstablecer: new FormControl(),
        fcontrolAreaBosque: new FormControl(),
        fcontrolSeguro: new FormControl(),
        fcontrolCercamiento: new FormControl()
    }),

    //Producto
    fgroupProducto: new FormGroup({
      fcontrolNombreProducto: new FormControl(''),
      fcontrolNombreOtro: new FormControl(''),
      fcontrolAprovechamiento: new FormControl(),
      fcontrolPrecioPrincipal: new FormControl(),
      fcontrolPrecioIntermedia: new FormControl(),
      fcontrolPrecioCarretera: new FormControl(),
      fcontrolPorcentajePrincipal: new FormControl(),
      fcontrolPorcentajeIntermedia: new FormControl(),
      fcontrolPorcentajeCarretera: new FormControl(),
      fcontrolSistemaTransformacion: new FormControl(''),
      fcontrolCostoTransPrimaria: new FormControl()
    }),

    //Entresaca
    fgroupEntresaca: new FormGroup({
      fcontrolPorcentajeEntresaca: new FormControl(),
      fcontrolAnioEntresaca: new FormControl(),
      fcontrolCostoAprovEntresaca: new FormControl(),
      fcontrolCortaFinal: new FormControl(''),
      fcontrolNoComercial: new FormControl(''),
      fcontrolTransMenorEntresaca: new FormControl(''),
    })
  });

  //Suscripciones
  suscripcionInfoGeneral!: Subscription;
  suscripcionUbicacion!: Subscription;

  // Helpers
  verFormEspecie = false;
  esOtraEspecie = false;
  esUpdate = false;
  verFormProducto = false;
  esUpdateProducto = false;
  verFormEntresaca = false;
  esUpdateEntresaca = false;
  esNoComercial = false;
  esOtroProducto = false;
  verFormEstablecimientos = false;
  especieActual!: Especie;
  entresacaActual!: Entresaca;  
  esCortaFinal = false;
  turnoSugerido = '';
  rendimientoSugerido = '';
  conoceRendimiento = false;
  
  //Data
  listaEspecies: ModeloEspecie[];
  listaEspeciesFiltrada:string[] = [];
  listaProductos = productos;
  listaSistemas = sistemasTransPrimaria;
  aniosEstablecimiento!: number;
  listaAniosEstablecimiento: number[] = [];
   
  productosUsuario: Producto[] = [];
  entresacasUsuario: Entresaca[] = [];
  volumenesProductos: VolumenXProducto[] = [];
  areaTotalProyecto!: number;
  establecimientos: Establecimiento[] = [];

  especiesUsuario: Especie[] = [];


  //Tabla especies
  displayedColumns: string[] = ['nombre','turno','rendimiento','accion'];
  dataEspeciesUsuario = new CustomDataTable<Especie>(this.especiesUsuario); 

  //Tabla establecimientos
  establecimientosColumns: string[] = ['anio', 'area','accion'];
  dataEstablecimientos = new CustomDataTable<Establecimiento>(this.establecimientos);

  //Tabla productos
  productoColumns: string[] = ['nombre', 'factor', 'precioPrincipal', 'precioIntermedia', 'precioCarretera', 'accion'];
  dataProductosUsuario = new CustomDataTable<Producto>(this.productosUsuario);

  //Tabla volumenes productos
  aniosTurno: number[] = [];
  dataProductosEntresaca = new CustomDataTable<VolumenXProducto>(this.volumenesProductos);
  volProductoColumns: string[] = ['producto', 'volumen'];

  //Tabla entresacas
  dataEntresacas = new CustomDataTable<Entresaca>(this.entresacasUsuario);
  entresacasColumns: string[] = ['anio', 'porcentaje', 'costoApro', 'accion'];

  //Observables
  filterEspecies!: Observable<string[]>;

  
  //Método constructor de la clase
  constructor(public dialog: MatDialog, private servicioEspecie: EspecieService, 
              private servicioInfoGeneral: InformacionGeneralService,
              private servicioUbicacion: UbicacionService
             ) 
  {
    this.flujoActual = sessionStorage.getItem('flujo') ?? '';

    this.suscripcionInfoGeneral = this.servicioInfoGeneral.OnInformacionGeneral().subscribe(nuevaInfoGen => {
      if (nuevaInfoGen) {
        this.aniosEstablecimiento = nuevaInfoGen.TiempoEstablecimiento;
        }
    });

    this.suscripcionUbicacion = this.servicioUbicacion.OnUbicacion().subscribe(nuevaUbicacion => {
      if (nuevaUbicacion) {
        this.filtrarEspecies(nuevaUbicacion);
      
      }
    });

   if (this.flujoActual != 'pfc') {
     this.listaEspecies = especiesRestauracion.sort();
   }
   else{
     this.listaEspecies = especies.sort();
   }

  }

  /*filtra las especies para plantar, según la temperatura
  y la precipitación del lugar de la plantación*/
  filtrarEspecies(nuevaUbicacion: Ubicacion) {
    let listaFiltrada: string[] = [];
    let menorTemperatura = 0;
    let mayorTemperatura = 0;
    let menorPrecipitacion = 0;
    let mayorPrecipitacion = 0;
    
   //Se comprueba temperatura
    if (nuevaUbicacion.RangoTemperatura) {
      if (nuevaUbicacion.RangoTemperatura.startsWith('<')) {
        menorTemperatura = -100;
        mayorTemperatura = parseInt(nuevaUbicacion.RangoTemperatura.slice(1));
      }
      else if (nuevaUbicacion.RangoTemperatura.startsWith('>')) {
        menorTemperatura = parseInt(nuevaUbicacion.RangoTemperatura.slice(1));
        mayorTemperatura = 100;
      }
      else {
        menorTemperatura = parseInt(nuevaUbicacion.RangoTemperatura.split('-')[0]);
        mayorTemperatura = parseInt(nuevaUbicacion.RangoTemperatura.split('-')[1])
      }      
    }

    //Se comprueba precipitación
    if (nuevaUbicacion.RangoPrecipitacion) {
      if (nuevaUbicacion.RangoTemperatura.startsWith('<')) {
        menorPrecipitacion = -100;
        mayorPrecipitacion = parseInt(nuevaUbicacion.RangoPrecipitacion.slice(1));
      }
      else if (nuevaUbicacion.RangoPrecipitacion.startsWith('>')) {
        menorPrecipitacion = parseInt(nuevaUbicacion.RangoPrecipitacion.slice(1));
        mayorPrecipitacion = 100;
      }
      else {
        menorPrecipitacion = parseInt(nuevaUbicacion.RangoPrecipitacion.split('-')[0]);
        mayorPrecipitacion = parseInt(nuevaUbicacion.RangoPrecipitacion.split('-')[1])
      }      
    }

  //Se comprueba especie
   if (this.listaEspecies && this.listaEspecies.length > 0) {
     this.listaEspecies.forEach(especie => {
       let cumpleTemperatura = false;
       let cumplePrecipitacion = false;
       for (let i = especie.LimiteInferiorTemperatura; i <= especie.LimiteSuperiorTemperatura; i++) {
        if(i >= menorTemperatura && i <= mayorTemperatura) {
          cumpleTemperatura = true;
        }        
       }
       for (let i = especie.LimiteInferiorPrecipitacion; i <= especie.LimiteSuperiorPrecipitacion; i++) {
        if(i >= menorPrecipitacion && i <= mayorPrecipitacion) {
          cumplePrecipitacion = true;
        }        
       }
       if (cumplePrecipitacion && cumpleTemperatura) {
         listaFiltrada.push(especie.Nombre);
       }
     });
   }

   this.listaEspeciesFiltrada = listaFiltrada;
  
   listaFiltrada.sort();
   listaFiltrada.push('Otra');
  }

  ngOnInit(): void {
    this.obtenerObjetoDeSesion();
  }

  //Obtiene el objeto de sesión correspondiente (revisar para varias especies)
  obtenerObjetoDeSesion(){
    if (sessionStorage.getItem('especies')) {
      const especies = JSON.parse(sessionStorage.getItem('especies') ?? '') as Especie[];
      
      this.especiesUsuario = especies;
      this.dataEspeciesUsuario.setData(this.especiesUsuario);
      
      let especieActual;
      if (especies && especies.length > 0) {
        especieActual = especies[0];
        this.establecimientos = especieActual.Establecimientos;
        this.dataEstablecimientos.setData(this.establecimientos);
        this.productosUsuario = especieActual.Productos;
        this.dataProductosUsuario.setData(this.productosUsuario);
        this.entresacasUsuario = especieActual.Entresacas;
        this.dataEntresacas.setData(this.entresacasUsuario); 
      }
    }
  }
  
  //Especie
  onSelectEspecie(){
    if(this.fgroupEspecie.get('fcontrolNombre')?.value === 'Otra'){
      this.esOtraEspecie = true;
      this.fgroupEspecie.controls.fcontrolNombreOtra.setValidators(Validators.required);
      this.fgroupEspecie.controls.fcontrolNombreOtra.updateValueAndValidity();
    }
    else{
      this.esOtraEspecie = false;
      this.fgroupEspecie.controls.fcontrolNombreOtra.clearValidators();
      this.fgroupEspecie.controls.fcontrolNombreOtra.updateValueAndValidity();
    }

    let especie = this.listaEspecies.find(e => e.Nombre === this.fgroupEspecie.get('fcontrolNombre')?.value);
    if (especie && especie.Turno) {
      this.turnoSugerido = especie.Turno.toString() + ' años';
    }
    else{
      this.turnoSugerido = '';
    }

    if (especie && especie.Rendimiento) {
      this.rendimientoSugerido = especie.Rendimiento.toString() + ' m³/ha/año';
    }
    else{
      this.rendimientoSugerido = '';
    }
  }

  //Cambió el valor del turno
  turnoChanged(){
    const turno = this.fgroupEspecie.get('fcontrolTurno')?.value;
    for (let index = 1; index < turno + 1; index++) {
        this.aniosTurno.push(index); 
    }
  } 

  //Establecimiento
  onAddEstablecimiento(){
    this.verFormEstablecimientos = true;
    this.listaAniosEstablecimiento = [];
    for (let index = 0; index < this.aniosEstablecimiento; index++) {
      this.listaAniosEstablecimiento.push(index + 1);
      
    } 
    this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAnioEstablecimiento')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAnioEstablecimiento')?.updateValueAndValidity();
    this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAreaEstablecer')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAreaEstablecer')?.updateValueAndValidity();
  }

  //Manejador del evento guardar establecimiento
  onGuardarEstablecimiento(){
    this.cerrarFormEstablecimiento();

    let establecimiento = new Establecimiento;
    establecimiento.Anio = this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAnioEstablecimiento')?.value;
    establecimiento.Area = this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAreaEstablecer')?.value;
    establecimiento.AreaBosque = this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAreaBosque')?.value;
    establecimiento.CostoSeguro = this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolSeguro')?.value;
    establecimiento.CostoCercamiento = this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolCercamiento')?.value;
    if(!this.establecimientos.some(e => e.Anio == establecimiento.Anio)){
      this.establecimientos.push(establecimiento);
    }

    this.dataEstablecimientos.setData(this.establecimientos);
  }

  //Permite salir del formulario de establecimiento
  cerrarFormEstablecimiento(){
    this.verFormEstablecimientos = false;

    this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAnioEstablecimiento')?.clearValidators();
    this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAnioEstablecimiento')?.updateValueAndValidity();
    this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAreaEstablecer')?.clearValidators();
    this.fgroupEspecie.controls.fgroupEstablecimiento.get('fcontrolAreaEstablecer')?.updateValueAndValidity();
  }

  //Manejador del evento borrar establecimiento
  onDeleteEstablecimiento(element: Establecimiento){
    this.establecimientos = this.establecimientos.filter(e => e.Anio != element.Anio);
    this.dataEstablecimientos.setData(this.establecimientos); 
    this.verFormEstablecimientos = false; 
  }
  
  //Rendimiento
  onCambioConoceRendimiento(){
    if (this.fgroupEspecie.controls.fcontrolConoceRendimiento.value == 'true') {
      this.conoceRendimiento = true;
    } 
    else {
      this.conoceRendimiento = false;
    }
  }

  //Entresaca
  cambioCheckCortaFinal(){
    this.esCortaFinal = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCortaFinal')?.value;
    let controlAnio = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolAnioEntresaca');
    let controlPorcentaje = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolPorcentajeEntresaca');
    let turno = this.fgroupEspecie.controls.fcontrolTurno.value;

    controlPorcentaje?.setValue(100);
    controlPorcentaje?.disable();

    if (this.esCortaFinal && controlAnio != undefined) {
      controlAnio.setValue(turno);
      controlAnio.disable();
     }
     else{
       controlAnio?.setValue(0);
       controlAnio?.enable();
       controlPorcentaje?.setValue(0);
       controlPorcentaje?.enable();
     }

    console.log("ES CORTA FINAL: " + this.esCortaFinal);
  }

  cambioCheckNoComercial(){
    this.esNoComercial = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolNoComercial')?.value;
    if (this.esNoComercial) {
      this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolTransMenorEntresaca')?.setValidators(Validators.required);
      this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolTransMenorEntresaca')?.updateValueAndValidity();
    }
    else{
      this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolTransMenorEntresaca')?.clearValidators();
      this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolTransMenorEntresaca')?.updateValueAndValidity();
    }
  }

  onSelectSistema(){
    const sistemaSeleccionado = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolSistemaTransformacion')?.value;

    if (sistemaSeleccionado == 'Ninguno') {
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolCostoTransPrimaria')?.setValue(0);
    }
  }

  //Manejador del evento al agregar la entresaca
  onAddEntresaca(){
    this.verFormEntresaca = true;
    let controlAnio = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolAnioEntresaca');
    this.esCortaFinal = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCortaFinal')?.value;

    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolPorcentajeEntresaca')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolPorcentajeEntresaca')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolAnioEntresaca')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolAnioEntresaca')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCostoAprovEntresaca')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCostoAprovEntresaca')?.updateValueAndValidity();

    if (!this.esCortaFinal) {
      controlAnio?.enable();
    }
    else{
      controlAnio?.setValue(this.fgroupEspecie.controls.fcontrolTurno.value);
      controlAnio?.disable();
    }
  
    this.volumenesProductos = [];

    this.productosUsuario.forEach(p => {
      const volumenP: VolumenXProducto = new VolumenXProducto();
      volumenP.producto = p.Nombre;
      volumenP.volumen = 0;
      //if (!this.volumenesProductos.find(v => v.producto === p.Nombre)) {
        this.volumenesProductos.push(volumenP);
      //}      
    });
    this.dataProductosEntresaca.setData(this.volumenesProductos);
  }

  //Manejador del evento al guardar la entresaca
  onGuardarEntresaca(){
    this.esCortaFinal = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCortaFinal')?.value;
    
    if (this.entresacasUsuario.filter(p => p.Anio === this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolAnioEntresaca')?.value).length == 0){

    this.entresacaActual = new Entresaca();

    this.entresacaActual.PorcentajeAprovechamiento = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolPorcentajeEntresaca')?.value;
    this.entresacaActual.VolumenXProducto = this.volumenesProductos;
    this.entresacaActual.CostoAprovechamiento = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCostoAprovEntresaca')?.value;

    if(this.esCortaFinal)
    {
      this.entresacaActual.Anio = this.fgroupEspecie.controls.fcontrolTurno.value;
      this.especieActual.CortaFinal = this.entresacaActual;
      this.entresacasUsuario.push(this.entresacaActual);
    }
    else
    {
      this.entresacaActual.Anio = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolAnioEntresaca')?.value;
      this.entresacasUsuario.push(this.entresacaActual);
    }

    if (this.esNoComercial) {
      this.entresacaActual.esNoComercial = true;
      this.entresacaActual.valorTransporteMenor = this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolTransMenorEntresaca')?.value;
    }

    this.cerrarFormEntresaca();
    this.dataEntresacas.setData(this.entresacasUsuario);
    
    }
  }

  //Cierra el formulario de enresaca
  cerrarFormEntresaca(){
    this.verFormEntresaca = false;

    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolPorcentajeEntresaca')?.clearValidators();
    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolPorcentajeEntresaca')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolAnioEntresaca')?.clearValidators();
    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolAnioEntresaca')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCostoAprovEntresaca')?.clearValidators();
    this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCostoAprovEntresaca')?.updateValueAndValidity();
  }

  //Manejador del evento al eliminar una entresaca
  onDeleteEntresaca(entresacaSeleccionada: Entresaca){
    this.entresacasUsuario = this.entresacasUsuario.filter(e => e != entresacaSeleccionada);
    this.dataEntresacas.setData(this.entresacasUsuario);
  }

  //Manejador del evento al especificar el costo de aprovechamiento
  onEspecificarCostoAprovechamiento(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      titulo: 'Costos de Aprovechamiento',
      unidades: 'm3',
      conceptos:[
        {concepto: '', valor: ''},
      ]};

    let dialogRef = this.dialog.open(EspecificacionCostoDialogComponent, dialogConfig); 
    dialogRef.afterClosed().subscribe(result => {
      this.fgroupEspecie.controls.fgroupEntresaca.get('fcontrolCostoAprovEntresaca')?.setValue(result);
      console.log("Dialog result: " +  result)
    });
  }

  //Producto
  onSelectProducto(){
    const productoSeleccionado = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.value;
    if(productoSeleccionado === 'Otro'){
      this.esOtroProducto = true;
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.setValidators(Validators.required);
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.updateValueAndValidity();
    }
    else{
      this.esOtroProducto = false;
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.clearValidators();
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.updateValueAndValidity();
    }
  }

  //Manejador del evento al especificar el costo de transformación primaria
  onEspecificarTransPrimaria(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      titulo: 'Costos de Transformación Primaria',
      unidades: 'm3',
      conceptos:[
        {concepto: 'Mano de obra', valor: 'Jornales'},
        {concepto: 'Mano de obra especializada', valor: 'Operario'},
        {concepto: 'Mano de obra especializada 2', valor: 'Operario 2'},
        {concepto: 'Insumos', valor: 'Combustible 1'},
        {concepto: 'Insumos', valor: 'Combustible 2'},
        {concepto: 'Insumos', valor: 'Combustible 3'},
        {concepto: 'Insumos', valor: 'Otros'},
        {concepto: 'Maquinaria', valor: 'Mantenimiento'},
        {concepto: 'Maquinaria', valor: 'Alquiler de la máquina o desgaste'}, 
      ]};

    let dialogRef = this.dialog.open(EspecificacionCostoDialogComponent, dialogConfig); 
    dialogRef.afterClosed().subscribe(result => {
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolCostoTransPrimaria')?.setValue(result);
      console.log("Dialog result: " +  result)
    });
  }

  //Manejador del evento agregar producto
  onAddProducto(){
    this.verFormProducto = true;
    this.esUpdateProducto = false;
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.enable();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.enable();
    this.agregarValidadoresProducto()
  }

  //Agrega los validadores para los campos del formulario de producto
  agregarValidadoresProducto(){
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolAprovechamiento')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolAprovechamiento')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioPrincipal')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioPrincipal')?.updateValueAndValidity();
    
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioIntermedia')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioIntermedia')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioCarretera')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioCarretera')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajePrincipal')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajePrincipal')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeIntermedia')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeIntermedia')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeCarretera')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeCarretera')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolSistemaTransformacion')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolSistemaTransformacion')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolCostoTransPrimaria')?.setValidators(Validators.required);
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolCostoTransPrimaria')?.updateValueAndValidity();
  }

  //Manejador del evento al guardar producto
  onGuardarProducto(){
    let producto = new Producto();
    let nombre = '';
    if (this.fgroupEspecie) {
      const productoSeleccionado = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.value;
      if (productoSeleccionado === 'Otro') {
        nombre = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.value;
      }
      else{
        nombre = productoSeleccionado;
      }
      if (!this.esUpdateProducto) {        
        if (this.productosUsuario.filter(p => p.Nombre === nombre).length == 0) {
          producto.Nombre = nombre;         
          producto.FactorAprovechamientoEfectivo = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolAprovechamiento')?.value;
          producto.PrecioCiudadPrincipal = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioPrincipal')?.value;
          producto.PrecioCiudadIntermedia = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioIntermedia')?.value;
          producto.PrecioBordeCarretera = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioCarretera')?.value;
          producto.PorcentajeCiudadPrincipal = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajePrincipal')?.value;
          producto.PorcentajeCiudadIntermedia = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeIntermedia')?.value;
          producto.PorcentajeBordeCarretera = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeCarretera')?.value;
          producto.SistemaTransPrimaria = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolSistemaTransformacion')?.value;          
          producto.CostoTransPrimaria = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolCostoTransPrimaria')?.value;
          this.productosUsuario.push(producto);
          this.dataProductosUsuario.setData(this.productosUsuario);
        }      
      }
      else {
        let productoCambiar = this.productosUsuario.find(p => p.Nombre === nombre);        
        if (productoCambiar) {
          let i = this.productosUsuario.indexOf(productoCambiar);
          productoCambiar.Nombre = nombre;
          productoCambiar.FactorAprovechamientoEfectivo = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolAprovechamiento')?.value;
          productoCambiar.PrecioCiudadPrincipal = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioPrincipal')?.value;
          productoCambiar.PrecioCiudadIntermedia = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioIntermedia')?.value;
          productoCambiar.PrecioBordeCarretera = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioCarretera')?.value;
          productoCambiar.PorcentajeCiudadPrincipal = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajePrincipal')?.value;
          productoCambiar.PorcentajeCiudadIntermedia = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeIntermedia')?.value;
          productoCambiar.PorcentajeBordeCarretera = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeCarretera')?.value;
          productoCambiar.SistemaTransPrimaria = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolSistemaTransformacion')?.value;            
          productoCambiar.CostoTransPrimaria = this.fgroupEspecie.controls.fgroupProducto.get('fcontrolCostoTransPrimaria')?.value;
          this.productosUsuario[i] = productoCambiar;
          this.dataProductosUsuario.setData(this.productosUsuario);
         
        }      
      }  
      this.cerrarFormProducto();
      
    }
        
  }

  //Cierra el formulario de producto
  cerrarFormProducto(){
    this.verFormProducto = false;
    
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolAprovechamiento')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolAprovechamiento')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioPrincipal')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioPrincipal')?.updateValueAndValidity();
    
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioIntermedia')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioIntermedia')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioCarretera')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioCarretera')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajePrincipal')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajePrincipal')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeIntermedia')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeIntermedia')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeCarretera')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeCarretera')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolSistemaTransformacion')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolSistemaTransformacion')?.updateValueAndValidity();

    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolCostoTransPrimaria')?.clearValidators();
    this.fgroupEspecie.controls.fgroupProducto.get('fcontrolCostoTransPrimaria')?.updateValueAndValidity();      
  }      

  //Actualiza un producto
  updateProducto(producto: Producto){
    this.esUpdateProducto = true; 
    this.verFormProducto = true;
    
    if (this.fgroupEspecie) {
      if (this.listaProductos.filter(p => p === producto.Nombre).length == 0) {
        this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.setValue('Otro');
        this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.disable();
        this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.setValue(producto.Nombre);
        this.esOtroProducto = true;
      }
      else{
        this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.setValue(producto.Nombre);
        this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.setValue('');
        this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.clearValidators();
        this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreOtro')?.updateValueAndValidity();
        this.esOtroProducto = false;
      }
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolNombreProducto')?.disable();      
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolAprovechamiento')?.setValue(producto.FactorAprovechamientoEfectivo);
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioPrincipal')?.setValue(producto.PrecioCiudadPrincipal);
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioIntermedia')?.setValue(producto.PrecioCiudadIntermedia);
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPrecioCarretera')?.setValue(producto.PrecioBordeCarretera);
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajePrincipal')?.setValue(producto.PorcentajeCiudadPrincipal);
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeIntermedia')?.setValue(producto.PorcentajeCiudadIntermedia);
      this.fgroupEspecie.controls.fgroupProducto.get('fcontrolPorcentajeCarretera')?.setValue(producto.PorcentajeBordeCarretera);
    }
   
  }

  //Elimina un producto
  deleteProducto(producto: Producto){
    this.productosUsuario = this.productosUsuario.filter(p => p.Nombre != producto.Nombre);
    this.dataProductosUsuario.setData(this.productosUsuario); 
    this.verFormProducto = false; 
  }

  //Especie
  onAddEspecie(){
    this.limpiarVariables();
    this.fgroupEspecie.get('fcontrolNombre')?.enable(); 
    this.esUpdate = false;
    this.verFormEspecie = true;
    this.especieActual = new Especie();
    this.fgroupEspecie.reset(); 
  }

  onGuardarEspecie(){
    let nombre = '';
    if (this.fgroupEspecie.get('fcontrolNombre')?.value === 'Otra') {
      nombre = this.fgroupEspecie.get('fcontrolNombreOtra')?.value
    }
    else
    {
      nombre = this.fgroupEspecie.get('fcontrolNombre')?.value;
    }    
    const turno = this.fgroupEspecie.get('fcontrolTurno')?.value;
    const rendimiento = this.fgroupEspecie.get('fcontrolRendimiento')?.value;
    if(!this.esUpdate)
    { 
      if (!this.especiesUsuario.find(e => e.Nombre === nombre)) {
        this.especieActual.Nombre = nombre;
        this.especieActual.Turno = turno;
        this.especieActual.RendimientoAnualPromedio = rendimiento;
        this.especieActual.Productos = this.productosUsuario;
        this.especieActual.Entresacas = this.entresacasUsuario;
        this.especieActual.Establecimientos = this.establecimientos;
        this.especieActual.ConoceRendimiento = this.conoceRendimiento;
        this.especiesUsuario.push(this.especieActual);
      }    
    }
    else{
      let especieCambiar = this.especiesUsuario.find(e => e.Nombre === nombre);
      if (especieCambiar) {
        especieCambiar.Nombre = nombre;
        especieCambiar.Turno = turno;
        especieCambiar.RendimientoAnualPromedio = rendimiento;
        especieCambiar.RendimientoAnualPromedio = rendimiento;
        especieCambiar.Productos = this.productosUsuario;
        especieCambiar.Entresacas = this.entresacasUsuario;
        especieCambiar.Establecimientos = this.establecimientos;
        especieCambiar.ConoceRendimiento = this.conoceRendimiento;
        let i = this.especiesUsuario.indexOf(especieCambiar);
        this.especiesUsuario[i] = especieCambiar;
      }      
    } 
    this.dataEspeciesUsuario.setData(this.especiesUsuario);
    
    this.enviarEspecies();
    this.verFormEspecie = false;  
  }

  updateEspecie(especie: Especie){
    this.esUpdate = true;
    this.verFormEspecie = true;
    this.fgroupEspecie.get('fcontrolNombre')?.disable(); 
    this.fgroupEspecie.get('fcontrolNombre')?.setValue(especie.Nombre);
    this.fgroupEspecie.get('fcontrolTurno')?.setValue(especie.Turno);
    this.fgroupEspecie.get('fcontrolRendimiento')?.setValue(especie.RendimientoAnualPromedio);
    this.fgroupEspecie.get('fcontrolConoceRendimiento')?.setValue(especie.ConoceRendimiento);
    this.conoceRendimiento = especie.ConoceRendimiento;
  }

  deleteEspecie(especie: Especie){
    this.especiesUsuario = this.especiesUsuario.filter(e => e.Nombre != especie.Nombre);
    this.dataEspeciesUsuario.setData(this.especiesUsuario); 
    this.verFormEspecie = false; 
  }

  //envíoEspecies
  enviarEspecies(){
    sessionStorage.setItem('especies', JSON.stringify(this.especiesUsuario));
    this.servicioEspecie.enviarEspecie(this.especiesUsuario);
  }

  //Establecimiento
  onChangeArea(element: any, event: any){
    let establecimiento = this.establecimientos.find(e => e.Anio === element.Anio);
    if (establecimiento) {
      establecimiento.Area = event.target.value;
    }
  }

  //Entresaca
  onChangeVolumen(element: any, event: any){
    let volumenxproducto =this.volumenesProductos.find(v => v.producto === element.producto);
      if (volumenxproducto) {
        volumenxproducto.volumen = Number.parseFloat(event.target.value);
      }
  }

  //Guardar especies
  guardarEspecies(){
    this.enviarEspecies()
  }

  cerrarFormEspecie(){
    this.verFormEspecie = false;
    this.fgroupEspecie.reset();
    this.limpiarVariables();
  }

  //Limpiar variables
  limpiarVariables(){
    this.productosUsuario = [];
    this.establecimientos = [];
    this.entresacasUsuario = [];
  }

}
