import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BonoCarbono } from 'src/app/core/entities/Ingresos/BonoCarbono';
import { CertificadoIncentivoForestal } from 'src/app/core/entities/Ingresos/CertificadoIncentivoForestal';
import { Ingreso } from 'src/app/core/entities/Ingresos/Ingreso';
import { ResumenIngresos } from 'src/app/core/entities/Ingresos/ResumenIngresos';
import { CustomDataTable } from 'src/app/core/helpers/DataTableEspecie';
import { ResumenIngresosService } from '../services/resumenIngresos/resumen-ingresos.service';

@Component({
  selector: 'app-ingresos',
  templateUrl: './ingresos.component.html',
  styleUrls: ['./ingresos.component.scss']
})
export class IngresosComponent implements OnInit {

  //Forms
  fgroupIngresos = new FormGroup({
    
    //Ingresos CIF
    fcontrolCifEstablecimiento: new FormControl('', Validators.required),
    fcontrolCifMant2: new FormControl('', Validators.required),
    fcontrolCifMant3: new FormControl('', Validators.required),
    fcontrolCifMant4: new FormControl('', Validators.required),
    fcontrolCifMant5: new FormControl('', Validators.required),

    //Ingresos CIF Bosque Natural
    fcontrolCifBosque2: new FormControl('', Validators.required),
    fcontrolCifBosque3: new FormControl('', Validators.required),
    fcontrolCifBosque4: new FormControl('', Validators.required),
    fcontrolCifBosque5: new FormControl('', Validators.required),


    //Ingresos Carbono
    fgroupCarbono: new FormGroup({
      fcontrolAnioBono: new FormControl(),
      fcontrolValorBono: new FormControl(),
    }),

    //Otros ingresos
    fgroupOtroIngreso: new FormGroup({
      fcontrolDescrOtroIngreso: new FormControl(''),
      fcontrolAnioOtroIngreso: new FormControl(),
      fcontroValorOtroIngreso: new FormControl()
    })
    
  });

  //Data
  resumenIngresos: ResumenIngresos = new ResumenIngresos();
  
  //Tabla Ingresos CIF
  /* cifDisplayedColumns: string[] = ['anio','valor','accion'];
  dataIngresosCif = new CustomDataTable<Ingreso>(this.resumenIngresos.ingresosCif); */

  //Tabka Ingresos Carbono
  carbonoDisplayedColumns: string[] = ['descripcion','valor','accion'];
  dataIngresosCarbono = new CustomDataTable<Ingreso>(this.resumenIngresos.ingresosBonos);

  otrosIngresosColumns: string[] = ['descripcion', 'anio', 'valor','accion']
  dataOtrosIngresos = new CustomDataTable<Ingreso>(this.resumenIngresos.otrosIngresos);

  //Conditionals
  verFormCif = false;
  verFormCarbono = false;
  esUpdateCif = false;
  esUpdateCarbono = false;
  verFormOtrosIngresos = false;


  constructor(private servicioIngresos: ResumenIngresosService) { }

  ngOnInit(): void {
  }

  //Elimina las validaciones a los controles indicados
  eliminarValidadores(controles: FormControl[]){
    controles.forEach(control => {
      control.clearValidators();
      control.updateValueAndValidity();
    });
  }

  //Agrega las validaciones a los controles indicados
  agregarValidadores(controles: FormControl[]){
    controles.forEach(control => {
      control.setValidators(Validators.required);
      control.updateValueAndValidity();
    });
  }
  
  //Event handlers Carbono
  onAddCarbono(){
    this.verFormCarbono = true;
    let controles: FormControl[] = [];
    controles.push( this.fgroupIngresos.controls.fgroupCarbono.get('fcontrolAnioBono') as FormControl);
    controles.push( this.fgroupIngresos.controls.fgroupCarbono.get('fcontrolValorBono') as FormControl);
    this.agregarValidadores(controles);
  }

  onCancelarCarbono(){
    this.verFormCarbono = false;
    let controles: FormControl[] = [];
    controles.push( this.fgroupIngresos.controls.fgroupCarbono.get('fcontrolAnioBono') as FormControl);
    controles.push( this.fgroupIngresos.controls.fgroupCarbono.get('fcontrolValorBono') as FormControl);
    this.eliminarValidadores(controles);
  }

  onSaveCarbono(){
    this.verFormCarbono = false;
    const ingreso = new BonoCarbono();
    ingreso.Anio = this.fgroupIngresos.controls.fgroupCarbono.get('fcontrolAnioBono')?.value;
    ingreso.Valor = this.fgroupIngresos.controls.fgroupCarbono.get('fcontrolValorBono')?.value;
    this.resumenIngresos.ingresosBonos.push(ingreso);
    this.dataIngresosCarbono.setData(this.resumenIngresos.ingresosBonos);
  }

  guardarIngresos(){
    this.resumenIngresos.ingresoCifEstablecimiento = this.obtenerControl('fcontrolCifEstablecimiento')?.value ?? 0;
    this.resumenIngresos.ingresoCifMant2 = this.obtenerControl('fcontrolCifMant2')?.value ?? 0;
    this.resumenIngresos.ingresoCifMant3 = this.obtenerControl('fcontrolCifMant3')?.value ?? 0;
    this.resumenIngresos.ingresoCifMant4 = this.obtenerControl('fcontrolCifMant4')?.value ?? 0;
    this.resumenIngresos.ingresoCifMant5 = this.obtenerControl('fcontrolCifMant5')?.value ?? 0;

    this.resumenIngresos.ingresoCifBosque2 = this.obtenerControl('fcontrolCifBosque2')?.value ?? 0;
    this.resumenIngresos.ingresoCifBosque3 = this.obtenerControl('fcontrolCifBosque3')?.value ?? 0;
    this.resumenIngresos.ingresoCifBosque4 = this.obtenerControl('fcontrolCifBosque4')?.value ?? 0;
    this.resumenIngresos.ingresoCifBosque5 = this.obtenerControl('fcontrolCifBosque5')?.value ?? 0;

    this.servicioIngresos.enviarResumenIngresos(this.resumenIngresos);

  }

  obtenerControl(nombreControl: string){
    return this.fgroupIngresos.get(nombreControl);
  }

  onDeleteIngresoCarbono(ingreso: Ingreso){
    this.resumenIngresos.ingresosBonos = this.resumenIngresos.ingresosBonos.filter(i => i !== ingreso);
    this.dataIngresosCarbono.setData(this.resumenIngresos.ingresosBonos);
    this.verFormCarbono = false;
  }

  //Otros ingresos

  onAddOtroIngreso(){
    this.verFormOtrosIngresos = true;

    let controles: FormControl[] = [];
    controles.push( this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontrolDescrOtroIngreso') as FormControl);
    controles.push( this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontrolAnioOtroIngreso') as FormControl);
    controles.push( this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontroValorOtroIngreso') as FormControl);

    this.agregarValidadores(controles);
  }

  onCancelarOtroIngreso(){
    this.verFormOtrosIngresos = false;

    let controles: FormControl[] = [];
    controles.push( this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontrolDescrOtroIngreso') as FormControl);
    controles.push( this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontrolAnioOtroIngreso') as FormControl);
    controles.push( this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontroValorOtroIngreso') as FormControl);

    this.eliminarValidadores(controles);
  }

  onSaveOtroIngreso(){
    this.verFormOtrosIngresos = false;
    const ingreso = new Ingreso();

    ingreso.Descripcion = this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontrolDescrOtroIngreso')?.value;
    ingreso.Anio = this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontrolAnioOtroIngreso')?.value;
    ingreso.Valor = this.fgroupIngresos.controls.fgroupOtroIngreso.get('fcontroValorOtroIngreso')?.value;
    this.resumenIngresos.otrosIngresos.push(ingreso);
    this.dataOtrosIngresos.setData(this.resumenIngresos.otrosIngresos);
  }

  onDeleteOtroIngreso(ingreso: Ingreso){
    this.resumenIngresos.otrosIngresos = this.resumenIngresos.otrosIngresos.filter(i => i.Descripcion !== ingreso.Descripcion);
    this.dataOtrosIngresos.setData(this.resumenIngresos.otrosIngresos);
    this.verFormOtrosIngresos = false;
  }

}
