import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeloInversionComponent } from './modelo-inversion.component';

describe('ModeloInversionComponent', () => {
  let component: ModeloInversionComponent;
  let fixture: ComponentFixture<ModeloInversionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeloInversionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeloInversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
