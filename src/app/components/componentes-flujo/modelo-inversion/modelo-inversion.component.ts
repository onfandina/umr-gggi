import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Costo } from 'src/app/core/entities/Costos/Costo';
import { Credito } from 'src/app/core/entities/Credito';
import { ModeloInversion } from 'src/app/core/entities/ModeloInversion';
import { rangoPagoCuotaValues } from 'src/app/models/rangoPagoCuota';
import { ModeloInversionService } from '../services/modeloInversion/modelo-inversion.service';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-modelo-inversion',
  templateUrl: './modelo-inversion.component.html',
  styleUrls: ['./modelo-inversion.component.scss']
})
export class ModeloInversionComponent implements OnInit {

  //Form controls
  formgModeloInversion = new FormGroup({
    fcontrolCredito: new FormControl(''),
    //fcontrolPeriodos: new FormControl(Validators.required),
    fcontrolIncentivo: new FormControl(),
    fcontrolTerceros: new FormControl(),
    fcontrolPropios: new FormControl(),
   
    //Credito
    fcontrolCapitalSolicitado: new FormControl(),
    fcontrolTasaInteres: new FormControl(),
    fcontrolPlazoDeuda: new FormControl(),
    fcontrolPeriodoCuota: new FormControl() 
  });

  //rangos
  rangoPagoCuota = rangoPagoCuotaValues;

  //datos
  periodos: string[] = [];

  //helpers
  esCredito = false;
  
  //Observables
  filterPeriodos! : Observable<string[]>;
  obsCredito!: Observable<string>;
  
  //Objeto modelo
  modeloInversion: ModeloInversion;

  constructor(
    private servicioModeloInversion: ModeloInversionService,
    private currencyPipe: CurrencyPipe
    ) { 
    this.modeloInversion = new ModeloInversion();
  }

  ngOnInit(): void {
   this.obtenerObjetoDeSesion();
   this.formgModeloInversion.controls.fcontrolIncentivo.setValidators(Validators.required);
   this.formgModeloInversion.controls.fcontrolTerceros.setValidators(Validators.required);
   this.formgModeloInversion.controls.fcontrolPropios.setValidators(Validators.required);
  } 

  //Obtiene la información almacenada en la sesión
  obtenerObjetoDeSesion(){
    if (sessionStorage.getItem('modeloInversion')) {
      const modelo = JSON.parse(sessionStorage.getItem('modeloInversion') ?? '') as ModeloInversion;
      this.esCredito = modelo.aplicaCredito;
    
      if(this.esCredito){
        this.obtenerFormControl('fcontrolCapitalSolicitado')?.setValue(modelo.credito.capitalSolicitado);
        this.obtenerFormControl('fcontrolTasaInteres')?.setValue(modelo.credito.tasaInteres);
        this.obtenerFormControl('fcontrolPlazoDeuda')?.setValue(modelo.credito.plazoDeuda);
        this.obtenerFormControl('fcontrolCuota')?.setValue(modelo.credito.valorCuota);
        this.obtenerFormControl('fcontrolPeriodoCuota')?.setValue(modelo.credito.periodicidadPago);
      }
      this.obtenerFormControl('fcontrolIncentivo')?.setValue(modelo.incentivo);
      this.obtenerFormControl('fcontrolTerceros')?.setValue(modelo.capitalTerceros);
      this.obtenerFormControl('fcontrolPropios')?.setValue(modelo.recursosPropios);
    }
  }

  //Event handlers
  cambioCheck() {
    this.esCredito = this.formgModeloInversion.get('fcontrolCredito')?.value;
    if (this.esCredito) {
      this.modeloInversion.credito = new Credito();
      this.formgModeloInversion.controls.fcontrolCapitalSolicitado.setValidators(Validators.required);
      this.formgModeloInversion.controls.fcontrolTasaInteres.setValidators(Validators.required);
      this.formgModeloInversion.controls.fcontrolPlazoDeuda.setValidators(Validators.required);
      this.formgModeloInversion.controls.fcontrolPeriodoCuota.setValidators(Validators.required);
    }
    else{
      this.formgModeloInversion.controls.fcontrolCapitalSolicitado.clearValidators();
      this.formgModeloInversion.controls.fcontrolTasaInteres.clearValidators();
      this.formgModeloInversion.controls.fcontrolPlazoDeuda.clearValidators();
      this.formgModeloInversion.controls.fcontrolPeriodoCuota.clearValidators();

      this.formgModeloInversion.controls.fcontrolCapitalSolicitado.updateValueAndValidity();
      this.formgModeloInversion.controls.fcontrolTasaInteres.updateValueAndValidity();
      this.formgModeloInversion.controls.fcontrolPlazoDeuda.updateValueAndValidity();
      this.formgModeloInversion.controls.fcontrolPeriodoCuota.updateValueAndValidity();
    }
  }

  cambioTasaInteres(){
    this.modeloInversion.credito.tasaInteres = this.obtenerFormControl('fcontrolTasaInteres')?.value;
  }
  
  cambioPlazoDeuda(){
    this.modeloInversion.credito.plazoDeuda = this.obtenerFormControl('fcontrolPlazoDeuda')?.value;
  }
 
  cambioValorCuota(){
    this.modeloInversion.credito.valorCuota = this.obtenerFormControl('fcontrolCuota')?.value;
  }

  cambioPeriodicidadPago(){
    this.modeloInversion.credito.periodicidadPago = this.obtenerFormControl('fcontrolPeriodoCuota')?.value;
  }

  cambioIncentivo(){
    this.modeloInversion.incentivo = this.obtenerFormControl('fcontrolIncentivo')?.value;
  }

  cambioCapitalTerceros(){
    this.modeloInversion.capitalTerceros = this.obtenerFormControl('fcontrolTerceros')?.value;
  }

  cambioRecursosPropios(){
    this.modeloInversion.recursosPropios = this.obtenerFormControl('fcontrolPropios')?.value;
  }

  simularCredito(){
    let tasaInteresAnual = this.obtenerTasaInteresMensual();

    let costosCuotas: Costo[] = [];

    for (let i = 0; i < (this.modeloInversion.credito.plazoDeuda); i++) {
      let costoCuota = new Costo();
      costoCuota.Anio = parseInt((i/12).toString()) + 1;
      costoCuota.Valor = Math.round(this.obtenerCostoCuota(tasaInteresAnual, i));
      costosCuotas.push(costoCuota);
    }

    for (let i = 1; i <= this.modeloInversion.credito.plazoDeuda/12; i++) {
      let costo = new Costo();
      costo.Anio = i;
      costo.Valor = costosCuotas.filter(c => c.Anio == i).map(c => c.Valor).reduce((a, b) => a + b, 0);
      this.modeloInversion.credito.costosAnio.push(costo);
    }

  }

  obtenerCostoCuota(tasaInteresAnual: number, mes: number){
    let aporteACapital = this.modeloInversion.credito.capitalSolicitado / this.modeloInversion.credito.plazoDeuda;
    let saldoCapital = this.modeloInversion.credito.capitalSolicitado - (aporteACapital * mes);
    let intereses = saldoCapital * tasaInteresAnual //* sensibilidad;
    let totalCuota = intereses + aporteACapital;
    return totalCuota;
  }

  //Obtiene la tasa de interés que se aplica mensualmente
  obtenerTasaInteresMensual(){

    let divisorExponente = 1;

    switch (this.modeloInversion.credito.periodicidadPago) {

      case 'Mensual':
        divisorExponente = 30;
        break;
      case 'Bimestral':
        divisorExponente = 60;
        break; 
      case 'Trimestral':
        divisorExponente = 90;
        break;
      case 'Cuatrimestral':
        divisorExponente = 120;
        break;
      case 'Semestral':
        divisorExponente = 180;
        break;  
      default:
        break;
    }

   let tasa = (Math.pow((1 + (this.modeloInversion.credito.tasaInteres/100)),(360/divisorExponente)) - 1)/12;

   return tasa; 

  }

  enviarModeloInversion(){
    this.servicioModeloInversion.enviarModeloInversion(this.modeloInversion);
  }

  //Obtiene el formControl correspondiente
  obtenerFormControl(nombreControl: string){
    return this.formgModeloInversion.get(nombreControl);
  }
  
  //guarda el formulario
  guardarModeloInversion(){

    this.esCredito = this.formgModeloInversion.get('fcontrolCredito')?.value;
    this.modeloInversion.credito = new Credito(); 
    if(this.esCredito){
      this.modeloInversion.credito.capitalSolicitado = this.obtenerFormControl('fcontrolCapitalSolicitado')?.value;
      this.modeloInversion.credito.tasaInteres = this.obtenerFormControl('fcontrolTasaInteres')?.value;
      this.modeloInversion.credito.plazoDeuda = this.obtenerFormControl('fcontrolPlazoDeuda')?.value;
      this.modeloInversion.credito.valorCuota = this.obtenerFormControl('fcontrolCuota')?.value;
      this.modeloInversion.credito.periodicidadPago = this.obtenerFormControl('fcontrolPeriodoCuota')?.value;
    }
    this.modeloInversion.incentivo = this.obtenerFormControl('fcontrolIncentivo')?.value;
    this.modeloInversion.capitalTerceros = this.obtenerFormControl('fcontrolTerceros')?.value;
    this.modeloInversion.recursosPropios = this.obtenerFormControl('fcontrolPropios')?.value;

    this.simularCredito();

    //Se guarda en sesión
    sessionStorage.setItem('modeloInversion', JSON.stringify(this.modeloInversion));

    this.servicioModeloInversion.enviarModeloInversion(this.modeloInversion);

    this.enviarModeloInversion();
  }

  //Limpia el formulario
  limpiarFormulario(){
    this.modeloInversion = new ModeloInversion();

  }
  
}
