import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { sistemasSiembra, sistemasSiembraAgroforestal, sistemasSiembraRestauracion, sistemasSiembraSilvopastoril } from 'src/app/models/sistemasSiembra';
import { distanciasSiembraCuadrado, distanciasSiembraDiferentesDistancias } from 'src/app/models/distanciasSiembra';
import { InformacionGeneralService } from '../services/informacionGeneral/informacion-general.service';
import { InformacionGeneral } from 'src/app/core/entities/informacionGeneral';
import { TipoPersona } from 'src/app/core/entities/Enumeradores/TipoPersona';
import { TipoEntidad } from 'src/app/core/entities/Enumeradores/TipoEntidad';
import { Observable } from 'rxjs';
import { configuracion } from 'src/app/models/configuracion';

@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html',
  styleUrls: ['./proyecto.component.scss']
})
export class ProyectoComponent implements OnInit {

  //Form controls
  formProyecto = new FormGroup({
    fcontrolInstrumentos: new FormControl(false),
    fcontrolCaracterizaciones: new FormControl(false),
    fcontrolAreaTotal: new FormControl(),
    fcontrolAreaProyecto: new FormControl('',Validators.required), 
    fcontrolTiempoEstablecimiento: new FormControl('',Validators.required),
    fcontrolSistema: new FormControl('', Validators.required),
    fcontrolDistancia: new FormControl('', Validators.required),
    fcontrolDistanciaAbierta: new FormControl(''),
    fcontrolTasaPerdida: new FormControl('', Validators.required),
    fcontrolTasaResiembra: new FormControl('', Validators.required),
    fcontrolTipoPersona: new FormControl('natural'),
    fcontrolTipoEntidad: new FormControl(''),
    fcontrolLineasCercas: new FormControl('simples'),
    fcontrolUVT: new FormControl(),
    fcontrolEspecificarNumero: new FormControl(),
    fcontrolArbolesHectarea: new FormControl()
  })

  //Tipo de flujo
  flujoActual = '';

  //Datos
  listaSistemas = sistemasSiembra;
  listaDistancias = distanciasSiembraCuadrado;

  //helpers
  tipoPersona = '';
  entidad = '';
  textoArea = '';
  textoInstrumentos = '';
  tipoSistema = '';
  filterSistemas = new Observable<string[]>();
  verDistanciaAbierta = false;
  ocultarDistanciasSiembra = false;
  verSistemasSiembra = true;
  validaInstrumentos = true;
  validaCaracterizaciones = true;
  especificarNumeroArboles = false;

  //Objeto principal
  infoGeneralProyecto = new InformacionGeneral();

  constructor(private servicioProyecto: InformacionGeneralService) {
    this.flujoActual = sessionStorage.getItem('flujo') ?? '';
    if (this.flujoActual == 'restauracion') {
      this.textoArea = 'Indique el área de la plantación (ha)';
      this.textoInstrumentos = '¿Sabe si los instrumentos de planeación le permiten realizar la actividad de restauración?';
      this.formProyecto.get('fcontrolInstrumentos')?.setValidators(Validators.requiredTrue);
      this.formProyecto.get('fcontrolCaracterizaciones')?.setValidators(Validators.requiredTrue);
      this.validaInstrumentos =  this.formProyecto.get('fcontrolInstrumentos')?.value;
      this.validaCaracterizaciones =  this.formProyecto.get('fcontrolCaracterizaciones')?.value;
      
    }
    else{
      this.textoArea = 'Área total del proyecto (ha)';
      this.textoInstrumentos = '¿Sabe si los instrumentos de planeación le permiten realizar la plantación?';
    }

    if(this.flujoActual == 'silvopastoril' || this.flujoActual == 'agroforestal'){
      this.formProyecto.controls.fcontrolDistancia.clearValidators();
      this.formProyecto.controls.fcontrolDistancia.updateValueAndValidity();
    }

   }

  //Manejador del evento al iniciar el componente
  ngOnInit(): void {
    //this.obtenerObjetoDeSesion();
    //const sistemaForm = this.formProyecto.get('fcontrolSistema');
    //const distanciaForm = this.formProyecto.get('fcontrolDistancia');

    if(this.flujoActual == 'restauracion'){
      this.listaSistemas = sistemasSiembraRestauracion;
    }
    else if(this.flujoActual == 'silvopastoril'){
      this.listaSistemas = sistemasSiembraSilvopastoril;
      this.ocultarDistanciasSiembra = true;
      this.verDistanciaAbierta = true;
    }
    else if(this.flujoActual == 'agroforestal'){
      this.listaSistemas = sistemasSiembraAgroforestal;
      this.ocultarDistanciasSiembra = false;
      this.verDistanciaAbierta = false;
    }
  }

  //Filtra las distancias de siembra correspondientes
  filterDistancias(){
    let distancias: string[] = [];
    let sistema = this.formProyecto.get('fcontrolSistema')?.value;
    if (sistema !== '') {
      switch (sistema) {
        case 'Árboles en diferentes distancias':
          this.tipoSistema = 'diferentes';
          distancias = distanciasSiembraDiferentesDistancias;
          this.verDistanciaAbierta = false;
        break;
        case 'Cuadrado':
          this.tipoSistema = 'cuadrado';
          this.verDistanciaAbierta = false;
          distancias = distanciasSiembraCuadrado;
        break;
        case 'Triangular':
          this.tipoSistema = 'triangular';
          this.verDistanciaAbierta = false;
          distancias = distanciasSiembraCuadrado;
        break;
        case 'En cercas o barreras rompevientos':
          this.tipoSistema = 'cercas';
          this.ocultarDistanciasSiembra = true;
          this.verDistanciaAbierta = true;
          break;
        case 'Café con sombrío arbóreo':
          this.tipoSistema = 'diferentes';
          distancias = distanciasSiembraDiferentesDistancias;
          this.ocultarDistanciasSiembra = false;
          this.verDistanciaAbierta = false;          
          break;
        case 'Cacao con sombrío arbóreo permamente':
          this.tipoSistema = 'diferentes';
          distancias = distanciasSiembraDiferentesDistancias;
          this.ocultarDistanciasSiembra = false;
          this.verDistanciaAbierta = false;
          break;
        case 'Sistemas Taungya':
          this.tipoSistema = 'diferentes';
          distancias = distanciasSiembraDiferentesDistancias;
          this.ocultarDistanciasSiembra = false;
          this.verDistanciaAbierta = false;
          break;
        case 'Árboles como linderos o cercas vivas en cultivos perennes':
          this.tipoSistema = 'diferentes';
          this.ocultarDistanciasSiembra = true;
          this.verDistanciaAbierta = true;
          break;
        default:
          distancias = distanciasSiembraCuadrado;
          break; 
      }
    }
    this.listaDistancias = distancias;
  }

  //Obtiene la información almacenada en la sesión
  obtenerObjetoDeSesion(){
    if (sessionStorage.getItem('informacionGeneral')) {
      const informacion = JSON.parse(sessionStorage.getItem('informacionGeneral') ?? '') as InformacionGeneral;
      
      this.formProyecto.get('fcontrolTipoPersona')?.setValue(informacion.Persona?.tipoDePersona);
      this.formProyecto.get('fcontrolTipoEntidad')?.setValue(informacion.Persona?.tipoDeEntidad);
      this.formProyecto.get('fcontrolAreaTotal')?.setValue(informacion.AreaTotalPredio);
      this.formProyecto.get('fcontrolAreaProyecto')?.setValue(informacion.AreaTotalProyecto);
      this.formProyecto.get('fcontrolSistema')?.setValue(informacion.SistemaSiembra);
  
      this.formProyecto.get('fcontrolDensidad')?.setValue(informacion.DistanciaSiembra); 
      this.formProyecto.get('fcontrolTasaPerdida')?.setValue(informacion.TasaPerdidaEstimada); 
      this.formProyecto.get('fcontrolTasaResiembra')?.setValue(informacion.TasaResiembra); 
      this.formProyecto.get('fcontrolTiempoEstablecimiento')?.setValue(informacion.TiempoEstablecimiento)
      this.formProyecto.get('fcontrolUVT')?.setValue(informacion.UVT);
    }
  }

  //Obtiene los árboles por hectárea
  obtenerArbolesHectarea(){
    let arbolesHectarea = 1111;
    if (this.especificarNumeroArboles) {
      arbolesHectarea = this.formProyecto.controls.fcontrolArbolesHectarea.value;
    }
    else{
      switch (this.flujoActual) {
        case 'pfc':
          arbolesHectarea = this.obtenerArbolesPFC();
          break;
        case 'silvopastoril':
          arbolesHectarea = this.obtenerArbolesSilvopastoril();
          break;
        case 'agroforestal':
          arbolesHectarea = this.obtenerArbolesAgroforestal();
          break;  
        case 'restauracion':
          arbolesHectarea = this.obtenerArbolesRestauracion();
        break;
        default:
          break;
      }
    }

    return arbolesHectarea;
  }

  //Obtiene el número de árboles por hectárea para el flujo PFC
  obtenerArbolesPFC(): number {
    let arbolesHectarea = 1111;
    let sistemaSiembra = this.formProyecto.get('fcontrolSistema')?.value;
    let distanciaSiembra: any;

    distanciaSiembra = this.formProyecto.get('fcontrolDistancia')?.value as string;

    switch (sistemaSiembra) {
      case 'Cuadrado':
        arbolesHectarea = this.obtenerArbolesCuadrado(distanciaSiembra);  
        break;
      case 'Triángulo (tres bolillos)':
        arbolesHectarea = this.obtenerArbolesTriangular(distanciaSiembra);
        break; 
      default:
        throw new Error('No se recibió correctamente la distancia de siembra');
    }

    return arbolesHectarea;
  }

  //Obtiene el número de árboles por hectárea para el flujo de restauración
  obtenerArbolesRestauracion(): number {
    let arbolesHectarea = 1111;
    let sistemaSiembra = this.formProyecto.get('fcontrolSistema')?.value;
    let distanciaSiembra: any;

    if (this.ocultarDistanciasSiembra && this.verDistanciaAbierta) {
      distanciaSiembra = this.formProyecto.get('fcontrolDistanciaAbierta')?.value;
    }
    else {
      distanciaSiembra = this.formProyecto.get('fcontrolDistancia')?.value as string;
    }

    switch (sistemaSiembra) {
      case 'Árboles en diferentes distancias':
        arbolesHectarea = this.obtenerArbolesDiferentesDistancias(distanciaSiembra);
        break;
      case 'Cuadrado':
        arbolesHectarea = this.obtenerArbolesCuadrado(distanciaSiembra);  
        break;
      case 'Triangular':
        arbolesHectarea = this.obtenerArbolesTriangular(distanciaSiembra);
        break;
      case 'En cercas o barreras rompevientos':
        arbolesHectarea = this.obtenerArbolesCercas(distanciaSiembra);
        break;   
      default:
        throw new Error('No se recibió correctamente la distancia de siembra');

    }

    return arbolesHectarea;

  }

  //Obtiene el número de árboles por hectárea de un sistema de cercas
  obtenerArbolesCercas(distanciaSiembra: number): number {
    let tipoLineas = this.formProyecto.get('fcontrolLineasCercas')?.value;
    switch (tipoLineas) {
      case 'simples':
        return 400/distanciaSiembra;
      case 'dobles':
        return 2 * (400/distanciaSiembra);
      case 'triples':
        return 3 * (400/distanciaSiembra);
      default:
        throw new Error('No se recibió correctamente la distancia de siembra');
    }
  }

  //Obtiene el número árboles por hectarea de un sistema triangular
  obtenerArbolesTriangular(distanciaSiembra: string): number {
    switch (distanciaSiembra) {
      case '2.5 X 2.5':
        return 1847;

      case '3 X 3':
        return 1283;

      case '3.5 X 3.5':
        return 942;

      default:
       throw new Error('No se recibió correctamente la distancia de siembra');
    }   
  }

  //Obtiene los árboles por hectarea de un sistema cuadrado
  obtenerArbolesCuadrado(distanciaSiembra: string): number {
    switch (distanciaSiembra) {
      case '2.5 X 2.5':
        return 1600;

      case '3 X 3':
        return 1111;

      case '3.5 X 3.5':
        return 816;

      default:
       throw new Error('No se recibió correctamente la distancia de siembra');
    }   
  }

  //Obtiene los árboles por hectárea de un sistema de diferentes distancias
  obtenerArbolesDiferentesDistancias(distanciaSiembra: string): number {
    switch (distanciaSiembra) {
      case '3 x 3':
        return 1111;

      case '6 x 6':
        return 278;

      case '9 x 9':
        return 123;

      case '12 x 12':
        return 70;

      case '15 x 3':
        return 222;

      case '18 x 3':
        return 185;

      case '3 x 3 x 21':
        return 278;

      case '3 x 2.5':
        return 1333;
 
      case '3.5 x 2.5':
        return 1142;
 
      default:
        throw new Error('No se recibió correctamente la distancia de siembra');
    }   
  }

  //Obtiene los árboles por hectárea de un sistema silvopastoril
  obtenerArbolesSilvopastoril(): number {

    let arbolesHectarea = 1111;
    let sistemaSiembra = this.formProyecto.get('fcontrolSistema')?.value;
    let distanciaSiembra = 0;

    if (this.ocultarDistanciasSiembra && this.verDistanciaAbierta) {
      distanciaSiembra = this.formProyecto.get('fcontrolDistanciaAbierta')?.value;
    }
    else {
      distanciaSiembra = this.formProyecto.get('fcontrolDistancia')?.value;
    }
    
    switch (sistemaSiembra) {
      case 'Cercas vivas en zonas ganaderas':
        arbolesHectarea = 400/distanciaSiembra;
        break;
      case 'Sistema Silvopastoril intensivo con árboles maderables':
          arbolesHectarea = 400/distanciaSiembra;
          break;  
      case 'Rompevientos línea triples':
        arbolesHectarea = 3 * (400/distanciaSiembra);  
        break;
      case 'Rompevientos línea dobles':
        arbolesHectarea = 2 * (400/distanciaSiembra);
        break;   
      default:
        break;
    }

    return arbolesHectarea;
  }

   //Obtiene los árboles por hectárea de un sistema agroforestal
   obtenerArbolesAgroforestal(): number {
    let arbolesHectarea = 1111;
    let sistemaSiembra = this.formProyecto.get('fcontrolSistema')?.value;
    let distanciaSiembra = 0;

    if (this.ocultarDistanciasSiembra && this.verDistanciaAbierta) {
      distanciaSiembra = this.formProyecto.get('fcontrolDistanciaAbierta')?.value;
    }
    else {
      distanciaSiembra = this.formProyecto.get('fcontrolDistancia')?.value;
    }

    switch (sistemaSiembra) {
      case 'Café con sombrío arbóreo':
        arbolesHectarea = this.obtenerArbolesDiferentesDistancias(distanciaSiembra.toString());
        break;
      case 'Cacao con sombrío arbóreo permamente':
        arbolesHectarea = this.obtenerArbolesDiferentesDistancias(distanciaSiembra.toString());
        break;  
      case 'Sistemas Taungya':
        arbolesHectarea = this.obtenerArbolesDiferentesDistancias(distanciaSiembra.toString());
        break;
      case 'Árboles como linderos o cercas vivas en cultivos perennes':
        arbolesHectarea = 400/distanciaSiembra;
        break;   
      default:
        break;
    }

    return arbolesHectarea;
  }

  //Cambio en la validación de instrumentos de planeación
  
  cambioInstrumento(){
    this.validaInstrumentos =  this.formProyecto.get('fcontrolInstrumentos')?.value;
  }

  //Cambio en la validación de caracterización
  cambioCaracter(){
    this.validaCaracterizaciones =  this.formProyecto.get('fcontrolCaracterizaciones')?.value;
  }

  //Manejador del evento al cambiar el tipo de persona
  cambioPersona() {
    this.tipoPersona = this.formProyecto.get('fcontrolTipoPersona')?.value; 
  }

  //Manejador del evento al canbiar el tipo de entidad
  cambioEntidad() {
    this.entidad = this.formProyecto.get('fcontrolTipoEntidad')?.value;
  }
  
  onEspecificarNumeroChange(){

    this.especificarNumeroArboles = this.formProyecto.controls.fcontrolEspecificarNumero.value;

    if (this.especificarNumeroArboles == true) {
      this.formProyecto.controls.fcontrolSistema.clearValidators();
      this.formProyecto.controls.fcontrolSistema.updateValueAndValidity();
      this.formProyecto.controls.fcontrolDistancia.clearValidators();
      this.formProyecto.controls.fcontrolDistancia.updateValueAndValidity();
      this.formProyecto.controls.fcontrolDistanciaAbierta.clearValidators();
      this.formProyecto.controls.fcontrolDistanciaAbierta.updateValueAndValidity();
      this.formProyecto.controls.fcontrolArbolesHectarea.setValidators(Validators.required);
      this.formProyecto.controls.fcontrolArbolesHectarea.updateValueAndValidity();
    }
    else if (this.especificarNumeroArboles == false) {
      this.formProyecto.controls.fcontrolSistema.setValidators(Validators.required);
      this.formProyecto.controls.fcontrolSistema.updateValueAndValidity();
     
      if (this.verDistanciaAbierta){
        this.formProyecto.controls.fcontrolDistanciaAbierta.setValidators(Validators.required);
        this.formProyecto.controls.fcontrolDistanciaAbierta.updateValueAndValidity();
        this.formProyecto.controls.fcontrolDistancia.clearValidators();
        this.formProyecto.controls.fcontrolDistancia.updateValueAndValidity();
      }
      else {
        this.formProyecto.controls.fcontrolDistancia.setValidators(Validators.required);
        this.formProyecto.controls.fcontrolDistancia.updateValueAndValidity();
        this.formProyecto.controls.fcontrolDistanciaAbierta.clearValidators();
        this.formProyecto.controls.fcontrolDistanciaAbierta.updateValueAndValidity();
      }
      this.formProyecto.controls.fcontrolArbolesHectarea.clearValidators();
      this.formProyecto.controls.fcontrolArbolesHectarea.updateValueAndValidity();
    }
    
  }

  //Envía la información del proyecto por medio del servicio correspondiente
  enviarInfoProyecto(){
    sessionStorage.setItem('informacionGeneral', JSON.stringify(this.infoGeneralProyecto));
    this.servicioProyecto.enviarInformacionGeneral(this.infoGeneralProyecto);
  }

  //Guarda la información en el formulario
  guardarInfoProyecto(){
    this.infoGeneralProyecto = new InformacionGeneral();
    if (this.formProyecto.get('fcontrolTipoPersona')?.value == 'natural') {
      this.infoGeneralProyecto.Persona.tipoDePersona = TipoPersona.Natural;
    }
    else
    {
      this.infoGeneralProyecto.Persona.tipoDePersona = TipoPersona.Juridica;
      if (this.formProyecto.get('fcontrolTipoEntidad')?.value == 'lucro') {
        this.infoGeneralProyecto.Persona.tipoDeEntidad = TipoEntidad.ConAnimoLucro;
      }
      else{
        this.infoGeneralProyecto.Persona.tipoDeEntidad = TipoEntidad.SinAnimoLucro;
      }
    }

    this.infoGeneralProyecto.AreaTotalPredio = this.formProyecto.get('fcontrolAreaTotal')?.value;
    this.infoGeneralProyecto.AreaTotalProyecto = this.formProyecto.get('fcontrolAreaProyecto')?.value;
    this.infoGeneralProyecto.SistemaSiembra = this.formProyecto.get('fcontrolSistema')?.value;
    if (this.ocultarDistanciasSiembra && this.verDistanciaAbierta) {
      this.infoGeneralProyecto.DistanciaSiembra = this.formProyecto.get('fcontrolDistanciaAbierta')?.value;
    }
    else {
      this.infoGeneralProyecto.DistanciaSiembra = this.formProyecto.get('fcontrolDistancia')?.value;
    }
    this.infoGeneralProyecto.TasaPerdidaEstimada = this.formProyecto.get('fcontrolTasaPerdida')?.value; 
    this.infoGeneralProyecto.TasaResiembra = this.formProyecto.get('fcontrolTasaResiembra')?.value; 
    this.infoGeneralProyecto.TiempoEstablecimiento = this.formProyecto.get('fcontrolTiempoEstablecimiento')?.value;
    this.infoGeneralProyecto.UVT = configuracion[0].valor;
    this.infoGeneralProyecto.ArbolesHectarea = this.obtenerArbolesHectarea();
    
    this.enviarInfoProyecto();
  }

}
