import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BLCostos } from 'src/app/core/business/BLCostos';
import { BLEspecie } from 'src/app/core/business/BLEspecie';
import { BLIngresos } from 'src/app/core/business/BLIngresos';
import { BLProyecto } from 'src/app/core/business/BLProyecto';
import { ResumenCostos } from 'src/app/core/entities/Costos/ResumenCostos';
import { TipoEntidad } from 'src/app/core/entities/Enumeradores/TipoEntidad';
import { TipoPersona } from 'src/app/core/entities/Enumeradores/TipoPersona';
import { Especie } from 'src/app/core/entities/Especie';
import { InformacionGeneral } from 'src/app/core/entities/informacionGeneral';
import { ResumenIngresos } from 'src/app/core/entities/Ingresos/ResumenIngresos';
import { ModeloInversion } from 'src/app/core/entities/ModeloInversion';
import { Proyecto } from 'src/app/core/entities/Proyecto';
import { Sensibilidad } from 'src/app/core/entities/Sensibilidad';
import { Ubicacion } from 'src/app/core/entities/Ubicacion';
import { EspecieService } from '../services/especie/especie.service';
import { InformacionGeneralService } from '../services/informacionGeneral/informacion-general.service';
import { ModeloInversionService } from '../services/modeloInversion/modelo-inversion.service';
import { ResumenCostosService } from '../services/resumenCostos/resumen-costos.service';
import { ResumenIngresosService } from '../services/resumenIngresos/resumen-ingresos.service';
import { SensibilidadService } from '../services/sensibilidad/sensibilidad.service';
import { UbicacionService } from '../services/ubicacion/ubicacion.service';
import { jsPDF } from "jspdf";
import autoTable from 'jspdf-autotable';
import { FlujoCajaAnual } from 'src/app/core/entities/Resultado/FlujoCajaAnual';
import { irr, npv } from 'financial';
import { FormControl, FormGroup } from '@angular/forms';
import { EspecificoTransporteMayor } from 'src/app/core/entities/Costos/EspecificoTransporteMayor';
import { TipoCiudad } from 'src/app/core/entities/Enumeradores/TipoCiudad';
import { TipoVehiculo } from 'src/app/core/entities/Enumeradores/TipoVehiculo';
import { RestauracionComponent } from 'src/app/pages/restauracion/restauracion.component';
import { Entresaca } from 'src/app/core/entities/Entresaca';
import { Establecimiento } from 'src/app/core/entities/Establecimiento';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.scss']
})
export class ResultadosComponent implements OnInit {

  //Forms
  fgroupResultados = new FormGroup({
    fcontrolIngresoEsperado: new FormControl(1000000),
    fcontrolTir: new FormControl(''),
    fcontrolTipoCalculo: new FormControl(''),

  });

  //Flujo
  flujoActual = '';

  //Condicionales
  aplicaTir = false;

  //Objeto principal
  proyecto!: Proyecto;
  flujosCaja!: FlujoCajaAnual[];

  //Objetos formularios
  ubicacion!: Ubicacion;
  modeloInversion!: ModeloInversion;
  infoGeneral!: InformacionGeneral;
  especies!: Especie[];
  resumenCostos!: ResumenCostos;
  ingresos!: ResumenIngresos;
  sensibilidad!: Sensibilidad;

  //Suscripciones
  suscripcionUbicacion!: Subscription;
  suscripcionModeloInversion!: Subscription;
  suscripcionInfoGeneral!: Subscription;
  suscripcionEspecies!: Subscription;
  suscripcionCostos!: Subscription;
  suscripcionIngresos!: Subscription;
  suscripcionSensibilidad!: Subscription;

  //Negocio
  blEspecie!: BLEspecie;
  blCostos!: BLCostos;
  blIngresos!: BLIngresos;
  blProyecto!: BLProyecto;

  //Helpers
  terrenoEstablecidoAcumulado!: number;
  numeroArbolesAcumulado!: number;

  //Para pintar en resultados
  tirPintar = '';
  vpnPintar = '';
  areaMinimaTirPintar = '';
  areaMinimaIngresoPintar = '';
  ingresoMensualPromedioPintar = '';
  inversionMinimaPintar = '';

  constructor(private servicioUbicacion: UbicacionService, 
              private servicioModeloInversion: ModeloInversionService, 
              private servicioInfoGeneral: InformacionGeneralService, 
              private servicioEspecies: EspecieService, 
              private servicioCostos: ResumenCostosService,
              private servicioIngresos: ResumenIngresosService,
              private servicioSensibilidad: SensibilidadService) {

      this.flujoActual = sessionStorage.getItem('flujo') ?? '';
     
      this.suscribirServicios();
   
    }
  
  //Suscripción a servicios para recibir info de los componentes
  suscribirServicios(){

    this.suscripcionUbicacion = this.servicioUbicacion.OnUbicacion().subscribe(nuevaUbicacion => {
      if (nuevaUbicacion) {
        this.ubicacion = nuevaUbicacion;
        let ubicacionString = JSON.stringify(this.ubicacion);
        console.log("--***se recibió ubicación " + ubicacionString + "***--");
      }
    });

    this.suscripcionModeloInversion = this.servicioModeloInversion.OnModeloInversion().subscribe(nuevoModeloInv => {
      if (nuevoModeloInv) {
        this.modeloInversion = nuevoModeloInv;
        let modeloInversionString = JSON.stringify(this.modeloInversion);
        console.log("--***se recibió modelo inversión " + modeloInversionString + "***--");
      }     
    });

    this.suscripcionInfoGeneral = this.servicioInfoGeneral.OnInformacionGeneral().subscribe(nuevaInfoGen => {
      if (nuevaInfoGen) {
        this.infoGeneral = nuevaInfoGen;
        let infoGeneralString = JSON.stringify(this.infoGeneral);
        console.log("--***se recibió modelo inversión " + infoGeneralString + "***--");
      }
    });

    this.suscripcionEspecies = this.servicioEspecies.OnEspecie().subscribe(nuevaInfoEspecie => {
      if (nuevaInfoEspecie) {
        this.especies = nuevaInfoEspecie;
        let especiesString = JSON.stringify(this.especies);
        console.log("--***se recibieron especies " + especiesString + "***--");
      }
    });

    this.suscripcionCostos = this.servicioCostos.OnResumenCostos().subscribe(nuevoResumenCostos => {
      if (nuevoResumenCostos) {
        this.resumenCostos = nuevoResumenCostos;
        let costosString = JSON.stringify(this.resumenCostos);
        console.log("--***se recibieron costos " + costosString + "***--");
        
      }
    });

    this.suscripcionIngresos = this.servicioIngresos.OnResumenIngresos().subscribe(nuevoResumenIngresos => {
      if (nuevoResumenIngresos) {
        this.ingresos = nuevoResumenIngresos;
        let ingresosString = JSON.stringify(this.ingresos);
        console.log("--***se recibieron costos " + ingresosString + "***--");
      }
    });

    this.suscripcionSensibilidad = this.servicioSensibilidad.OnSensibilidad().subscribe(nuevaSensibilidad => {
      if (nuevaSensibilidad) {
        this.sensibilidad = nuevaSensibilidad;
        let sensibilidadString = JSON.stringify(this.sensibilidad);
        console.log("--***se recibieron costos " + sensibilidadString + "***--");
      }
    });

  } 

  ngOnInit(): void {
    this.proyecto = new Proyecto();
    this.proyecto.Dtf = 3.13;
  }

  //Calcula el tiempo total de la plantación
  calcularTiempoTotalPlantacion(){
    let tiempoTotal = 0;
    this.especies?.forEach(especie => {
      especie.Establecimientos.forEach(establecimiento => {
        if(establecimiento.Anio + especie.Turno > tiempoTotal){
          tiempoTotal = establecimiento.Anio + especie.Turno;
          //console.log("***El tiempo total es: " + tiempoTotal + "***");
        }
      });
    });
    this.proyecto.TiempoTotal = tiempoTotal - 1;
  }
  
  obtenerNumeroArbolesEstablecimiento(anioActual: number, entresacas: Entresaca[], establecimiento: Establecimiento ){
    const entresacasOrdenadas = entresacas.sort((a,b) => (a.Anio > b.Anio) ? 1 : -1);

    let numeroArboles = this.infoGeneral.ArbolesHectarea;

    for (let i = 0; i < entresacasOrdenadas.length; i++) {
     
      const entresacaActual = entresacasOrdenadas[i];

      if ( entresacaActual.Anio <= anioActual - establecimiento.Anio + 1) {
        numeroArboles = numeroArboles * (1 - (entresacasOrdenadas[i].PorcentajeAprovechamiento/100));
      }

    }

    // for (let i = 0; i < entresacasOrdenadas.length; i++) {
    //   const entresacaActual = entresacasOrdenadas[i];

    //   if (anioActual == entresacaActual.Anio + establecimiento.Anio - 1) {
      
    //   for (let j = 0; j < entresacasOrdenadas.length; j++) {
    //     const entresacaAnterior = entresacasOrdenadas[j];
    //     if (entresacaAnterior.Anio < entresacaAnterior.Anio) {
    //       numeroArboles -= numeroArboles * (1-entresacaAnterior.PorcentajeAprovechamiento/100);
    //     }
    //   }

      // //Primera entresaca
      // if (i == 0 && (anioActual - (establecimientoActual.Anio + 1)) <= entresacaActual.Anio) {
      //   numeroArboles = this.infoGeneral.ArbolesHectarea;
      // }
      // else if (anioActual - (establecimientoActual.Anio + 1) > entresacasOrdenadas[i].Anio && anioActual <= entresacasOrdenadas[i + 1].Anio) {
      //    numeroArboles = numeroArboles * (1 - (entresacasOrdenadas[i].PorcentajeAprovechamiento/100));
      // }
  

    return numeroArboles;

  }


  //Calcula el factor de proporcionalidad para cada entresaca
  calcularFactoresProporcionalidadEntresacas():void {
    this.especies.forEach(especie => {
      for (let i = 0; i < especie.Entresacas.length; i++) {
        const entresaca = especie.Entresacas[i];

        if (i == 0) {
          entresaca.FactorProporcionalidad = parseFloat((this.infoGeneral.ArbolesHectarea / 1111).toFixed(2));
        }
        else {
          const entresacaAnterior = especie.Entresacas[i - 1];
          entresaca.FactorProporcionalidad = parseFloat(((this.infoGeneral.ArbolesHectarea - 
            (this.infoGeneral.ArbolesHectarea * entresacaAnterior.PorcentajeAprovechamiento/100))/
            (((especie.Turno/entresaca.Anio)/25) * (9 * 1111))).toFixed(2));
        }
        
        if (entresaca.FactorProporcionalidad > 1 || especie.ConoceRendimiento) {
          entresaca.FactorProporcionalidad = 1;
        }
        
      }
    });
  }

  //Método principal de la aplicación que calcula el flujo de caja
  calcularFlujoCaja(): void{

    if (this.flujoActual != 'pfc') {
      this.calcularFactoresProporcionalidadEntresacas();
    }

    this.calcularTiempoTotalPlantacion();
    this.terrenoEstablecidoAcumulado = 0;
    this.numeroArbolesAcumulado = 0;
    this.flujosCaja = [];

    for (let anioActual = 1; anioActual <= this.proyecto.TiempoTotal; anioActual++) {

      let flujoCajaAnio = new FlujoCajaAnual();
      flujoCajaAnio.anio = anioActual;

      this.ObtenerCostosAnio(flujoCajaAnio, anioActual);
      this.ObtenerIngresosAnio(anioActual, flujoCajaAnio);
      this.ObtenerTotalesAnio(flujoCajaAnio);
    }
  }

  //Obtiene los valores totales del año correspondiente
  ObtenerTotalesAnio(flujoCajaAnio: FlujoCajaAnual) {
    flujoCajaAnio.subtotalPodas = this.ObtenerSumaArray(flujoCajaAnio.valoresPodas);
    flujoCajaAnio.subtotalMantenimiento = this.ObtenerSumaArray(flujoCajaAnio.valoresMantenimiento);
    flujoCajaAnio.subtotalAprovechamiento = this.ObtenerSumaArray(flujoCajaAnio.valoresAprovechamiento);
    flujoCajaAnio.subtotalTransformacion = this.ObtenerSumaArray(flujoCajaAnio.valoresTransformacionPrimaria);
    flujoCajaAnio.subtotalTransporteMenor = this.ObtenerSumaArray(flujoCajaAnio.valoresTransporteMenor);
    flujoCajaAnio.subtotalTransporteMayor = flujoCajaAnio.valorTransporteMayorPrincipal + flujoCajaAnio.valorTransporteMayorIntermedia;

    //Se obtiene el subtotal de costos de año
    flujoCajaAnio.subtotalCostos = flujoCajaAnio.valorTerreno + flujoCajaAnio.financiacion + flujoCajaAnio.subtotalMantenimiento + flujoCajaAnio.valorEstablecimiento + flujoCajaAnio.subtotalAprovechamiento
      + flujoCajaAnio.subtotalOtrosCostos + flujoCajaAnio.subtotalTransformacion + flujoCajaAnio.subtotalTransporteMenor + flujoCajaAnio.subtotalTransporteMayor + flujoCajaAnio.valCarbono +
      flujoCajaAnio.costoConstruccionCaminos + flujoCajaAnio.costoMantenimientoCaminos + flujoCajaAnio.subtotalPodas;

    //Se obtiene el valor de la administración
    if (this.resumenCostos.modoCobroAdmin === 'Valor fijo anual') {
      flujoCajaAnio.valAdministracion = this.resumenCostos.costoAdmin.Valor * (this.sensibilidad?.Administracion ?? 1);
    }
    else {
      flujoCajaAnio.valAdministracion = flujoCajaAnio.subtotalCostos * (this.resumenCostos.costoAdmin.Valor / 100) * (this.sensibilidad?.Administracion ?? 1);
    }

    //Se obtiene el valor de la asistencia técnica
    if (this.resumenCostos.modoCobroAsistencia === 'Valor fijo anual') {
      flujoCajaAnio.valAsistenciaTecnica = this.resumenCostos.costoAsistencia.Valor * (this.sensibilidad?.Asistencia ?? 1);
    }
    else {
      flujoCajaAnio.valAsistenciaTecnica = flujoCajaAnio.subtotalCostos * (this.resumenCostos.costoAsistencia.Valor / 100) * (this.sensibilidad?.Asistencia ?? 1);
    }

    //Se obtiene el total de ingresos del año 
    flujoCajaAnio.totalIngresos = (flujoCajaAnio.ingresosVentas + flujoCajaAnio.totalIngresosCif + flujoCajaAnio.totalIngresosCarbono + flujoCajaAnio.ingresoFinanciacion) * (this.sensibilidad?.Ingresos ?? 1);

    //se obtiene el costo de industria y comercio - *hay que obtener de configuración el costo de industria y comercio
    flujoCajaAnio.impuestoIndustriaComercio = flujoCajaAnio.totalIngresos * 0.01;

    //Se obtiene el total de costos del año
    flujoCajaAnio.totalCostosAnio = (flujoCajaAnio.subtotalCostos + flujoCajaAnio.valAdministracion + flujoCajaAnio.valAsistenciaTecnica + flujoCajaAnio.impuestoIndustriaComercio) * (this.sensibilidad?.Costos ?? 1);


    //--Impuestos--//
    //Se obtiene la utilidad bruta
    flujoCajaAnio.utilidadBruta = flujoCajaAnio.totalIngresos - flujoCajaAnio.totalCostosAnio;

    //se obtiene el costo del impuesto de renta
    if (this.infoGeneral.Persona.tipoDePersona == TipoPersona.Juridica && this.infoGeneral.Persona.tipoDeEntidad == TipoEntidad.ConAnimoLucro
      && flujoCajaAnio.utilidadBruta > 0) {
      flujoCajaAnio.impuestoRenta = 0.31 * flujoCajaAnio.utilidadBruta;
    }
    else if (this.infoGeneral.Persona.tipoDePersona == TipoPersona.Juridica && this.infoGeneral.Persona.tipoDeEntidad == TipoEntidad.SinAnimoLucro
      && flujoCajaAnio.utilidadBruta > 0) {
      flujoCajaAnio.impuestoRenta = 0.2 * flujoCajaAnio.utilidadBruta;
    }
    else if (this.infoGeneral.Persona.tipoDePersona == TipoPersona.Natural) {
      flujoCajaAnio.impuestoRenta = this.calcularImpuestoRentaNatural(flujoCajaAnio.utilidadBruta, this.infoGeneral.UVT);
    }

    //Se obtiene la utilidad después de impuestos
    flujoCajaAnio.utilidadDespuesImpuestos = flujoCajaAnio.utilidadBruta - flujoCajaAnio.impuestoRenta;

    this.proyecto.UtilidadesAnios.push(flujoCajaAnio.utilidadDespuesImpuestos);

    this.flujosCaja.push(flujoCajaAnio);
  }

  //Obtiene los ingresos del año
  ObtenerIngresosAnio(anioActual: number, flujoCajaAnio: FlujoCajaAnual) {
    if (this.especies.length > 0) {
      this.especies.forEach(especie => {
        if (especie.Establecimientos.length > 0) {
          especie.Establecimientos.forEach(establecimiento => {
            //Se obtienen los ingresos por ventas y el costo de transporte
            for (let i = 0; i < especie.Entresacas.length; i++) {

              const entresaca = especie.Entresacas[i];
              entresaca.CostoTransPrimaria = 0;

              if (anioActual == entresaca.Anio + establecimiento.Anio - 1) {

                let totalTransporteMenor = 0;
                let volumenAprovechadoAcumulado = 0;

                for (let j = 0; j < especie.Entresacas.length; j++) {
                  const otraEntresaca = especie.Entresacas[j];
                  if (otraEntresaca.Anio < entresaca.Anio) {
                    volumenAprovechadoAcumulado += otraEntresaca.VolumenAprovechado;
                  }

                }
                entresaca.VolumenAprovechado = this.ObtenerVolumenAprovechado(entresaca.Anio, (especie.RendimientoAnualPromedio * entresaca.FactorProporcionalidad * (this.sensibilidad?.Rendimiento ?? 1)), volumenAprovechadoAcumulado,
                  this.infoGeneral.TasaPerdidaEstimada, entresaca.PorcentajeAprovechamiento);

                let totalCantProducidaPrincipal = 0;
                let totalCantProducidaIntermedia = 0;
                let totalCantProducidaProductos = 0;

                especie.Productos.forEach(p => {
                  let volumenProducto = entresaca.VolumenXProducto.find(v => v.producto == p.Nombre)?.volumen ?? 0;

                  //Se obtiene la cantidad producida                             
                  let cantidadProducida = establecimiento.Area *
                    (entresaca.VolumenAprovechado) * (p.FactorAprovechamientoEfectivo / 100) * (volumenProducto / 100);
                  
                  totalCantProducidaProductos += cantidadProducida;

                  //Se obtiene el costo de transformación primaria por producto
                  entresaca.CostoTransPrimaria += p.CostoTransPrimaria * cantidadProducida;

                  flujoCajaAnio.ventasCiudadPrincipal += cantidadProducida * (p.PorcentajeCiudadPrincipal / 100) * p.PrecioCiudadPrincipal;
                  flujoCajaAnio.ventasCiudadIntermedia += cantidadProducida * (p.PorcentajeCiudadIntermedia / 100) * p.PrecioCiudadIntermedia;
                  flujoCajaAnio.ventasBordeCarretera += cantidadProducida * (p.PorcentajeBordeCarretera / 100) * p.PrecioBordeCarretera;

                  totalCantProducidaPrincipal += cantidadProducida * (p.PorcentajeCiudadPrincipal / 100);
                  totalCantProducidaIntermedia += cantidadProducida * (p.PorcentajeCiudadIntermedia / 100);                 
                });

                let totalVentas = flujoCajaAnio.ventasCiudadPrincipal + flujoCajaAnio.ventasCiudadIntermedia + flujoCajaAnio.ventasBordeCarretera;
                flujoCajaAnio.ingresosVentas = totalVentas;

                //Se obtienen los costos de transporte menor
                if(!entresaca.esNoComercial){                       
                  this.resumenCostos.transportesMenor.forEach(transporte => {
                    totalTransporteMenor += transporte.CostoM3 * totalCantProducidaProductos * (transporte.porcentajeVolumen / 100);
                  });
                }
                else{
                  totalTransporteMenor += entresaca.valorTransporteMenor * entresaca.VolumenAprovechado * establecimiento.Area;
                }

                flujoCajaAnio.valoresTransporteMenor[i] = totalTransporteMenor;

                flujoCajaAnio.valorTransporteMayorPrincipal += this.obtenerValorTransporteMayor(anioActual, totalCantProducidaPrincipal, TipoCiudad.Principal);
                flujoCajaAnio.valorTransporteMayorIntermedia += this.obtenerValorTransporteMayor(anioActual, totalCantProducidaIntermedia, TipoCiudad.Intermedia);
                flujoCajaAnio.valoresTransformacionPrimaria[i] = entresaca.CostoTransPrimaria;
              }
              else {
                flujoCajaAnio.valoresTransporteMenor.push(0);
                flujoCajaAnio.valoresTransformacionPrimaria.push(0);
              }

            }

            //Se obtienen los ingresos por CIF          
            if (anioActual == 1 + establecimiento.Anio - 1) {
              flujoCajaAnio.ingresoCifEstablecimiento = this.ingresos.ingresoCifEstablecimiento * establecimiento.Area * (this.sensibilidad?.Cif ?? 1);
              flujoCajaAnio.totalIngresosCif += flujoCajaAnio.ingresoCifEstablecimiento;
            }
            if (anioActual == 6 + establecimiento.Anio - 1) {
              flujoCajaAnio.ingresoCifMantenimientos = (this.ingresos.ingresoCifMant2 +
                this.ingresos.ingresoCifMant3 +
                this.ingresos.ingresoCifMant4 +
                this.ingresos.ingresoCifMant5) * establecimiento.Area * (this.sensibilidad?.Cif ?? 1);
              flujoCajaAnio.totalIngresosCif += flujoCajaAnio.ingresoCifMantenimientos;

              //Se agrega CIF Bosque natural
              if (establecimiento.AreaBosque / establecimiento.Area < 0.2) {
                flujoCajaAnio.ingresoCifBosque = (this.ingresos.ingresoCifBosque2 +
                  this.ingresos.ingresoCifBosque3 +
                  this.ingresos.ingresoCifBosque4 +
                  this.ingresos.ingresoCifBosque5
                ) * establecimiento.AreaBosque;
              }
              else {
                flujoCajaAnio.ingresoCifBosque = (this.ingresos.ingresoCifBosque2 +
                  this.ingresos.ingresoCifBosque3 +
                  this.ingresos.ingresoCifBosque4 +
                  this.ingresos.ingresoCifBosque5
                ) * (establecimiento.Area * 0.2);
              }
              flujoCajaAnio.totalIngresosCif += flujoCajaAnio.ingresoCifBosque;
            }
          });
        }
      });
    }

    //Se obtienen los ingresos por bonos carbono
    if (this.ingresos.ingresosBonos.length > 0) {
      this.ingresos.ingresosBonos.forEach(bono => {
        if (bono.Anio == anioActual) {
          flujoCajaAnio.totalIngresosCarbono += bono.Valor * (this.sensibilidad?.Carbono ?? 1);
        }
      });
    }

    //Se obtienen otros ingresos
    flujoCajaAnio.subtotalOtrosIngresos = this.ingresos.otrosIngresos.filter(i => i.Anio == anioActual).map(i => i.Valor).reduce((a, b) => a + b, 0);

    //Si hay crédito, se obtiene ingreso por financiación
    if (anioActual == 1) {
      flujoCajaAnio.ingresoFinanciacion = this.modeloInversion.credito.capitalSolicitado;
    }
  }

  //Obtiene los costos del año
  ObtenerCostosAnio(flujoCajaAnio: FlujoCajaAnual, anioActual: number) {
    if (this.especies.length > 0) {
      //Se recorren las especies      
      this.especies.forEach(especie => {
        flujoCajaAnio.valoresAprovechamiento = new Array<number>(especie.Entresacas.length);

        if (especie.Establecimientos.length > 0) {
          //Se recorren los establecimientos
          especie.Establecimientos.forEach(establecimiento => {
            if (establecimiento.Anio == anioActual) {
              this.terrenoEstablecidoAcumulado += establecimiento.Area;
            }
            else if (anioActual == establecimiento.Anio + especie.Turno) {
              this.terrenoEstablecidoAcumulado -= establecimiento.Area;
            }
            if (anioActual >= establecimiento.Anio && anioActual < establecimiento.Anio + especie.Turno) {
              //Se obtiene el valor del terreno de la plantación para el año
              flujoCajaAnio.valorTerreno = this.terrenoEstablecidoAcumulado * this.resumenCostos.costoTierra.Valor;

              //Se agregan costos correspondientes al flujo de restauración
              if (this.flujoActual == 'restauracion') {
                flujoCajaAnio.costoSeguro = establecimiento.CostoSeguro;
                flujoCajaAnio.costoCercamiento = establecimiento.CostoCercamiento;
              }

              //Se obienen los valores de mantenimiento
              if (this.resumenCostos.mantenimientos.length > 0) {
                flujoCajaAnio.valoresMantenimiento.length = this.resumenCostos.mantenimientos.length;
                for (let i = 0; i < this.resumenCostos.mantenimientos.length; i++) {
                  let mantenimiento = this.resumenCostos.mantenimientos[i];
                  let valorMantenimiento = 0;
                  if (anioActual == mantenimiento.Anio + establecimiento.Anio - 1) {
                    valorMantenimiento = establecimiento.Area * mantenimiento.Valor;
                  }
                  if (flujoCajaAnio.valoresMantenimiento[i] === undefined || flujoCajaAnio.valoresMantenimiento[i] === 0) {
                    flujoCajaAnio.valoresMantenimiento[i] = valorMantenimiento;
                  }
                }
              }

              //Se calcula el número de árboles en cada año
              let numeroArbolesEstablecimiento = 0;
              numeroArbolesEstablecimiento = this.obtenerNumeroArbolesEstablecimiento(anioActual, especie.Entresacas,establecimiento);
              this.numeroArbolesAcumulado = flujoCajaAnio.numeroArboles;

              //Se calculan los costos de las podas
              if (this.resumenCostos.podas.length > 0) {
                  flujoCajaAnio.valoresPodas.length = this.resumenCostos.podas.length;

                  for (let i = 0; i < this.resumenCostos.podas.length; i++) {
                   let poda = this.resumenCostos.podas[i];
                   let valorPoda = 0;
  
                    if (anioActual == poda.Anio + establecimiento.Anio - 1) {
                      if (this.resumenCostos.modoCobroPodas == 'Valor por hectárea') {
                        valorPoda = establecimiento.Area * poda.Valor;
                      }
                      else if (this.resumenCostos.modoCobroPodas == 'Valor por árbol'){
                        valorPoda = numeroArbolesEstablecimiento * poda.Valor * establecimiento.Area;
                      }
                    }
  
                    if (flujoCajaAnio.valoresPodas[i] === undefined || flujoCajaAnio.valoresPodas[i] === 0) {
                      flujoCajaAnio.valoresPodas[i] = valorPoda;
                    }
                }
              }

              //Se recorren las entresacas
              if (especie.Entresacas.length > 0) {
                especie.Entresacas.sort((a, b) => a.Anio - b.Anio);
                for (let i = 0; i < especie.Entresacas.length; i++) {
                  const entresaca = especie.Entresacas[i];

                  //Se obtienen los valores de aprovechamiento y transformación primaria
                  let valorAprovechamiento = 0;

                  if (anioActual == entresaca.Anio + establecimiento.Anio - 1) {

                    let volumenAprovechadoAcumulado = 0;
                    for (let j = 0; j < especie.Entresacas.length; j++) {
                      const otraEntresaca = especie.Entresacas[j];
                      if (otraEntresaca.Anio < entresaca.Anio) {
                        volumenAprovechadoAcumulado += otraEntresaca.VolumenAprovechado;
                      }
                    }
                    entresaca.VolumenAprovechado = this.ObtenerVolumenAprovechado(entresaca.Anio, (especie.RendimientoAnualPromedio * entresaca.FactorProporcionalidad * (this.sensibilidad?.Rendimiento ?? 1)), volumenAprovechadoAcumulado,
                      this.infoGeneral.TasaPerdidaEstimada, entresaca.PorcentajeAprovechamiento);

                    valorAprovechamiento = entresaca.CostoAprovechamiento * entresaca.VolumenAprovechado * establecimiento.Area;

                    flujoCajaAnio.valoresAprovechamiento[i] = valorAprovechamiento;

                  } else if (!flujoCajaAnio.valoresAprovechamiento[i]) {
                    flujoCajaAnio.valoresAprovechamiento[i] = 0;
                  }
                }
              }
            }
            if (establecimiento.Anio == anioActual) {
              //Se obtiene el valor del establecimiento para los años a establecer            
              flujoCajaAnio.valorEstablecimiento = this.resumenCostos.costoEstablecimiento.Valor * establecimiento.Area;
              flujoCajaAnio.valorEstablecimiento = flujoCajaAnio.valorEstablecimiento + (flujoCajaAnio.valorEstablecimiento * (this.infoGeneral.TasaResiembra/300))
            }
          });
        }
      });
    }

    flujoCajaAnio.costoConstruccionCaminos = this.resumenCostos.costoConstCaminos.Valor * this.resumenCostos.kmAConstruir;
    flujoCajaAnio.costoMantenimientoCaminos = this.resumenCostos.costoMantCaminos.Valor * this.resumenCostos.kmAMantener;

    //Se obtienen los costos de carbono
    if (this.resumenCostos.costosCarbono.length > 0) {
      let costoCarbono = this.resumenCostos.costosCarbono.find(c => c.Anio == anioActual);
      if (costoCarbono) {
        flujoCajaAnio.valCarbono = costoCarbono.Valor;
      }
    }

    //Se agregan otros costos
    flujoCajaAnio.otrosCostos = this.resumenCostos.otrosCostos.map(c => c.Anio == anioActual ? c.Valor : 0);
    flujoCajaAnio.subtotalOtrosCostos += flujoCajaAnio.otrosCostos.reduce((a, b) => a + b, 0);

    //Se obtienen costos por financiación (crédito)
    flujoCajaAnio.financiacion = this.modeloInversion.credito.costosAnio.filter(c => c.Anio == anioActual)?.map(c => c.Valor)[0] ?? 0; 
    
  }

  //Obtiene el valor del transporte mayor
  obtenerValorTransporteMayor(anioActual: number, cantProducida: number, tipoCiudad: TipoCiudad): number {
    let valorTransporteMayor = 0;
    
    let listaCostosMayor: EspecificoTransporteMayor[] = [];

    if (this.resumenCostos.transportesMayor && this.resumenCostos.transportesMayor.length > 0) {
      for (let i = 0; i < this.resumenCostos.transportesMayor.length; i++) {
      
        const transporte = this.resumenCostos.transportesMayor[i];
     
          let especificoTransporte = new EspecificoTransporteMayor();
          especificoTransporte.anio = anioActual;
          especificoTransporte.tipoVehiculo = transporte.TipoVehiculo;

          if(tipoCiudad == TipoCiudad.Principal){
            let tipoCiudad = TipoCiudad.Principal;
            especificoTransporte.tipoCiudad = tipoCiudad;
            especificoTransporte.volumenATransportar = cantProducida * (transporte.porcentajeCiudadPrincipal/100);
            especificoTransporte.costoTotalViaje = transporte.costoViajeCiudadPrincipal;
            especificoTransporte.costoSalvoconductoViaje = transporte.costoSalvoconductoCiudadPrincipal;
            especificoTransporte.costoCargueDescargue = transporte.costoCargueCiudadPrincipal;
          }
          else if (tipoCiudad == TipoCiudad.Intermedia) {
            let tipoCiudad = TipoCiudad.Intermedia
            especificoTransporte.tipoCiudad = tipoCiudad;
            especificoTransporte.volumenATransportar = cantProducida * (transporte.porcentajeCiudadIntermedia/100);
            especificoTransporte.costoTotalViaje = transporte.costoViajeCiudadIntermedia;
            especificoTransporte.costoSalvoconductoViaje = transporte.costoSalvoconductoCiudadIntermedia;
            especificoTransporte.costoCargueDescargue = transporte.costoCargueCiudadIntermedia;
          }
         
          switch (true) {
            case transporte.TipoVehiculo === TipoVehiculo.CamionSencillo:
              especificoTransporte.capacidadPorViaje = 10;
              break;

            case transporte.TipoVehiculo === TipoVehiculo.DobleTroque:
              especificoTransporte.capacidadPorViaje = 19;
              break;
            
            case transporte.TipoVehiculo === TipoVehiculo.TractoMula:
              especificoTransporte.capacidadPorViaje = 30;
              break;
          
            default:
              break;
          }

          especificoTransporte.numeroViajesRealizar = Math.round(especificoTransporte.volumenATransportar / especificoTransporte.capacidadPorViaje);
          especificoTransporte.costoFlete = especificoTransporte.costoTotalViaje / especificoTransporte.capacidadPorViaje;
          especificoTransporte.costoTotalTransporte = especificoTransporte.costoFlete * especificoTransporte.volumenATransportar;
          especificoTransporte.totalSalvoconductos = especificoTransporte.costoSalvoconductoViaje * especificoTransporte.numeroViajesRealizar;
          especificoTransporte.totalCargueDescargue = especificoTransporte.costoCargueDescargue * especificoTransporte.numeroViajesRealizar;
          especificoTransporte.totalVehiculo = especificoTransporte.totalCargueDescargue + especificoTransporte.totalSalvoconductos + especificoTransporte.costoTotalTransporte;

          listaCostosMayor.push(especificoTransporte);        
      }
    }

    valorTransporteMayor = listaCostosMayor.map(c => c.totalVehiculo).reduce((a,b)=> a + b, 0);

    return valorTransporteMayor;
  }

  //Obtiene los resultados para pintar en pantalla
  obtenerResultados(){
    this.proyecto.Tir = this.calcularTirManual(this.flujosCaja.map(f => f.utilidadDespuesImpuestos));
    this.proyecto.Vpn = this.calcularVpn();
    this.proyecto.AreaMinimaTir = this.ObtenerAreaMinimaTir();
    this.proyecto.AreaMinimaIngreso = this.ObtenerAreaMinimaIngreso();
    this.proyecto.IngresoMensualPromedioPercibido = this.obtenerIngresoMensualPromedio();
    this.proyecto.InversionMinimaRequerida = this.obtenerinversionMinima();
    
    this.tirPintar = ( !Number.isNaN(this.proyecto.Tir) ? (this.proyecto.Tir * 100).toFixed(2) : "No es posible calcular la TIR con los valores indicados");
    this.vpnPintar = this.convertirAPesos(this.proyecto.Vpn);
    this.areaMinimaTirPintar = this.proyecto.AreaMinimaTir.toString();
    this.areaMinimaIngresoPintar = this.proyecto.AreaMinimaIngreso.toString();
    this.ingresoMensualPromedioPintar = this.convertirAPesos(this.proyecto.IngresoMensualPromedioPercibido);
    this.inversionMinimaPintar = this.convertirAPesos(this.proyecto.InversionMinimaRequerida);

    this.proyecto.FuncionOptimizarTir = 0.0025 * Math.log(this.proyecto.AreaMinimaTir) + this.proyecto.Tir;
  }

  //Obtiene la inversión mínima
  obtenerinversionMinima(): number {
    let totalCostoEstablecimientos = 0;
    let totalCostosMantenimientos = 0;
    let totalCostosPodas = 0;
    let totalCostosEntresacas = 0;
    this.flujosCaja.forEach(flujoCaja => {
      totalCostoEstablecimientos += flujoCaja.valorEstablecimiento;
      totalCostosMantenimientos += flujoCaja.subtotalMantenimiento;
      totalCostosPodas += flujoCaja.subtotalPodas;
      for (let i = 0; i < flujoCaja.valoresAprovechamiento.length; i++) { 
        if (i < flujoCaja.valoresAprovechamiento.length - 1) {
          totalCostosEntresacas += flujoCaja.valoresAprovechamiento[i];
        }        
      }
    });
    
    let inversionMinima = (totalCostoEstablecimientos + totalCostosMantenimientos + totalCostosEntresacas + totalCostosPodas) * (this.sensibilidad?.Costos ?? 1)

    return Math.round(inversionMinima);

  }

  //Obtiene el ingreso mensual promedio
  obtenerIngresoMensualPromedio(): number {
    let ingresoMensualPromedio = 0;
    let totalFlujosCaja = 0;
    this.flujosCaja.forEach(flujoCaja => {
      totalFlujosCaja += flujoCaja.utilidadDespuesImpuestos;
    });
    ingresoMensualPromedio = (totalFlujosCaja / this.proyecto.TiempoTotal) / 12;

    return Math.round(ingresoMensualPromedio);

  }

  //Obtiene el impuesto de rentaa natural
  calcularImpuestoRentaNatural(utilidadBrutaAnio: number, valorUVT: number): number {
    let impuesto = 0;
    const baseGravable = utilidadBrutaAnio/valorUVT;

    switch (true) {
      case baseGravable <= 1090 :
        impuesto = 0;
        break;
      case baseGravable > 1090 && baseGravable <= 1700:
        impuesto = (baseGravable - 1090) * 0.19;
        break;
      case baseGravable > 1700 && baseGravable <= 4100:
        impuesto = ((baseGravable - 1700) * 0.28) + 116;
        break;
      case baseGravable > 4100 && baseGravable <= 8670:
        impuesto = ((baseGravable - 4100) * 0.33)  + 788;
        break;
      case baseGravable > 8670 && baseGravable <= 18970:
        impuesto = ((baseGravable - 8670) * 0.35) + 2296;
        break;
      case baseGravable > 18970 && baseGravable <= 31000:
        impuesto = ((baseGravable - 18970) * 0.37) + 5901;
        break;
      case baseGravable > 31000:
        impuesto = ((baseGravable - 31000)  * 0.39) + 10352;
        break;
      default:
        break;
    }

    impuesto = impuesto * valorUVT;

    return impuesto;
  }

  //Obtiene el volúmen aprovechado
  ObtenerVolumenAprovechado(anioEntresaca: number, rendimientoEspecie: number, volumenAcumulado: number, 
    tasaPerdida: number, porcentajeEntresaca: number): number{
    return ((anioEntresaca * rendimientoEspecie) - volumenAcumulado) * 
                          (1 - tasaPerdida/100) * (porcentajeEntresaca/100);
  }

  //Obtiene la suma de valores en un arreglo de números
  ObtenerSumaArray(arreglo: number[]){
    return arreglo.reduce((a,b) => a + b, 0);
  }

  //Obtiene la Tir
  calcularTir(){ 
    let tir = irr(this.flujosCaja.map(f => f.utilidadDespuesImpuestos));
    if (tir == Number.NaN) {
      this.fgroupResultados.get('fcontrolTir')?.setValue("No es posible calcular el valor de la tir");
    }
    else{
      this.fgroupResultados.get('fcontrolTir')?.setValue(tir);
    }
    return tir;
  }

  //Obtiene la tir de forma manual
  calcularTirManual(values: number[], guess = 0.1, tol = 1e-6, maxIter = 100){
    const dates : number[] = []
    let positive = false
    let negative = false
    for (let i = 0; i < values.length; i++) {
      dates[i] = (i === 0) ? 0 : dates[i - 1] + 365
      if (values[i] > 0) {
        positive = true
      }
      if (values[i] < 0) {
        negative = true
      }
    }

    if (!positive || !negative) {
      return Number.NaN
    }

    let resultRate = guess

    // Implementa el método de Newton
    let newRate, epsRate, resultValue
    let iteration = 0
    let contLoop = true
    do {
      resultValue = this._irrResult(values, dates, resultRate)
      newRate = this._irrResultDeriv(values, dates, resultRate) != 0 ? (resultRate - resultValue / this._irrResultDeriv(values, dates, resultRate)) : 0;
      epsRate = Math.abs(newRate - resultRate)
      resultRate = newRate
      contLoop = (epsRate > tol) && (Math.abs(resultValue) > tol)
    } while (contLoop && (++iteration < maxIter))

    if (contLoop) {
      return Number.NaN
    }

    return resultRate

  }


  //Calcula el valor resultante
  private _irrResult (values: number[], dates: number[], rate: number): number {
    const r = rate + 1
    let result = values[0]
    for (let i = 1; i < values.length; i++) {
      result += values[i] / Math.pow(r, (dates[i] - dates[0]) / 365)
    }
    return result
  }

  //Calcula la primera derivación
  private _irrResultDeriv (values: number[], dates: number[], rate: number) : number {
    const r = rate + 1
    let result = 0
    for (let i = 1; i < values.length; i++) {
      const frac = (dates[i] - dates[0]) / 365
      result -= frac * values[i] / Math.pow(r, frac + 1)
    }
    return result
  }

  //Obtiene el valor presente neto
  calcularVpn(){

    let dtf = this.proyecto.Dtf/100;

    let vpn = 0;

    for (let i = 0; i < this.flujosCaja.length; i++) {
      const utilidad = this.flujosCaja[i].utilidadDespuesImpuestos;
      
      let valorConDescuento = utilidad/Math.pow((1 + dtf), (i + 1));
      vpn += valorConDescuento;      
    }

    return vpn;
  }

  //Obtiene el área mínima de la plantación
  ObtenerAreaMinimaTir(){
    return Math.round(Math.pow(Math.E, this.proyecto.Tir/0.025));
  }

  //Obtiene el área mínima por ingreso
  ObtenerAreaMinimaIngreso(){
    this.proyecto.IngresoMinimoMensualEsperado = this.fgroupResultados.get('fcontrolIngresoEsperado')?.value;
    return Math.round((this.proyecto.IngresoMinimoMensualEsperado/this.obtenerIngresoMensualPromedio()) * this.infoGeneral.AreaTotalProyecto);
  }

  //Convierte un valor a pesos colombianos
  convertirAPesos(valor: number): string{
    try {
      if (valor == undefined || Number.isNaN(valor))  {
        return '0';
      }

      let valorEnPesos = new Intl.NumberFormat('es-CO',  {
        style: 'currency',
        currency: 'COP',
      }).format(valor);
     

      return valorEnPesos;
    } catch (error) {
      console.log('error al convertir a pesos: ' + error);
      return '0';
    }
  }

  //Permite obtener la tabla de flujo de caja
  obtenerTablaFlujosCaja(){
      let tabla = [] ;
      
      tabla.push(["Inversión - Actividad/año"].concat(this.flujosCaja.map(f => f.anio.toString()))); 
      tabla.push(["COSTOS"]);
      tabla.push(["Valor terreno plantación"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.valorTerreno))));
      tabla.push(["Valor financiación"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.financiacion))));
      tabla.push(["Mantenimientos"]);

      //Se agregan los mantenimientos
      let mantenimientos = this.resumenCostos.mantenimientos;

      if (mantenimientos.length > 0) {
        for (let i = 0; i < mantenimientos.length; i++) {
          let mantenimiento = mantenimientos[i];
          let filaMantenimiento = [];
          filaMantenimiento.push("Mantenimiento año " + mantenimiento.Anio);
        
          for (let j = 0; j < this.flujosCaja.length; j++) {
            let flujoCaja = this.flujosCaja[j];
            filaMantenimiento.push(this.convertirAPesos(flujoCaja.valoresMantenimiento[i]));
            
          }
  
          tabla.push(filaMantenimiento);
        }
      }
      

      tabla.push(["Subtotal Mantenimiento"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalMantenimiento))));

      tabla.push(["Establecimiento"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.valorEstablecimiento))));

      
      //Se agregan los aprovechamientos
      tabla.push(["Aprovechamiento"]);
      if (this.especies.length > 0) {
        this.especies.forEach(especie => {
          for (let i = 0; i < especie.Entresacas.length; i++) {
            let entresaca = especie.Entresacas[i];
            let filaEntresaca = [];
            if(i < especie.Entresacas.length - 1)
            {
              filaEntresaca.push("Entresaca año " + entresaca.Anio);
            }
            else{
              filaEntresaca.push("Corta final año " + entresaca.Anio);
            }

            for (let j = 0; j < this.flujosCaja.length; j++) {
              let flujoCaja = this.flujosCaja[j];
              filaEntresaca.push(this.convertirAPesos(flujoCaja.valoresAprovechamiento[i]));
              
            }

            tabla.push(filaEntresaca);
          }
        });
      }
     
      tabla.push(["Subtotal Aprovechamiento"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalAprovechamiento))));

      
      //Se agrega la transformación primaria
      tabla.push(["Transformación Primaria"]);

      if (this.especies.length > 0) {
        this.especies.forEach(especie => {
          for (let i = 0; i < especie.Entresacas.length; i++) {
            let entresaca = especie.Entresacas[i];
            let filaEntresaca = [];
            if(i < especie.Entresacas.length - 1)
            {
              filaEntresaca.push("Entresaca año " + entresaca.Anio);
            }
            else{
              filaEntresaca.push("Corta final año " + entresaca.Anio);
            }
  
            for (let j = 0; j < this.flujosCaja.length; j++) {
              let flujoCaja = this.flujosCaja[j];
              filaEntresaca.push(this.convertirAPesos(flujoCaja.valoresTransformacionPrimaria[i]));
              
            }

            tabla.push(filaEntresaca);
          }

        });
      }
      
      tabla.push(["Subtotal Transformación Primaria"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalTransformacion))));
      
      //Se agrega el transporte menor
      tabla.push(["Transporte Menor"]);

      if (this.especies.length > 0) {
        this.especies.forEach(especie => {
          for (let i = 0; i < especie.Entresacas.length; i++) {
            let entresaca = especie.Entresacas[i];
            let filaEntresaca = [];
            if(i < especie.Entresacas.length - 1)
            {
              filaEntresaca.push("Entresaca año " + entresaca.Anio);
            }
            else{
              filaEntresaca.push("Corta final año " + entresaca.Anio);
            }

            for (let j = 0; j < this.flujosCaja.length; j++) {
              let flujoCaja = this.flujosCaja[j];
              filaEntresaca.push(this.convertirAPesos(flujoCaja.valoresTransporteMenor[i]));
              
            }
            tabla.push(filaEntresaca);
          }
          
        });
      }
     
    tabla.push(["Subtotal Transporte Menor"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalTransporteMenor))));

    //Se agrega el transporte mayor
    tabla.push(["Transporte Mayor"]);
    tabla.push(["Ciudad Principal"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.valorTransporteMayorPrincipal))));
    tabla.push(["Ciudad Intermedia"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.valorTransporteMayorIntermedia))));

    tabla.push(["Subtotal Transporte Mayor"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalTransporteMayor))));

    tabla.push(["Costo Carbono"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.valCarbono))));

    tabla.push(["Costo Construcción Caminos"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.costoConstruccionCaminos))));

    tabla.push(["Costo Mantenimiento Caminos"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.costoMantenimientoCaminos))));

    //Se agregan los costos de las podas

    tabla.push(["Podas"]);

    //Se agregan los mantenimientos
    let podas = this.resumenCostos.podas;

    if (podas.length > 0) {
      for (let i = 0; i < podas.length; i++) {
        let poda = podas[i];
        let filaPoda = [];
        filaPoda.push("Poda año " + poda.Anio);
      
        for (let j = 0; j < this.flujosCaja.length; j++) {
          let flujoCaja = this.flujosCaja[j];
          filaPoda.push(this.convertirAPesos(flujoCaja.valoresPodas[i]));      
        }

        tabla.push(filaPoda);
      }
    }

    tabla.push(["Subtotal Podas"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalPodas))));

    //Se agregan otros costos
    tabla.push(["Otros Costos"]);

    let otrosCostos = this.resumenCostos.otrosCostos;

    if (otrosCostos.length > 0) {
      for (let i = 0; i < otrosCostos.length; i++) {
        let otroCosto = otrosCostos[i];
        let filaOtroCosto = [];
        filaOtroCosto.push(otroCosto.Nombre + " año " + otroCosto.Anio);
      
        for (let j = 0; j < this.flujosCaja.length; j++) {
          let flujoCaja = this.flujosCaja[j];
          filaOtroCosto.push(this.convertirAPesos(flujoCaja.otrosCostos[i]));
          
        }
        tabla.push(filaOtroCosto);
      }
    }

    tabla.push(["Subtotal Otros Costos"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalOtrosCostos))));

    tabla.push(["Subtotal Costos"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalCostos))));

    tabla.push(["Administración"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.valAdministracion))));

    tabla.push(["Asistencia Técnica"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.valAsistenciaTecnica))));

    tabla.push(["Impuesto Industria y Comercio"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.impuestoIndustriaComercio))));

    tabla.push(["TOTAL COSTOS"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.totalCostosAnio))));

    tabla.push(["Ingresos"]);

    tabla.push(["Financiación"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.ingresoFinanciacion))));

    tabla.push(["Ventas"]);

    tabla.push(["Ingresos Ciudad Principal"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.ventasCiudadPrincipal)))); 

    tabla.push(["Ingresos Ciudad Intermedia"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.ventasCiudadIntermedia))));
    
    tabla.push(["Ingresos Borde Carretera"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.ventasBordeCarretera)))); 

    tabla.push(["Subtotal Ventas"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.ingresosVentas))));

    //Se agrega certificado de incentivo forestal
    
    tabla.push(["Certificado de Incentivo Forestal CIF"]);
    
    tabla.push(["CIF establecimiento"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.ingresoCifEstablecimiento)))); 
    
    tabla.push(["CIF Mantenimiento (año 2-5)"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.ingresoCifMantenimientos)))); 
    
    tabla.push(["Subtotal Ingreso CIF"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.totalIngresosCif))));

    tabla.push(["Ingreso Carbono"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.totalIngresosCarbono))));

    tabla.push(["Otros Ingresos"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.subtotalOtrosIngresos))));

    tabla.push(["TOTAL INGRESOS"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.totalIngresos))));

    tabla.push(["Utilidad Bruta"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.utilidadBruta))));

    tabla.push(["Impuesto Renta"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.impuestoRenta))));

    tabla.push(["Utilidad Después de Impuestos"].concat(this.flujosCaja.map(f => this.convertirAPesos(f.utilidadDespuesImpuestos))));

    return tabla;

  }

  //Manejador del evento al cambiar la TIR
  onTirChanged(){
    let nuevaTir = this.fgroupResultados.get('fcontrolTir')?.value;
    this.proyecto.Tir = nuevaTir;
    this.proyecto.AreaMinimaTir = this.ObtenerAreaMinimaTir();
  }

  //Manejador del evento al cambiar el ingreso
  onIngresoChanged(){
    this.proyecto.AreaMinimaIngreso = this.ObtenerAreaMinimaIngreso();
  }

  //Manejador del evento al cambiar el tipo de cálculo
  onCambioTipoCalculo(){
    let tipo = this.fgroupResultados.get('fcontrolTipoCalculo')?.value;
    if(tipo != 'ingreso'){
      this.aplicaTir = true;
      this.ObtenerAreaMinimaTir()
    }
    else{
      this.aplicaTir = false;
      this.ObtenerAreaMinimaIngreso();
    }
  }

  //Genera el pdf para mostrar el resumen de resultados
  onGenerarPdf(){
    //const doc = new jsPDF('l', 'mm', [2600, 1350]);;

    let largoPagina = 0;
    let anchoPagina = 0;

    let tablaFlujos = this.obtenerTablaFlujosCaja();

    //Se obtiene la medida de la página
    
    for (let i = 0; i < tablaFlujos[0].length; i++) {
      let palabraMasLarga = '';
      for (let j = 0; j < tablaFlujos.length; j++) {
        let palabra = tablaFlujos[j][i];

        if (palabra != undefined && palabra.length > palabraMasLarga.length) {
          palabraMasLarga = palabra;
        }
      }
      largoPagina += palabraMasLarga.length;    
    }

    const doc = new jsPDF('l','pt',[largoPagina * 15, tablaFlujos.length * 25]);

    console.log(JSON.stringify(this.flujosCaja));

    autoTable(doc,{
      head: [tablaFlujos[0]],
      body: tablaFlujos, 
      tableLineColor: [189, 195, 199],
      tableLineWidth: 0.75,
      theme:'grid',
      //columnStyles: {0:{cellWidth:'auto'}}
    });
    
    doc.save('flujo_de_caja_'+this.flujoActual+'.pdf');
  }

}
 