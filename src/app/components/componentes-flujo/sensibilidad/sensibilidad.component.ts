import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Sensibilidad } from 'src/app/core/entities/Sensibilidad';
import { SensibilidadService } from '../services/sensibilidad/sensibilidad.service';

@Component({
  selector: 'app-sensibilidad',
  templateUrl: './sensibilidad.component.html',
  styleUrls: ['./sensibilidad.component.scss']
})
export class SensibilidadComponent implements OnInit {

  //form
  fgroupSensibilidad = new FormGroup({
    fcontrolRendimiento: new FormControl(1),
    fcontrolAsistencia: new FormControl(1),
    fcontrolAdmin: new FormControl(1),
    fcontrolIngresos: new FormControl(1),
    fcontrolCostos: new FormControl(1),
    fcontrolCif: new FormControl(1),
    fcontrolCarbono: new FormControl(1),
  });


  constructor(private servicioSensibilidad: SensibilidadService) { }

  //Objeto sensibillidad
  sensibilidad: Sensibilidad = new Sensibilidad();

  ngOnInit(): void {
    this.obtenerControl('fcontrolRendimiento')?.setValue(1);
    this.obtenerControl('fcontrolDistanciaCentro')?.setValue(1);
    this.obtenerControl('fcontrolAsistencia')?.setValue(1);
    this.obtenerControl('fcontrolAdmin')?.setValue(1);
    this.obtenerControl('fcontrolIngresos')?.setValue(1);
    this.obtenerControl('fcontrolCostos')?.setValue(1);
    this.obtenerControl('fcontrolCif')?.setValue(1);
    this.obtenerControl('fcontrolCarbono')?.setValue(1);
    this.obtenerControl('fcontrolTasa')?.setValue(1);
  }

  guardarSensibilidad(){
    this.sensibilidad.Rendimiento = this.obtenerControl('fcontrolRendimiento')?.value;
    this.sensibilidad.Asistencia = this.obtenerControl('fcontrolAsistencia')?.value;
    this.sensibilidad.Administracion = this.obtenerControl('fcontrolAdmin')?.value;
    this.sensibilidad.Ingresos = this.obtenerControl('fcontrolIngresos')?.value;
    this.sensibilidad.Costos = this.obtenerControl('fcontrolCostos')?.value;
    this.sensibilidad.Cif = this.obtenerControl('fcontrolCif')?.value;
    this.sensibilidad.Carbono = this.obtenerControl('fcontrolCarbono')?.value;

    this.servicioSensibilidad.enviarSensibilidad(this.sensibilidad);
  }

  obtenerControl(control: string){
    return this.fgroupSensibilidad.get(control);
  }

}
