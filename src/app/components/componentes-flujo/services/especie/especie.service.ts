import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Especie } from 'src/app/core/entities/Especie';

@Injectable({
  providedIn: 'root'
})
export class EspecieService {
  private subject = new Subject<Especie[]>();

  enviarEspecie(modelo: Especie[]){
    this.subject.next(modelo);
  }

  limpiarEspecie(){
    this.subject.next();
  }

  OnEspecie(): Observable<Especie[]>{
    return this.subject.asObservable();
  }
  
  constructor() { }
}
