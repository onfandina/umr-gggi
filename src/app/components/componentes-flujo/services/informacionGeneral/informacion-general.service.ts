import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { InformacionGeneral } from 'src/app/core/entities/informacionGeneral';

@Injectable({
  providedIn: 'root'
})
export class InformacionGeneralService {

  private subject = new Subject<InformacionGeneral>();

  enviarInformacionGeneral(modelo: InformacionGeneral){
    this.subject.next(modelo);
  }

  limpiarInformacionGeneral(){
    this.subject.next();
  }

  OnInformacionGeneral(): Observable<InformacionGeneral>{
    return this.subject.asObservable();
  }

  constructor() { }
}
