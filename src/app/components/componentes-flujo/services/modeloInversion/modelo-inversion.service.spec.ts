import { TestBed } from '@angular/core/testing';

import { ModeloInversionService } from './modelo-inversion.service';

describe('ModeloInversionService', () => {
  let service: ModeloInversionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModeloInversionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
