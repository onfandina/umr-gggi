import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ModeloInversion } from 'src/app/core/entities/ModeloInversion';

@Injectable({
  providedIn: 'root'
})
export class ModeloInversionService {

  private subject = new Subject<ModeloInversion>();

  enviarModeloInversion(modelo: ModeloInversion){
    this.subject.next(modelo);
  }

  limpiarModeloInversion(){
    this.subject.next();
  }

  OnModeloInversion(): Observable<ModeloInversion>{
    return this.subject.asObservable();
  }
  constructor() { }
}
