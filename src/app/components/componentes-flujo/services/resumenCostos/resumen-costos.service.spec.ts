import { TestBed } from '@angular/core/testing';

import { ResumenCostosService } from './resumen-costos.service';

describe('ResumenCostosService', () => {
  let service: ResumenCostosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResumenCostosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
