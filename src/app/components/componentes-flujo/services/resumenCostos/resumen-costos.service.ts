import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ResumenCostos } from 'src/app/core/entities/Costos/ResumenCostos';

@Injectable({
  providedIn: 'root'
})
export class ResumenCostosService {

  private subject = new Subject<ResumenCostos>();

  enviarResumenCostos(modelo: ResumenCostos){
    this.subject.next(modelo);
  }

  limpiarResumenCostos(){
    this.subject.next();
  }

  OnResumenCostos(): Observable<ResumenCostos>{
    return this.subject.asObservable();
  }

  constructor() { }
}
