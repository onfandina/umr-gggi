import { TestBed } from '@angular/core/testing';

import { ResumenIngresosService } from './resumen-ingresos.service';

describe('ResumenIngresosService', () => {
  let service: ResumenIngresosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResumenIngresosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
