import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ResumenCostos } from 'src/app/core/entities/Costos/ResumenCostos';
import { ResumenIngresos } from 'src/app/core/entities/Ingresos/ResumenIngresos';

@Injectable({
  providedIn: 'root'
})
export class ResumenIngresosService {

  constructor() { }

  private subject = new Subject<ResumenIngresos>();

  enviarResumenIngresos(modelo: ResumenIngresos){
    this.subject.next(modelo);
  }

  limpiarResumenIngresos(){
    this.subject.next();
  }

  OnResumenIngresos(): Observable<ResumenIngresos>{
    return this.subject.asObservable();
  }
}
