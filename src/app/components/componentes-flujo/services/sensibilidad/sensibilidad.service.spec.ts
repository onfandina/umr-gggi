import { TestBed } from '@angular/core/testing';

import { SensibilidadService } from './sensibilidad.service';

describe('SensibilidadService', () => {
  let service: SensibilidadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SensibilidadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
