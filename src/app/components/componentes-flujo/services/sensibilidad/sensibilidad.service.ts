import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Sensibilidad } from 'src/app/core/entities/Sensibilidad';

@Injectable({
  providedIn: 'root'
})
export class SensibilidadService {

  constructor() { }

  private subject = new Subject<Sensibilidad>();

  enviarSensibilidad(modelo: Sensibilidad){
    this.subject.next(modelo);
  }

  limpiarSensibilidad(){
    this.subject.next();
  }

  OnSensibilidad(): Observable<Sensibilidad>{
    return this.subject.asObservable();
  }

}
