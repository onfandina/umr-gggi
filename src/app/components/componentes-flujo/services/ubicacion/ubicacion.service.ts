import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Ubicacion } from 'src/app/core/entities/Ubicacion';

@Injectable({
  providedIn: 'root'
})
export class UbicacionService {

  private subject = new Subject<Ubicacion>();

  enviarUbicacion(ubicacion: Ubicacion){
    this.subject.next(ubicacion);
  }

  limpiarUbicacion(){
    this.subject.next();
  }

  OnUbicacion(): Observable<Ubicacion>{
    return this.subject.asObservable();
  }

  constructor() { }
}
