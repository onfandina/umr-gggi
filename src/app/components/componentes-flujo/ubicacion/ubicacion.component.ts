import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Ubicacion } from 'src/app/core/entities/Ubicacion';
import { locations } from 'src/app/models/departamentos';
import { rangoAlturaValues } from 'src/app/models/rangoAltura';
import { rangoPendienteValues } from 'src/app/models/rangoPendiente';
import { rangoPrecipitacionValues } from 'src/app/models/rangoPrecipitacion';
import { rangoTemperaturaValues } from 'src/app/models/rangoTemperatura';
import { UbicacionService } from '../services/ubicacion/ubicacion.service';

@Component({
  selector: 'app-ubicacion',
  templateUrl: './ubicacion.component.html',
  styleUrls: ['./ubicacion.component.scss']
})
export class UbicacionComponent implements OnInit {

  //Form controls

  fgroupUbicacion = new FormGroup({

    departamentoForm: new FormControl('', Validators.required),
    municipioForm: new FormControl('', Validators.required),
    alturaForm: new FormControl('', Validators.required),
    precipitacionForm: new FormControl('', Validators.required),
    temperaturaForm: new FormControl('', Validators.required),
    pendienteForm: new FormControl('', Validators.required)
  });
 
  //Flujo
  flujoActual = sessionStorage.getItem('flujo');

  //Validación
  esValido = false;

  // Helpers
  completeDepartment = false;

  //Ranges
  departamentos = Array.from(new Set(locations.map((key) => key.departamento))).sort();
  municipios: string[] = [];
  rangoAlturaValues = rangoAlturaValues;
  rangoPrecipitacionValues = rangoPrecipitacionValues;
  rangoTemperaturaValues = rangoTemperaturaValues;
  rangoPendienteValues = rangoPendienteValues;

   // Observables to filter
   filterDepartamentos!: Observable<string[]>;
   filterMunicipios!: Observable<string[]>;

  //Ubicacion
  ubicacion = new Ubicacion();
   
  //Método constructor de la clase
  constructor(private servicioUbicacion: UbicacionService) {
    
   }

  ngOnInit(): void {

    console.log("El flujo actual es: " + this.flujoActual);

    this.limpiarFormulario();

    if (sessionStorage.getItem('ubicacion')) {
      const ubicacion = JSON.parse(sessionStorage.getItem('ubicacion') ?? '') as Ubicacion;
      this.obtenerForm('departamentoForm')?.setValue(ubicacion.Departamento);
      this.obtenerForm('municipioForm')?.setValue(ubicacion.Municipio);
      this.obtenerForm('alturaForm')?.setValue(ubicacion.RangoAlturaNivelMar);
      this.obtenerForm('precipitacionForm')?.setValue(ubicacion.RangoPrecipitacion);
      this.obtenerForm('temperaturaForm')?.setValue(ubicacion.RangoTemperatura);
      this.obtenerForm('pendienteForm')?.setValue(ubicacion.RangoPendiente);
    }

    const departamentoForm = this.obtenerForm('departamentoForm');
    const municipioForm = this.obtenerForm('municipioForm');
    
    if (departamentoForm) {
      this.filterDepartamentos = departamentoForm.valueChanges.pipe(
        startWith(''),
        map((value) => this._filter(value)),
      );
    }
    
    if (municipioForm) {
      this.filterMunicipios = municipioForm.valueChanges.pipe(
        startWith(''),
        map((value) => this.municipios)
      );
    } 
  }

  //Carga los municipios en el control
  loadMunicipios(value: string){
    const getMunicipioByName = locations.reduce(
      (filtered: string[], option) => {
        if (option.departamento.toLowerCase() === value.toLowerCase()) filtered.push(option.municipio);
        return filtered;
      }, 
      []
    );
    this.municipios = getMunicipioByName;
    const municipioForm = this.obtenerForm('municipioForm');
    if (municipioForm) {
      municipioForm.setValue(''); 
    }
  }
  
  //Carga los municipios filtrados por departamento
  private _filter(value: any): string[] {
   if (value !== '') {
      this.completeDepartment = true;
      this.loadMunicipios(value);
      this.ubicacion.Departamento = value;
    }
    return this.departamentos;
  }


  //Define el municipio y envía al servicio
  _filterMunicipio(value: any): string[] {
   if(value !== ''){
    this.ubicacion.Municipio = value;
   }
   return this.municipios;
  } 
  
  //Envía la ubicación por medio del servicio mensaje
  enviarUbicacion():void{
    this.ubicacion.Municipio = this.obtenerForm('municipioForm')?.value;
    this.ubicacion.RangoAlturaNivelMar = this.obtenerForm('alturaForm')?.value;
    this.ubicacion.RangoPrecipitacion = this.obtenerForm('precipitacionForm')?.value;
    this.ubicacion.RangoTemperatura = this.obtenerForm('temperaturaForm')?.value;
    this.ubicacion.RangoPendiente = this.obtenerForm('pendienteForm')?.value;

    sessionStorage.setItem('ubicacion', JSON.stringify(this.ubicacion));

    this.servicioUbicacion.enviarUbicacion(this.ubicacion);
  }

  //Event handlers

  obtenerForm(nombreForm: string){
    return this.fgroupUbicacion.get(nombreForm);
  }

  guardarUbicacion(){
    this.enviarUbicacion();
  }

  limpiarFormulario(){
    this.ubicacion = new Ubicacion();

    this.obtenerForm('departamentoForm')?.setValue('');
    this.obtenerForm('municipioForm')?.setValue('');
    this.obtenerForm('alturaForm')?.setValue('');
    this.obtenerForm('precipitacionForm')?.setValue('');
    this.obtenerForm('temperaturaForm')?.setValue('');
    this.obtenerForm('pendienteForm')?.setValue('');
  }

}
