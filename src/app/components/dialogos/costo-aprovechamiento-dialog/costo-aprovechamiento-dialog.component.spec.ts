import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostoAprovechamientoDialogComponent } from './costo-aprovechamiento-dialog.component';

describe('CostoAprovechamientoDialogComponent', () => {
  let component: CostoAprovechamientoDialogComponent;
  let fixture: ComponentFixture<CostoAprovechamientoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostoAprovechamientoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostoAprovechamientoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
