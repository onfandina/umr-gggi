import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-costo-aprovechamiento-dialog',
  templateUrl: './costo-aprovechamiento-dialog.component.html',
  styleUrls: ['./costo-aprovechamiento-dialog.component.scss']
})
export class CostoAprovechamientoDialogComponent implements OnInit {

  totalCostosAprovechamiento = 0;
  costoAprovechamientoForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CostoAprovechamientoDialogComponent>
    ) {
    this.costoAprovechamientoForm = this.fb.group({
      costos: this.fb.array([]),
      fcontrolRendimiento: new FormControl(0)
    });
   }

  ngOnInit(): void {
  }    

//Obtiene los costos
  get costos():FormArray{
    return <FormArray> this.costoAprovechamientoForm.get('costos');
  }

  //Abre el cuadro de diálogo
  openDialog(): void {
    
  }

  //Cierra el cuadro de diálogo
  closeDialog(): void {
    this.dialogRef.close(this.totalCostosAprovechamiento);
  }

  //Crear el form group correspondiente al costo agregado
  crearCosto(concepto = '', unidades = '', cantidad = 0, costoHectarea = 0): FormGroup{
    return this.fb.group({
      concepto:[concepto],
      unidades: unidades,
      cantidad: cantidad,
      costoHectarea: costoHectarea
    });
  } 

  //Añade un ìtem a la tabla
  onAddItem(){
    this.costos.push(this.crearCosto());
  }

  //Quita un ítem de la tabla
  onRemoveItem(rowIndex: number){
    this.costos.removeAt(rowIndex);
  }

  //Obtiene el total de costos de mantenimiento
  obtenerTotalCostos(){
    this.totalCostosAprovechamiento = 0;
    this.costos.controls.forEach(c => {
      this.totalCostosAprovechamiento += c.value['cantidad'] * c.value['costoHectarea'];
    });

    const rendimiento = this.costoAprovechamientoForm.get('fcontrolRendimiento')?.value ?? 0;
    if(rendimiento > 0)
    {
      this.totalCostosAprovechamiento = this.totalCostosAprovechamiento / rendimiento;
    }
    else {
      this.totalCostosAprovechamiento = 0;
    }
  }  

}
