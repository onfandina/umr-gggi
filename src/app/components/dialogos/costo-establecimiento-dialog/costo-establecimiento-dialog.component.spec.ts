import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostoEstablecimientoDialogComponent } from './costo-establecimiento-dialog.component';

describe('CostoEstablecimientoDialogComponent', () => {
  let component: CostoEstablecimientoDialogComponent;
  let fixture: ComponentFixture<CostoEstablecimientoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostoEstablecimientoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostoEstablecimientoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
