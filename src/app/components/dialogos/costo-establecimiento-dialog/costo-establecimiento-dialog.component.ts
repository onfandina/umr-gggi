import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-costo-establecimiento-dialog',
  templateUrl: './costo-establecimiento-dialog.component.html',
  styleUrls: ['./costo-establecimiento-dialog.component.scss']
})
export class CostoEstablecimientoDialogComponent implements OnInit {

  //Form
  costoEstablecimientoForm: FormGroup;
  totalCostosEstablecimiento = 0;

  constructor(
    public dialogRef: MatDialogRef<CostoEstablecimientoDialogComponent>,
    private fb: FormBuilder
    ) {
      this.costoEstablecimientoForm = this.fb.group({
        costos: this.fb.array([
          this.crearCosto('Mano de obra', 'Limpia ha'),
          this.crearCosto('Mano de obra', 'Ahoyado'),
          this.crearCosto('Mano de obra', 'Trazado'),
          this.crearCosto('Mano de obra', 'Plateo'),
          this.crearCosto('Mano de obra', 'Plantación'),
          this.crearCosto('Mano de obra', 'Fertilización/Rustificación'),
        ])
      });
    }

  ngOnInit(): void {
  }

  //Obtiene los costos
  get costos():FormArray{
    return <FormArray> this.costoEstablecimientoForm.get('costos');
  }

  //Abre el cuadro de diálogo
  openDialog(): void {
    
  }

  //Cierra el cuadro de diálogo
  closeDialog(): void {
    this.dialogRef.close(this.totalCostosEstablecimiento);
  }

  //Crear el form group correspondiente al costo agregado
  crearCosto(concepto = '', actividad = '', valor = 0, rendimiento = 0, costoHectarea = 0): FormGroup{
    return this.fb.group({
      concepto:[concepto],
      actividad:[actividad], 
      valor: valor,
      rendimiento: rendimiento,
      costoHectarea: costoHectarea
    });
  } 

  //Añade un ìtem a la tabla
  onAddItem(){
    this.costos.push(this.crearCosto());
  }

  //Quita un ítem de la tabla
  onRemoveItem(rowIndex: number){
    this.costos.removeAt(rowIndex);
  }

  //Obtiene el total de costos de mantenimiento
  obtenerTotalCostos(){
    this.totalCostosEstablecimiento = 0;
    this.costos.controls.forEach(c => {
      this.totalCostosEstablecimiento += c.value['costoHectarea']/c.value['rendimiento'];
    });
  }

  
}
