import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostoMantenimientoDialogComponent } from './costo-mantenimiento-dialog.component';

describe('CostoMantenimientooDialogComponent', () => {
  let component: CostoMantenimientoDialogComponent;
  let fixture: ComponentFixture<CostoMantenimientoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostoMantenimientoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostoMantenimientoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
