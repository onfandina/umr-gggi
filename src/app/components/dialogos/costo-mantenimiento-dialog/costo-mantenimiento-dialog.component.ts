import { Component, NgIterable, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-costo-mantenimiento-dialog',
  templateUrl: './costo-mantenimiento-dialog.component.html',
  styleUrls: ['./costo-mantenimiento-dialog.component.scss']
})
export class CostoMantenimientoDialogComponent implements OnInit {

  //Total de costos de mantenimiento
  totalCostosMantenimiento = 0;
  //Form principal
  costoMantenimientoForm: FormGroup;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<CostoMantenimientoDialogComponent>) {
    this.costoMantenimientoForm = this.fb.group({
      costos: this.fb.array([this.crearCosto('Mano de obra', 'Limpías'), this.crearCosto('Mano de obra', 'Fertilización')])
     });
   }

  ngOnInit(): void {
  
  }

   //Abre el cuadro de diálogo
   openDialog(): void {

  }

  //Cierra el cuadro de diálogo
  closeDialog(): void {
    this.dialogRef.close(this.totalCostosMantenimiento);
  }


  //Manejadores de evento

  //Añade un ítem a la tabla
  onAddItem(){
    this.costos.push(this.crearCosto());
  }

  //Quita un ítem de la tabla
  onRemoveItem(rowIndex: number){
    this.costos.removeAt(rowIndex);
  }

  //Crea el form group correspondiente al costo agregado
  crearCosto(concepto = '', actividad = '', valorUnitario = 0, cantidadesHectareas = 0, costoHectareas = 0): FormGroup{
    return this.fb.group({
      concepto:[concepto],
      actividad:[actividad], 
      valorUnitario: valorUnitario,
      cantidadesHectarea: cantidadesHectareas,
      costoHectarea: costoHectareas
    });
  } 

  //Obtiene los costos
  get costos():FormArray{
    return <FormArray> this.costoMantenimientoForm.get('costos');
  }

  //Obtiene el total de costos de mantenimiento
  obtenerTotalCostosMantenimiento(){

    this.costos.controls.forEach(c => {
      c.get('costoHectarea')?.setValue(c.value['valorUnitario'] * c.value['cantidadesHectarea']);
    });
    this.totalCostosMantenimiento = this.costos.controls.map(c => c.value['costoHectarea']).reduce((a,b)=> a + b, 0);

  }
}
