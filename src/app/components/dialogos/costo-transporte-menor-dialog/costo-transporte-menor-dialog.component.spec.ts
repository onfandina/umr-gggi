import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostoTransporteMenorDialogComponent } from './costo-transporte-menor-dialog.component';

describe('CostoTransporteMenorDialogComponent', () => {
  let component: CostoTransporteMenorDialogComponent;
  let fixture: ComponentFixture<CostoTransporteMenorDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostoTransporteMenorDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostoTransporteMenorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
