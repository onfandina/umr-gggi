import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { DetalleTransporteMenor } from 'src/app/core/entities/Costos/detalleTransporteMenor';

@Component({
  selector: 'app-costo-transporte-menor-dialog',
  templateUrl: './costo-transporte-menor-dialog.component.html',
  styleUrls: ['./costo-transporte-menor-dialog.component.scss']
})
export class CostoTransporteMenorDialogComponent implements OnInit {

  costoTransporteMenorForm: FormGroup;
  totalCostosMenor: number = 0;
  //detallesCosto: DetalleTransporteMenor[] = [];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CostoTransporteMenorDialogComponent>
    ) {
    this.costoTransporteMenorForm = this.fb.group({
      costos: this.fb.array([]),
      fcontrolRendimiento: new FormControl(0)
     });
   }

  //Manejador del evento Init
  ngOnInit(): void {
  
  }

  //Obtiene los costos
  get costos():FormArray{
    return <FormArray> this.costoTransporteMenorForm.get('costos');
  }
  
  //Abre el cuadro de diálogo
  openDialog(): void {
    
  }

  //Cierra el cuadro de diálogo
  closeDialog(): void {
    this.dialogRef.close(this.costoTransporteMenorForm);
  }

  //Crear el form group correspondiente al costo agregado
  crearCosto(concepto = '', unidades = '', cantidades = 0, valorDiario = 0): FormGroup{
    return this.fb.group({
      concepto:[concepto],
      unidades:[unidades], 
      cantidades: cantidades,
      valorDiario: valorDiario,
    });
  } 

  //Añade un ìtem a la tabla
  onAddItem(){
    this.costos.push(this.crearCosto());
  }

  //Quita un ítem de la tabla
  onRemoveItem(rowIndex: number){
    this.costos.removeAt(rowIndex);
  }

  //Obtiene el total de costos de mantenimiento
  obtenerTotalCostos(){
    this.totalCostosMenor = 0;
    this.costos.controls.forEach(c => {
      this.totalCostosMenor += c.value['cantidades'] * c.value['valorDiario'];
    });
    const rendimiento = this.costoTransporteMenorForm.get('fcontrolRendimiento')?.value ?? 0;
    if(rendimiento > 0)
    {
      this.totalCostosMenor = this.totalCostosMenor / rendimiento;
    }
    else {
      this.totalCostosMenor = 0;
    }
    
  }

}
