import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-especificacion-costo-dialog',
  templateUrl: './especificacion-costo-dialog.component.html',
  styleUrls: ['./especificacion-costo-dialog.component.scss']
})

export class EspecificacionCostoDialogComponent implements OnInit {
 
  //Total de costos de transformación primaria
  tituloDialogo = '';
  unidadesDialogo = '';
  totalCostosPrimaria = 0;
  totalRendimiento = 0;
  costoPrimariaForm!: FormGroup;
  conceptosPreCargados: Array<{concepto: string, valor: string}>; 
  fgCostos: Array<FormGroup> = [];
  constructor(
    public dialogRef: MatDialogRef<EspecificacionCostoDialogComponent>, 
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) data: any) {
      this.tituloDialogo = data.titulo;
      this.unidadesDialogo = data.unidades;
      this.conceptosPreCargados = data.conceptos;
   }

  //Manejador del evento Init
  ngOnInit(): void {
   
    this.conceptosPreCargados.forEach(concepto => {
      const costo = this.crearCosto(concepto.concepto, concepto.valor);
      this.fgCostos.push(costo);
    });

    this.costoPrimariaForm = this.fb.group({
      costos: this.fb.array(this.fgCostos),
      fcontrolRendimiento: new FormControl(0)
    });

  }

  //Obtiene los costos
  get costos():FormArray{
    return <FormArray> this.costoPrimariaForm.get('costos');
  }

  //Abre el cuadro de diálogo
  openDialog(): void {
    
  }

  //Cierra el cuadro de diálogo
  closeDialog(): void {
    this.dialogRef.close(this.totalCostosPrimaria);
  }

  //Crear el form group correspondiente al costo agregado
  crearCosto(concepto = '', unidades = '', cantidades = 0, valorDiario = 0): FormGroup{
    return this.fb.group({
      concepto:[concepto],
      unidades:[unidades], 
      cantidades: cantidades,
      valorDiario: valorDiario
    });
  } 

  //Añade un ìtem a la tabla
  onAddItem(){
    this.costos.push(this.crearCosto());
  }

  //Quita un ítem de la tabla
  onRemoveItem(rowIndex: number){
    this.costos.removeAt(rowIndex);
  }

  //Obtiene el total de costos de mantenimiento
  obtenerTotalCostos(){
    this.totalCostosPrimaria = 0;
    this.costos.controls.forEach(c => {
      this.totalCostosPrimaria += (c.value['cantidades'] * c.value['valorDiario']);
    });
    const rendimiento = this.costoPrimariaForm.get('fcontrolRendimiento')?.value ?? 0;
    if(rendimiento > 0)
    {
      this.totalCostosPrimaria = this.totalCostosPrimaria / rendimiento;
    }
    else {
      this.totalCostosPrimaria = 0;
    }
    
  }

}
