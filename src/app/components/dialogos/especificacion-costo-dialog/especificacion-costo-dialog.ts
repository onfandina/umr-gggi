import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EspecificacionCostoDialogComponent } from './especificacion-costo-dialog.component';

describe('EspecificacionCostoDialogComponent', () => {
  let component: EspecificacionCostoDialogComponent;
  let fixture: ComponentFixture<EspecificacionCostoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EspecificacionCostoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EspecificacionCostoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
