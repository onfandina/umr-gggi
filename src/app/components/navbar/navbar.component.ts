import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  openNavBar = false;
  constructor() {}

  ngOnInit(): void { }
  
  updateNavBar(event: boolean) {
    this.openNavBar = event;
  }
}
