import {
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  openNavBar = false;

  @Input() navBar = false;
  @Output() getNavBar = new EventEmitter<boolean>();

  constructor(private eRef: ElementRef, private router: Router) {}

  ngOnInit(): void {}

  @HostListener('document:click', ['$event'])
  clickout(event: Event) {
    this.openNavBar = this.eRef.nativeElement.contains(event.target)
      ? true
      : false;
    this.getNavBar.emit(this.openNavBar);
  }

  @HostListener('wheel', ['$event'])
  handleWheelEvent(event: Event) {
    event.preventDefault();
  }

  openNavbar() {
    this.openNavBar = !this.openNavBar;
    this.getNavBar.emit(this.openNavBar);
  }

  getSelectedItem(item: string) {
    return this.router.url === item;
  }
  
  disableNavBar() {
    this.openNavBar = false;
    this.getNavBar.emit(false);
  }

  navigateTo(url: string) {
    this.disableNavBar();
    this.router.navigateByUrl(url);
  }
}
