import { Especie } from "../entities/Especie";

export class BLCortaFinal {
    ObtenerVolumenEstimado(pEspecie: Especie){
        let volumen = 0;
        volumen = pEspecie.Perdida * pEspecie.RendimientoAnualPromedio * pEspecie.Turno;
        
        pEspecie.CortaFinal.VolumenEstimado = volumen;
    }
}