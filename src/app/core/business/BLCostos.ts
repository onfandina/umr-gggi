import { Costo } from "../entities/Costos/Costo";
import { CostoTierra } from "../entities/Costos/CostoTierra";
import { InsumosPrePlan } from "../entities/Costos/InsumosPrePlan";
import { PreparacionYPlantacion } from "../entities/Costos/PreparacionYPlantacion";
import { Transformacion } from "../entities/Costos/Transformacion";
import { Tecnologia } from "../entities/Enumeradores/Tecnologia";
import { Establecimiento } from "../entities/Establecimiento";
import { Proyecto } from "../entities/Proyecto";

export class BLCostos {
    ObtenerCostoTierra(pCostoTierra: CostoTierra, pProyecto: Proyecto){
        switch (pCostoTierra.Propia) {
            case false:
                pCostoTierra.Valor = pCostoTierra.Arrendamiento;
                break;
            case true:
                pCostoTierra.Valor = 300 * pProyecto.AreaTotal;
                break;
        }
    }

    ObtenerCostoTotalEstablecimiento(pPrePlan: PreparacionYPlantacion, pInsumosPrePlan: InsumosPrePlan, 
        pCostoJornal: Costo, pEstablecimiento: Establecimiento): number {
        if(pPrePlan.Tecnologia === Tecnologia.Manual){
            return ((pPrePlan.Valor * pCostoJornal.Valor) + pInsumosPrePlan.Valor) * pEstablecimiento.Area;
        }
        else {
            return (pPrePlan.Valor + pInsumosPrePlan.Valor) * pEstablecimiento.Area;   
        }
    }

    ObtenerCostoTransformacion(pCostoTransformacion: Transformacion) {
        //De donde obtengo la producción?
    }

    ObtenerCostosTransformaciones(pCostosTransformaciones: Transformacion[]): number
    {
        let sumTransformacion = 0;
        pCostosTransformaciones.forEach((transformacion) => {
            sumTransformacion += transformacion.Valor;
        });
        return sumTransformacion;
    }

    ObtenerCostosOperacionales(pCostoCarbono: number, pCostoTransporte: number, pCostoTransformacion: number,
        pCostoAprovechamiento: number, pCostoMantenimiento: number, pValorTerrenoTotal: number, pCostoEstablecimiento: number,
        pPagoFinanciacion: number, pAsistencia: number): number{
            return pCostoCarbono +
            pCostoTransporte +
            pCostoTransformacion +
            pCostoAprovechamiento +
            pCostoMantenimiento +
            pValorTerrenoTotal +
            pCostoEstablecimiento +
            pPagoFinanciacion +
            pAsistencia;
    }

    ObtenerCostosAdministracion(pCostosOperacionales: number, pPorcentaje: number) : number{
        return pCostosOperacionales * pPorcentaje;
    }

    ObtenerCostosTotales(pCostosAdministracion: number, pCostosOperacionales: number){
        return pCostosAdministracion + pCostosOperacionales;
    };
}