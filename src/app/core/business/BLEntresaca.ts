import { Entresaca } from "../entities/Entresaca";
import { Especie } from "../entities/Especie";
import { Establecimiento } from "../entities/Establecimiento";

export class BLEntresaca {

    //Revisar cómo computar el rendimiento madera
    ObtenerVolumenesEstimados(pEspecie: Especie) {
        let volumen = 0;
        pEspecie.Entresacas.forEach((entresaca) => {
            volumen = pEspecie.Perdida * 
            pEspecie.RendimientoAnualPromedio * 
            entresaca.PorcentajeAprovechamiento; 
            
            entresaca.VolumenEstimado = volumen;
        });        
    }
}