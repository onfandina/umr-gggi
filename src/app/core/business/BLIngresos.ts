import { Proyecto } from "../entities/Proyecto";
import { Entresaca } from "../entities/Entresaca";
import { Especie } from "../entities/Especie";
import { Establecimiento } from "../entities/Establecimiento";
import { Produccion } from "../entities/Produccion";
import { Producto } from "../entities/Producto";
import { CertificadoIncentivoForestal } from "../entities/Ingresos/CertificadoIncentivoForestal";
import { Ingreso } from "../entities/Ingresos/Ingreso";

export class BLIngresos {

    //Revisar, no se cómo definir ahí la corta, 
    //por ahora no la voy a poner porque creo que corresponde a la entresaca
    //El área es total o del establecimiento?
    ObtenerProduccion(pProyecto: Proyecto, pEntresaca: Entresaca, pEspecie: Especie){
        return pProyecto.AreaTotal * pEntresaca.VolumenEstimado * pEspecie.RendimientoAnualPromedio;
    }

    ObtenerTotalProduccion(){

    }


    //Cómo calculo el precio en esta fórmula? ya que tenemos el precio diferenciado según tipo de ciudad
    ObtenerIngresosProduccion(pProduccion: Produccion, pProducto: Producto){
        
    }

    ObtenerIngresoTotalProduccion(){

    }

    //Cómo calculo que esté vigente el CIF si no tengo la fecha de inicio del proyecto?
    //Cómo se relaciona con el área?
    ObtenerIngresosCif(pIngresosCif: CertificadoIncentivoForestal[]): number{
        let ingresos = 0;
        pIngresosCif.forEach((ingreso) => {
           ingresos += ingreso.Valor;
        });
        return ingresos;
    }

    ObtenerIngresoTotal(pIngresoProduccion: number, pIngresosCif: number, pIngresoCarbono: number, pFinanciacion: number){
        return pIngresoProduccion + pIngresosCif + pIngresoCarbono + pFinanciacion;
    }
}