import { Especie } from "../entities/Especie";
import { Impuesto } from "../entities/Impuesto";
import { Proyecto } from "../entities/Proyecto";

export class BLProyecto {

    constructor(){
    }
        
    ObtenerAreaTotal(pProyecto: Proyecto) {
        //Será mejor manejar establecimiento por especie? o especie por establecimiento?
        //Cómo definir el periodo?  

        let sumaAreas = 0;
        
        pProyecto.Establecimientos.forEach((establecimiento) => {
            sumaAreas += establecimiento.Area;
        });

        pProyecto.AreaTotal = sumaAreas;
    }

    ObtenerArbolesPorHectarea(pProyecto: Proyecto): void{
        let arbolesXHectarea = 0;

        if (pProyecto.DistanciaDeSiembra > 0) {
            switch (pProyecto.SistemaDeSiembra) {
                case 'Triangulo':
                    arbolesXHectarea = (10000 * 2) / (Math.pow(pProyecto.DistanciaDeSiembra, 2) + Math.sqrt(3));
                    break;
                case 'Cuadrado':                              
                    arbolesXHectarea = 10000/(Math.pow(pProyecto.DistanciaDeSiembra, 2));
                    break;
            }
        }

        pProyecto.ArbolesXHectarea = arbolesXHectarea;
    }

    DefinirProyecto(pProyecto: Proyecto){
        this.ObtenerAreaTotal(pProyecto);
        this.ObtenerArbolesPorHectarea(pProyecto);
        
    }

    ObtenerUtilidadBruta(pIngresoTotal: number, pCostoTotal: number){
        return pIngresoTotal - pCostoTotal;
    }

    ObtenerUtilidadDespuesImpuestos(pUtilidadBruta: number, pImpuesto: Impuesto){
        return (1-pImpuesto.Porcentaje) * pUtilidadBruta;
    }

    ObtenerMargenBruto(){

    }

    ObtenerMargenNeto(){
        
    }
}