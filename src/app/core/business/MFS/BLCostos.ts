import {CostosComputados} from "../../entities/MFS/Costos/CostosComputados";

export class BLCostos {

  public static calcularTotal(costos: CostosComputados) {
    costos.Total = costos.Costos.reduce((total, costo) => total + costo.Valor, 0);
  }
}
