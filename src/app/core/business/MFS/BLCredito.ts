import {MfsModuloFinanciero} from "../../entities/MFS/MfsModuloFinanciero";
import {SimulacionCredito} from "../../entities/MFS/Costos/Credito/SimulacionCredito";
import {Cuota} from "../../entities/MFS/Costos/Credito/Cuota";
import {PeriodosInteres} from "../../entities/MFS/Enums/PeriodosInteres";

export class BLCredito {
  static Periodos = {
    [PeriodosInteres.MENSUAL]: 30,
    [PeriodosInteres.BIMESTRAL]: 60,
    [PeriodosInteres.TRIMESTRAL]: 90,
    [PeriodosInteres.CUATRIMESTRAL]: 120,
    [PeriodosInteres.SEMESTRAL]: 180,
  };
  private _MfsModuloFinanciero: MfsModuloFinanciero;
  private _SensibilidadInteres: number;

  constructor(mfsModuloFinanciero: MfsModuloFinanciero, sensibilidadInteres: number = 1) {
    this._MfsModuloFinanciero = mfsModuloFinanciero;
    this._SensibilidadInteres = sensibilidadInteres;
    this._MfsModuloFinanciero.Tasa = (1 + mfsModuloFinanciero.Interes) ** (360 / (BLCredito.Periodos[mfsModuloFinanciero.PeriodoInteres])) - 1;
  }

  public obtenerSimulacion(): SimulacionCredito {
    const {Capital, Tasa, Plazo} = this._MfsModuloFinanciero;
    const simulacion = new SimulacionCredito();
    let deuda = Capital;
    for (let i = 0; i < Plazo; i++) {
      const intereses = deuda * (Tasa ?? 0) * this._SensibilidadInteres / 12;
      const cap = Capital / Plazo;
      simulacion.Cuotas.push(
        new Cuota(intereses, cap, intereses + cap)
      );
      deuda -= cap;
    }

    simulacion.CuotasAnuales = new Array(Math.floor(Plazo / 12)).fill(0);
    simulacion.Cuotas.forEach((cuota, i) => {
      simulacion.CuotasAnuales[Math.floor(i / 12)] += cuota.Total;
    });
    simulacion.Total = simulacion.CuotasAnuales.reduce((a, b) => a + b, 0);
    return simulacion;
  }

  public static calcularInteresAnual(mfsModuloFinanciero: MfsModuloFinanciero) {
    mfsModuloFinanciero.Tasa = (1 + mfsModuloFinanciero.Interes) ** (360 / (BLCredito.Periodos[mfsModuloFinanciero.PeriodoInteres])) - 1;
  }
}
