import {Mfs} from "../../entities/MFS/Mfs";
import {FlujoCajaUca} from "../../entities/MFS/Resultados/FlujoCajaUca";
import {CostoSistema} from "../../entities/MFS/Resultados/CostoSistema";
import {ValorMaterial} from "../../entities/MFS/ValorMaterial";
import {ResultadoTransporteMayor} from "../../entities/MFS/Resultados/ResultadoTransporteMayor";
import {CostoTransportesMayores} from "../../entities/MFS/Costos/CostoTransportesMayores";
import {TiposTrasportesMayores} from "../../entities/MFS/Enums/TiposTrasportesMayores";
import {CostoTransporteMayor} from "../../entities/MFS/Costos/CostoTransporteMayor";
import {BLTransporteMayor} from "./BLTransporteMayor";
import {Costo} from "../../entities/Costos/Costo";
import {TiposCosto} from "../../entities/MFS/Enums/TiposCosto";
import {TipoPersona} from "../../entities/Enumeradores/TipoPersona";
import {BLImpuestosPersonaNatural} from "./BLImpuestosPersonaNatural";
import {TipoEntidad} from "../../entities/Enumeradores/TipoEntidad";
import {InversionInicial} from "../../entities/MFS/Resultados/InversionInicial";
import {Aprovechamiento} from "../../entities/MFS/Resultados/Aprovechamiento";
import {CostoAprovechamiento} from "../../entities/MFS/Resultados/CostoAprovechamiento";
import {FlujoCaja} from "../../entities/MFS/Resultados/FlujoCaja";
import {BLSensibilidad} from "./BLSensibilidad";
import {BLCredito} from "./BLCredito";
import {irr, npv} from "financial";
import {BLMfs} from "./BLMfs";

export class BLFlujoCaja {
  public static obtenerInversionInicial(mfsData: Mfs): InversionInicial {
    const inversionInicial = new InversionInicial();
    inversionInicial.Asistencia.Pmf = mfsData.Costos.AsistenciaTecanica.PlanManejoForestal[0] || 0;
    inversionInicial.Asistencia.Campamento = mfsData.Costos.AsistenciaTecanica.ConstruccionCampamento[0] || 0;
    inversionInicial.Asistencia.Total = inversionInicial.Asistencia.Pmf + inversionInicial.Asistencia.Campamento;
    inversionInicial.Concertacion = mfsData.Costos.AsistenciaTecanica.CostosPreoperativos.Total;
    inversionInicial.Maquinaria = mfsData.Costos.Maquinaria.Total;
    inversionInicial.Aprovechamiento = this.obtenerAprovechamiento(mfsData, 1);
    inversionInicial.InversionTotal = (inversionInicial.Maquinaria + inversionInicial.Concertacion);
    inversionInicial.Total = (inversionInicial.Aprovechamiento.Total + inversionInicial.Asistencia.Total + inversionInicial.InversionTotal) * mfsData.Sensibilidad.Costos;
    return inversionInicial;
  };

  public static obtenerAprovechamiento(mfsData: Mfs, uca: number): CostoAprovechamiento {
    // const aprovechamientos: Array<Record<string, Aprovechamiento>> = new Array(mfsData.InformacionGeneral.Uca).fill(0).map(() => ({}));
    const aprovechamiento: Record<string, Aprovechamiento> = {};
    mfsData.InformacionEspecies.Especies.forEach(especie => {
      if (!aprovechamiento[especie.CalidadMadera.Nombre]) {
        aprovechamiento[especie.CalidadMadera.Nombre] = new Aprovechamiento();
      }
      aprovechamiento[especie.CalidadMadera.Nombre].Material = especie.CalidadMadera;
      especie.Productos.forEach((producto) => {
        aprovechamiento[especie.CalidadMadera.Nombre].Volumen += producto.Volumen * mfsData.InformacionGeneral.AreaUca[uca];
      });
    });
    mfsData.Costos.Aprovechamiento.Costos.forEach(costo => {
      if (aprovechamiento[costo.Material.Nombre]) {
        aprovechamiento[costo.Material.Nombre].Costo = aprovechamiento[costo.Material.Nombre].Volumen * costo.Valor;
      }
    });
    const costoAprovechamiento = new CostoAprovechamiento();
    costoAprovechamiento.Aprovechamientos = Object.values(aprovechamiento);
    costoAprovechamiento.Total = costoAprovechamiento.Aprovechamientos.reduce((total, aprovechamiento) => aprovechamiento.Costo + total, 0);
    return costoAprovechamiento;
  };

  public static obtenerFlujoCajaUca(mfsData: Mfs, uca: number): FlujoCajaUca {
    const flujo = new FlujoCajaUca();

    // Ingresos
    flujo.IngresoVentas.IngresoUbicaciones = mfsData.IngresosUbicaciones[uca];
    flujo.IngresoVentas.Total = Object.values(mfsData.IngresosUbicaciones[uca]).reduce((val, total) => val + total, 0);
    if (uca === 0) {
      flujo.OtrosIngresos.Financiacion = mfsData.ModuloFinanciero.Capital;
    }
    flujo.OtrosIngresos.Total = flujo.OtrosIngresos.Financiacion + flujo.OtrosIngresos.Carbono + flujo.OtrosIngresos.Incentivos;
    flujo.TotalIngresos = (flujo.IngresoVentas.Total + flujo.OtrosIngresos.Total) * mfsData.Sensibilidad.Ingresos;
    // Aprovechamiento
    flujo.Aprovechamiento = this.obtenerAprovechamiento(mfsData, uca);
    if (uca === 0) {
      flujo.Aprovechamiento.Total = 0;
    }

    // Transformacion
    mfsData.Costos.Transformacion.forEach((pocentaje) => {
      const costo = new CostoSistema();
      costo.Porcentaje = pocentaje;
      costo.Valor = pocentaje.Porcentaje * pocentaje.CostoAprovechamiento * (mfsData.ProduccionMateriales[uca].find((produccion) => produccion.Material === pocentaje.Material) as ValorMaterial)?.Valor || 0;
      flujo.Transformacion.Costos.push(costo);
    });
    flujo.Transformacion.Total = flujo.Transformacion.Costos.reduce((total, costo) => total + costo.Valor, 0);

    // Transporte menor
    mfsData.Costos.TransporteMenor.forEach((pocentaje) => {
      const costo = new CostoSistema();
      costo.Porcentaje = pocentaje;
      costo.Valor = pocentaje.Porcentaje * pocentaje.CostoAprovechamiento * (mfsData.ProduccionMateriales[uca].find((produccion) => produccion.Material === pocentaje.Material) as ValorMaterial)?.Valor || 0;
      flujo.TransporteMenor.Costos.push(costo);
    });
    flujo.TransporteMenor.Total = flujo.TransporteMenor.Costos.reduce((total, costo) => total + costo.Valor, 0);

    // Transporte Mayor
    flujo.TransporteMayor = new ResultadoTransporteMayor();
    flujo.TransporteMayor.TransporteMayor = mfsData.Costos.TransporteMayor.map(transporte => {
      const trasporteMayor = new CostoTransportesMayores(transporte.Ciudad);
      trasporteMayor[TiposTrasportesMayores.CAMION_SENCILLO] = new CostoTransporteMayor(transporte[TiposTrasportesMayores.CAMION_SENCILLO]);
      trasporteMayor[TiposTrasportesMayores.DOBLE_TROQUE] = new CostoTransporteMayor(transporte[TiposTrasportesMayores.DOBLE_TROQUE]);
      trasporteMayor[TiposTrasportesMayores.TRACTO_MULA] = new CostoTransporteMayor(transporte[TiposTrasportesMayores.TRACTO_MULA]);
      BLTransporteMayor.obtenerCostosTransporteMayor(trasporteMayor[TiposTrasportesMayores.CAMION_SENCILLO], mfsData.VolumenUbicaciones[uca][transporte.Ciudad]);
      BLTransporteMayor.obtenerCostosTransporteMayor(trasporteMayor[TiposTrasportesMayores.DOBLE_TROQUE], mfsData.VolumenUbicaciones[uca][transporte.Ciudad]);
      BLTransporteMayor.obtenerCostosTransporteMayor(trasporteMayor[TiposTrasportesMayores.TRACTO_MULA], mfsData.VolumenUbicaciones[uca][transporte.Ciudad]);
      trasporteMayor.CostoTotal = trasporteMayor[TiposTrasportesMayores.CAMION_SENCILLO].CostoTotal + trasporteMayor[TiposTrasportesMayores.DOBLE_TROQUE].CostoTotal + trasporteMayor[TiposTrasportesMayores.TRACTO_MULA].CostoTotal;
      return trasporteMayor;
    });
    flujo.TransporteMayor.Total = flujo.TransporteMayor.TransporteMayor.reduce((total, transporte) => total + transporte.CostoTotal, 0);

    // Costos tierra
    flujo.CostoTierra = (mfsData.Costos.Tierra.terreno + mfsData.Costos.Tierra.derechosUso + mfsData.Costos.Tierra.oportunidad) * mfsData.InformacionGeneral.AreaUca[uca];

    //Costos Asistencia Tecnica
    flujo.Asistencia.AsistenciaAnual = mfsData.Costos.AsistenciaTecanica.AsistenciaAnual.Total;
    if (uca > 0) {
      flujo.Asistencia.Pmf = mfsData.Costos.AsistenciaTecanica.PlanManejoForestal[uca] || 0;
      flujo.Asistencia.Campamento = mfsData.Costos.AsistenciaTecanica.ConstruccionCampamento[uca] || 0;
    }
    flujo.Asistencia.Total = flujo.Asistencia.AsistenciaAnual + flujo.Asistencia.Pmf + flujo.Asistencia.Campamento;

    // Otros Costos
    // Caminos
    const adecuacionCaminos = new Costo();
    adecuacionCaminos.Nombre = 'Costo adecuación caminos';
    adecuacionCaminos.Valor = mfsData.Costos.Caminos.Total / mfsData.InformacionGeneral.Uca;
    // Caminos
    const tcaf = new Costo();
    tcaf.Nombre = 'Costos TCAF';
    tcaf.Valor = mfsData.InformacionEspecies.Especies.reduce((total, especie) => total + especie.TcafFlujo[uca], 0);
    // console.log( tcaf.Valor.toLocaleString('en-US', { style: 'currency', currency: 'USD' }))
    // Carbono
    const carbono = new Costo();
    carbono.Nombre = 'Carbono';
    carbono.Valor = mfsData.Costos.Otros.Carbono[uca] || 0;
    // Medidas compensacion
    const compensacion = new Costo();
    compensacion.Nombre = 'Medidas de compensación';
    compensacion.Valor = mfsData.Costos.Otros.MedidasCompensacion;
    // Financiación
    const financiacion = new Costo();
    financiacion.Nombre = 'Financiación';
    financiacion.Valor = mfsData.Credito.CuotasAnuales[uca] || 0;
    // Otros Gastos
    const otros = new Costo();
    otros.Nombre = 'Otros gastos';
    otros.Valor = mfsData.Costos.Otros.OtrosCostos[uca]?.reduce((total, costo) => total + costo.Valor, 0) || 0;

    flujo.OtrosCostos.Costos.push(adecuacionCaminos);
    flujo.OtrosCostos.Costos.push(tcaf);
    flujo.OtrosCostos.Costos.push(carbono);
    flujo.OtrosCostos.Costos.push(compensacion);
    flujo.OtrosCostos.Costos.push(financiacion);
    flujo.OtrosCostos.Costos.push(otros);
    flujo.OtrosCostos.Total = flujo.OtrosCostos.Costos.reduce((total, costo) => total + costo.Valor, 0);

    flujo.SubtotalCostos = flujo.Asistencia.Total + flujo.CostoTierra + flujo.OtrosCostos.Total + flujo.TransporteMayor.Total + flujo.TransporteMenor.Total + flujo.Transformacion.Total + flujo.Aprovechamiento.Total;

    flujo.CostoAdministracion = mfsData.Costos.Otros.Administracion.Valor;
    if (mfsData.Costos.Otros.Administracion.Tipo === TiposCosto.PORCENTUAL) {
      flujo.CostoAdministracion *= flujo.SubtotalCostos;
    }
    flujo.CostoIndustriaComercio = flujo.TotalIngresos * 0.01;
    flujo.TotalCostos = (flujo.SubtotalCostos + flujo.CostoIndustriaComercio + flujo.CostoAdministracion) * mfsData.Sensibilidad.Costos;

    flujo.UtilidadBruta = flujo.TotalIngresos - flujo.TotalCostos;
    if (flujo.UtilidadBruta > 0) {
      if (mfsData.InformacionGeneral.Persona.tipoDePersona === TipoPersona.Natural) {
        flujo.ImpuestoRenta = BLImpuestosPersonaNatural.calcularImpuestoRenta(flujo.UtilidadBruta);
      } else {
        if (mfsData.InformacionGeneral.Persona.tipoDeEntidad === TipoEntidad.ConAnimoLucro) {
          flujo.ImpuestoRenta = flujo.UtilidadBruta * mfsData.Costos.Otros.TasaRenta.Valor;
        } else {
          flujo.ImpuestoRenta = flujo.UtilidadBruta * 0.2;
        }
      }
    }
    flujo.UtilidadNeta = flujo.UtilidadBruta - flujo.ImpuestoRenta;
    return flujo;
  }

  public static obtenerFlujoCaja(mfsData: Mfs): FlujoCaja {
    const mfsFlujo = BLSensibilidad.aplicarSensibilidad(mfsData);
    const blCredito = new BLCredito(mfsData.ModuloFinanciero, mfsData.Sensibilidad.TasaInteres);
    mfsData.Credito = blCredito.obtenerSimulacion();
    const flujo = new FlujoCaja();
    flujo.InversionInicial = this.obtenerInversionInicial(mfsFlujo);
    for (let i = 0; i < mfsFlujo.InformacionGeneral.Uca; i++) {
      flujo.FlujoUcas.push(this.obtenerFlujoCajaUca(mfsFlujo, i));
    }
    const resultados = [];
    resultados.push((flujo?.InversionInicial.Total || 0) * -1);
    flujo?.FlujoUcas.forEach((flujoUca) => {
      resultados.push(flujoUca.UtilidadNeta);
    });
    flujo.TIR = irr(resultados);
    flujo.VPN = npv(flujo.DTF, resultados);
    flujo.IngresoPromedio = resultados.reduce((total, res) => total + res, 0) / ((resultados.length - 1) * 12);
    if (mfsData.InformacionGeneral.Familias > 0) {
      flujo.IngresoPromedioFamilia = flujo.IngresoPromedio / mfsData.InformacionGeneral.Familias;
    }
    return flujo;
  };

  public static asignarFlujoCaja(mfsData: Mfs): void {
    BLMfs.calcular(mfsData);
    mfsData.FlujoCaja = this.obtenerFlujoCaja(mfsData);
  };


}
