export const rentaSinLucro = 0.2;

export class BLImpuestosPersonaNatural {

  public static UVT = 36308;

  private static _Rangos = [
    {
      min: 0,
      max: 1090,
      porcentaje: 0,
      extra: 0,
      excluido: 0,
    },
    {
      min: 1090,
      max: 1700,
      porcentaje: 0.19,
      extra: 0,
      excluido: 1090,
    },
    {
      min: 1700,
      max: 4100,
      porcentaje: 0.28,
      extra: 116,
      excluido: 1700,
    },
    {
      min: 4100,
      max: 8670,
      porcentaje: 0.33,
      extra: 788,
      excluido: 4100,
    },
    {
      min: 8670,
      max: 18970,
      porcentaje: 0.35,
      extra: 2296,
      excluido: 8670,
    },
    {
      min: 18970,
      max: 31000,
      porcentaje: 0.37,
      extra: 5901,
      excluido: 18970,
    },
    {
      min: 31000,
      max: -1,
      porcentaje: 0.39,
      extra: 10352,
      excluido: 31000,
    },
  ];


  public static calcularImpuestoRenta(utilidad: number, uvt: number = this.UVT) {
    const utilidadUvt = utilidad / uvt;
    const regla = this._Rangos.find(rango => utilidadUvt > rango.min && (utilidadUvt < rango.max || rango.max < 0)) || this._Rangos[this._Rangos.length];
    return ((utilidadUvt - regla.excluido) * regla.porcentaje + regla.extra) * uvt;
  }
}
