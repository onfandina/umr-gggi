import {Especie} from "../../entities/MFS/Especie";
import {Ubicaciones} from "../../entities/MFS/Enums/Ubicaciones";
import {ValorUbicaciones} from "../../entities/MFS/ValorUbicaciones";

export class BLIngresos {
  public static obtenerIngresosEspecie(especie: Especie, areaUca: number[]) {
    const ubicaciones: ValorUbicaciones = {
      [Ubicaciones.CIUDAD_PRINCIPAL]: 0,
      [Ubicaciones.CIUDAD_INTERMEDIA]: 0,
      [Ubicaciones.BORDE_CARRETERA]: 0,
    };
    especie.Ingreso = areaUca.map(uca => {
      return especie.Productos.reduce((u, producto) => {
        const prod = producto.Volumen * producto.FactorAprovechamientoEfectivo * uca;
        return {
          [Ubicaciones.CIUDAD_PRINCIPAL]: ubicaciones[Ubicaciones.CIUDAD_PRINCIPAL] + prod * producto.PorcentajeCiudadPrincipal * producto.PrecioCiudadPrincipal,
          [Ubicaciones.CIUDAD_INTERMEDIA]: ubicaciones[Ubicaciones.CIUDAD_INTERMEDIA] + prod * producto.PorcentajeCiudadIntermedia * producto.PrecioCiudadIntermedia,
          [Ubicaciones.BORDE_CARRETERA]: ubicaciones[Ubicaciones.BORDE_CARRETERA] + prod * producto.PorcentajeBordeCarretera * producto.PrecioBordeCarretera,
        };
      }, ubicaciones);
    });
  }

  public static obtenerIngresosUbicacion(especies: Especie[], areaUca: number[]): ValorUbicaciones[] {
    const valores: ValorUbicaciones[] = areaUca.map(() => ({
      [Ubicaciones.CIUDAD_PRINCIPAL]: 0,
      [Ubicaciones.CIUDAD_INTERMEDIA]: 0,
      [Ubicaciones.BORDE_CARRETERA]: 0,
    }));
    especies.forEach(especie => {
      if(especie.Ingreso.length === 0) {
        BLIngresos.obtenerIngresosEspecie(especie, areaUca)
      }
      especie.Ingreso.forEach((ingreso, i) => {
        valores[i][Ubicaciones.CIUDAD_PRINCIPAL] += ingreso[Ubicaciones.CIUDAD_PRINCIPAL]
        valores[i][Ubicaciones.CIUDAD_INTERMEDIA] += ingreso[Ubicaciones.CIUDAD_INTERMEDIA]
        valores[i][Ubicaciones.BORDE_CARRETERA] += ingreso[Ubicaciones.BORDE_CARRETERA]
      })
    })
    return valores
  }
}
