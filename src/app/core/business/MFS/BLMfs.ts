import {Mfs} from "../../entities/MFS/Mfs";
import {BLProduccion} from "./BLProduccion";
import {BLVolumenVenta} from "./BLVolumenVenta";
import {BLTcafFlujo} from "./BLTcafFlujo";
import {BLIngresos} from "./BLIngresos";
import {BLCredito} from "./BLCredito";
import {PorcentajeSistema} from "../../entities/MFS/PorcentajeSistema";
import {BLCostos} from "./BLCostos";
import {Sistema} from "../../entities/MFS/Sistema";

export class BLMfs {

  public static calcularPorcentajesTransformacion(mfsData: Mfs): PorcentajeSistema[] {

    const volumenesTransformacion = mfsData.Materiales.map(material => ({
      material,
      volumen: mfsData.InformacionEspecies.Especies.filter(especie => especie.CalidadMadera === material).reduce((volumen, especie) => volumen + especie.Productos.reduce((vol, producto) => vol + producto.Volumen, 0), 0)
    }));
    const porcentajes: PorcentajeSistema[] = [];
    mfsData.SistemasTransformacion.forEach(sistema => {
      sistema.Rendimientos.forEach((rendimiento) => {
        const volumen = volumenesTransformacion.find(vol => vol.material === rendimiento.Material)?.volumen || 0;
        const porcentaje = new PorcentajeSistema();
        porcentaje.Sistema = sistema;
        porcentaje.Material = rendimiento.Material;
        porcentaje.CostoAprovechamiento = rendimiento.Rendimiento;
        porcentaje.Porcentaje = volumen > 0 ? mfsData.InformacionEspecies.Especies.filter(especie => especie.CalidadMadera === rendimiento.Material).reduce((volumen, especie) => volumen + especie.Productos.filter(producto => producto.SistemaTransPrimaria === sistema.Nombre).reduce((vol, producto) => vol + producto.Volumen, 0), 0) / volumen : 0;
        porcentajes.push(porcentaje);
      });
    });
    mfsData.Costos.Transformacion = porcentajes;
    return porcentajes;
  }

  public static calcularPorcentajesTransporteMenor(mfsData: Mfs): PorcentajeSistema[] {
    mfsData.Costos.TransporteMenor.forEach((porcentaje, i) => {
      const sistema = mfsData.SistemasTransporteMenor.find(s => s.Nombre === porcentaje.Sistema.Nombre);
      if (sistema) {
        porcentaje.Sistema = sistema;
        porcentaje.CostoAprovechamiento = porcentaje.Sistema?.Rendimientos.find(rendimiento => rendimiento.Material.Nombre === porcentaje.Material.Nombre)?.Rendimiento || -1;
      }
    });
    return mfsData.Costos.TransporteMenor.filter(porcentaje => porcentaje.CostoAprovechamiento >= 0);
  }

  public static calcular(mfsData: Mfs) {
    // Especie
    mfsData.InformacionEspecies.Especies.forEach(especie => {
      // Produccion
      BLProduccion.obtenerProduccionEspecie(especie, mfsData.InformacionGeneral.AreaUca);
      //Venta
      BLVolumenVenta.obtenerVolumenVenta(especie, mfsData.InformacionGeneral.AreaUca);
      // Tcaf
      BLTcafFlujo.obtenerTcafFlujoEspecie(especie, mfsData.InformacionGeneral.AreaUca);
      // Ingresos
      BLIngresos.obtenerIngresosEspecie(especie, mfsData.InformacionGeneral.AreaUca);
    });
    // Asistencia Tecnica
    BLCostos.calcularTotal(mfsData.Costos.AsistenciaTecanica.AsistenciaAnual);
    BLCostos.calcularTotal(mfsData.Costos.AsistenciaTecanica.CostosPreoperativos);
    // Maquinaria y Equipos
    BLCostos.calcularTotal(mfsData.Costos.Maquinaria);

    // Ubicaciones
    mfsData.Costos.Caminos.Total = mfsData.Costos.Caminos.Construccion.Valor * mfsData.Costos.Caminos.Construccion.Cantidad + mfsData.Costos.Caminos.Mantenimiento.Valor * mfsData.Costos.Caminos.Mantenimiento.Cantidad;
    mfsData.IngresosUbicaciones = BLIngresos.obtenerIngresosUbicacion(mfsData.InformacionEspecies.Especies, mfsData.InformacionGeneral.AreaUca);
    mfsData.VolumenUbicaciones = BLVolumenVenta.obtenerVolumenVentaUbicaciones(mfsData.InformacionEspecies.Especies, mfsData.InformacionGeneral.AreaUca);
    // Materiales
    mfsData.ProduccionMateriales = BLProduccion.obtenerProduccionMateriales(mfsData.InformacionEspecies.Especies, mfsData.InformacionGeneral.AreaUca);
    // Credito
    const credito = new BLCredito(mfsData.ModuloFinanciero, mfsData.Sensibilidad.TasaInteres);
    mfsData.Credito = credito.obtenerSimulacion();
    // Transformacion
    mfsData.Costos.Transformacion = this.calcularPorcentajesTransformacion(mfsData);
    // Transporte Menor
    mfsData.Costos.TransporteMenor = this.calcularPorcentajesTransporteMenor(mfsData);
  }
}
