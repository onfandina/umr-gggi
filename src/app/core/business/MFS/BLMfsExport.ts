import {Mfs} from "../../entities/MFS/Mfs";
import {merge} from 'lodash';
import {Material} from "../../entities/MFS/Material";
import {Madera} from "../../entities/MFS/Madera";
import {PFNM} from "../../entities/MFS/PFNM";
import {Cuota} from "../../entities/MFS/Costos/Credito/Cuota";
import {Sistema} from "../../entities/MFS/Sistema";
import {SistemaModelo} from "../../entities/MFS/SistemaModelo";
import {SistemaFijo} from "../../entities/MFS/SistemaFijo";
import {Costo} from "../../entities/Costos/Costo";
import {CostoSistema} from "../../entities/MFS/Costos/CostoSistema";
import {MfsCosto} from "../../entities/MFS/Costos/MfsCosto";
import {FactorRendimiento} from "../../entities/MFS/FactorRendimiento";
import {Rendimiento} from "../../entities/MFS/Rendimiento";
import {BLSistemas} from "./BLSistemas";
import {ListaCostos} from "../../entities/MFS/Costos/ListaCostos";
import {CostoAprovechamiento} from "../../entities/MFS/Costos/CostoAprovechamiento";
import {CostoCamino} from "../../entities/MFS/Costos/CostoCamino";
import {BLCostos} from "./BLCostos";
import {TransportesMayores} from "../../entities/MFS/TransportesMayores";
import {TiposTrasportesMayores} from "../../entities/MFS/Enums/TiposTrasportesMayores";
import {TransporteMayor} from "../../entities/MFS/TransporteMayor";
import {Ubicaciones} from "../../entities/MFS/Enums/Ubicaciones";
import {PorcentajeSistema} from "../../entities/MFS/PorcentajeSistema";
import {OtrosGastos} from "../../entities/MFS/Costos/OtrosGastos";
import {BLMfs} from "./BLMfs";
import {Especie} from "../../entities/MFS/Especie";
import {Producto} from "../../entities/Producto";


const mappers = {
  [Material.name]: (values: any): Material => {
    if (values.TipoMadera) {
      return new Madera(values.Nombre, values.TipoMadera);
    }
    return new PFNM(values.Nombre, values.Unidades);
  },
  [Cuota.name]: (values: any): Cuota => {
    const cuota = new Cuota();
    merge(cuota, values);
    return cuota;
  },
  [Costo.name]: (values: any, materiales: Material[] = []): Costo => {
    let costo: Costo;
    if (values.Cantidad && values.Unidad) {
      costo = new CostoSistema();
    } else if (values.Cantidad) {
      costo = new CostoCamino();
    } else if (values.Tipo) {
      costo = new MfsCosto();
    } else if (values.Material) {
      const material = materiales.find(material => material.Nombre === values.Material.Nombre) as Material;
      return new CostoAprovechamiento(values.Nombre, values.Valor, material);
    } else {
      costo = new Costo();
    }
    merge(costo, values);
    return costo;
  },
  [FactorRendimiento.name]: (values: any, materiales: Material[] = []): FactorRendimiento => {
    const material = materiales.find(material => material.Nombre === values.Material.Nombre) as Material;
    const factor = new FactorRendimiento(material, values.FactorRendimiento);
    return factor;
  },
  [Rendimiento.name]: (values: any, materiales: Material[] = []): Rendimiento => {
    const material = materiales.find(material => material.Nombre === values.Material.Nombre) as Material;
    return new Rendimiento(material, values.Rendimiento);
  },
  [TransportesMayores.name]: (values: any): TransportesMayores => {
    const transporteMayor = new TransportesMayores();
    transporteMayor.Ciudad = values.Ciudad as Ubicaciones;
    transporteMayor[TiposTrasportesMayores.CAMION_SENCILLO] = new TransporteMayor();
    transporteMayor[TiposTrasportesMayores.DOBLE_TROQUE] = new TransporteMayor();
    transporteMayor[TiposTrasportesMayores.TRACTO_MULA] = new TransporteMayor();
    merge(transporteMayor[TiposTrasportesMayores.CAMION_SENCILLO], values[TiposTrasportesMayores.CAMION_SENCILLO]);
    merge(transporteMayor[TiposTrasportesMayores.DOBLE_TROQUE], values[TiposTrasportesMayores.DOBLE_TROQUE]);
    merge(transporteMayor[TiposTrasportesMayores.TRACTO_MULA], values[TiposTrasportesMayores.TRACTO_MULA]);
    return transporteMayor;
  },
  [PorcentajeSistema.name]: (values: any, materiales: Material[] = [], sistemas: Sistema[] = []): PorcentajeSistema => {
    const porcentajeSistema = new PorcentajeSistema();
    porcentajeSistema.Material = materiales.find(material => material.Nombre === values.Material.Nombre) as Material;
    porcentajeSistema.Sistema = sistemas.find(sistema => sistema.Nombre === values.Sistema.Nombre) as Sistema;
    porcentajeSistema.Porcentaje = values.Porcentaje;
    porcentajeSistema.CostoAprovechamiento = values.CostoAprovechamiento;
    return porcentajeSistema;
  },
  [Especie.name]: (values: any, materiales: Material[] = []): Especie => {
    const especie = new Especie();
    merge(especie, values);
    especie.CalidadMadera = materiales.find(material => material.Nombre === especie.CalidadMadera.Nombre) as Material;
    especie.Productos = especie.Productos.map((producto) => {
      const prod = new Producto();
      merge(prod, producto);
      return prod;
    });
    return especie;
  },
};

const complexMappers = {
  [Sistema.name]: (values: any, materiales: Material[] = []): Sistema => {
    if (values.FactoresRendimiento) {
      const sistema = new SistemaModelo();
      merge(sistema, values);
      sistema.Costos = sistema.Costos.map((costo) => mappers[Costo.name](costo) as CostoSistema);
      sistema.FactoresRendimiento = sistema.FactoresRendimiento.map((factor) => mappers[FactorRendimiento.name](factor, materiales) as FactorRendimiento);
      BLSistemas.CalcularRendimientos(sistema);
      return sistema;
    }
    const sistema = new SistemaFijo();
    merge(sistema, values);
    sistema.Rendimientos = sistema.Rendimientos.map((rendimiento) => mappers[Rendimiento.name](rendimiento, materiales) as Rendimiento);
    return sistema;
  },
  [OtrosGastos.name]: (values: any): OtrosGastos => {
    const otrosGastos = new OtrosGastos();
    merge(otrosGastos, values);
    otrosGastos.Administracion = mappers[Costo.name](otrosGastos.Administracion) as MfsCosto;
    otrosGastos.TasaRenta = mappers[Costo.name](otrosGastos.TasaRenta) as MfsCosto;
    otrosGastos.OtrosCostos = otrosGastos.OtrosCostos.map((costos) => costos.map(costo => mappers[Costo.name](costo) as Costo));

    return otrosGastos;
  }
};

export class BLMfsExport {
  public static export(mfs: Mfs): string {
    return JSON.stringify(mfs);
  }


  public static import(exportMfs: string): Mfs {
    const rawData = JSON.parse(exportMfs);
    const mfs = new Mfs();
    if (rawData.Ubicacion) {
      merge(mfs.Ubicacion, rawData.Ubicacion);
    }
    if (rawData.InformacionGeneral) {
      merge(mfs.InformacionGeneral, rawData.InformacionGeneral);
    }
    if (rawData.ModuloFinanciero) {
      merge(mfs.ModuloFinanciero, rawData.ModuloFinanciero);
    }
    if (rawData.Credito) {
      merge(mfs.Credito, rawData.Credito);
      mfs.Credito.Cuotas = mfs.Credito.Cuotas.map((cuota) => mappers[Cuota.name](cuota) as Cuota);
    }
    if (rawData.Materiales) {
      mfs.Materiales = rawData.Materiales.map(mappers[Material.name]);
    }
    if (rawData.SistemasTransformacion) {
      mfs.SistemasTransformacion = rawData.SistemasTransformacion.map((sistema: any) => complexMappers[Sistema.name](sistema, mfs.Materiales));
    }
    if (rawData.SistemasTransporteMenor) {
      mfs.SistemasTransporteMenor = rawData.SistemasTransporteMenor.map((sistema: any) => complexMappers[Sistema.name](sistema, mfs.Materiales));
    }
    if (rawData.InformacionEspecies) {
      mfs.InformacionEspecies.Especies = rawData.InformacionEspecies.Especies?.map((especie: any) => mappers[Especie.name](especie, mfs.Materiales));
      merge(mfs.InformacionEspecies.PlazaPrincipal, rawData.InformacionEspecies.PlazaPrincipal);
      merge(mfs.InformacionEspecies.PlazaIntermedia, rawData.InformacionEspecies.PlazaIntermedia);
    }
    if (rawData.Costos) {
      mfs.Costos = new ListaCostos();
      if (rawData.Costos.Aprovechamiento.Sistema) {
        mfs.Costos.Aprovechamiento.Costos = rawData.Costos.Aprovechamiento.Costos.map((costo: any) => mappers[Costo.name](costo) as CostoAprovechamiento);
        mfs.Costos.Aprovechamiento.Sistema = complexMappers[Sistema.name](rawData.Costos.Aprovechamiento.Sistema, mfs.Materiales) as Sistema;
      }
      if (mfs.Costos.Tierra) {
        merge(mfs.Costos.Tierra, rawData.Costos.Tierra);
      }
      if (mfs.Costos.Maquinaria) {
        mfs.Costos.Maquinaria.Costos = rawData.Costos.Maquinaria.Costos.map((costo: any) => mappers[Costo.name](costo) as Costo);
      }
      if (rawData.Costos.Caminos.Construccion) {
        mfs.Costos.Caminos.Construccion = mappers[Costo.name](rawData.Costos.Caminos.Construccion) as CostoCamino;
      }
      if (rawData.Costos.Caminos.Mantenimiento) {
        mfs.Costos.Caminos.Mantenimiento = mappers[Costo.name](rawData.Costos.Caminos.Mantenimiento) as CostoCamino;
      }
      if (rawData.Costos.Caminos.Mantenimiento) {
        mfs.Costos.Caminos.Mantenimiento = mappers[Costo.name](rawData.Costos.Caminos.Mantenimiento) as CostoCamino;
      }
      if (rawData.Costos.AsistenciaTecanica) {
        merge(mfs.Costos.AsistenciaTecanica, rawData.Costos.AsistenciaTecanica);
        mfs.Costos.AsistenciaTecanica.AsistenciaAnual.Costos = mfs.Costos.AsistenciaTecanica.AsistenciaAnual.Costos.map(costo => mappers[Costo.name](costo) as Costo);
        mfs.Costos.AsistenciaTecanica.CostosPreoperativos.Costos = mfs.Costos.AsistenciaTecanica.CostosPreoperativos.Costos.map(costo => mappers[Costo.name](costo) as Costo);
        BLCostos.calcularTotal(mfs.Costos.AsistenciaTecanica.AsistenciaAnual);
        BLCostos.calcularTotal(mfs.Costos.AsistenciaTecanica.CostosPreoperativos);
      }
      if (rawData.Costos.TransporteMayor) {
        merge(mfs.Costos.TransporteMayor, rawData.Costos.TransporteMayor);
        mfs.Costos.TransporteMayor = rawData.Costos.TransporteMayor.map((transporte: any) => mappers[TransportesMayores.name](transporte));
      }
      if (rawData.Costos.TransporteMenor) {
        merge(mfs.Costos.TransporteMenor, rawData.Costos.TransporteMenor);
        mfs.Costos.TransporteMenor = rawData.Costos.TransporteMenor.map((transporte: any) => mappers[PorcentajeSistema.name](transporte, mfs.Materiales, mfs.SistemasTransporteMenor));
      }
      if (rawData.Costos.Transformacion) {
        merge(mfs.Costos.Transformacion, rawData.Costos.Transformacion);
        mfs.Costos.Transformacion = rawData.Costos.Transformacion.map((transporte: any) => mappers[PorcentajeSistema.name](transporte, mfs.Materiales, mfs.SistemasTransformacion));
      }
      if (rawData.Costos.Otros) {
        mfs.Costos.Otros = complexMappers[OtrosGastos.name](rawData.Costos.Otros) as OtrosGastos;
      }
    }
    if (rawData.OtrosIngresos) {
      mfs.OtrosIngresos.Incentivos = mappers[Costo.name](rawData.OtrosIngresos.Incentivos) as MfsCosto;
      mfs.OtrosIngresos.Carbono = mappers[Costo.name](rawData.OtrosIngresos.Carbono) as MfsCosto;
    }
    BLMfs.calcular(mfs);
    return mfs;
  }
}
