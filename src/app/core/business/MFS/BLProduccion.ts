import {Especie} from "../../entities/MFS/Especie";
import {ValorMaterial} from "../../entities/MFS/ValorMaterial";

export class BLProduccion {

  public static obtenerProduccionEspecie(especie: Especie, areaUca: number[]) {
    especie.Produccion = areaUca.map(uca => especie.Productos.reduce(
      (produccion, producto) => produccion + ((producto.Volumen ?? 0) * producto.FactorAprovechamientoEfectivo), 0) * uca
    );
  }

  public static obtenerProduccionMateriales(especies: Especie[], areaUca: number[]): ValorMaterial[][] {
    const produccion: ValorMaterial[][] = new Array(areaUca.length).fill(null).map(()=>[])
    especies.forEach(especie => {
      if (especie.Produccion.length === 0) {
        BLProduccion.obtenerProduccionEspecie(especie, areaUca);
      }
      especie.Produccion.forEach((p, i) => {
        let valorMaterialIndex = produccion[i].findIndex(val => val.Material === especie.CalidadMadera);
        if (valorMaterialIndex === -1) {
          produccion[i].push(new ValorMaterial(especie.CalidadMadera));
          valorMaterialIndex = produccion[i].length - 1;
        }
        produccion[i][valorMaterialIndex].Valor += p;
      });

    });
    return produccion;
  }

}
