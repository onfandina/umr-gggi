import {Mfs} from "../../entities/MFS/Mfs";
import {BLMfsExport} from "./BLMfsExport";
import {BLMfs} from "./BLMfs";

export class BLSensibilidad {
  public static aplicarSensibilidad(mfs: Mfs): Mfs {
    const newMfs = BLMfsExport.import(BLMfsExport.export(mfs));
    newMfs.Sensibilidad = {...mfs.Sensibilidad}
    // Area proyecto
    newMfs.InformacionGeneral.AreaBosque *= newMfs.Sensibilidad.Area;
    newMfs.InformacionGeneral.AreaUca = newMfs.InformacionGeneral.AreaUca.map(area => area * newMfs.Sensibilidad.Area);
    // Asistencia Tecnica
    newMfs.Costos.AsistenciaTecanica.AsistenciaAnual.Costos.forEach(area => area.Valor *= newMfs.Sensibilidad.AsistenciaTecnica);
    // Administracion
    newMfs.Costos.Otros.Administracion.Valor *= newMfs.Sensibilidad.Administracion;
    BLMfs.calcular(newMfs);

    return newMfs;
  }
}
