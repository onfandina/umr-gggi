import {Rendimiento} from "../../entities/MFS/Rendimiento";
import {Material} from "../../entities/MFS/Material";
import {SistemaModelo} from "../../entities/MFS/SistemaModelo";

export abstract class BLSistema {
  protected _RendimientosUsuario: Rendimiento[];
  protected _Rendimientos: Rendimiento[] = [];
  protected _Sistema?: SistemaModelo;

  constructor(rendimientos: Rendimiento[] = [], sistema?: SistemaModelo) {
    this._RendimientosUsuario = rendimientos;
    if (sistema) {
      this._Sistema = sistema;
      this.CalcularRendimientos();
    }
  }

  protected CalcularRendimientos() {
    if (this._Sistema) {
      const total = this._Sistema.Costos.reduce((a, b) => a + b.Cantidad * b.Valor, 0);
      this._Rendimientos = this._Sistema.FactoresRendimiento.map((factor) => new Rendimiento(factor.Material, total / factor.FactorRendimiento));
    }
  }

  public AdicionarRendimiento(rendimiento: Rendimiento) {
    this._Rendimientos.push(rendimiento);
  }

  public AsignarSistema(sistema: SistemaModelo) {
    this._Sistema = sistema;
    this.CalcularRendimientos();
  }

  public ObtenerRendimiento(material: Material): Rendimiento | null {
    return this._RendimientosUsuario.find((rendimiento) => rendimiento.Material == material) ||
      this._Rendimientos.find((rendimiento) => rendimiento.Material == material) ||
      null;
  }
}
