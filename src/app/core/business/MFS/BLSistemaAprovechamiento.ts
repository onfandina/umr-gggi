import {BLSistema} from "./BLSistema";
import {Material} from "../../entities/MFS/Material";

export class BLSistemaAprovechamiento extends BLSistema {
  public getCostoAprovechamiento(material: Material): number {
    return this.ObtenerRendimiento(material)?.Rendimiento || 0;
  }
}
