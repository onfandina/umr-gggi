import {BLSistema} from "./BLSistema";
import {Material} from "../../entities/MFS/Material";

export class BLSistemaTransformacion extends BLSistema {
  public getCostoTransfomacion(material: Material): number {
    return this.ObtenerRendimiento(material)?.Rendimiento || 0;
  }
}
