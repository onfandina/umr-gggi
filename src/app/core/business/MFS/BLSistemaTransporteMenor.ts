import {BLSistema} from "./BLSistema";
import {Material} from "../../entities/MFS/Material";

export class BLSistemaTransporteMenor extends BLSistema {
  public getCostoTransporte(material: Material): number {
    return this.ObtenerRendimiento(material)?.Rendimiento || 0;
  }
}
