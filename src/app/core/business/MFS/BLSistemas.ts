import {Rendimiento} from "../../entities/MFS/Rendimiento";
import {Material} from "../../entities/MFS/Material";
import {Sistema} from "../../entities/MFS/Sistema";
import {SistemaModelo} from "../../entities/MFS/SistemaModelo";

export abstract class BLSistemas {

  public static CalcularRendimientos(sistema: SistemaModelo) {
      const total = sistema.Costos.reduce((a, b) => a + b.Cantidad * b.Valor, 0);
      sistema.Rendimientos = sistema.FactoresRendimiento.map((factor) => new Rendimiento(factor.Material, total / factor.FactorRendimiento));
  }

  public static ObtenerRendimiento(sistema: Sistema, material: Material): Rendimiento | null {
    return sistema.Rendimientos.find((rendimiento) => rendimiento.Material == material) || null;
  }
}
