import {Especie} from "../../entities/MFS/Especie";

export class BLTcafFlujo {
  public static obtenerTcafFlujoEspecie(especie: Especie, areaUca: number[]) {
    especie.TcafFlujo = areaUca.map(uca => especie.Tcaf * especie.VolumenOtrogado * uca);
  }
}
