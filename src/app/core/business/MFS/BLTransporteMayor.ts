import {CostoTransporteMayor} from "../../entities/MFS/Costos/CostoTransporteMayor";
import {CapacidadTransporte} from "../../entities/MFS/Enums/TiposTrasportesMayores";

export class BLTransporteMayor {
  public static obtenerCostosTransporteMayor(costo: CostoTransporteMayor, volumen: number) {
    costo.Volumen = volumen * costo.Transporte.PorcentajeMaterialTransportado;
    if (!costo.Capacidad) {
      costo.Capacidad = CapacidadTransporte[costo.Transporte.TipoTransporte];
    }
    costo.Viajes = Math.round(costo.Volumen / costo.Capacidad);
    costo.CostoFlete = costo.Transporte.CostoViaje / costo.Capacidad;
    costo.CostoTransporte = costo.Volumen * costo.CostoFlete;
    costo.CostoSalvoconductos = costo.Transporte.CostoSalvoconducto * costo.Viajes;
    costo.CostoCargue = costo.Transporte.CostoCargue * costo.Viajes;
    costo.CostoTotal = costo.CostoTransporte + costo.CostoCargue + costo.CostoSalvoconductos;
  }
}
