import {Especie} from "../../entities/MFS/Especie";
import {Ubicaciones} from "../../entities/MFS/Enums/Ubicaciones";
import {ValorUbicaciones} from "../../entities/MFS/ValorUbicaciones";
import {ValorMaterial} from "../../entities/MFS/ValorMaterial";

export class BLVolumenVenta {
  public static obtenerVolumenVenta(especie: Especie, areaUca: number[]) {
    const ubicaciones: ValorUbicaciones = {
      [Ubicaciones.CIUDAD_PRINCIPAL]: 0,
      [Ubicaciones.CIUDAD_INTERMEDIA]: 0,
      [Ubicaciones.BORDE_CARRETERA]: 0,
    };
    especie.Venta = areaUca.map(uca => {
      return especie.Productos.reduce((u, producto) => {
        const prod = producto.Volumen * producto.FactorAprovechamientoEfectivo * uca
        return {
          [Ubicaciones.CIUDAD_PRINCIPAL]: ubicaciones[Ubicaciones.CIUDAD_PRINCIPAL] + prod * producto.PorcentajeCiudadPrincipal,
          [Ubicaciones.CIUDAD_INTERMEDIA]: ubicaciones[Ubicaciones.CIUDAD_INTERMEDIA] + prod * producto.PorcentajeCiudadIntermedia,
          [Ubicaciones.BORDE_CARRETERA]: ubicaciones[Ubicaciones.BORDE_CARRETERA] + prod * producto.PorcentajeBordeCarretera,
        };
      }, ubicaciones)
    })
  }

  public static obtenerVolumenVentaUbicaciones(especies: Especie[], areaUca: number[]): ValorUbicaciones[] {
    const valores: ValorUbicaciones[] = areaUca.map(() => ({
      [Ubicaciones.CIUDAD_PRINCIPAL]: 0,
      [Ubicaciones.CIUDAD_INTERMEDIA]: 0,
      [Ubicaciones.BORDE_CARRETERA]: 0,
    }));
    especies.forEach(especie => {
      if(especie.Venta.length === 0) {
        BLVolumenVenta.obtenerVolumenVenta(especie, areaUca)
      }
      especie.Venta.forEach((volumen, i) => {
        valores[i][Ubicaciones.CIUDAD_PRINCIPAL] += volumen[Ubicaciones.CIUDAD_PRINCIPAL]
        valores[i][Ubicaciones.CIUDAD_INTERMEDIA] += volumen[Ubicaciones.CIUDAD_INTERMEDIA]
        valores[i][Ubicaciones.BORDE_CARRETERA] += volumen[Ubicaciones.BORDE_CARRETERA]
      })
    })
    return valores
  }
}
