import { Producto } from "./Producto";
import { VolumenXProducto } from "./VolumenProducto";

export class CortaFinal {
    Anio: number;
    VolumenXProducto: VolumenXProducto[];
    PorcentajeAprovechamiento: number;
    VolumenEstimado: number;
    CostoAprovechamiento: number;
    CostoTransPrimaria: number;

    constructor(){
        this.Anio = 0;
        this.VolumenXProducto = [];
        this.PorcentajeAprovechamiento = 0;
        this.VolumenEstimado = 0;
        this.CostoAprovechamiento = 0;
        this.CostoTransPrimaria = 0;
    }
}