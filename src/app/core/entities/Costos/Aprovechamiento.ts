import { Costo } from "./Costo";
import { Entresaca } from "../Entresaca";

export class Aprovechamiento extends Costo {
    Entresaca: Entresaca;

    constructor(){
        super();
        this.Entresaca = new Entresaca();
    }
}