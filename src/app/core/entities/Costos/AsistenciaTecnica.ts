import { Periodo } from "../Periodo";
import { Costo } from "./Costo";

export class AsistenciaTecnica extends Costo {
    Periodo: Periodo;
    
    constructor(){
        super();
        this.Periodo = new Periodo();
    }
}