import { Costo } from "./Costo";

export class CostoTierra extends Costo {
    Propia: boolean;
    Arrendamiento: number;
    constructor(){
        super();
        this.Propia = false;
        this.Arrendamiento = 0;
    }
}