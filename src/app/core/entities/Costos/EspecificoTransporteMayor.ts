import { TipoCiudad } from "../Enumeradores/TipoCiudad";
import { TipoVehiculo } from "../Enumeradores/TipoVehiculo";

export class EspecificoTransporteMayor{
    anio!: number;
    tipoCiudad?: TipoCiudad;
    tipoVehiculo?: TipoVehiculo;
    volumenATransportar!: number;
    capacidadPorViaje!: number;
    numeroViajesRealizar!: number;
    costoTotalViaje!: number;
    costoFlete!: number;
    costoTotalTransporte!: number;
    costoSalvoconductoViaje!: number;
    totalSalvoconductos!: number;
    costoCargueDescargue!: number;
    totalCargueDescargue!: number;
    totalVehiculo!: number;
    //totalCiudad!: number;

}