import { Costo } from "./Costo";

export class InsumosPrePlan extends Costo {
    ProveeValorTotal: boolean;
    CostoPlantas: number;
    CostoFertilizante: number;
    CostoHerramientas: number;
    CostoFungicidas: number;
    CostoInsecticidas:number;

    constructor(){
        super();
        this.ProveeValorTotal = false;
        this.CostoPlantas = 0;
        this.CostoFertilizante = 0;
        this.CostoHerramientas = 0;
        this.CostoFungicidas = 0;
        this.CostoInsecticidas = 0;

        this.Valor = this.getValor();
    }

    getValor(){
        if (this.ProveeValorTotal) {
            const insPrePlan = this.CostoPlantas + this.CostoFertilizante + this.CostoHerramientas +
            this.CostoHerramientas + this.CostoFungicidas + this.CostoInsecticidas;

            return insPrePlan;
        }
        else {
            return this.Valor;
        }
    }
}