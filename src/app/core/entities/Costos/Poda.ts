import { Costo } from "./Costo";

export class Poda extends Costo {

    //Año en que se realizará la poda
    Anio: number;

    constructor(){        
        super();
        this.Anio = 0;
    }
}