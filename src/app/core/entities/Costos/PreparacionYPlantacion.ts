import { Tecnologia } from "../Enumeradores/Tecnologia";
import { Costo } from "./Costo";

export class PreparacionYPlantacion extends Costo {
        Tecnologia: Tecnologia;
        AdecuacionTerreno: number;
        TrazadoYEstacado: number;
        Ahoyado: number;
        Siembra: number;
        Jornales: number;

        constructor(){
            super();
            this.Tecnologia = Tecnologia.Manual;
            this.AdecuacionTerreno = 0;
            this.TrazadoYEstacado = 0;
            this.Ahoyado = 0;
            this.Siembra = 0;
            this.Jornales = 0;
            this.Valor = this.getValor();
        }

        getValor(){
            return this.AdecuacionTerreno + this.TrazadoYEstacado + this.Ahoyado + this.Siembra;
        }

}