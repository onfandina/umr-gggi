import { Costo } from "./Costo";
import { Mantenimiento } from "./Mantenimiento";
import { Poda } from "./Poda";
import { TransporteMayor } from "./TransporteMayor";
import { TransporteMenor } from "./TransporteMenor";

export class ResumenCostos{
    
    modoCobroPodas: string;
    modoCobroAsistencia: string;
    modoCobroAdmin: string;
    valorJornal: number;

    podas: Poda[];
    mantenimientos: Mantenimiento[];
    transportesMayor: TransporteMayor[];
    transportesMenor: TransporteMenor[];
    insumos: Costo[];
    
    costoAsistencia: Costo;
    costoTierra: Costo;
    costoTerrenoLimpio: Costo;
    costoTerrenoSinLimpiar: Costo;
    costoArrendamientoTerreno: Costo;
    costoOportunidad: Costo;
    costoConstCaminos: Costo;
    kmAConstruir: number;
    costoMantCaminos: Costo;
    kmAMantener: number;
    costoEstablecimiento: Costo;
    costoCIF: Costo;
    //costoCarbono: Costo;
    costoAdmin: Costo;
    otrosCostos: Costo[];
    costosCarbono: Costo[];

    constructor(){

        this.costoAsistencia = new Costo();
        this.costoTierra = new Costo();
        this.costoTerrenoLimpio = new Costo();
        this.costoTerrenoSinLimpiar = new Costo();
        this.costoArrendamientoTerreno = new Costo();
        this.costoOportunidad = new Costo();
        this.costoConstCaminos = new Costo();
        this.kmAConstruir = 0;
        this.costoMantCaminos = new Costo();
        this.kmAMantener = 0;
        this.costoEstablecimiento = new Costo();
        this.costoCIF = new Costo();
        //this.costoCarbono = new Costo();
        this.costoAdmin = new Costo();

        this.podas = [];
        this.mantenimientos = [];
        this.costosCarbono = [];
        this.transportesMayor = [];
        this.transportesMenor = [];
        this.insumos = [];
        this.modoCobroPodas = '';
        this.modoCobroAsistencia = '';
        this.modoCobroAdmin = '';
        this.valorJornal = 0;
        this.otrosCostos = [];
    };
}