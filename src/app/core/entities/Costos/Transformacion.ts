import { Periodo } from "../Periodo";
import { Producto } from "../Producto";
import { Costo } from "./Costo";

export class Transformacion extends Costo {
    Producto: Producto;
    Periodo: Periodo;

    constructor() {
        super();
        this.Producto = new Producto();
        this.Periodo = new Periodo();
    }
}