import { TipoVehiculo } from "../Enumeradores/TipoVehiculo";
import { Costo } from "./Costo";

export class TransporteMayor extends Costo {
    Via: string;
    Distancia: number;
    CostoKm: number;
    TipoVehiculo?: TipoVehiculo;
    
    porcentajeCiudadPrincipal: number;
    porcentajeCiudadIntermedia: number;
     
    costoViajeCiudadPrincipal: number;
    costoViajeCiudadIntermedia: number;

    costoSalvoconductoCiudadPrincipal: number;
    costoSalvoconductoCiudadIntermedia: number;

    costoCargueCiudadPrincipal: number;
    costoCargueCiudadIntermedia: number;

    constructor(){
        super();
        this.Via = '';
        this.Distancia = 0;
        this.CostoKm = 0;
        
        this.porcentajeCiudadPrincipal = 0;
        this.porcentajeCiudadIntermedia = 0;

        this.costoViajeCiudadPrincipal = 0;
        this.costoViajeCiudadIntermedia = 0;

        this.costoSalvoconductoCiudadPrincipal = 0;
        this.costoSalvoconductoCiudadIntermedia = 0;

        this.costoCargueCiudadPrincipal = 0;
        this.costoCargueCiudadIntermedia = 0;
        
    }
}