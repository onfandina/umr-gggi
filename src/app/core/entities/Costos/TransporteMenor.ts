import { Costo } from "./Costo";

export class TransporteMenor extends Costo {
    Tecnologia: string;
    Distancia: number;
    CostoM3: number;

    porcentajeVolumen: number;

    constructor(){
        super();
        this.Tecnologia = '';
        this.Distancia = 0;
        this.CostoM3 = 0;

        this.porcentajeVolumen = 0;
    }
}