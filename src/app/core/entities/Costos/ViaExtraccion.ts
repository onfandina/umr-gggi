import { Costo } from "./Costo";

export class ViaExtraccion extends Costo {
    Tipo: string;
    CostoKm: number;

    constructor(){
        super();
        this.Tipo = '';
        this.CostoKm = 0;
    }
}