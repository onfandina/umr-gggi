export interface DetalleTransporteMenor{
    concepto: string,
    unidades: string,
    cantidades: number,
    valorDiario: number,
    rendimiento: number
}