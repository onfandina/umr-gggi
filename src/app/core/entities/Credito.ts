import { Costo } from "./Costos/Costo";

export class Credito{
    
    capitalSolicitado: number;
    tasaInteres: number;
    plazoDeuda: number;
    periodicidadPago: string;
    valorCuota: number;
    costosAnio: Costo[];
    
    constructor(){
        this.capitalSolicitado = 0;
        this.tasaInteres = 0;
        this.plazoDeuda = 0;
        this.periodicidadPago = '';
        this.valorCuota = 0;
        this.costosAnio = [];
    }
}