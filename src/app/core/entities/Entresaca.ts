import { Producto } from "./Producto";
import { VolumenXProducto } from "./VolumenProducto";

export class Entresaca {
    Anio: number;
    VolumenXProducto: VolumenXProducto[];
    PorcentajeAprovechamiento: number;
    VolumenEstimado: number;
    CostoAprovechamiento: number;
    CostoTransPrimaria: number;
    VolumenAprovechado: number;
    FactorProporcionalidad: number;
    esNoComercial: boolean;
    valorTransporteMenor: number;

    constructor(){
        this.Anio = 0;
        this.VolumenXProducto = [];
        this.PorcentajeAprovechamiento = 0;
        this.VolumenEstimado = 0;
        this.CostoAprovechamiento = 0;
        this.CostoTransPrimaria = 0;
        this.VolumenAprovechado = 0;
        this.FactorProporcionalidad = 1;
        this.esNoComercial = false;
        this.valorTransporteMenor = 0;
    }
} 