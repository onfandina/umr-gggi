export enum TipoTransporteMenor {
    FuerzaHumana = 0,
    Mecanizado = 1,
    FuerzaAnimal = 2
}