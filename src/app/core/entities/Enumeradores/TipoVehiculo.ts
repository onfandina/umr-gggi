export enum TipoVehiculo {
    CamionSencillo = 0,
    DobleTroque = 1,
    TractoMula = 2
}