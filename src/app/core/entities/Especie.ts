import { CortaFinal } from "./CortaFinal";
import { Entresaca } from "./Entresaca";
import { Establecimiento } from "./Establecimiento";
import { Producto } from "./Producto";

export class Especie {
    Nombre: string;
    Turno: number;
    DensidadesSiembra: number[];
    Establecimientos: Establecimiento[];
    
    Replante: number; //Revisar estas que se definen al principio
    Perdida: number;

    Productos: Producto[];

    //Se van a poner los rangos como una sugerencia. 
    //La persona escribe el rendimiento (igual pasa con el turno)
    RendimientoAnualPromedio: number;

    Entresacas: Entresaca[];

    //Saqué la corta final del producto y la agregué a la especie
    CortaFinal: Entresaca;   
    ConoceRendimiento: boolean;

    constructor(){
        this.Nombre = '';
        this.Turno = 0;
        this.DensidadesSiembra = [];
        this.Establecimientos = [];
        this.Productos = [];
        this.Replante = 0;
        this.Perdida = 0;
        this.RendimientoAnualPromedio = 0;
        this.Entresacas = [];
        this.CortaFinal = new Entresaca;
        this.ConoceRendimiento = false;
    }
}