import { Especie } from "./Especie";

export class Establecimiento {
    Area: number;
    Anio: number;
    AreaBosque: number;
    CostoSeguro: number;
    CostoCercamiento: number;

    constructor(){
        this.Area = 0;
        this.Anio = 0;
        this.AreaBosque = 0;
        this.CostoSeguro = 0;
        this.CostoCercamiento = 0;
    }
}