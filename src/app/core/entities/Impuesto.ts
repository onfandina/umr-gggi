import { Periodo } from "./Periodo";

export class Impuesto {
    Porcentaje: number;
    Periodo: Periodo;

    constructor(){
        this.Porcentaje = 0;
        this.Periodo = new Periodo();
    }
}