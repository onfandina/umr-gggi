import { Especie } from "../Especie";
import { Ingreso } from "./Ingreso";

export class CertificadoIncentivoForestal extends Ingreso{
    Especie: Especie;
    Region: string;
    Actividad: string;
    Anio: number;

    constructor(){
        super();
        this.Especie = new Especie();
        this.Region = '';
        this.Actividad = '';
        this.Anio = 0;
    }
} 