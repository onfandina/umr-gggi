import { Especie } from "../Especie";
import { Producto } from "../Producto";
import { Ingreso } from "./Ingreso";

export class IngresoProductoEspecie extends Ingreso{
    Producto: Producto;
    Especie: Especie;
    CantidadProducida: number;

    constructor(){
        super();
        this.Producto = new Producto();
        this.Especie = new Especie();
        this.CantidadProducida = 0;
    }
}