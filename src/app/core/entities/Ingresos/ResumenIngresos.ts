import { BonoCarbono } from "./BonoCarbono";
import { CertificadoIncentivoForestal } from "./CertificadoIncentivoForestal";
import { Ingreso } from "./Ingreso";

export class ResumenIngresos{
    
    ingresoCifEstablecimiento: number;
    ingresoCifMant2: number;
    ingresoCifMant3: number;
    ingresoCifMant4: number;
    ingresoCifMant5: number;
    ingresosBonos: BonoCarbono[];
    ingresoCifBosque2: number;
    ingresoCifBosque3: number;
    ingresoCifBosque4: number;
    ingresoCifBosque5: number;
    otrosIngresos: Ingreso[];

    constructor(){
        this.ingresoCifEstablecimiento = 0;
        this.ingresoCifMant2 = 0;
        this.ingresoCifMant3 = 0;
        this.ingresoCifMant4 = 0;
        this.ingresoCifMant5 = 0;
        this.ingresoCifBosque2 = 0;
        this.ingresoCifBosque3 = 0;
        this.ingresoCifBosque4 = 0;
        this.ingresoCifBosque5 = 0;

        this.ingresosBonos = [];
        this.otrosIngresos = [];
    }
}