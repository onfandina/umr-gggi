import {CostoAprovechamiento} from "./CostoAprovechamiento";
import {Sistema} from "../Sistema";
import {SistemaFijo} from "../SistemaFijo";

export class Aprovechamiento {
  Costos: CostoAprovechamiento[] = [];
  Sistema: Sistema = new SistemaFijo();
}
