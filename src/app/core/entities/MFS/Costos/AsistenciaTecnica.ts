import {CostosComputados} from "./CostosComputados";

export class AsistenciaTecnica {
  PlanManejoForestal: number[];
  ConstruccionCampamento: number[];
  AsistenciaAnual: CostosComputados;
  CostosPreoperativos: CostosComputados;

  constructor(asistenciaAnual: CostosComputados = new CostosComputados(), planManejoForestal: number[] = [], construccionCampamento: number[] = [], costosPreoperativos: CostosComputados = new CostosComputados()) {
    this.AsistenciaAnual = asistenciaAnual;
    this.PlanManejoForestal = planManejoForestal;
    this.ConstruccionCampamento = construccionCampamento;
    this.CostosPreoperativos = costosPreoperativos;
  }
}
