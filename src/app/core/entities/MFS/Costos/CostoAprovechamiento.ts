import {MfsCosto} from "./MfsCosto";
import {Material} from "../Material";
import {Madera} from "../Madera";

export class CostoAprovechamiento extends MfsCosto {
  Material: Material;

  constructor(nombre: string = '', valor: number = 0, material: Material = new Madera('')) {
    super(nombre, valor);
    this.Material = material;
  }
}
