import {Costo} from "../../Costos/Costo";

export class CostoCamino extends Costo {
  Cantidad: number;

  constructor(valor: number = 0, cantidad: number = 0) {
    super();
    this.Valor = valor;
    this.Cantidad = cantidad;
  }
}
