import {Costo} from "../../Costos/Costo";
import {MfsCosto} from "./MfsCosto";

export class CostoSistema extends MfsCosto {
  Cantidad: number;
  Unidad: string;

  constructor(nombre: string = '', valor: number = 0, cantidad: number = 0, unidad: string = '') {
    super(nombre, valor);
    this.Cantidad = cantidad;
    this.Unidad = unidad;
  }
}
