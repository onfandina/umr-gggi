import {TransporteMayor} from "../TransporteMayor";

export class CostoTransporteMayor{
  Transporte: TransporteMayor
  Volumen: number;
  Capacidad: number;
  Viajes: number;
  CostoFlete: number;
  CostoTransporte: number;
  CostoSalvoconductos: number;
  CostoCargue:number
  CostoTotal: number;

  constructor(Transporte: TransporteMayor) {
    this.Transporte = Transporte;
    this.Volumen = 0;
    this.Capacidad = 0;
    this.Viajes = 0;
    this.CostoFlete = 0;
    this.CostoTransporte = 0;
    this.CostoSalvoconductos = 0;
    this.CostoCargue = 0;
    this.CostoTotal = 0;
  }
}
