import {TiposTrasportesMayores} from "../Enums/TiposTrasportesMayores";
import {Ubicaciones} from "../Enums/Ubicaciones";
import {CostoTransporteMayor} from "./CostoTransporteMayor";

export class CostoTransportesMayores {
  Ciudad: Ubicaciones;
  CostoTotal: number;
  [TiposTrasportesMayores.CAMION_SENCILLO]: CostoTransporteMayor;
  [TiposTrasportesMayores.DOBLE_TROQUE]: CostoTransporteMayor;
  [TiposTrasportesMayores.TRACTO_MULA]: CostoTransporteMayor;

  constructor(ciudad: Ubicaciones = Ubicaciones.CIUDAD_PRINCIPAL, costoTotal =0 ) {
    this.Ciudad = ciudad;
    this.CostoTotal = costoTotal
  }
}
