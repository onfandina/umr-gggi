import {CostoCamino} from "./CostoCamino";

export class CostosCaminos {
  Construccion: CostoCamino;
  Mantenimiento: CostoCamino;
  Total: number;

  constructor(Construccion: CostoCamino = new CostoCamino(), Mantenimiento: CostoCamino = new CostoCamino(), total: number = 0) {
    this.Construccion = Construccion;
    this.Mantenimiento = Mantenimiento;
    this.Total = total;
  }
}
