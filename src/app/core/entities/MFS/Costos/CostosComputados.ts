import {Costo} from "../../Costos/Costo";

export class CostosComputados {
  Costos: Costo[];
  Total: number = 0;

  constructor(Costos: Costo[] = []) {
    this.Costos = Costos;
  }
}
