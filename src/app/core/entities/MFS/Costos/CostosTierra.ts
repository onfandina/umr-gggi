export class CostosTierra {
  terreno: number;
  derechosUso: number;
  oportunidad: number;

  constructor(terreno: number = 0, derechosUso: number = 0, oportunidad: number = 0) {
    this.terreno = terreno;
    this.derechosUso = derechosUso;
    this.oportunidad = oportunidad;
  }
}
