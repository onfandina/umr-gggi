export class Cuota {
  Interes: number;
  Capital: number;
  Total: number;


  constructor(interes: number = 0, capital: number = 0, total: number = 0) {
    this.Interes = interes;
    this.Capital = capital;
    this.Total = total;
  }
}
