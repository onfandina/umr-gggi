import {Cuota} from "./Cuota";

export class SimulacionCredito {
  Capital: number = 0;
  Cuotas: Cuota[] = [];
  CuotasAnuales: number[] = [];
  Total: number = 0;
}
