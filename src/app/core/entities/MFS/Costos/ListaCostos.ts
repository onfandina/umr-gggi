import {Costo} from "../../Costos/Costo";
import {AsistenciaTecnica} from "./AsistenciaTecnica";
import {CostosTierra} from "./CostosTierra";
import {CostosCaminos} from "./CostosCaminos";
import {OtrosGastos} from "./OtrosGastos";
import {TransportesMayores} from "../TransportesMayores";
import {Aprovechamiento} from "./Aprovechamiento";
import {PorcentajeSistema} from "../PorcentajeSistema";
import {CostosComputados} from "./CostosComputados";

export class ListaCostos {
  Jornal: Costo = new Costo();
  Maquinaria: CostosComputados = new CostosComputados();
  AsistenciaTecanica: AsistenciaTecnica = new AsistenciaTecnica();
  Tierra: CostosTierra = new CostosTierra();
  Caminos: CostosCaminos = new CostosCaminos();
  Aprovechamiento: Aprovechamiento = new Aprovechamiento();
  TransporteMayor: TransportesMayores[] = [];
  TransporteMenor: PorcentajeSistema[] = [];
  Transformacion: PorcentajeSistema[] = [];
  Otros: OtrosGastos = new OtrosGastos();
}
