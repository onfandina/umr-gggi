import {Costo} from "../../Costos/Costo";
import {TiposCosto} from "../Enums/TiposCosto";

export class MfsCosto extends Costo {
  Tipo: TiposCosto;

  constructor(nombre: string = '', valor: number = 0, tipo: TiposCosto = TiposCosto.FIJO) {
    super();
    this.Nombre = nombre;
    this.Valor = valor;
    this.Tipo = tipo;
  }
}
