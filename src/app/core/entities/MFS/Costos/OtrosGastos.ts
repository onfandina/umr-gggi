import {MfsCosto} from "./MfsCosto";
import {TiposCosto} from "../Enums/TiposCosto";
import {Costo} from "../../Costos/Costo";

export class OtrosGastos {
  Administracion: MfsCosto;
  TasaRenta: MfsCosto;
  OtrosCostos: Costo[][];
  Carbono: number[];
  MedidasCompensacion: number;

  constructor(
    Administracion: MfsCosto = new MfsCosto('Administración % sobre los costos totales', 0, TiposCosto.PORCENTUAL),
    Carbono: number[] = [],
    TasaRenta: MfsCosto = new MfsCosto('Tasa de renta', 0, TiposCosto.PORCENTUAL),
    MedidasCompensacion: number = 0,
    OtrosCostos: MfsCosto[][] = []
  ) {
    this.Administracion = Administracion;
    this.Carbono = Carbono;
    this.TasaRenta = TasaRenta;
    this.MedidasCompensacion = MedidasCompensacion;
    this.OtrosCostos = OtrosCostos;
  }
}
