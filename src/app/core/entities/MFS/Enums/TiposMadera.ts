export enum TiposMadera {
  MUY_DURA = "MUY_DURA",
  DURA = "DURA",
  BLANDA = "BLANDA",
  OTRA = "OTRA",
}
