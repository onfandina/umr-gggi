export enum TiposTrasportesMayores {
  CAMION_SENCILLO = "CAMION_SENCILLO",
  DOBLE_TROQUE = "DOBLE_TROQUE",
  TRACTO_MULA = "TRACTO_MULA",
}

export const CapacidadTransporte = {
  [TiposTrasportesMayores.CAMION_SENCILLO]: 10,
  [TiposTrasportesMayores.DOBLE_TROQUE]: 19,
  [TiposTrasportesMayores.TRACTO_MULA]: 30,
};
