import {Madera} from "./Madera";
import {Producto} from "../Producto";
import {ValorUbicaciones} from "./ValorUbicaciones";
import {Material} from "./Material";

export class Especie {
  Nombre: string;
  CalidadMadera: Material;
  Tcaf: number;
  TcafFlujo: number[];
  VolumenOtrogado: number;
  Productos: Producto[] = [];
  Produccion: number[] = [];
  Venta: ValorUbicaciones[] = [];
  Ingreso: ValorUbicaciones[] = [];

  constructor(Nombre: string = '', CalidadMadera: Madera = new Madera('')) {
    this.Nombre = Nombre;
    this.CalidadMadera = CalidadMadera;
    this.Tcaf = 0;
    this.TcafFlujo = [];
    this.VolumenOtrogado = 0;
  }

  public addProducto(producto: Producto) {
    this.Productos.push(producto);
  }
}
