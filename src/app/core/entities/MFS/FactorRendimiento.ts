import {Material} from "./Material";

export class FactorRendimiento {
  Material: Material;
  FactorRendimiento: number = 0;

  constructor(material: Material, factorRendimiento: number = 0) {
    this.Material = material;
    this.FactorRendimiento = factorRendimiento;
  }
}
