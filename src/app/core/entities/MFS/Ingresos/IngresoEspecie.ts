import {Ubicaciones} from "../Enums/Ubicaciones";

export class IngresoEspecie {
  Ubicacion: Ubicaciones;
  Ingreso: number;

  constructor(ubicacion: Ubicaciones = Ubicaciones.CIUDAD_PRINCIPAL, ingreso: number = 0) {
    this.Ubicacion = ubicacion;
    this.Ingreso = ingreso;
  }
}
