import {MfsCosto} from "../Costos/MfsCosto";
import {TiposCosto} from "../Enums/TiposCosto";

export class OtrosIngresos {
  Incentivos: MfsCosto = new MfsCosto('Incentivos', 0, TiposCosto.PORCENTUAL)
  Carbono: MfsCosto = new MfsCosto('Carbono', 0, TiposCosto.PORCENTUAL)
}
