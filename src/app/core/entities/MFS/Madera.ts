import {TiposMadera} from "./Enums/TiposMadera";
import {Material} from "./Material";

export class Madera extends Material {
  TipoMadera: TiposMadera;

  constructor(name: string, TipoMadera: TiposMadera = TiposMadera.OTRA) {
    super(name);
    this.TipoMadera = TipoMadera;
  }
}
