import {MfsInformacionGeneral} from "./MfsInformacionGeneral";
import {Sistema} from "./Sistema";
import {Material} from "./Material";
import {ListaCostos} from "./Costos/ListaCostos";
import {MfsModuloFinanciero} from "./MfsModuloFinanciero";
import {ValorUbicaciones} from "./ValorUbicaciones";
import {ValorMaterial} from "./ValorMaterial";
import {SimulacionCredito} from "./Costos/Credito/SimulacionCredito";
import {MfsInformacionEspecies} from "./MfsInformacionEspecies";
import {OtrosIngresos} from "./Ingresos/OtrosIngresos";
import {Sensibilidad} from "./Sensibilidad";
import {FlujoCaja} from "./Resultados/FlujoCaja";
import {Ubicacion} from "./Ubicacion";

export class Mfs {
  Ubicacion: Ubicacion = new Ubicacion();
  InformacionGeneral: MfsInformacionGeneral = new MfsInformacionGeneral();
  ModuloFinanciero: MfsModuloFinanciero = new MfsModuloFinanciero();
  Credito: SimulacionCredito = new SimulacionCredito();
  Materiales: Material[] = [];
  SistemasTransformacion: Sistema[] = [];
  SistemasTransporteMenor: Sistema[] = [];
  InformacionEspecies: MfsInformacionEspecies = new MfsInformacionEspecies();
  Costos: ListaCostos = new ListaCostos();
  IngresosUbicaciones: ValorUbicaciones[] = [];
  OtrosIngresos: OtrosIngresos = new OtrosIngresos();
  VolumenUbicaciones: ValorUbicaciones[] = [];
  ProduccionMateriales: ValorMaterial[][] = [];
  Sensibilidad: Sensibilidad = new Sensibilidad();
  FlujoCaja: FlujoCaja = new FlujoCaja();
}
