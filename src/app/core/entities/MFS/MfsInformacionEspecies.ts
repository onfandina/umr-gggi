import {Especie} from "./Especie";
import {PlazaVenta} from "./PlazaVenta";

export class MfsInformacionEspecies {
  Especies: Especie[] = [];
  PlazaPrincipal: PlazaVenta = new PlazaVenta();
  PlazaIntermedia: PlazaVenta = new PlazaVenta();
}
