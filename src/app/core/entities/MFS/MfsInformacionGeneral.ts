import {Persona} from "../Persona";

export interface MfsInformacionGeneralType {
  Persona: Persona;
  UVT: number;
  AreaUmf: number;
  AreaBosque: number;
  Turnos: number;
  Uca?: number;
  AreaUca: number[];
  M3HaOtorgadosUca1: number;
  M3HaOtorgados?: number;
}

export class MfsInformacionGeneral implements MfsInformacionGeneralType {

  Persona: Persona = new Persona();
  UVT: number = 38004;
  AreaUmf: number = 0;
  AreaBosque: number = 0;
  Turnos: number = 0;
  Uca: number = 0;
  AreaUca: number[] = [];
  M3HaOtorgadosUca1: number = 0;
  M3HaOtorgados: number = 0;
  Familias: number = 0;
}
