import {PeriodosInteres} from "./Enums/PeriodosInteres";

export class MfsModuloFinanciero {
  Interes: number = 0;
  PeriodoInteres: PeriodosInteres = PeriodosInteres.MENSUAL;
  Tasa: number = 0;
  Capital: number = 0;
  Plazo: number = 0;
}
