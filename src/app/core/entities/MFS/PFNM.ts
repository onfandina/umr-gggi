import {Material} from "./Material";

export class PFNM extends Material {
  Unidades: string;

  constructor(nombre: string = '', Unidades: string = '') {
    super(nombre);
    this.Unidades = Unidades;
  }
}
