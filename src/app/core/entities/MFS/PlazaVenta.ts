import {Ubicaciones} from "./Enums/Ubicaciones";


export class PlazaVenta {
  Ubicacion: Ubicaciones = Ubicaciones.CIUDAD_PRINCIPAL;
  Ciudad: string = '';
  Departamento: string = '';
  Distancia: number = 0;
}
