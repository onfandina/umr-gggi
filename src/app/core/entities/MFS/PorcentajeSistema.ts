import {Material} from "./Material";
import {Madera} from "./Madera";
import {Sistema} from "./Sistema";
import {SistemaFijo} from "./SistemaFijo";

export class PorcentajeSistema {
  Material: Material = new Madera('');
  Sistema: Sistema = new SistemaFijo();
  CostoAprovechamiento: number = 0;
  Porcentaje: number = 0;
}
