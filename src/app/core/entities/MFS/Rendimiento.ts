import {Material} from "./Material";

export class Rendimiento {
  Material: Material;
  Rendimiento: number;

  constructor(material: Material, rendimiento: number = 0) {
    this.Material = material;
    this.Rendimiento = rendimiento;
  }
}
