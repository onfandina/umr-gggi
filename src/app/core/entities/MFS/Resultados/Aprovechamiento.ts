import {Material} from "../Material";
import {Madera} from "../Madera";

export class Aprovechamiento {
  Material: Material = new Madera('');
  Volumen: number = 0;
  Costo: number = 0
}
