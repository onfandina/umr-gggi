import {PorcentajeSistema} from "../PorcentajeSistema";

export class CostoSistema {
  Porcentaje: PorcentajeSistema = new PorcentajeSistema();
  Valor: number = 0;
}
