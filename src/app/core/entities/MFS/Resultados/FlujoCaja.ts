import {InversionInicial} from "./InversionInicial";
import {FlujoCajaUca} from "./FlujoCajaUca";

export class FlujoCaja {
  InversionInicial: InversionInicial = new InversionInicial();
  FlujoUcas: FlujoCajaUca[] = [];
  DTF: number = 0.0313;
  TIR: number = 0;
  VPN: number = 0;
  IngresoPromedio: number = 0;
  IngresoPromedioFamilia: number = 0;
}
