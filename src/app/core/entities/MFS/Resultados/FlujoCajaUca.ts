import {IngresoVentas} from "./IngresoVentas";
import {OtrosIngresos} from "./OtrosIngresos";
import {CostoAprovechamiento} from "./CostoAprovechamiento";
import {CostosSistema} from "./CostosSistema";
import {ResultadoTransporteMayor} from "./ResultadoTransporteMayor";
import {OtrosCostos} from "./OtrosCostos";
import {CostosAsistenciaTecnica} from "./CostosAsistenciaTecnica";

export class FlujoCajaUca {
  IngresoVentas: IngresoVentas = new IngresoVentas();
  OtrosIngresos: OtrosIngresos = new OtrosIngresos();
  Aprovechamiento: CostoAprovechamiento = new CostoAprovechamiento();
  Transformacion: CostosSistema = new CostosSistema();
  TransporteMenor: CostosSistema = new CostosSistema();
  TransporteMayor: ResultadoTransporteMayor = new ResultadoTransporteMayor();
  OtrosCostos: OtrosCostos = new OtrosCostos();
  SubtotalCostos: number = 0;
  CostoAdministracion: number = 0;
  CostoIndustriaComercio: number = 0;
  TotalCostos: number = 0;
  TotalIngresos: number = 0;
  UtilidadBruta: number = 0;
  ImpuestoRenta: number = 0;
  UtilidadNeta: number = 0;
  CostoTierra: number = 0;
  Asistencia: CostosAsistenciaTecnica = new CostosAsistenciaTecnica();
}
