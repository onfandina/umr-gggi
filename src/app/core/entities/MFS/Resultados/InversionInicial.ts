import {CostosAsistenciaTecnica} from "./CostosAsistenciaTecnica";
import {CostoAprovechamiento} from "./CostoAprovechamiento";

export class InversionInicial {
  Asistencia: CostosAsistenciaTecnica = new CostosAsistenciaTecnica();
  Aprovechamiento: CostoAprovechamiento = new CostoAprovechamiento();
  Concertacion: number = 0;
  Maquinaria: number = 0;
  InversionTotal: number = 0;
  Total: number = 0;
}
