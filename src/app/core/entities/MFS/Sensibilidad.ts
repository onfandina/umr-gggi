export class Sensibilidad {
  RendimientoAprovechamiento: number = 1;
  Area: number = 1;
  AsistenciaTecnica: number = 1;
  Administracion: number = 1;
  Ingresos: number = 1;
  Costos: number = 1;
  TasaInteres: number = 1;
  IngresoPromedioFamilia: number = 908526;
  IngresoPromedio: number = 0;
  IngresosTotal: number = 0;
  TIR: number = 0;
  VPN: number = 0;
  DTF: number = 0;
}
