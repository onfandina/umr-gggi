import {Rendimiento} from "./Rendimiento";

export abstract class Sistema {
  Nombre: string;
  Rendimientos: Rendimiento[];


  constructor(nombre: string = '', rendimientos: Rendimiento[] = []) {
    this.Nombre = nombre;
    this.Rendimientos = rendimientos;
  }
}
