import {CostoSistema} from "./Costos/CostoSistema";
import {FactorRendimiento} from "./FactorRendimiento";
import {Sistema} from "./Sistema";
import {Rendimiento} from "./Rendimiento";

export class SistemaModelo extends Sistema {
  FactoresRendimiento: FactorRendimiento[];
  Costos: CostoSistema[];
  Rendimientos: Rendimiento[] = [];

  constructor(nombre: string = '', FactoresRendimiento: FactorRendimiento[] = [], Costos: CostoSistema[] = []) {
    super(nombre);
    this.FactoresRendimiento = FactoresRendimiento;
    this.Costos = Costos;
  }
}
