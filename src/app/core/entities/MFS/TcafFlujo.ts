export class TcafFlujo {
  uca: number;
  total: number;

  constructor(uca: number, total: number) {
    this.uca = uca;
    this.total = total;
  }
}
