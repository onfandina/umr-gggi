import {TiposTrasportesMayores} from "./Enums/TiposTrasportesMayores";

export class TransporteMayor {
  TipoTransporte: TiposTrasportesMayores;
  CostoViaje: number;
  CostoSalvoconducto: number;
  CostoCargue: number;
  PorcentajeMaterialTransportado: number;

  constructor(TipoTransporte: TiposTrasportesMayores = TiposTrasportesMayores.CAMION_SENCILLO, CostoViaje: number = 0, CostoSalvoconducto: number = 0, CostoCargue: number = 0, PorcentajeMaterialTransportado: number = 0) {
    this.TipoTransporte = TipoTransporte;
    this.CostoViaje = CostoViaje;
    this.CostoSalvoconducto = CostoSalvoconducto;
    this.CostoCargue = CostoCargue;
    this.PorcentajeMaterialTransportado = PorcentajeMaterialTransportado;
  }
}
