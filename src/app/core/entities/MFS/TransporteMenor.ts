import {TransporteMenorAsignacion} from "./TransporteMenorAsignacion";
import {Material} from "./Material";

export class TransporteMenor {
  Material: Material;
  Asignaciones: TransporteMenorAsignacion[];

  constructor(material: Material, asignaciones: TransporteMenorAsignacion[] = []) {
    this.Material = material;
    this.Asignaciones = asignaciones;
  }
}
