export class TransporteMenorAsignacion {
  Nombre: string;
  Porcentaje: number;

  constructor(nombre: string, Porcentaje: number = 0) {
    this.Nombre = nombre;
    this.Porcentaje = Porcentaje;
  }
}
