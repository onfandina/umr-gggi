import {TiposTrasportesMayores} from "./Enums/TiposTrasportesMayores";
import {TransporteMayor} from "./TransporteMayor";
import {Ubicaciones} from "./Enums/Ubicaciones";

export class TransportesMayores {
  Ciudad: Ubicaciones
  [TiposTrasportesMayores.CAMION_SENCILLO]: TransporteMayor;
  [TiposTrasportesMayores.DOBLE_TROQUE]: TransporteMayor;
  [TiposTrasportesMayores.TRACTO_MULA]: TransporteMayor;

  constructor(Ciudad: Ubicaciones = Ubicaciones.CIUDAD_PRINCIPAL) {
    this.Ciudad = Ciudad;
  }
}
