export class Ubicacion {
  Departamento: string = '';
  Municipio: string = '';
  Altura: string = '';
  Autoridad: string = '';
  Pendiente: boolean = false;
  PendienteArea: boolean = false;
  POF: boolean = false;
  PMF: boolean = false;
}
