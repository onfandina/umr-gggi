import {Material} from "./Material";

export class ValorMaterial {
  Material: Material;
  Valor: number;

  constructor(material: Material, Valor: number = 0) {
    this.Material = material;
    this.Valor = Valor;
  }
}
