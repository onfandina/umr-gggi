import {Ubicaciones} from "./Enums/Ubicaciones";

export class ValorUbicaciones {
  [Ubicaciones.CIUDAD_PRINCIPAL]: number = 0;
  [Ubicaciones.CIUDAD_INTERMEDIA]: number = 0;
  [Ubicaciones.BORDE_CARRETERA]: number = 0;
}
