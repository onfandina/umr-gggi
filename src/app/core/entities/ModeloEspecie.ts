//Interfaz que representa los atributos por defecto de una especie
export interface ModeloEspecie {
    Nombre: string;
    Turno: string;
    Rendimiento?: string;
    LimiteInferiorTemperatura: number;
    LimiteSuperiorTemperatura: number;
    LimiteInferiorPrecipitacion: number;
    LimiteSuperiorPrecipitacion: number;   
}