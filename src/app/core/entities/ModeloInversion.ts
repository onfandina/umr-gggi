import { Credito } from "./Credito";

export class ModeloInversion{
    aplicaCredito: boolean;
    credito: Credito;
    incentivo: number;
    capitalTerceros: number;
    recursosPropios: number;
    
    constructor(){
        this.aplicaCredito = false;
        this.credito = new Credito();
        
        this.incentivo = 0;
        this.capitalTerceros = 0;
        this.recursosPropios = 0;
    }
}