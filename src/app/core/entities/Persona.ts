import { TipoEntidad } from "./Enumeradores/TipoEntidad";
import { TipoPersona } from "./Enumeradores/TipoPersona";

export class Persona {
    tipoDePersona?: TipoPersona;
    tipoDeEntidad?: TipoEntidad; 

    constructor(){
    }
}