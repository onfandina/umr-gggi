import { Especie } from "./Especie";
import { Periodo } from "./Periodo";
import { Producto } from "./Producto";

export class Produccion {
    Cantidad: number;
    Producto: Producto;
    Especie: Especie;
    Periodo: Periodo;

    constructor(){
        this.Cantidad = 0;
        this.Producto = new Producto();
        this.Especie = new Especie();
        this.Periodo = new Periodo();
    }
}