export class Producto {
    Nombre: string;
    Volumen: number;
    FactorAprovechamientoEfectivo: number;
    PrecioCiudadPrincipal: number;
    PrecioCiudadIntermedia: number;
    PrecioBordeCarretera: number;
    PorcentajeCiudadPrincipal: number;
    PorcentajeCiudadIntermedia: number;
    PorcentajeBordeCarretera: number;
    CostoTransPrimaria: number;
    SistemaTransPrimaria: string;
    
    constructor(){
        this.Nombre = '';
        this.Volumen = 0;
        this.FactorAprovechamientoEfectivo = 0;
        this.PrecioCiudadPrincipal = 0;
        this.PrecioCiudadIntermedia = 0;
        this.PrecioBordeCarretera = 0;
        this.PorcentajeCiudadPrincipal = 0;
        this.PorcentajeCiudadIntermedia = 0;
        this.PorcentajeBordeCarretera = 0;
        this.CostoTransPrimaria = 0;
        this.SistemaTransPrimaria = '';
    }
}