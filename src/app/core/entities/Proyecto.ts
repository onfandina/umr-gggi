import { Costo } from "./Costos/Costo";
import { Especie } from "./Especie";
import { Establecimiento } from "./Establecimiento";

export class Proyecto {
    Duracion: number;
    AreaTotal: number;
    TiempoTotal: number;
    SistemaDeSiembra: string;
    ArbolesXHectarea: number;
    DistanciaDeSiembra: number;
    EstimacionPerdidaArboles: number;
    MetodoPreparacion: string;
    Establecimientos: Establecimiento[];
    Dtf: number;
    Tir: number;
    IngresoMinimoMensualEsperado: number;
    UtilidadBruta: number; 
    MargenBruto: number;
    ProvisionImpuestos: number;
    UtilidadDespuesImpuestos: number;
    MargenNeto: number;
    Vpn: number;
    FuncionOptimizarTir: number;
    FuncionOptimizarMinimoIngreso: number;
    IngresoMensualPromedioPercibido: number;
    Costos: Costo[];
    AreaMinimaTir: number;
    AreaMinimaIngreso: number;
    InversionMinimaRequerida: number;
    UtilidadesAnios: number[];

    constructor(){
        this.Duracion = 0;
        this.AreaTotal = 0;
        this.TiempoTotal = 0;
        this.SistemaDeSiembra = '';
        this.DistanciaDeSiembra = 0;
        this.ArbolesXHectarea = 0;
        this.EstimacionPerdidaArboles = 0;
        this.MetodoPreparacion = '';
        this.Establecimientos = [];
        this.Dtf = 0;
        this.Tir = 0;
        this.IngresoMinimoMensualEsperado = 0;
        this.UtilidadBruta = 0;
        this.MargenBruto = 0;
        this.ProvisionImpuestos = 0;
        this.UtilidadDespuesImpuestos = 0;
        this.MargenNeto = 0;
        this.Vpn = 0;
        this.FuncionOptimizarTir = 0;
        this.FuncionOptimizarMinimoIngreso = 0;
        this.IngresoMensualPromedioPercibido = 0;
        this.Costos = [];
        this.AreaMinimaTir = 0;
        this.AreaMinimaIngreso = 0;
        this.InversionMinimaRequerida = 0;
        this.UtilidadesAnios = [];
    }
}