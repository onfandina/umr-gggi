export class Ubicacion {
    Departamento: string;
    Municipio: string;
    RangoAlturaNivelMar: string;
    RangoPrecipitacion: string;
    RangoTemperatura: string;
    RangoPendiente: string;

    constructor(){
        this.Departamento = '';
        this.Municipio = '';
        this.RangoAlturaNivelMar = '';
        this.RangoPrecipitacion = '';
        this.RangoTemperatura = '';
        this.RangoPendiente = '';
    }
}