import { Persona } from "./Persona";

export class InformacionGeneral {
    
    Persona: Persona;
    AreaTotalPredio: number;
    AreaTotalProyecto: number;
    SistemaSiembra: string;
    DistanciaSiembra: number;
    TasaPerdidaEstimada: number;
    TasaResiembra: number;
    TiempoEstablecimiento: number;
    UVT: number;
    ArbolesHectarea: number;

    constructor(){
        this.Persona = new Persona();
        this.AreaTotalPredio = 0;
        this.AreaTotalProyecto = 0;
        this.SistemaSiembra = '';
        this.DistanciaSiembra = 0;
        this.TasaPerdidaEstimada = 0;
        this.TasaResiembra = 0;
        this.TiempoEstablecimiento = 0;
        this.UVT = 0;
        this.ArbolesHectarea = 0;
    }
}