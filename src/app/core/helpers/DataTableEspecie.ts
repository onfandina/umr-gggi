import { DataSource } from "@angular/cdk/table";
import { Observable, ReplaySubject } from "rxjs";

export class CustomDataTable<T> extends DataSource<T> {
  
    
    private _dataStream = new ReplaySubject<T[]>();

    constructor(especies: T[]){
      super();
      this.setData(especies);
    }

    setData(data: T[]){
      this._dataStream.next(data);
    }
  
    connect(): Observable<T[]>{
        return this._dataStream;
    }
    disconnect(){}
  }