import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function requireTrue(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const errorDeValidacion = control.value != true;
      return errorDeValidacion ? {error: {value: "El valor debe ser true"}} : null;
    };
  }