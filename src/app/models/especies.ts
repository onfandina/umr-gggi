import { ModeloEspecie } from "../core/entities/ModeloEspecie";

//Listado de especies y sus atributos por defecto

const acaciaMagnum: ModeloEspecie = {
    Nombre: 'Acacia mangium',
    Turno: '12',
    Rendimiento: '26-30',
    LimiteInferiorTemperatura: 12,
    LimiteSuperiorTemperatura: 34,
    LimiteInferiorPrecipitacion: 700,
    LimiteSuperiorPrecipitacion: 4500
} 

const bombacopsisQuinata: ModeloEspecie = {
    Nombre: 'Bombacopsis quinata',
    Turno: '20',
    Rendimiento: 'menor a 18',
    LimiteInferiorTemperatura: 24,
    LimiteSuperiorTemperatura: 28,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 3000
} 

const cupressusLusitanica: ModeloEspecie = {
    Nombre: 'Cupressus lusitanica',
    Turno: '12',
    Rendimiento: '20 a 30',
    LimiteInferiorTemperatura: 14,
    LimiteSuperiorTemperatura: 20,
    LimiteInferiorPrecipitacion: 1500,
    LimiteSuperiorPrecipitacion: 3000
} 

const eucalyptusGlobulus: ModeloEspecie = {
    Nombre: 'Eucalyptus globulus',
    Turno: '8 a 12',
    Rendimiento: '15 a 35',
    LimiteInferiorTemperatura: 12,
    LimiteSuperiorTemperatura: 18,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 2500
} 

const eucalyptusCamaldulensis: ModeloEspecie = {
    Nombre: 'Eucalyptus camaldulensis',
    Turno: '10 a 12',
    Rendimiento: '25 a 30',
    LimiteInferiorTemperatura: 15,
    LimiteSuperiorTemperatura: 25,
    LimiteInferiorPrecipitacion: 600,
    LimiteSuperiorPrecipitacion: 1700
} 

const eucalyptusGrandis: ModeloEspecie = {
    Nombre: 'Eucalyptus grandis',
    Turno: '8 a 12',
    Rendimiento: '25 a 40',
    LimiteInferiorTemperatura: 20,
    LimiteSuperiorTemperatura: 26,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 3000
} 

const eucalyptusPellita: ModeloEspecie = {
    Nombre: 'Eucalyptus pellita',
    Turno: '12',
    Rendimiento: '15 a 20',
    LimiteInferiorTemperatura: 19,
    LimiteSuperiorTemperatura: 29,
    LimiteInferiorPrecipitacion: 900,
    LimiteSuperiorPrecipitacion: 3000
} 

const eucalyptusTereticornis: ModeloEspecie = {
    Nombre: 'Eucalyptus tereticornis',
    Turno: '8 a 12',
    Rendimiento: '20',
    LimiteInferiorTemperatura: 15,
    LimiteSuperiorTemperatura: 25,
    LimiteInferiorPrecipitacion: 500,
    LimiteSuperiorPrecipitacion: 1500
} 

const eucalyptusUrograndis: ModeloEspecie = {
    Nombre: 'Eucalyptus urograndis',
    Turno: '10',
    Rendimiento: '25',
    LimiteInferiorTemperatura: 24,
    LimiteSuperiorTemperatura: 24,
    LimiteInferiorPrecipitacion: 800,
    LimiteSuperiorPrecipitacion: 1200
} 

const gmelinaArborea: ModeloEspecie = {
    Nombre: 'Gmelina arborea',
    Turno: '10 a 14',
    Rendimiento: '20 a 25',
    LimiteInferiorTemperatura: 24,
    LimiteSuperiorTemperatura: 28,
    LimiteInferiorPrecipitacion: 2000,
    LimiteSuperiorPrecipitacion: 3000
} 

const heveaBrasiliensis: ModeloEspecie = {
    Nombre: 'Hevea brasiliensis',
    Turno: '20 a 30',
    LimiteInferiorTemperatura: 23,
    LimiteSuperiorTemperatura: 30,
    LimiteInferiorPrecipitacion: 2000,
    LimiteSuperiorPrecipitacion: 4000
} 

const ochromaPyramidale: ModeloEspecie = {
    Nombre: 'Ochroma pyramidale',
    Turno: '4 a 7',
    LimiteInferiorTemperatura: 22,
    LimiteSuperiorTemperatura: 27,
    LimiteInferiorPrecipitacion: 1500,
    LimiteSuperiorPrecipitacion: 3000
} 

const pinusCaribaea: ModeloEspecie = {
    Nombre: 'Pinus caribaea',
    Turno: '15',
    Rendimiento: '10 a 15',
    LimiteInferiorTemperatura: 22,
    LimiteSuperiorTemperatura: 28,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 2500
} 

const pinusMaximinoi: ModeloEspecie = {
    Nombre: 'Pinus maximinoi',
    Turno: '15',
    Rendimiento: '10',
    LimiteInferiorTemperatura: 18,
    LimiteSuperiorTemperatura: 22,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 2000
} 

const pinusOocarpa: ModeloEspecie = {
    Nombre: 'Pinus oocarpa',
    Turno: '15',
    Rendimiento: '10 a 15',
    LimiteInferiorTemperatura: 17,
    LimiteSuperiorTemperatura: 24,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 2000
} 
const pinusPatula: ModeloEspecie = {
    Nombre: 'Pinus patula',
    Turno: '16',
    Rendimiento: '12 a 22',
    LimiteInferiorTemperatura: 10,
    LimiteSuperiorTemperatura: 19,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 2500
} 

const pinusTecunumanii: ModeloEspecie = {
    Nombre: 'Pinus tecunumanii',
    Turno: '15 a 25',
    Rendimiento: '30 a 40',
    LimiteInferiorTemperatura: 14,
    LimiteSuperiorTemperatura: 22,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 2400
} 

const tectonaGrandis: ModeloEspecie = {
    Nombre: 'Tectona grandis',
    Turno: '25 a 30',
    Rendimiento: '7 a 10',
    LimiteInferiorTemperatura: 24,
    LimiteSuperiorTemperatura: 28,
    LimiteInferiorPrecipitacion: 1000,
    LimiteSuperiorPrecipitacion: 2000
} 

const cordiaAlliodora: ModeloEspecie = {
    Nombre: 'Cordia alliodora',
    Turno: '15 a 20',
    Rendimiento: '8 a 20',
    LimiteInferiorTemperatura: 17,
    LimiteSuperiorTemperatura: 28,
    LimiteInferiorPrecipitacion: 1400,
    LimiteSuperiorPrecipitacion: 3400
} 



export const especies = [
    acaciaMagnum,
    bombacopsisQuinata,
    cupressusLusitanica,
    eucalyptusGlobulus,
    eucalyptusCamaldulensis,
    eucalyptusGrandis,
    eucalyptusPellita,
    eucalyptusTereticornis,
    eucalyptusUrograndis,
    gmelinaArborea,
    heveaBrasiliensis,
    ochromaPyramidale,
    pinusCaribaea,
    pinusMaximinoi,
    pinusOocarpa,
    pinusPatula,
    pinusTecunumanii,
    tectonaGrandis,
    cordiaAlliodora,
];