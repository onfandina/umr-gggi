export const sistemasSiembra = ['Cuadrado', 'Triángulo (tres bolillos)'];
export const sistemasSiembraRestauracion = ['Árboles en diferentes distancias', 'Cuadrado','Triangular','En cercas o barreras rompevientos'];
export const sistemasSiembraSilvopastoril = ['Cercas vivas en zonas ganaderas', 'Rompevientos línea triples', 'Rompevientos línea dobles', 
'Sistema Silvopastoril intensivo con árboles maderables', 'Pastoreo bajo plantaciones comerciales'];
export const sistemasSiembraAgroforestal = ['Café con sombrío arbóreo', 'Cacao con sombrío arbóreo permamente', 'Sistemas Taungya', 'Árboles como linderos o cercas vivas en cultivos perennes'];