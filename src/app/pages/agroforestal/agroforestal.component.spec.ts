import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgroforestalComponent } from './agroforestal.component';

describe('AgroforestalComponent', () => {
  let component: AgroforestalComponent;
  let fixture: ComponentFixture<AgroforestalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgroforestalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgroforestalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
