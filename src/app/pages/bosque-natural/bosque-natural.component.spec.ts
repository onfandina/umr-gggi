import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BosqueNaturalComponent } from './bosque-natural.component';

describe('BosqueNaturalComponent', () => {
  let component: BosqueNaturalComponent;
  let fixture: ComponentFixture<BosqueNaturalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BosqueNaturalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BosqueNaturalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
