export const models = [
  {
    name: 'Plantaciones Forestales Comerciales',
    href: '/pfc',
  },
  {
    name: 'Restauración',
    href: '/restauracion',
  },
  {
    name: 'Silvopastoril',
    href: '/silvopastoril',
  },
  {
    name: 'Manejo Forestal Sostenible',
    href: '/mfs',
  },
  {
    name: 'Agroforestal',
    href: '/agroforestal',
  },
];
