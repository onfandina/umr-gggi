import { Component, OnInit } from '@angular/core';
import { models } from './config';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {
  models = models;

  constructor() {}

  ngOnInit(): void {}
}
