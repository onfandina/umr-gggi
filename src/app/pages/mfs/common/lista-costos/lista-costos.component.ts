import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MatTableDataSource} from "@angular/material/table";
import {Costo} from "../../../../core/entities/Costos/Costo";

@Component({
  selector: 'mfs-lista-costos',
  templateUrl: './lista-costos.component.html',
  styleUrls: ['./lista-costos.component.scss']
})

export class ListaCostosComponent implements OnInit {
  @Input() default?: Costo[];
  @Input() label: string = 'Costos';
  @Output() guardarCostos = new EventEmitter<Costo[]>();

  formGroupFields = new FormGroup({
    nombre: new FormControl(''),
    valor: new FormControl(''),
  });

  actualCosto = -1;
  verFormulario = false;
  listaCostos: Costo[] = [];

  tablaCostos: string[] = [
    'nombre',
    'valor',
    'operaciones',
  ];
  dataSourceCostos = new MatTableDataSource<any>();


  ngOnInit(): void {
    if (this.default) {
      this.listaCostos = this.default;
      this.actualizarTabla();
    }
  }

  agregarCosto() {
    this.verFormulario = true;
  }

  crearCosto() {
    let data = this.formGroupFields.value;
    const costo = new Costo();
    costo.Nombre = data.nombre;
    costo.Valor = data.valor;

    if (this.actualCosto < 0) {
      this.listaCostos.push(costo);
    } else {
      this.listaCostos[this.actualCosto] = costo;
    }
    this.actualizarTabla();
    this.reiniciar();
  }

  editarCosto(index: number) {
    const costo = this.listaCostos[index];
    this.actualCosto = index;
    this.formGroupFields.reset({
      nombre: costo.Nombre,
      valor: costo.Valor,
    });

    this.verFormulario = true;
    this.actualizarTabla();
  }

  eliminarCosto(index: number) {
    if (confirm("Está seguro de eliminar el costo: " + this.listaCostos[index].Nombre + "?")) {

      this.listaCostos.splice(index, 1);
      this.actualizarTabla();
    }
  }

  actualizarTabla() {
    this.dataSourceCostos.data = this.listaCostos.map(costo => ({
      nombre: costo.Nombre,
      valor: costo.Valor,
    }));
    this.guardarCostos.emit(this.listaCostos);
  }

  reiniciar() {
    this.formGroupFields.reset();
    this.verFormulario = false;
    this.actualCosto = -1;
  }

}


