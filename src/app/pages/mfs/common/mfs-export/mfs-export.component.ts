import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Mfs} from "../../../../core/entities/MFS/Mfs";
import {BLMfsExport} from "../../../../core/business/MFS/BLMfsExport";

@Component({
  selector: 'mfs-export',
  templateUrl: './mfs-export.component.html',
  styleUrls: ['./mfs-export.component.scss']
})

export class MfsExportComponent implements OnInit {
  @Input() mfsData = new Mfs();


  ngOnInit(): void {
  }

  downloadMfs() {
    const blob = new Blob([BLMfsExport.export(this.mfsData)], {type: 'text/json'});
    const url = window.URL.createObjectURL(blob);
    const element = document.createElement('a');
    const date = new Date();
    element.setAttribute('href', url);
    element.setAttribute('download', `mfs_${date.toISOString()}.json`);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

}
