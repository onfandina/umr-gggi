import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Mfs} from "../../../../core/entities/MFS/Mfs";
import {BLMfsExport} from "../../../../core/business/MFS/BLMfsExport";

@Component({
  selector: 'mfs-import',
  templateUrl: './mfs-import.component.html',
  styleUrls: ['./mfs-import.component.scss']
})

export class MfsImportComponent implements OnInit {
  @Output() eventoImportarMfs = new EventEmitter<Mfs>();

  ngOnInit(): void {
  }

  subirMfsJson(event: any): void {
    const file = event.target.files[0];
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      if (fileReader.result) {
        const mfs = BLMfsExport.import(fileReader.result as string);
        this.eventoImportarMfs.emit(mfs);
      }
    };
    fileReader.readAsText(file);
  }
}
