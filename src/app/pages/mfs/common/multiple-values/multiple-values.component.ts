import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'mfs-multiple-values',
  templateUrl: './multiple-values.component.html',
  styleUrls: ['./multiple-values.component.scss']
})

export class MultipleValuesComponent implements OnInit {
  @Input() size = 0;
  @Input() start = 1;
  @Input() label: string = '';
  @Input() inputLabel: string = '';
  @Input() control: Controls = Controls.NUMBER;
  @Input() default: Array<string | number> = [];
  @Output() eventoMultipleValues = new EventEmitter<any>();

  values: (string | number)[] = [];
  type = 'number';
  panelOpenState = false;

  formValues = new FormGroup({});

  ngOnInit(): void {
    this.updateFormFields();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.updateFormFields();
  }

  updateFormFields(): void {
    this.type = this.control === Controls.NUMBER ? 'number' : 'text';
    this.formValues = new FormGroup({});
    for (let i = 0; i < this.size; i++) {
      this.formValues.addControl(`control${i}`, new FormControl(this.default[i]));
    }
    this.values = new Array(this.size).fill(0);
  }

  updateValues() {
    let values = Object.values(this.formValues.value);
    if (this.control === Controls.NUMBER) {
      values = values.map(Number);
    }
    this.eventoMultipleValues.emit(values);
  }

}

export enum Controls {
  NUMBER = 'NUMBER',
  STRING = 'STRING',
}
