import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MatTableDataSource} from "@angular/material/table";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Sistema} from "../../../../core/entities/MFS/Sistema";
import {Material} from "../../../../core/entities/MFS/Material";
import {PorcentajeSistema} from "../../../../core/entities/MFS/PorcentajeSistema";

@Component({
  selector: 'mfs-porcentajes-asignacion-sistema',
  templateUrl: './porcentajes-asignacion.component.html',
  styleUrls: ['./porcentajes-asignacion.component.scss']
})

export class PorcentajesAsignacionComponent implements OnInit {
  @Input() default?: PorcentajeSistema[];
  @Input() materiales: Material[] = [];
  @Input() sistemas: Sistema[] = [];
  @Output() porcentajeAsignacionEvent = new EventEmitter<PorcentajeSistema[]>();

  porcentajesSistema: PorcentajeSistema[] = [];
  formulario = new FormGroup({});
  filas = Array<any>();
  dataSource = new MatTableDataSource<any>();

  tabla: string[] = [
    'producto',
    'porcentaje',
  ];

  constructor(
    private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.initForm();
    if (this.default) {
      this.default.forEach(porcentaje => {
        this.formulario.get(`porcentaje_${porcentaje.Sistema.Nombre}_${porcentaje.Material.Nombre}`)?.setValue(porcentaje.Porcentaje * 100);
      });
      this.onKey('');
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.sistemas) {
      this.sistemas = changes.sistemas.currentValue;
      this.initForm(this.formulario.value);
    }
    if (changes.materiales) {
      this.materiales = changes.materiales.currentValue;
      this.initForm(this.formulario.value);
    }
  }

  initForm(formValues?: any) {
    const porcentajesSistema: PorcentajeSistema[] = [];
    this.filas = [];
    this.formulario = new FormGroup({});
    this.sistemas.forEach(sistema => {
      const fila = {
        nombre: sistema.Nombre,
        campos: new Array<any>()
      };
      sistema.Rendimientos.forEach(rendimiento => {
        const porcentaje = new PorcentajeSistema();
        porcentaje.Sistema = sistema;
        porcentaje.Material = rendimiento.Material;
        porcentaje.Porcentaje = (formValues?.[`porcentaje_${porcentaje.Sistema.Nombre}_${porcentaje.Material.Nombre}`] || 0) / 100;
        porcentaje.CostoAprovechamiento = rendimiento.Rendimiento;
        porcentajesSistema.push(porcentaje);
        fila.campos.push({
          producto: porcentaje.Material.Nombre,
          nombreCampo: `porcentaje_${sistema.Nombre}_${porcentaje.Material.Nombre}`
        });
        this.formulario.addControl(
          `porcentaje_${sistema.Nombre}_${porcentaje.Material.Nombre}`,
          new FormControl(porcentaje.Porcentaje ? porcentaje?.Porcentaje * 100 : undefined)
        );
      });
      this.filas.push(fila);
    });
    this.dataSource.data = this.materiales.map(material => ({producto: material.Nombre, porcentaje: 0}));
    this.porcentajesSistema = porcentajesSistema;
    this.onKey('');
  }

  onKey(event: any) {
    this.porcentajesSistema.forEach(porcentaje => {
      porcentaje.Porcentaje = (this.formulario.get(`porcentaje_${porcentaje.Sistema.Nombre}_${porcentaje.Material.Nombre}`)?.value || 0) / 100;
    });
    this.dataSource.data = this.materiales.map(material => ({
      producto: material.Nombre,
      porcentaje: this.porcentajesSistema.filter(porcentaje => porcentaje.Material === material).reduce((total, porcentaje) => total + porcentaje.Porcentaje, 0) * 100
    }));
  }

  onSubmit(): void {
    this.porcentajesSistema.forEach(porcentaje => {
      porcentaje.Porcentaje = (this.formulario.get(`porcentaje_${porcentaje.Sistema.Nombre}_${porcentaje.Material.Nombre}`)?.value || 0) / 100;
    });
    this.porcentajeAsignacionEvent.emit(this.porcentajesSistema);
  }
}
