import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {Concepto} from "../models/concepto.model";
import {ListaCostos} from "../../../core/entities/MFS/Costos/ListaCostos";
import {CostosTierra} from "../../../core/entities/MFS/Costos/CostosTierra";
import {CostosCaminos} from "../../../core/entities/MFS/Costos/CostosCaminos";
import {Material} from "../../../core/entities/MFS/Material";
import {AsistenciaTecnica} from "../../../core/entities/MFS/Costos/AsistenciaTecnica";
import {OtrosGastos} from "../../../core/entities/MFS/Costos/OtrosGastos";
import {Sistema} from "../../../core/entities/MFS/Sistema";
import {TransporteMenor} from "../../../core/entities/MFS/TransporteMenor";
import {TransportesMayores} from "../../../core/entities/MFS/TransportesMayores";
import {Aprovechamiento} from "../../../core/entities/MFS/Costos/Aprovechamiento";
import {Persona} from "../../../core/entities/Persona";
import {PorcentajeSistema} from "../../../core/entities/MFS/PorcentajeSistema";
import {Costo} from "../../../core/entities/Costos/Costo";

@Component({
  selector: 'mfs-costos',
  templateUrl: './costos.component.html',
  styleUrls: ['./costos.component.scss']
})

export class CostosComponent implements OnInit {
  @Input() default?: ListaCostos;
  @Input() ucas: number = 1;
  @Input() persona?: Persona;
  @Input() materiales: Material[] = [];
  @Input() sistemasTransporteMenor: Sistema[] = [];
  @Input() sistemasTransformacion: Sistema[] = [];
  @Input() ciudades = {
    principal: '',
    intermedia: '',
  };
  @Input() transporteMenor = {
    productos: Array<any>(),
    tipos: Array<any>(),
  };
  @Input() costosDeAprovechamiento = {
    conceptos: Array<Concepto>()
  };
  @Output() costosEvent = new EventEmitter<ListaCostos>();

  listaCostos = new ListaCostos();

  costos = {
    transporteMayor: {},
    costoDeLaTierra: {},
    costosDeAprovechamiento: Array<any>(),
    transporteMenor: {},
    impuestosOtrosCostos: {},
  };

  ngOnInit(): void {
    if (this.default) {
      this.listaCostos = this.default;
    }
  }

  actualizarAsistenciaTecnica(asistenciaTecnica: AsistenciaTecnica): void {
    this.listaCostos.AsistenciaTecanica = asistenciaTecnica;
    this.costosEvent.emit(this.listaCostos);
  }

  actualizarTransporteMayor(transporteMayor: TransportesMayores[]): void {
    this.listaCostos.TransporteMayor = transporteMayor;
    this.costosEvent.emit(this.listaCostos);
  }

  actualizarMaquinaria(costos: Costo[]): void {
    this.listaCostos.Maquinaria.Costos = costos;
    this.costosEvent.emit(this.listaCostos);
  }

  actualizarTierra(costoDeLaTierra: CostosTierra): void {
    this.listaCostos.Tierra = costoDeLaTierra;
    this.costosEvent.emit(this.listaCostos);
  }

  actualizarCostosCaminos(costosCaminos: CostosCaminos) {
    this.listaCostos.Caminos = costosCaminos;
    this.costosEvent.emit(this.listaCostos);
  }

  actualizarTransporteMenor(transporteMenor: PorcentajeSistema[]): void {
    this.listaCostos.TransporteMenor = transporteMenor;
    this.costosEvent.emit(this.listaCostos);
  }

  actualizarAprovechamiento(aprovechamiento: Aprovechamiento): void {
    this.listaCostos.Aprovechamiento = aprovechamiento;
    this.costosEvent.emit(this.listaCostos);
  }

  actualizarImpuestosOtrosCostos(otrosCostos: OtrosGastos): void {
    this.listaCostos.Otros = otrosCostos;
    this.costosEvent.emit(this.listaCostos);
  }

  guardar() {
    this.costosEvent.emit(this.listaCostos);
  }
}
