import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Controls} from "../../../common/multiple-values/multiple-values.component";
import {Costo} from "../../../../../core/entities/Costos/Costo";
import {AsistenciaTecnica} from "../../../../../core/entities/MFS/Costos/AsistenciaTecnica";
import {CostosComputados} from "../../../../../core/entities/MFS/Costos/CostosComputados";

@Component({
  selector: 'mfs-costos-asistencia-tecnica',
  templateUrl: './asistencia-tecnica.component.html',
  styleUrls: ['./asistencia-tecnica.component.scss']
})

export class AsistenciaTecnicaComponent implements OnInit {
  @Input() default?: AsistenciaTecnica;
  @Input() ucas = 1;
  @Output() eventoAsistencia = new EventEmitter<AsistenciaTecnica>();

  inputType = Controls.NUMBER;

  asistencia = new AsistenciaTecnica();

  ngOnInit(): void {
    if (this.default) {
      this.asistencia = this.default;
    }
  }

  actualizarPmf(values: number[]) {
    this.asistencia.PlanManejoForestal = values;
  }

  actualizarCampamento(values: number[]) {
    this.asistencia.ConstruccionCampamento = values;
  }

  actualizarCostosPreoperativos(values: Costo[]) {
    this.asistencia.CostosPreoperativos = new CostosComputados(values);
  }

  actualizarCostos(values: Costo[]) {
    this.asistencia.AsistenciaAnual = new CostosComputados(values);
  }

  //Guarda la información en el formulario
  guardarAsistencia() {
    this.eventoAsistencia.emit(this.asistencia);
  }

}
