import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CostosCaminos} from "../../../../../core/entities/MFS/Costos/CostosCaminos";
import {FormControl, FormGroup} from "@angular/forms";
import {CostoCamino} from "../../../../../core/entities/MFS/Costos/CostoCamino";

@Component({
  selector: 'mfs-costos-construccion-y-mantenimiento',
  templateUrl: './construccion-y-mantenimiento.component.html',
  styleUrls: ['./construccion-y-mantenimiento.component.scss']
})

export class ConstruccionYMantenimientoComponent implements OnInit {

  @Input() default?: CostosCaminos;
  @Output() costosCaminosEvent = new EventEmitter<CostosCaminos>();
  formulario = new FormGroup({
    construccionValorKm: new FormControl(''),
    construccionCantidadKm: new FormControl(''),
    mantenimientoValorKm: new FormControl(''),
    mantenimientoCantidadKm: new FormControl(''),
  });
  total = 0;

  ngOnInit(): void {
    if(this.default) {
      this.formulario.get('construccionValorKm')?.setValue(this.default.Construccion.Valor)
      this.formulario.get('construccionCantidadKm')?.setValue(this.default.Construccion.Cantidad)
      this.formulario.get('mantenimientoValorKm')?.setValue(this.default.Mantenimiento.Valor)
      this.formulario.get('mantenimientoCantidadKm')?.setValue(this.default.Mantenimiento.Cantidad)
      this.onKey('')
    }
  }

  onKey(event: any) { // without type info
    const formValues = this.formulario.value;
    this.total = (formValues.construccionValorKm * formValues.construccionCantidadKm) + (formValues.mantenimientoValorKm * formValues.mantenimientoCantidadKm);
  }

  onSubmit(): void {
    const {
      construccionValorKm,
      construccionCantidadKm,
      mantenimientoValorKm,
      mantenimientoCantidadKm
    } = this.formulario.value;
    const caminos = new CostosCaminos(
      new CostoCamino(construccionValorKm, construccionCantidadKm),
      new CostoCamino(mantenimientoValorKm, mantenimientoCantidadKm),
      this.total
    );
    this.costosCaminosEvent.emit(caminos);
  }


}
