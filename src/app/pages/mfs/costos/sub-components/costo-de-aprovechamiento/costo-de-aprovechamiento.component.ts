import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {formatCurrency} from "@angular/common";
import {Sistema} from "../../../../../core/entities/MFS/Sistema";
import {Material} from "../../../../../core/entities/MFS/Material";
import {CostoAprovechamiento} from "../../../../../core/entities/MFS/Costos/CostoAprovechamiento";
import {Aprovechamiento} from "../../../../../core/entities/MFS/Costos/Aprovechamiento";

@Component({
  selector: 'mfs-costos-costo-de-aprovechamiento',
  templateUrl: './costo-de-aprovechamiento.component.html',
  styleUrls: ['./costo-de-aprovechamiento.component.scss']
})

export class CostoDeAprovechamientoComponent implements OnInit {
  @Input() default?: Aprovechamiento;
  @Output() aprovecamientoEvent = new EventEmitter<Aprovechamiento>();
  @Input() materiales: Material[] = [];

  aprovechamiento: Aprovechamiento = new Aprovechamiento();

  tabla: string[] = [
    'tipoMaderaProducto',
    'costosCalculados'
  ];

  dataSource = new MatTableDataSource<any>();

  ngOnInit(): void {
    if (this.default) {
      this.aprovechamiento = this.default;
    }
    this.actualizarTabla();
  }

  guardarSistema(sistema: Sistema) {
    this.aprovechamiento.Sistema = sistema;
    this.actualizarTabla();
    this.aprovechamiento.Costos = sistema.Rendimientos.map(rendimiento => new CostoAprovechamiento(rendimiento.Material.Nombre, rendimiento.Rendimiento, rendimiento.Material));
    this.aprovecamientoEvent.emit(this.aprovechamiento);
  }

  actualizarTabla() {

    let rows = Array<any>();
    this.aprovechamiento.Sistema.Rendimientos.forEach(rendimiento => {
      rows.push({
        tipoMaderaProducto: rendimiento.Material.Nombre,
        costosCalculados: formatCurrency(rendimiento.Rendimiento, 'en-CO', '$')
      });
    });
    this.dataSource.data = rows;

  }
}
