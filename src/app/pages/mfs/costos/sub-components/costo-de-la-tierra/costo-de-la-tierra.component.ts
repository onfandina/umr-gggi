import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {CostosTierra} from "../../../../../core/entities/MFS/Costos/CostosTierra";

@Component({
  selector: 'mfs-costos-costo-de-la-tierra',
  templateUrl: './costo-de-la-tierra.component.html',
  styleUrls: ['./costo-de-la-tierra.component.scss']
})

export class CostoDeLaTierraComponent implements OnInit {
  @Input() default?: CostosTierra;
  @Output() tierraEvent = new EventEmitter<CostosTierra>();

  formulario = new FormGroup({
    tipo: new FormControl(''),
    costo: new FormControl(),
  });

  ngOnInit(): void {
    if (this.default) {
      if (this.default.terreno > 0) {
        this.formulario.get('tipo')?.setValue('terreno');
        this.formulario.get('costo')?.setValue(this.default.terreno);
      } else if (this.default.derechosUso > 0) {
        this.formulario.get('tipo')?.setValue('derechos');
        this.formulario.get('costo')?.setValue(this.default.derechosUso);
      } else if (this.default.oportunidad > 0) {
        this.formulario.get('tipo')?.setValue('oportunidad');
        this.formulario.get('costo')?.setValue(this.default.oportunidad);
      }
    }
  }

  onSubmit(): void {
    const tierra = new CostosTierra();
    switch (this.formulario.get('tipo')?.value) {
      case 'terreno':
        tierra.terreno = this.formulario.get('costo')?.value;
        break;
      case 'derecho':
        tierra.derechosUso = this.formulario.get('costo')?.value;
        break;
      case 'oportunidad':
        tierra.oportunidad = this.formulario.get('costo')?.value;
        break;
    }
    this.tierraEvent.emit(tierra);
  }
}
