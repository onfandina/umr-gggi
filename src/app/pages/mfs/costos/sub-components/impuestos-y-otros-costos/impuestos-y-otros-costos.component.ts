import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MatSnackBar} from '@angular/material/snack-bar';
import {OtrosGastos} from "../../../../../core/entities/MFS/Costos/OtrosGastos";
import {TiposCosto} from "../../../../../core/entities/MFS/Enums/TiposCosto";
import {Costo} from "../../../../../core/entities/Costos/Costo";
import {Persona} from "../../../../../core/entities/Persona";
import {TipoPersona} from "../../../../../core/entities/Enumeradores/TipoPersona";
import {TipoEntidad} from "../../../../../core/entities/Enumeradores/TipoEntidad";

@Component({
  selector: 'mfs-costos-impuestos-y-otros-costos',
  templateUrl: './impuestos-y-otros-costos.component.html',
  styleUrls: ['./impuestos-y-otros-costos.component.scss']
})

export class ImpuestosYOtrosCostosComponent implements OnInit {
  @Input() default?: OtrosGastos;
  @Input() ucas = 1;
  @Input() persona?: Persona;
  @Output() impuestosEvent = new EventEmitter<OtrosGastos>();

  formulario = new FormGroup({
    tipoDeAdministracion: new FormControl(''),
    administracionDeLosCostos: new FormControl(''),
    medidasDeCompensacion: new FormControl(''),
    tasaDeRenta: new FormControl(''),
    otrosCostos: new FormControl(''),
  });
  tieneRenta = false;
  carbono: number[] = [];
  otrosCostos: Costo[][] = [];

  constructor(
    private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.otrosCostos = new Array(this.ucas).fill(0).map(() => []);
    this.tieneRenta = (this.persona && this.persona.tipoDePersona === TipoPersona.Juridica && this.persona.tipoDeEntidad === TipoEntidad.ConAnimoLucro) || false;
    if (this.default) {
      const defecto = this.default
      this.formulario.get('administracionDeLosCostos')?.setValue(defecto.Administracion.Valor);
      if (defecto.Administracion.Tipo === TiposCosto.PORCENTUAL) {
        this.formulario.get('tipoDeAdministracion')?.setValue(defecto.Administracion.Tipo === TiposCosto.PORCENTUAL);
        this.formulario.get('administracionDeLosCostos')?.setValue(defecto.Administracion.Valor * 100);
      }
      this.formulario.get('medidasDeCompensacion')?.setValue(defecto.MedidasCompensacion);
      this.formulario.get('tasaDeRenta')?.setValue(defecto.TasaRenta.Valor * 100);
      this.carbono = defecto.Carbono;
      this.otrosCostos = this.otrosCostos.map((costos, i) => {
        return defecto.OtrosCostos[i] || costos;
      });
    }
  }

  actualizarCarbono(valores: number[]) {
    this.carbono = valores;
  }

  actualizarCostos(costos: Costo[], index = 0) {
    this.otrosCostos[index] = costos;
  }

  onSubmit(): void {
    const {
      tipoDeAdministracion,
      administracionDeLosCostos,
      medidasDeCompensacion,
      tasaDeRenta
    } = this.formulario.value;

    const otrosGastos = new OtrosGastos();
    otrosGastos.Administracion.Tipo = tipoDeAdministracion ? TiposCosto.PORCENTUAL : TiposCosto.FIJO;
    otrosGastos.Administracion.Valor = tipoDeAdministracion ? administracionDeLosCostos / 100 : administracionDeLosCostos;
    otrosGastos.Carbono = this.carbono;
    otrosGastos.TasaRenta.Valor = tasaDeRenta / 100;
    otrosGastos.MedidasCompensacion = medidasDeCompensacion;
    otrosGastos.OtrosCostos = this.otrosCostos;

    this.impuestosEvent.emit(otrosGastos);

  }
}
