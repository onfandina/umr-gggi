import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Ubicaciones} from "../../../../../core/entities/MFS/Enums/Ubicaciones";
import {TiposTrasportesMayores} from "../../../../../core/entities/MFS/Enums/TiposTrasportesMayores";
import {TransporteMayor} from "../../../../../core/entities/MFS/TransporteMayor";
import {TransportesMayores} from "../../../../../core/entities/MFS/TransportesMayores";

@Component({
  selector: 'mfs-costos-transporte-mayor',
  templateUrl: './transporte-mayor.component.html',
  styleUrls: ['./transporte-mayor.component.scss']
})

export class TransporteMayorComponent implements OnInit {
  @Input() default?: TransportesMayores[];
  @Input() ciudades = {
    principal: '',
    intermedia: '',
  };
  @Output() transporteMayorEvent = new EventEmitter<TransportesMayores[]>();

  formularioSuffix: Partial<Record<Ubicaciones, string>> = {
    [Ubicaciones.CIUDAD_PRINCIPAL]: 'CiudadPrincipal',
    [Ubicaciones.CIUDAD_INTERMEDIA]: 'CiudadIntermedia'
  };

  formularioTransporteMayor = new FormGroup({
    costoSalvoconducto: new FormControl(''),
    [`formulario${this.formularioSuffix[Ubicaciones.CIUDAD_PRINCIPAL]}`]: new FormGroup({
      camionSencillo: new FormGroup({
        porcentajeDestino: new FormControl(''),
        costoDelViaje: new FormControl(''),
        costoCargueDescargue: new FormControl(''),
      }),
      dobleTroque: new FormGroup({
        porcentajeDestino: new FormControl(''),
        costoDelViaje: new FormControl(''),
        costoCargueDescargue: new FormControl(''),
      }),
      tractoMula: new FormGroup({
        porcentajeDestino: new FormControl(''),
        costoDelViaje: new FormControl(''),
        costoCargueDescargue: new FormControl(''),
      }),
    }),
    [`formulario${this.formularioSuffix[Ubicaciones.CIUDAD_INTERMEDIA]}`]: new FormGroup({
      camionSencillo: new FormGroup({
        porcentajeDestino: new FormControl(''),
        costoDelViaje: new FormControl(''),
        costoCargueDescargue: new FormControl(''),
      }),
      dobleTroque: new FormGroup({
        porcentajeDestino: new FormControl(''),
        costoDelViaje: new FormControl(''),
        costoCargueDescargue: new FormControl(''),
      }),
      tractoMula: new FormGroup({
        porcentajeDestino: new FormControl(''),
        costoDelViaje: new FormControl(''),
        costoCargueDescargue: new FormControl(''),
      }),
    })
  });

  ngOnInit(): void {
    if (this.default && this.default.length > 0) {
      this.formularioTransporteMayor.get('costoSalvoconducto')?.setValue(this.default[0][TiposTrasportesMayores.CAMION_SENCILLO].CostoSalvoconducto);
      this.default.forEach(transporte => {
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('camionSencillo')?.get('porcentajeDestino')?.setValue(transporte[TiposTrasportesMayores.CAMION_SENCILLO].PorcentajeMaterialTransportado * 100);
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('camionSencillo')?.get('costoDelViaje')?.setValue(transporte[TiposTrasportesMayores.CAMION_SENCILLO].CostoViaje);
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('camionSencillo')?.get('costoCargueDescargue')?.setValue(transporte[TiposTrasportesMayores.CAMION_SENCILLO].CostoCargue);
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('dobleTroque')?.get('porcentajeDestino')?.setValue(transporte[TiposTrasportesMayores.DOBLE_TROQUE].PorcentajeMaterialTransportado * 100);
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('dobleTroque')?.get('costoDelViaje')?.setValue(transporte[TiposTrasportesMayores.DOBLE_TROQUE].CostoViaje);
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('dobleTroque')?.get('costoCargueDescargue')?.setValue(transporte[TiposTrasportesMayores.DOBLE_TROQUE].CostoCargue);
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('tractoMula')?.get('porcentajeDestino')?.setValue(transporte[TiposTrasportesMayores.TRACTO_MULA].PorcentajeMaterialTransportado * 100);
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('tractoMula')?.get('costoDelViaje')?.setValue(transporte[TiposTrasportesMayores.TRACTO_MULA].CostoViaje);
        this.formularioTransporteMayor.get(`formulario${this.formularioSuffix[transporte.Ciudad]}`)?.get('tractoMula')?.get('costoCargueDescargue')?.setValue(transporte[TiposTrasportesMayores.TRACTO_MULA].CostoCargue);
      });
    }
  }

  onSubmit(): void {
    const costoSalvoconducto = this.formularioTransporteMayor.get('costoSalvoconducto')!.value;

    const transporteMayorPrincipal = new TransportesMayores(Ubicaciones.CIUDAD_PRINCIPAL);
    transporteMayorPrincipal[TiposTrasportesMayores.CAMION_SENCILLO] = this.obtenerTransporteMayor(this.formularioTransporteMayor.get('formularioCiudadPrincipal.camionSencillo')!.value, costoSalvoconducto, TiposTrasportesMayores.CAMION_SENCILLO);
    transporteMayorPrincipal[TiposTrasportesMayores.DOBLE_TROQUE] = this.obtenerTransporteMayor(this.formularioTransporteMayor.get('formularioCiudadPrincipal.dobleTroque')!.value, costoSalvoconducto, TiposTrasportesMayores.DOBLE_TROQUE);
    transporteMayorPrincipal[TiposTrasportesMayores.TRACTO_MULA] = this.obtenerTransporteMayor(this.formularioTransporteMayor.get('formularioCiudadPrincipal.tractoMula')!.value, costoSalvoconducto, TiposTrasportesMayores.TRACTO_MULA);

    const transporteMayorIntermedia = new TransportesMayores(Ubicaciones.CIUDAD_INTERMEDIA);
    transporteMayorIntermedia[TiposTrasportesMayores.CAMION_SENCILLO] = this.obtenerTransporteMayor(this.formularioTransporteMayor.get('formularioCiudadIntermedia.camionSencillo')!.value, costoSalvoconducto, TiposTrasportesMayores.CAMION_SENCILLO);
    transporteMayorIntermedia[TiposTrasportesMayores.DOBLE_TROQUE] = this.obtenerTransporteMayor(this.formularioTransporteMayor.get('formularioCiudadIntermedia.dobleTroque')!.value, costoSalvoconducto, TiposTrasportesMayores.DOBLE_TROQUE);
    transporteMayorIntermedia[TiposTrasportesMayores.TRACTO_MULA] = this.obtenerTransporteMayor(this.formularioTransporteMayor.get('formularioCiudadIntermedia.tractoMula')!.value, costoSalvoconducto, TiposTrasportesMayores.TRACTO_MULA);

    this.transporteMayorEvent.emit([transporteMayorPrincipal, transporteMayorIntermedia]);
  }

  obtenerTransporteMayor(formValue: any, costoSalvoconducto: number = 0, tipo: TiposTrasportesMayores) {
    const transporte = new TransporteMayor(
      tipo,
      formValue.costoDelViaje,
      costoSalvoconducto,
      formValue.costoCargueDescargue,
      formValue.porcentajeDestino / 100
    );
    return transporte;
  }

}
