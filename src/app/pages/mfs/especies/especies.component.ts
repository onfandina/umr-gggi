import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {Material} from "../../../core/entities/MFS/Material";
import {Sistema} from "../../../core/entities/MFS/Sistema";
import {Especie} from "../../../core/entities/MFS/Especie";
import {MfsInformacionEspecies} from "../../../core/entities/MFS/MfsInformacionEspecies";
import {PlazaVenta} from "../../../core/entities/MFS/PlazaVenta";

@Component({
  selector: 'mfs-especies',
  templateUrl: './especies.component.html',
  styleUrls: ['./especies.component.scss']
})

export class EspeciesComponent implements OnInit {
  @Input() default?: MfsInformacionEspecies;
  @Input() materiales: Material[] = [];
  @Input() sistemasTransformacion: Sistema[] = [];
  @Output() eventoEspecies = new EventEmitter<MfsInformacionEspecies>();

  especiesData: MfsInformacionEspecies = new MfsInformacionEspecies();
  plazas?: any;

  ngOnInit(): void {
    if (this.default) {
      this.especiesData = this.default;
      this.plazas = {
        PlazaPrincipal: this.especiesData.PlazaPrincipal,
        PlazaIntermedia: this.especiesData.PlazaIntermedia
      };
    }
  }

  actualizarPlazasDeVenta(plazas: { PlazaPrincipal: PlazaVenta, PlazaIntermedia: PlazaVenta }) {
    this.especiesData.PlazaPrincipal = plazas.PlazaPrincipal;
    this.especiesData.PlazaIntermedia = plazas.PlazaIntermedia;
    this.eventoEspecies.emit(this.especiesData);
  }

  actualizarEspecies(especies: Especie[]) {
    this.especiesData.Especies = especies;
    this.eventoEspecies.emit(this.especiesData);
  }

  guardar() {
    this.eventoEspecies.emit(this.especiesData);
  }
}
