import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from '@angular/material/dialog';
import {ProductoDialogComponent} from "../producto/dialog/producto.dialog.component";
import {Material} from "../../../../../core/entities/MFS/Material";
import {Madera} from "../../../../../core/entities/MFS/Madera";
import {Sistema} from "../../../../../core/entities/MFS/Sistema";
import {Producto} from "../../../../../core/entities/Producto";
import {BLSistemas} from "../../../../../core/business/MFS/BLSistemas";
import {Especie} from "../../../../../core/entities/MFS/Especie";
import {mfsEspecies} from "../../../../../models/mfsEspecies";
import {Observable} from "rxjs";
import {map, startWith} from "rxjs/operators";
import {MfsInformacionEspecies} from "../../../../../core/entities/MFS/MfsInformacionEspecies";
import {PlazaVenta} from "../../../../../core/entities/MFS/PlazaVenta";
import {PFNM} from "../../../../../core/entities/MFS/PFNM";

@Component({
  selector: 'mfs-especies-info-de-las-especies',
  templateUrl: './info-de-las-especies.component.html',
  styleUrls: ['./info-de-las-especies.component.scss']
})

export class InfoDeLasEspeciesComponent implements OnInit {

  @Input() default?: MfsInformacionEspecies;
  @Input() materiales: Material[] = [];
  @Input() plazaPrincipal: PlazaVenta = new PlazaVenta();
  @Input() plazaIntermedia: PlazaVenta = new PlazaVenta();
  @Input() sistemasTransformacion: Sistema[] = [];

  @Output() guardarEspecie = new EventEmitter<any>();
  @Output() guardarEspecies = new EventEmitter<Especie[]>();

  verFormularioEspecie = false;

  placeholder = ''
  listaDeEspecies = mfsEspecies;
  // @ts-ignore
  especiesFiltradas: Observable<string[]>;

  listaCalidadMadera: string[] = [];

  sistemaDeTransformacionPrimaria: string[] = [];

  unidadBase = 'm3';
  formGroupFields = new FormGroup({
    nombreEspecie: new FormControl(''),
    calidadMadera: new FormControl(''),
    tcaf: new FormControl(''),
    volumenOtorgado: new FormControl(''),
  });

  ciudadPrincipal = '';
  ciudadIntermedia = '';

  dataSourceEspecies = new MatTableDataSource<any>();

  tablaDeEspecies: string[] = [
    'nombreEspecie',
    'calidadMadera',
    'tcaf',
    'volumenOtorgado',
    'productos',
    'operaciones',
  ];

  dataSourceProductos = new MatTableDataSource<any>();

  tablaDeProductos: string[] = [
    'nombreProducto',
    'factorTransformacion',
    'sistemaDeTransformacionPrimaria',
    'volumenPorProducto',
    'costoTransformacionPrimaria',
    'operaciones'
  ];

  especies: Especie[] = [];
  productosEspecie: Producto[] = [];

  actualEspecieId = -1;
  actualProductoId = -1;

  constructor(
    private _snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.materiales) {
      this.listaCalidadMadera = (changes.materiales.currentValue as Material[]).filter(material => (material instanceof Madera)).map(material => material.Nombre);
    }
    if (changes.sistemasTransformacion) {
      this.sistemaDeTransformacionPrimaria = (changes.sistemasTransformacion.currentValue as Sistema[]).map(sistema => sistema.Nombre);
    }
  }

  ngOnInit(): void {
    if (this.default) {
      this.especies = this.default.Especies;
      this.plazaPrincipal = this.default.PlazaPrincipal;
      this.plazaIntermedia = this.default.PlazaIntermedia;
      this.actualizarTablaEspecies();
    }

    this.especiesFiltradas = this.formGroupFields.controls.nombreEspecie.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
  }

  private _filter(value: string = ''): string[] {
    const filterValue = value?.toLowerCase() || '';

    return this.listaDeEspecies.filter(option => option.toLowerCase().includes(filterValue));
  }
  actualizarPlaceholder(option: string) {
    if (option === 'Otra') {
      this.formGroupFields.get('nombreEspecie')?.setValue('')
      this.placeholder = 'Digite el nomble de la especie';
    } else {
      this.placeholder = '';
    }
  }


  agregarEspecie() {
    this.verFormularioEspecie = true;
  }

  cancelarEspecie() {
    this.verFormularioEspecie = false;
    this.formGroupFields.reset();
  }

  reiniciarForm() {
    this.productosEspecie = [];
    this.formGroupFields.reset();
    this.verFormularioEspecie = false;
    this.actualEspecieId = -1;
  }

  crearEspecie() {

    let data = this.formGroupFields.value;
    const especie = new Especie(data.nombreEspecie, data.calidadMadera);
    especie.Tcaf = data.tcaf;
    especie.VolumenOtrogado = data.volumenOtorgado;
    especie.Productos = this.productosEspecie;


    if (this.actualEspecieId < 0) {
      this.especies.push(especie);
    } else {
      this.especies[this.actualEspecieId] = especie;
    }
    this.guardarEspecies.emit(this.especies);
    this.actualizarTablaEspecies();
    this.reiniciarForm();
  }

  eliminarEspecie(especieId: number) {
    if (confirm("Está seguro de eliminar la especie: " + this.especies[especieId].Nombre + "?")) {
      this.especies.splice(especieId, 1);
      this.actualizarTablaEspecies();
    }
  }

  editarEspecie(especieId: number) {
    const especie = this.especies[especieId];
    this.actualEspecieId = especieId;
    this.productosEspecie = especie.Productos;
    ;
    this.actualizarTablaProductos();

    this.formGroupFields.reset({
      nombreEspecie: especie.Nombre,
      calidadMadera: especie.CalidadMadera,
      tcaf: especie.Tcaf,
      volumenOtorgado: especie.VolumenOtrogado,
    });

    this.verFormularioEspecie = true;
  }

  eliminarProducto(productoId: number) {
    if (confirm("Está seguro de eliminar el producto: " + this.productosEspecie[productoId].Nombre + "?")) {
      this.productosEspecie.splice(productoId, 1);
      this.actualizarTablaProductos();
    }
  }

  editarProducto(productoId: number) {
    this.actualProductoId = productoId;

    const dialogRef = this.dialog.open(ProductoDialogComponent, {
      data: {
        titulo: 'Editar producto',
        producto: this.obtenerDialogData(this.productosEspecie[productoId]),
        ciudadPrincipal: this.ciudadPrincipal,
        ciudadIntermedia: this.ciudadIntermedia,
        sistemaDeTransformacionPrimaria: this.sistemaDeTransformacionPrimaria,
        isReadOnly: false,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== null) {
        this.productosEspecie[this.actualProductoId] = this.obtenerProducto(result);
        this.actualizarTablaProductos();
      }
    });
  }

  crearProducto() {
    if (
      this.plazaPrincipal.Ciudad.length < 1 || this.plazaIntermedia.Ciudad.length < 1
    ) {
      this._snackBar.open(
        'Debe actualizar las plazas de venta.',
        'Cerrar',
        {
          panelClass: ['red-snackbar']
        }
      );
    } else {
      this.ciudadPrincipal = this.plazaPrincipal.Ciudad;
      this.ciudadIntermedia = this.plazaIntermedia.Ciudad;

      const dialogRef = this.dialog.open(ProductoDialogComponent, {
        data: {
          titulo: 'Crear producto',
          producto: {
            nombreProducto: '',
            factorTransformacion: '',
            sistemaDeTransformacionPrimaria: '',
            volumenPorProducto: '',
            costoTransformacionPrimaria: '',
            preciosCiudadBordeCarretera: '',
            preciosCiudadPrincipal: '',
            preciosCiudadIntermedia: '',
            porcentajeProductoCiudadPrincipal: '',
            porcentajeProductoCiudadIntermedia: '',
            porcentajeProductoBordeCarretera: ''
          },
          ciudadPrincipal: this.ciudadPrincipal,
          ciudadIntermedia: this.ciudadIntermedia,
          sistemaDeTransformacionPrimaria: this.sistemaDeTransformacionPrimaria,
          isReadOnly: false,
        },
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.productosEspecie.push(this.obtenerProducto(result));
          this.actualizarTablaProductos();
        }
      });
    }
  }

  actualizarTablaProductos() {
    this.dataSourceProductos.data = this.productosEspecie.map(producto => ({
      nombreProducto: producto.Nombre,
      factorTransformacion: producto.FactorAprovechamientoEfectivo,
      sistemaDeTransformacionPrimaria: producto.SistemaTransPrimaria,
      volumenPorProducto: producto.Volumen,
      costoTransformacionPrimaria: producto.CostoTransPrimaria,
    }));
  }

  actualizarTablaEspecies() {
    this.dataSourceEspecies.data = this.especies.map(especie => ({
      nombreEspecie: especie.Nombre,
      calidadMadera: especie.CalidadMadera.Nombre,
      tcaf: especie.Tcaf,
      volumenOtorgado: especie.VolumenOtrogado,
      productos: especie.Productos
    }));
  }

  protected obtenerProducto(dialogData: any) {
    const producto = new Producto();
    producto.Nombre = dialogData.nombreProducto;
    producto.FactorAprovechamientoEfectivo = dialogData.factorTransformacion / 100;
    producto.SistemaTransPrimaria = dialogData.sistemaDeTransformacionPrimaria;
    producto.Volumen = dialogData.volumenPorProducto;
    producto.PrecioCiudadPrincipal = dialogData.preciosCiudadPrincipal;
    producto.PrecioCiudadIntermedia = dialogData.preciosCiudadIntermedia;
    producto.PrecioBordeCarretera = dialogData.preciosCiudadBordeCarretera;
    producto.PorcentajeCiudadPrincipal = dialogData.porcentajeProductoCiudadPrincipal / 100;
    producto.PorcentajeCiudadIntermedia = dialogData.porcentajeProductoCiudadIntermedia / 100;
    producto.PorcentajeBordeCarretera = dialogData.porcentajeProductoBordeCarretera / 100;

    const sistema = this.sistemasTransformacion.find(sistema => sistema.Nombre === producto.SistemaTransPrimaria);
    if (sistema && this.formGroupFields.get('calidadMadera')?.value) {
      producto.CostoTransPrimaria = BLSistemas.ObtenerRendimiento(sistema, this.formGroupFields.get('calidadMadera')?.value)?.Rendimiento || 0;
    }
    return producto;
  }

  obtenerDialogData(producto: Producto) {
    const dialogData: any = {};
    dialogData.nombreProducto = producto.Nombre;
    dialogData.factorTransformacion = producto.FactorAprovechamientoEfectivo;
    dialogData.sistemaDeTransformacionPrimaria = producto.SistemaTransPrimaria;
    dialogData.volumenPorProducto = producto.Volumen;
    dialogData.preciosCiudadPrincipal = producto.PrecioCiudadPrincipal;
    dialogData.preciosCiudadIntermedia = producto.PrecioCiudadIntermedia;
    dialogData.preciosCiudadBordeCarretera = producto.PrecioBordeCarretera;
    dialogData.porcentajeProductoCiudadPrincipal = producto.PorcentajeCiudadPrincipal;
    dialogData.porcentajeProductoCiudadIntermedia = producto.PorcentajeCiudadIntermedia;
    dialogData.porcentajeProductoBordeCarretera = producto.PorcentajeBordeCarretera;
    return dialogData;
  }

  actualizarUnidad() {
    const material = this.formGroupFields.get('calidadMadera')?.value;
    if (material) {
      this.unidadBase = material.Unidades || 'm3';
    }
  }
}


