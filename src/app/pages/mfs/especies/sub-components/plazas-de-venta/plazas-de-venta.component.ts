import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {PlazaVenta} from "../../../../../core/entities/MFS/PlazaVenta";

@Component({
  selector: 'mfs-especies-plazas-de-venta',
  templateUrl: './plazas-de-venta.component.html',
  styleUrls: ['./plazas-de-venta.component.scss']
})

export class PlazasDeVentaComponent implements OnInit {
  @Input() default?: { PlazaPrincipal: PlazaVenta, PlazaIntermedia: PlazaVenta };
  @Output() eventoPlazasDeVenta = new EventEmitter<{ PlazaPrincipal: PlazaVenta, PlazaIntermedia: PlazaVenta }>();

  data = {
    plazaPrincipal: {
      ciudad: '',
      departamento: '',
      distanciaAPlazaDeVenta: 0
    },
    plazaIntermedia: {
      ciudad: '',
      departamento: '',
      distanciaAPlazaDeVenta: 0
    }
  };
  plazas = {
    PlazaPrincipal: new PlazaVenta(),
    PlazaIntermedia: new PlazaVenta()
  };

  ngOnInit(): void {
    if (this.default) {
      this.plazas = this.default;
      this.data.plazaPrincipal.distanciaAPlazaDeVenta = this.plazas.PlazaPrincipal.Distancia;
      this.data.plazaPrincipal.ciudad = this.plazas.PlazaPrincipal.Ciudad;
      this.data.plazaPrincipal.departamento = this.plazas.PlazaPrincipal.Departamento;
      this.data.plazaIntermedia.distanciaAPlazaDeVenta = this.plazas.PlazaIntermedia.Distancia;
      this.data.plazaIntermedia.ciudad = this.plazas.PlazaIntermedia.Ciudad;
      this.data.plazaIntermedia.departamento = this.plazas.PlazaIntermedia.Departamento;
    }
  }

  onKey(event: any) { // without type info
    this.guardarPlazas();
  }

  guardarPlazas() {
    this.plazas.PlazaPrincipal.Distancia = this.data.plazaPrincipal.distanciaAPlazaDeVenta;
    this.plazas.PlazaPrincipal.Ciudad = this.data.plazaPrincipal.ciudad;
    this.plazas.PlazaPrincipal.Departamento = this.data.plazaPrincipal.departamento;
    this.plazas.PlazaIntermedia.Distancia = this.data.plazaIntermedia.distanciaAPlazaDeVenta;
    this.plazas.PlazaIntermedia.Ciudad = this.data.plazaIntermedia.ciudad;
    this.plazas.PlazaIntermedia.Departamento = this.data.plazaIntermedia.departamento;
    this.eventoPlazasDeVenta.emit(this.plazas);
  }
}
