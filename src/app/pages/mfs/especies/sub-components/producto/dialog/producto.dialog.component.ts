import {Component, Inject} from '@angular/core';

import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {mfsProductos} from "../../../../../../models/mfsProductos";
import {map, startWith} from "rxjs/operators";

export interface DialogData {
  titulo: any;
  producto: any;
  ciudadPrincipal: any;
  ciudadIntermedia: any;
  sistemaDeTransformacionPrimaria: any;
  isReadOnly: boolean;
};

@Component({
  selector: 'mfs-especies-producto-dialog',
  templateUrl: './product.dialog.component.html',
  styleUrls: ['./producto.dialog.component.scss'],
})

export class ProductoDialogComponent {
  productoFormGroupFields = new FormGroup({
    nombreProducto: new FormControl(this.data.producto.nombreProducto),
    factorTransformacion: new FormControl(Number((this.data.producto.factorTransformacion * 100).toFixed(4))),
    sistemaDeTransformacionPrimaria: new FormControl(this.data.producto.sistemaDeTransformacionPrimaria),
    volumenPorProducto: new FormControl(this.data.producto.volumenPorProducto),
    costoTransformacionPrimaria: new FormControl(this.data.producto.costoTransformacionPrimaria),
    preciosCiudadBordeCarretera: new FormControl(this.data.producto.preciosCiudadBordeCarretera),
    preciosCiudadPrincipal: new FormControl(this.data.producto.preciosCiudadPrincipal),
    preciosCiudadIntermedia: new FormControl(this.data.producto.preciosCiudadIntermedia),
    porcentajeProductoCiudadPrincipal: new FormControl(Number((this.data.producto.porcentajeProductoCiudadPrincipal * 100).toFixed(4))),
    porcentajeProductoCiudadIntermedia: new FormControl(Number((this.data.producto.porcentajeProductoCiudadIntermedia * 100).toFixed(4))),
    porcentajeProductoBordeCarretera: new FormControl(Number((this.data.producto.porcentajeProductoBordeCarretera * 100).toFixed(4))),
  });

  listaDeEspecies = mfsProductos;
  // @ts-ignore
  productosFiltrados: Observable<string[]>;

  constructor(
    public dialogRef: MatDialogRef<ProductoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
    this.productosFiltrados = this.productoFormGroupFields.controls.nombreProducto.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.listaDeEspecies.filter(option => option.toLowerCase().includes(filterValue));
  }
}
