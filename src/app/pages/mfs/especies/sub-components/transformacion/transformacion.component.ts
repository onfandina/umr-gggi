import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'mfs-especies-transformacion',
  templateUrl: './transformacion.component.html',
  styleUrls: ['./transformacion.component.scss']
})

export class TransformacionComponent implements OnInit {

  @Input() especies: Array<any> = [];

  dataSourceProductos = new MatTableDataSource<any>();

  tablaDeProductos: string[] = [
    'nombreProducto',
    'factorTransformacion',
  ];

  productos: Array<any> = [];

  ngOnInit(): void {
  }

  update(): void {
    this.productos = [];

    let parent = this;

    this.especies.forEach(function(especie) {
      especie.productos.forEach(function(producto: any) {
        parent.productos.push({
          'nombreProducto' : producto.nombreProducto,
          'factorTransformacion' : (producto.factorTransformacion * 100),
        });
      });
    });

    this.dataSourceProductos.data = parent.productos;
  }
}
