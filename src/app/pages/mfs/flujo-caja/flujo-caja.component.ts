import {Component, Input, OnInit} from '@angular/core';
import {FlujoCaja} from "../../../core/entities/MFS/Resultados/FlujoCaja";
import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";
import {Mfs} from "../../../core/entities/MFS/Mfs";
import {Ubicaciones} from "../../../core/entities/MFS/Enums/Ubicaciones";
import {BLFlujoCaja} from "../../../core/business/MFS/BLFlujoCaja";

interface RowData {
  nombre: string;
  col1: number;
  col2: number;
  col3: number;
}

@Component({
  selector: 'mfs-flujo-caja',
  templateUrl: './flujo-caja.component.html',
  styleUrls: ['./flujo-caja.component.scss']
})
export class FlujoCajaComponent implements OnInit {
  @Input() mfs: Mfs = new Mfs();
  flujo: FlujoCaja = new FlujoCaja();
  indeces: Record<string, any> = {};
  rowData = '';

  cabeceraTablaPdf: any[] = [];
  datosTablaPdf: any[] = [];

  dataSourceTable: any[] = [];
  displayedColumnsTable: any[] = ['uca', 'ingresos', 'costosSubtotal', 'administracion', 'impuestoIndustria', 'costos', 'utilidadBruta', 'renta', 'utilidad'];
  irr: number = 0;
  ingresoPromedio: number = 0;

  ngOnInit(): void {
    this.actualizarTabla();
  }

  actualizarTabla(mfs?: Mfs) {
    if (this.mfs.Materiales.length === 0) {
      // return;
    }
    if (mfs) {
      this.mfs = mfs;
    }
    BLFlujoCaja.asignarFlujoCaja(this.mfs);
    this.flujo = this.mfs.FlujoCaja;
    this.ingresoPromedio = this.flujo.IngresoPromedio || 0;
    this.irr = this.flujo.TIR || 0;
    this.dataSourceTable = [];
    this.dataSourceTable.push({
      uca: 0,
      costos: this.flujo.InversionInicial.Total,
      utilidadBruta: this.flujo.InversionInicial.Total * -1,
      utilidad: this.flujo.InversionInicial.Total * -1
    });
    this.flujo.FlujoUcas.forEach((flujoUca, i) => {
      this.dataSourceTable.push({
        uca: i + 1,
        ingresos: flujoUca.TotalIngresos,
        costosSubtotal: flujoUca.SubtotalCostos,
        administracion: flujoUca.CostoAdministracion,
        impuestoIndustria: flujoUca.CostoIndustriaComercio,
        costos: flujoUca.TotalCostos,
        utilidadBruta: flujoUca.UtilidadBruta,
        renta: flujoUca.ImpuestoRenta,
        utilidad: flujoUca.UtilidadNeta,
      });
    });
    this.actualizarTablaPdf();
  }

  actualizarTablaPdf() {
    this.cabeceraTablaPdf = ['Concepto', ...new Array(this.flujo.FlujoUcas.length + 1).fill('').map((v, i) => `UCA ${i}`)];
    this.datosTablaPdf = [];
    this.datosTablaPdf.push(['Iversion Inicial', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.datosTablaPdf.push(['Maquinaria y equipo', this.flujo.InversionInicial.Maquinaria, ...new Array(this.flujo.FlujoUcas.length).fill(0)]);
    this.datosTablaPdf.push(['Concertación y demás costos preoperativos', this.flujo.InversionInicial.Concertacion, ...new Array(this.flujo.FlujoUcas.length).fill(0)]);
    this.datosTablaPdf.push(['Total inversión inicial', this.flujo.InversionInicial.InversionTotal, ...new Array(this.flujo.FlujoUcas.length).fill(0)]);
    this.datosTablaPdf.push(['Ingreso por ventas', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.datosTablaPdf.push(['Ingreso por ventas ciudad principal', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.IngresoVentas.IngresoUbicaciones[Ubicaciones.CIUDAD_PRINCIPAL])]);
    this.datosTablaPdf.push(['Ingreso por ventas ciudad intermedia', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.IngresoVentas.IngresoUbicaciones[Ubicaciones.CIUDAD_INTERMEDIA])]);
    this.datosTablaPdf.push(['Ingreso por ventas borde carretera', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.IngresoVentas.IngresoUbicaciones[Ubicaciones.BORDE_CARRETERA])]);
    this.datosTablaPdf.push(['Subtotal Ingreso por ventas', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.IngresoVentas.Total)]);
    this.datosTablaPdf.push(['Otros ingresos', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.datosTablaPdf.push(['Carbono', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.OtrosIngresos.Carbono)]);
    this.datosTablaPdf.push(['Incentivos', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.OtrosIngresos.Incentivos)]);
    this.datosTablaPdf.push(['Financiación', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.OtrosIngresos.Financiacion)]);
    this.datosTablaPdf.push(['Subtotal Otros Ingresos', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.OtrosIngresos.Total)]);
    this.datosTablaPdf.push(['INGRESOS TOTALES', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.TotalIngresos)]);
    this.datosTablaPdf.push(['COSTOS', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.datosTablaPdf.push(['Costos de Asistencia Técnicaa', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.datosTablaPdf.push(['Asistencia técnica Anual', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.Asistencia.AsistenciaAnual)]);
    this.datosTablaPdf.push(['Plan de Manejo Forestal', this.flujo.InversionInicial.Asistencia.Pmf, ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.Asistencia.Pmf)]);
    this.datosTablaPdf.push(['Construcción campamento', this.flujo.InversionInicial.Asistencia.Campamento, ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.Asistencia.Campamento)]);
    this.datosTablaPdf.push(['Subtotal costos Asistencia técnica', this.flujo.InversionInicial.Asistencia.Total, ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.Asistencia.Total)]);
    this.datosTablaPdf.push(['Costos asociados a la tierra', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.CostoTierra)]);
    this.datosTablaPdf.push(['Costos de aprovechamiento (A partir del año 2)', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.mfs.Costos.Aprovechamiento.Costos.forEach((sistemaCostosAprovechamiento) => {
      if (sistemaCostosAprovechamiento.Valor > 0) {
        this.datosTablaPdf.push([`${sistemaCostosAprovechamiento.Material.Nombre}`, '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.Aprovechamiento.Aprovechamientos.find(aprovechamiento => aprovechamiento.Material.Nombre === sistemaCostosAprovechamiento.Material.Nombre)?.Costo)]);
      }
    });
    this.datosTablaPdf.push(['Subtotal Costo de aprovechamiento', this.flujo.InversionInicial.Aprovechamiento.Total, ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.Aprovechamiento.Total)]);
    this.datosTablaPdf.push(['Costos de transformación primaria', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.mfs.SistemasTransformacion.forEach(sistema => {
      sistema.Rendimientos.forEach((rendimiento) => {
        if (rendimiento.Rendimiento > 0) {
          this.datosTablaPdf.push([`${rendimiento.Material.Nombre} - ${sistema.Nombre}`, '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.Transformacion.Costos.find(costo => costo.Porcentaje.Material.Nombre === rendimiento.Material.Nombre && costo.Porcentaje.Sistema.Nombre === sistema.Nombre)?.Valor)]);
        }
      });
    });
    this.datosTablaPdf.push(['Subtotal transformación primaria', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.Transformacion.Total)]);
    this.datosTablaPdf.push(['Costos de transporte menor', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.mfs.SistemasTransporteMenor.forEach(sistema => {
      sistema.Rendimientos.forEach((rendimiento) => {
        if (rendimiento.Rendimiento > 0) {
          this.datosTablaPdf.push([`${rendimiento.Material.Nombre} - ${sistema.Nombre}`, '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.TransporteMenor.Costos.find(costo => costo.Porcentaje.Material.Nombre === rendimiento.Material.Nombre && costo.Porcentaje.Sistema.Nombre === sistema.Nombre)?.Valor)]);
        }
      });
    });
    this.datosTablaPdf.push(['Subtotal transporte menor', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.TransporteMenor.Total)]);
    this.datosTablaPdf.push(['Costos de transporte mayor', ...new Array(this.flujo.FlujoUcas.length + 1).fill('')]);
    this.datosTablaPdf.push([`Transporte mayor a Ciudad principal - ${this.mfs.InformacionEspecies.PlazaPrincipal.Ciudad}`, '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.TransporteMayor.TransporteMayor.find(transporte => transporte.Ciudad === Ubicaciones.CIUDAD_PRINCIPAL)?.CostoTotal)]);
    this.datosTablaPdf.push([`Transporte mayor a Ciudad intermedia - ${this.mfs.InformacionEspecies.PlazaIntermedia.Ciudad}`, '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.TransporteMayor.TransporteMayor.find(transporte => transporte.Ciudad === Ubicaciones.CIUDAD_INTERMEDIA)?.CostoTotal)]);
    this.datosTablaPdf.push(['Subtotal transporte mayor', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.TransporteMayor.Total)]);
    this.flujo.FlujoUcas[0]?.OtrosCostos.Costos.forEach((costo, i) => this.datosTablaPdf.push([costo.Nombre, '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.OtrosCostos.Costos[i].Valor)]));
    this.datosTablaPdf.push(['SUBTOTAL COSTOS', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.SubtotalCostos)]);
    this.datosTablaPdf.push(['Administración', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.CostoAdministracion)]);
    this.datosTablaPdf.push(['Impuesto de industria y comercio', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.CostoIndustriaComercio)]);
    this.datosTablaPdf.push(['COSTOS TOTALES', this.flujo.InversionInicial.Total, ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.TotalCostos)]);
    this.datosTablaPdf.push(['UTILIDAD BRUTA', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.UtilidadBruta)]);
    this.datosTablaPdf.push(['Impuesto de renta', '', ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.ImpuestoRenta)]);
    this.datosTablaPdf.push(['UTILIDAD NETA', this.flujo.InversionInicial.Total * -1, ...this.flujo.FlujoUcas.map(flujoUca => flujoUca.UtilidadNeta)]);
    this.datosTablaPdf.push(['TIR', (this.flujo.TIR * 100).toFixed(2) + "%", ...new Array(this.flujo.FlujoUcas.length).fill('')]);
    this.datosTablaPdf.push(['Ingreso promedio mensual', this.flujo.IngresoPromedio, ...new Array(this.flujo.FlujoUcas.length).fill('')]);
    this.flujo.IngresoPromedioFamilia && this.datosTablaPdf.push(['Ingreso promedio mensual por familia', this.flujo.IngresoPromedioFamilia, ...new Array(this.flujo.FlujoUcas.length).fill('')]);
    this.formatearDatosPdf();
  }

  formatearDatosPdf() {
    let moneda = Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      useGrouping: true,
    });
    this.datosTablaPdf = this.datosTablaPdf.map(datos => datos.map((dato: any) => typeof dato === 'number' ? moneda.format(dato) : dato));
  }


  openPDF(): void {
    //Parámetro de área=exp (TIR deseada-TIR proyecto-0,0003)
    //unidad mínima rentable=Parámetro de área*área de bosque
    const PDF = new jsPDF('l', 'pt', [150 + 100 * this.flujo.FlujoUcas.length, this.datosTablaPdf.length * 20]);
    this.actualizarTablaPdf();
    autoTable(PDF, {
      head: [this.cabeceraTablaPdf],
      body: this.datosTablaPdf,
      tableLineColor: [189, 195, 199],
      tableLineWidth: 0.75,
      theme: 'striped',
      columnStyles: {0: {cellWidth: 'auto'}}
    });
    PDF.save('flujo-de-caja.pdf');
  }

}
