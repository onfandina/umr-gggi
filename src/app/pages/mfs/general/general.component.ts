import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {TipoPersona} from "../../../core/entities/Enumeradores/TipoPersona";
import {TipoEntidad} from "../../../core/entities/Enumeradores/TipoEntidad";
import {MfsInformacionGeneral} from "../../../core/entities/MFS/MfsInformacionGeneral";
import {Controls} from "../common/multiple-values/multiple-values.component";

@Component({
  selector: 'mfs-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})

export class GeneralComponent implements OnInit {
  @Input() default?: MfsInformacionGeneral;
  @Output() eventoInformacionGeneral = new EventEmitter<MfsInformacionGeneral>();

  //Form controls
  formGeneral = new FormGroup({
    fcontrolTipoPersona: new FormControl(''),
    fcontrolTipoEntidad: new FormControl(''),
    fcontrolUVT: new FormControl(),
    fcontrolAreaUmf: new FormControl(),
    fcontrolAreaBosque: new FormControl(),
    fcontrolTurnos: new FormControl(),
    fcontrolUca: new FormControl(),
    fcontrolM3HaOtorgados: new FormControl({value: 0, disabled: true}),
    fcontrolM3HaOtorgadosUca1: new FormControl(),
    fcontrolFamilias: new FormControl(),
  });

  //helpers
  tipoPersona = '';
  inputType = Controls.NUMBER;
  ucas = 1;
  areaUca: number[] = [];


  //Objeto principal
  infoGeneral = new MfsInformacionGeneral();

  ngOnInit(): void {
    if (this.default) {
      this.formGeneral.get('fcontrolTipoPersona')?.setValue(this.default.Persona.tipoDePersona === TipoPersona.Natural ? 'natural' : 'juridica');
      this.cambioPersona();
      this.formGeneral.get('fcontrolTipoEntidad')?.setValue(this.default.Persona.tipoDeEntidad === TipoEntidad.ConAnimoLucro ? 'lucro' : 'sinLucro');
      this.formGeneral.get('fcontrolUVT')?.setValue(this.default.UVT);
      this.formGeneral.get('fcontrolAreaUmf')?.setValue(this.default.AreaUmf);
      this.formGeneral.get('fcontrolAreaBosque')?.setValue(this.default.AreaBosque);
      this.formGeneral.get('fcontrolTurnos')?.setValue(this.default.Turnos);
      this.formGeneral.get('fcontrolUca')?.setValue(this.default.Uca);
      this.formGeneral.get('fcontrolM3HaOtorgados')?.setValue(this.default.M3HaOtorgados);
      this.formGeneral.get('fcontrolM3HaOtorgadosUca1')?.setValue(this.default.M3HaOtorgadosUca1);
      this.formGeneral.get('fcontrolFamilias')?.setValue(this.default.Familias);
      this.ucas = this.default.Uca;
      this.areaUca = this.default.AreaUca;
    }
  }

  cambioPersona() {
    this.tipoPersona = this.formGeneral.get('fcontrolTipoPersona')?.value;
  }

  cambioTurnos() {
    if (this.formGeneral.get('fcontrolUca')?.value < 1 && this.formGeneral.get('fcontrolTurnos')?.value > 0) {
      this.formGeneral.get('fcontrolUca')?.setValue(this.formGeneral.get('fcontrolTurnos')?.value);
      this.cambioUcas();
    }
  }

  cambioUcas() {
    this.ucas = this.formGeneral.get('fcontrolUca')?.value;
    this.areaUca = new Array(this.ucas).fill(this.formGeneral.get('fcontrolAreaBosque')?.value / this.ucas);
  }

  actualizarAreaAnual(valores: number[]) {
    this.areaUca = valores;
  }

  actualizarInfoGeneral() {
    this.eventoInformacionGeneral.emit(this.infoGeneral);
  }

  //Guarda la información en el formulario
  guardar() {
    this.infoGeneral = new MfsInformacionGeneral();
    if (this.formGeneral.get('fcontrolTipoPersona')?.value == 'natural') {
      this.infoGeneral.Persona.tipoDePersona = TipoPersona.Natural;
    } else {
      this.infoGeneral.Persona.tipoDePersona = TipoPersona.Juridica;
      if (this.formGeneral.get('fcontrolTipoEntidad')?.value == 'lucro') {
        this.infoGeneral.Persona.tipoDeEntidad = TipoEntidad.ConAnimoLucro;
      } else {
        this.infoGeneral.Persona.tipoDeEntidad = TipoEntidad.SinAnimoLucro;
      }
    }

    this.infoGeneral.UVT = this.formGeneral.get('fcontrolUVT')?.value;
    this.infoGeneral.AreaUmf = this.formGeneral.get('fcontrolAreaUmf')?.value;
    this.infoGeneral.AreaBosque = this.formGeneral.get('fcontrolAreaBosque')?.value;
    this.infoGeneral.Turnos = this.formGeneral.get('fcontrolTurnos')?.value;
    this.infoGeneral.Uca = this.formGeneral.get('fcontrolUca')?.value;
    this.infoGeneral.M3HaOtorgadosUca1 = this.formGeneral.get('fcontrolM3HaOtorgadosUca1')?.value;
    this.infoGeneral.Familias = this.formGeneral.get('fcontrolFamilias')?.value;
    this.infoGeneral.AreaUca = this.areaUca;

    this.actualizarInfoGeneral();
  }

}
