import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {OtrosIngresos} from "../../../core/entities/MFS/Ingresos/OtrosIngresos";
import {Mfs} from "../../../core/entities/MFS/Mfs";
import {BLIngresos} from "../../../core/business/MFS/BLIngresos";
import {ListaCostos} from "../../../core/entities/MFS/Costos/ListaCostos";

interface IngresoFila {
  uca: number;
  principal: number;
  intermedia: number;
  carretera: number;
  total: number;
}

@Component({
  selector: 'mfs-ingresos',
  templateUrl: './ingreso.component.html',
  styleUrls: ['./ingreso.component.scss']
})
export class IngresoComponent implements OnInit {
  @Input() default?: OtrosIngresos;
  @Input() mfs?: Mfs;
  @Output() ingresosEvent = new EventEmitter<OtrosIngresos>();

  ingresos = new OtrosIngresos();
  dataSource: any[] = [];
  columnas: string[] = ['uca', 'principal', 'intermedia', 'carretera', 'total'];


  //Form controls
  formIngresos = new FormGroup({
    fcontrolIncentivos: new FormControl(),
    fcontrolCarbono: new FormControl(),
  });

  ngOnInit(): void {
    if (this.default) {
      this.formIngresos.get('fcontrolIncentivos')?.setValue(this.default.Incentivos.Valor * 100);
      this.formIngresos.get('fcontrolCarbono')?.setValue(this.default.Carbono.Valor * 100);
    }
    this.actualizarTabla();
  }

  actualizarTabla() {
    if (this.mfs) {
      this.dataSource = [];
      if (this.mfs.IngresosUbicaciones.length === 0) {
        this.mfs.IngresosUbicaciones = BLIngresos.obtenerIngresosUbicacion(this.mfs.InformacionEspecies.Especies, this.mfs.InformacionGeneral.AreaUca);
      }
      this.mfs.IngresosUbicaciones.forEach((ingreso, i) => {
        const fila = {
          uca: i + 1,
          principal: ingreso.CIUDAD_PRINCIPAL,
          intermedia: ingreso.CIUDAD_INTERMEDIA,
          carretera: ingreso.BORDE_CARRETERA,
          total: ingreso.CIUDAD_PRINCIPAL + ingreso.CIUDAD_INTERMEDIA + ingreso.BORDE_CARRETERA,
        };
        this.dataSource.push(fila);
      });
    }

  }

  guardarIngresos() {
    this.ingresos.Carbono.Valor = (this.formIngresos.get('fcontrolCarbono')?.value || 0) / 100;
    this.ingresos.Incentivos.Valor = (this.formIngresos.get('fcontrolIncentivos')?.value || 0) / 100;
    this.ingresosEvent.emit(this.ingresos);
  }

  guardar() {
    this.ingresosEvent.emit(this.ingresos);
  }

}
