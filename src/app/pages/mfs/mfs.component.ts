import {Component, OnInit, ViewChild} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {StepperOrientation} from '@angular/material/stepper';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {CostosComponent} from './costos/costos.component';
import {GeneralComponent} from './general/general.component';
import {Mfs} from "../../core/entities/MFS/Mfs";
import {MfsInformacionGeneral} from "../../core/entities/MFS/MfsInformacionGeneral";
import {Sistema} from "../../core/entities/MFS/Sistema";
import {SistemasTransformacionComponent} from "./sistemas-transformacion/sistemas-transformacion.component";
import {UbicacionComponent} from "./ubicacion/ubicacion.component";
import {ModeloFinancieroComponent} from "./modelo-financiero/modelo-financiero.component";
import {ProductosComponent} from "./productos/productos.component";
import {Material} from "../../core/entities/MFS/Material";
import {MfsModuloFinanciero} from "../../core/entities/MFS/MfsModuloFinanciero";
import {MfsInformacionEspecies} from "../../core/entities/MFS/MfsInformacionEspecies";
import {ListaCostos} from "../../core/entities/MFS/Costos/ListaCostos";
import {SistemasTransporteMenorComponent} from "./sistemas-transporte-menor/sistemas-transporte-menor.component";
import {EspeciesComponent} from "./especies/especies.component";
import {IngresoComponent} from "./ingreso/ingreso.component";
import {FlujoCaja} from "../../core/entities/MFS/Resultados/FlujoCaja";
import {BLFlujoCaja} from "../../core/business/MFS/BLFlujoCaja";
import {DomSanitizer} from "@angular/platform-browser";
import {Sensibilidad} from "../../core/entities/MFS/Sensibilidad";
import {SensibilidadComponent} from "./sensibilidad/sensibilidad.component";
import {FlujoCajaComponent} from "./flujo-caja/flujo-caja.component";
import {Madera} from "../../core/entities/MFS/Madera";
import {TiposMadera} from "../../core/entities/MFS/Enums/TiposMadera";
import {Ubicacion} from "../../core/entities/MFS/Ubicacion";
import {PFNM} from "../../core/entities/MFS/PFNM";

@Component({
  selector: 'app-mfs',
  templateUrl: './mfs.component.html',
  styleUrls: ['./mfs.component.scss']
})

export class MfsComponent implements OnInit {
  mfsData: Mfs = new Mfs();
  flujo?: FlujoCaja;
  sistemasTransformacion: Sistema[] = [];
  sistemasTransporteMenor: Sistema[] = [];
  materiales: Material[] = [];
  ucas = 1;
  editing = false;
  completo = {
    ubicacion: false,
  };

  @ViewChild(UbicacionComponent) ubicacion!: UbicacionComponent;
  @ViewChild(ModeloFinancieroComponent) moduloFinanciero!: ModeloFinancieroComponent;
  @ViewChild(GeneralComponent) general!: GeneralComponent;
  @ViewChild(ProductosComponent) productos!: ProductosComponent;
  @ViewChild(SistemasTransformacionComponent) childSistemasTransformacion!: SistemasTransformacionComponent;
  @ViewChild(SistemasTransporteMenorComponent) childSistemasTransporteMenor!: SistemasTransporteMenorComponent;
  @ViewChild(EspeciesComponent) especies!: EspeciesComponent;
  @ViewChild(CostosComponent) costos!: CostosComponent;
  @ViewChild(IngresoComponent) ingresos!: IngresoComponent;
  @ViewChild(FlujoCajaComponent) flujoCaja!: FlujoCajaComponent;
  @ViewChild(SensibilidadComponent) sensibilidad!: SensibilidadComponent;
  stepperOrientation: Observable<StepperOrientation>;

  constructor(breakpointObserver: BreakpointObserver, private sanitizer: DomSanitizer) {
    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 800px)')
      .pipe(map(({matches}) => (matches ? 'horizontal' : 'vertical')));

  }

  ngOnInit(): void {
    this.inicializarMfs();
  }

  inicializarMfs() {
    if (this.mfsData.Materiales.length === 0) {
      const maderaMuyDura = new Madera('Muy Dura', TiposMadera.MUY_DURA);
      const maderaDura = new Madera('Dura', TiposMadera.DURA);
      const maderaBlanda = new Madera('Blanda', TiposMadera.BLANDA);
      const maderaOtra = new Madera('Otra', TiposMadera.OTRA);
      this.mfsData.Materiales = [maderaMuyDura, maderaDura, maderaBlanda, maderaOtra];
    }
    this.ucas = this.mfsData.InformacionGeneral.Uca > 0 ? this.mfsData.InformacionGeneral.Uca : 1;
    this.materiales = this.mfsData.Materiales;
    this.sistemasTransporteMenor = this.mfsData.SistemasTransporteMenor;
    this.sistemasTransformacion = this.mfsData.SistemasTransformacion;
    this.mfsData.FlujoCaja = BLFlujoCaja.obtenerFlujoCaja(this.mfsData);
    this.flujo = this.mfsData.FlujoCaja;
  }

  selectionChange(event: any) {
    switch (event.previouslySelectedIndex) {
      case 0:
        this.ubicacion.guardar();
        break;
      case 1:
        this.moduloFinanciero.guardar();
        break;
      case 2:
        this.general.guardar();
        break;
      case 3:
        this.productos.guardar();
        break;
      case 4:
        this.childSistemasTransformacion.guardar();
        break;
      case 5:
        this.especies.guardar();
        break;
      case 6:
        this.childSistemasTransporteMenor.guardar();
        break;
      case 7:
        this.costos.guardar();
        break;
      case 8:
        this.ingresos.guardar();
        break;
      case 10:
        this.sensibilidad.guardar();
        break;
    }
    switch (event.selectedIndex) {
      case 8:
        this.ingresos.actualizarTabla();
        break;
      case 9:
        this.flujoCaja.actualizarTabla();
        break;
      case 10:
        this.sensibilidad.actualizarTabla();
        break;
    }
  }

  // actualizar subcomponentes
  actualizarEspecies(especiesData: MfsInformacionEspecies) {
    this.mfsData.InformacionEspecies = especiesData;
  }

  actualizarUbicacion(ubicacion: Ubicacion) {
    this.mfsData.Ubicacion = ubicacion;
  }

  actualizarModuloFinanciero(mfsModuloFinanciero: MfsModuloFinanciero) {
    this.mfsData.ModuloFinanciero = mfsModuloFinanciero;
  }

  actualizarInformacionGeneral(informacionGeneral: MfsInformacionGeneral) {
    this.mfsData.InformacionGeneral = informacionGeneral;
    this.ucas = this.mfsData.InformacionGeneral.Uca;
  }

  actualizarMateriales(pfnm: Material[]) {
    this.mfsData.Materiales = [...this.mfsData.Materiales.filter(material => !(material instanceof PFNM)), ...pfnm];
    this.materiales = this.mfsData.Materiales;
  }

  actualizarSistemasTransformacion(sitemasTransformacion: Sistema[]) {
    this.mfsData.SistemasTransformacion = sitemasTransformacion;
    this.sistemasTransformacion = [...sitemasTransformacion];
  }

  actualizarSistemasTransporteMenor(sitemasTransporteMenor: Sistema[]) {
    this.mfsData.SistemasTransporteMenor = sitemasTransporteMenor;
    this.sistemasTransporteMenor = [...sitemasTransporteMenor];
  }

  actualizarCostos(costos: ListaCostos) {
    this.mfsData.Costos = costos;
  }

  actualizarSensibilidad(sensibilidad: Sensibilidad) {
    this.mfsData.Sensibilidad = sensibilidad;
    const mfs = this.mfsData;
    mfs.FlujoCaja = BLFlujoCaja.obtenerFlujoCaja(mfs);
  }

  // Import
  importarMfs(mfs: Mfs) {
    this.mfsData = mfs;
    this.inicializarMfs();
    this.editing = true;
  }

  empezarMfs() {
    this.editing = true;
  }
}
