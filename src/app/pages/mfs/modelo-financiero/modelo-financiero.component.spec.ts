import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeloFinancieroComponent } from './ubicacion.component';

describe('GeneralComponent', () => {
  let component: ModeloFinancieroComponent;
  let fixture: ComponentFixture<ModeloFinancieroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeloFinancieroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeloFinancieroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
