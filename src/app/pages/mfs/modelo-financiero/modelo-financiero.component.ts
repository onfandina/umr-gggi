import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MfsModuloFinanciero} from "../../../core/entities/MFS/MfsModuloFinanciero";
import {BLCredito} from "../../../core/business/MFS/BLCredito";
import {PeriodosInteres} from "../../../core/entities/MFS/Enums/PeriodosInteres";

@Component({
  selector: 'mfs-modelo-financiero',
  templateUrl: './modelo-financiero.component.html',
  styleUrls: ['./modelo-financiero.component.scss']
})

export class ModeloFinancieroComponent implements OnInit {

  @Input() default?: MfsModuloFinanciero;
  @Output() eventoModuloFinanciero = new EventEmitter<any>();

  //Form controls
  formModuloFinanciero = new FormGroup({
    fcontrolTasaInteres: new FormControl(),
    fcontrolPeriodo: new FormControl(''),
    fcontrolTasaInteresAnual: new FormControl({value: 0, disabled: true}),
    fcontrolCapital: new FormControl(),
    fcontrolPlazo: new FormControl(),
  });

  periodosIntereses = [
    PeriodosInteres.MENSUAL,
    PeriodosInteres.BIMESTRAL,
    PeriodosInteres.TRIMESTRAL,
    PeriodosInteres.CUATRIMESTRAL,
    PeriodosInteres.SEMESTRAL,
  ];

  mfsModuloFinanciero = new MfsModuloFinanciero();


  ngOnInit(): void {
    if (this.default) {
      this.updateForm(this.default);
    }
  }

  updateForm(moduloFinanciero: MfsModuloFinanciero) {
    this.formModuloFinanciero.get('fcontrolTasaInteres')?.setValue((moduloFinanciero.Interes * 100).toFixed(2));
    this.formModuloFinanciero.get('fcontrolPeriodo')?.setValue(moduloFinanciero.PeriodoInteres);
    this.formModuloFinanciero.get('fcontrolCapital')?.setValue(moduloFinanciero.Capital);
    this.formModuloFinanciero.get('fcontrolPlazo')?.setValue(moduloFinanciero.Plazo);
    BLCredito.calcularInteresAnual(moduloFinanciero);
    this.formModuloFinanciero.get('fcontrolTasaInteresAnual')?.setValue((moduloFinanciero.Tasa * 100).toFixed(2));
  }

  cambioPeriodo() {
    this.mfsModuloFinanciero.PeriodoInteres = this.formModuloFinanciero.get('fcontrolPeriodo')?.value;
    this.mfsModuloFinanciero.Interes = this.formModuloFinanciero.get('fcontrolTasaInteres')?.value / 100;
    if (this.mfsModuloFinanciero.Interes > 0) {
      BLCredito.calcularInteresAnual(this.mfsModuloFinanciero);
      this.formModuloFinanciero.get('fcontrolTasaInteresAnual')?.setValue((this.mfsModuloFinanciero.Tasa * 100).toFixed(2));
    }
  }

  guardar() {
    this.mfsModuloFinanciero.PeriodoInteres = this.formModuloFinanciero.get('fcontrolPeriodo')?.value;
    this.mfsModuloFinanciero.Interes = this.formModuloFinanciero.get('fcontrolTasaInteres')?.value / 100;
    this.mfsModuloFinanciero.Capital = this.formModuloFinanciero.get('fcontrolCapital')?.value;
    this.mfsModuloFinanciero.Plazo = this.formModuloFinanciero.get('fcontrolPlazo')?.value;
    BLCredito.calcularInteresAnual(this.mfsModuloFinanciero);
    this.eventoModuloFinanciero.emit(this.mfsModuloFinanciero);
  }

}
