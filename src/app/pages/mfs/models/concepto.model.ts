export class Concepto {
  nombre: string = '';
  cantidad: number = 0;
  valor: number = 0;

  public constructor(init?: Partial<Concepto>) {
    Object.assign(this, init);
  }
}
