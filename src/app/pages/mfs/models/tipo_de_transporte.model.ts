import {Concepto} from "./concepto.model";

export class TipoDeTransporte {
  nombre: string = '';
  valores: Array<number> = new Array<number>();

  public constructor(init?: Partial<TipoDeTransporte>) {
    Object.assign(this, init);
  }
}
