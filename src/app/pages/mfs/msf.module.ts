import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GeneralComponent} from './general/general.component';
import {CommonLibsModule} from 'src/app/components/common.module';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {EspeciesComponent} from './especies/especies.component';
import {PlazasDeVentaComponent} from './especies/sub-components/plazas-de-venta/plazas-de-venta.component';
import {InfoDeLasEspeciesComponent} from './especies/sub-components/info-de-las-especies/info-de-las-especies.component';
import {ProductoDialogComponent} from './especies/sub-components/producto/dialog/producto.dialog.component';
import {TransformacionComponent} from './especies/sub-components/transformacion/transformacion.component';
import {CostosComponent} from './costos/costos.component';
import {AsistenciaTecnicaComponent} from "./costos/sub-components/asistencia-tecnica/asistencia-tecnica.component";
import {CostoDeLaTierraComponent} from "./costos/sub-components/costo-de-la-tierra/costo-de-la-tierra.component";
import {ConstruccionYMantenimientoComponent} from "./costos/sub-components/construccion-y-mantenimiento/construccion-y-mantenimiento.component";
import {CostoDeAprovechamientoComponent} from "./costos/sub-components/costo-de-aprovechamiento/costo-de-aprovechamiento.component";
import {TransporteMayorComponent} from "./costos/sub-components/transporte-mayor/transporte-mayor.component";
import {ImpuestosYOtrosCostosComponent} from "./costos/sub-components/impuestos-y-otros-costos/impuestos-y-otros-costos.component";
import {MatTabsModule} from "@angular/material/tabs";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatCardModule} from "@angular/material/card";
import {MatMenuModule} from "@angular/material/menu";
import {MatDialogModule} from "@angular/material/dialog";
import {SistemasTransformacionComponent} from "./sistemas-transformacion/sistemas-transformacion.component";
import {SistemaComponent} from "./sistema/sistema.component";
import {SistemaCostosComponent} from "./sistema/sub-components/sistema-costos/sistema-costos.component";
import {SistemasTransporteMenorComponent} from "./sistemas-transporte-menor/sistemas-transporte-menor.component";
import {UbicacionComponent} from "./ubicacion/ubicacion.component";
import {ModeloFinancieroComponent} from "./modelo-financiero/modelo-financiero.component";
import {MultipleValuesComponent} from "./common/multiple-values/multiple-values.component";
import {ListaCostosComponent} from "./common/lista-costos/lista-costos.component";
import {IngresoComponent} from "./ingreso/ingreso.component";
import {ProductosComponent} from "./productos/productos.component";
import {PorcentajesAsignacionComponent} from "./common/porcentajes-asignacion/porcentajes-asignacion.component";
import {FlujoCajaComponent} from "./flujo-caja/flujo-caja.component";
import {SensibilidadComponent} from "./sensibilidad/sensibilidad.component";
import {MfsExportComponent} from "./common/mfs-export/mfs-export.component";
import {MfsImportComponent} from "./common/mfs-import/mfs-import.component";

@NgModule({
  declarations: [
    UbicacionComponent,
    GeneralComponent,
    ModeloFinancieroComponent,
    MultipleValuesComponent,
    EspeciesComponent,
    PlazasDeVentaComponent,
    InfoDeLasEspeciesComponent,
    ProductoDialogComponent,
    TransformacionComponent,
    FlujoCajaComponent,
    SistemaComponent,
    SistemaCostosComponent,
    SistemasTransporteMenorComponent,
    SistemasTransformacionComponent,
    CostosComponent,
    AsistenciaTecnicaComponent,
    CostoDeLaTierraComponent,
    ConstruccionYMantenimientoComponent,
    CostoDeAprovechamientoComponent,
    TransporteMayorComponent,
    ImpuestosYOtrosCostosComponent,
    ListaCostosComponent,
    IngresoComponent,
    ProductosComponent,
    TransformacionComponent,
    PorcentajesAsignacionComponent,
    SensibilidadComponent,
    MfsExportComponent,
    MfsImportComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    CommonLibsModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatExpansionModule,
    MatCardModule,
    MatMenuModule,
    MatDialogModule,
  ],
  exports: [
    UbicacionComponent,
    GeneralComponent,
    ModeloFinancieroComponent,
    EspeciesComponent,
    SistemaComponent,
    SistemasTransformacionComponent,
    CostosComponent,
    SistemasTransporteMenorComponent,
    IngresoComponent,
    ProductosComponent,
    TransformacionComponent,
    FlujoCajaComponent,
    SensibilidadComponent,
    MfsExportComponent,
    MfsImportComponent
  ],
})
export class MsfModule {
}
