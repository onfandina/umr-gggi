import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MatTableDataSource} from "@angular/material/table";
import {PFNM} from "../../../core/entities/MFS/PFNM";

@Component({
  selector: 'mfs-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})

export class ProductosComponent implements OnInit {
  @Output() guardarProductos = new EventEmitter<PFNM[]>();

  formGroupFields = new FormGroup({
    nombre: new FormControl(''),
    unidad: new FormControl(''),
  });

  actualProducto = -1;
  verFormulario = false;
  listaPfnm: PFNM[] = [];

  tablaProductos: string[] = [
    'nombre',
    'unidad',
    'operaciones',
  ];
  dataSourcePfnm = new MatTableDataSource<any>();


  ngOnInit(): void {
  }

  agregarProducto() {
    this.verFormulario = true;
  }

  crearProducto() {
    let data = this.formGroupFields.value;
    const producto = new PFNM();
    producto.Nombre = data.nombre;
    producto.Unidades = data.unidad;

    if (this.actualProducto < 0) {
      this.listaPfnm.push(producto);
    } else {
      this.listaPfnm[this.actualProducto] = producto;
    }
    this.actualizarTabla();
    this.reiniciar();
  }

  editarProducto(index: number) {
    const pfnm = this.listaPfnm[index];
    this.actualProducto = index;
    this.formGroupFields.reset({
      nombre: pfnm.Nombre,
      unidad: pfnm.Unidades
    });

    this.verFormulario = true;
    this.actualizarTabla();
  }

  eliminarProducto(index: number) {
    if (confirm("Está seguro de eliminar el producto: " + this.listaPfnm[index].Nombre + "?")) {

      this.listaPfnm.splice(index, 1);
      this.actualizarTabla();
    }
  }

  actualizarTabla() {
    this.dataSourcePfnm.data = this.listaPfnm.map(pfnm => ({
      nombre: pfnm.Nombre,
      unidad: pfnm.Unidades,
    }));
    this.guardarProductos.emit(this.listaPfnm);
  }

  reiniciar() {
    this.formGroupFields.reset();
    this.verFormulario = false;
    this.actualProducto = -1;
  }

  guardar() {
    this.guardarProductos.emit(this.listaPfnm);
  }

}


