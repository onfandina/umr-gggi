import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Sensibilidad} from "../../../core/entities/MFS/Sensibilidad";
import {merge} from "lodash";
import {Mfs} from "../../../core/entities/MFS/Mfs";
import {BLFlujoCaja} from "../../../core/business/MFS/BLFlujoCaja";
// @ts-ignore
import SimpleLinearRegression from 'ml-regression-simple-linear';
// @ts-ignore
import ExponentialRegression from 'ml-regression-exponential';
import {ExpReg} from 'exponential-regression';
import {formatCurrency} from "@angular/common";

@Component({
  selector: 'mfs-sensibilidad',
  templateUrl: './sensibilidad.component.html',
  styleUrls: ['./sensibilidad.component.scss']
})

export class SensibilidadComponent implements OnInit {
  @Input() default?: Sensibilidad;
  @Input() mfs: Mfs = new Mfs();
  @Output() eventoSensibilidad = new EventEmitter<any>();

  //Form controls
  formulario = new FormGroup({
    fcontrolRendimientoAprovechamiento: new FormControl(),
    fcontrolArea: new FormControl(),
    fcontrolAsistenciaTecnica: new FormControl(),
    fcontrolAdministracion: new FormControl(),
    fcontrolIngresos: new FormControl(),
    fcontrolCostos: new FormControl(),
    fcontrolTasaInteres: new FormControl(),
    fcontrolIngresoPromedio: new FormControl(),
    fcontrolIngresoPromedioTotal: new FormControl({value: '', disabled: true}),
    fcontrolIngresoTotal: new FormControl({value: '', disabled: true}),
    fcontrolIngresoEsperado: new FormControl(),
    fcontrolTirEsperada: new FormControl(),
  });

  sensibilidad = new Sensibilidad();

  sensibilidadIngresoPromedio = {
    ingresoEsperado: 0,
    ingresoEstimado: 0,
    sensibilidadArea: 1,
    area: 0,
    regression: null,
  };
  sensibilidadTIR = {
    tirEsperada: 0,
    tirEstimada: 0,
    sensibilidadArea: 1,
    area: 0,
    regression2: {predict: (n: number) => n},
    regression: null
  };

  columns = ['nombre', 'valor', 'observacion'];
  dataSource: any[] = [];

  ngOnInit(): void {
    if (this.default) {
      merge(this.sensibilidad, this.default);
      this.actualizarForm();
      this.cambiarIngresoPromedio();
    }
    this.sensibilidadIngresoPromedio.area = this.mfs.InformacionGeneral.AreaBosque;
  }

  actualizarForm() {
    this.sensibilidad.IngresoPromedio = this.sensibilidad.IngresoPromedioFamilia * (this.mfs.InformacionGeneral.Familias || 1);
    this.sensibilidad.IngresosTotal = this.sensibilidad.IngresoPromedio * this.mfs.InformacionGeneral.Uca * 12;
    this.formulario.get('fcontrolRendimientoAprovechamiento')?.setValue(this.sensibilidad.RendimientoAprovechamiento);
    this.formulario.get('fcontrolArea')?.setValue(this.sensibilidad.Area);
    this.formulario.get('fcontrolAsistenciaTecnica')?.setValue(this.sensibilidad.AsistenciaTecnica);
    this.formulario.get('fcontrolAdministracion')?.setValue(this.sensibilidad.Administracion);
    this.formulario.get('fcontrolIngresos')?.setValue(this.sensibilidad.Ingresos);
    this.formulario.get('fcontrolCostos')?.setValue(this.sensibilidad.Costos);
    this.formulario.get('fcontrolTasaInteres')?.setValue(this.sensibilidad.TasaInteres);
    this.formulario.get('fcontrolIngresoPromedio')?.setValue(this.sensibilidad.IngresoPromedioFamilia);
    this.formulario.get('fcontrolIngresoPromedioTotal')?.setValue(formatCurrency(this.sensibilidad.IngresoPromedio, 'en-CO', '$'));
    this.formulario.get('fcontrolIngresoTotal')?.setValue(formatCurrency(this.sensibilidad.IngresosTotal, 'en-CO', '$'));
  }

  cambiarIngresoPromedio() {
    this.sensibilidad.IngresoPromedioFamilia = this.formulario.get('fcontrolIngresoPromedio')?.value;
    this.sensibilidad.IngresoPromedio = this.formulario.get('fcontrolIngresoPromedio')?.value * (this.mfs.InformacionGeneral.Familias || 1);
    this.sensibilidad.IngresosTotal = this.sensibilidad.IngresoPromedio * this.mfs.InformacionGeneral.Uca * 12;
    this.formulario.get('fcontrolIngresoPromedioTotal')?.setValue(formatCurrency(this.sensibilidad.IngresoPromedio, 'en-CO', '$'));
    this.formulario.get('fcontrolIngresoTotal')?.setValue(formatCurrency(this.sensibilidad.IngresosTotal, 'en-CO', '$'));
    this.actualizarTabla();
  }

  actualizarSensibilidad() {
    this.sensibilidad.RendimientoAprovechamiento = this.formulario.get('fcontrolRendimientoAprovechamiento')?.value;
    this.sensibilidad.Area = this.formulario.get('fcontrolArea')?.value;
    this.sensibilidad.AsistenciaTecnica = this.formulario.get('fcontrolAsistenciaTecnica')?.value;
    this.sensibilidad.Administracion = this.formulario.get('fcontrolAdministracion')?.value;
    this.sensibilidad.Ingresos = this.formulario.get('fcontrolIngresos')?.value;
    this.sensibilidad.Costos = this.formulario.get('fcontrolCostos')?.value;
    this.sensibilidad.TasaInteres = this.formulario.get('fcontrolTasaInteres')?.value;
    this.sensibilidad.IngresoPromedioFamilia = this.formulario.get('fcontrolIngresoPromedio')?.value;
    this.sensibilidad.IngresoPromedio = this.sensibilidad.IngresoPromedioFamilia * (this.mfs.InformacionGeneral.Familias || 1);
    this.sensibilidad.IngresosTotal = this.sensibilidad.IngresoPromedio * this.mfs.InformacionGeneral.Uca * 12;
    this.mfs.Sensibilidad = this.sensibilidad;

  }

  actualizarTabla() {
    this.actualizarSensibilidad();
    this.actualizarForm();
    this.dataSource = [];
    const flujo = BLFlujoCaja.obtenerFlujoCaja(this.mfs);
    if (this.sensibilidad.TIR !== flujo.TIR) {
      this.calcularRegresiones();
    }
    this.sensibilidad.TIR = flujo.TIR;
    this.sensibilidad.VPN = flujo.VPN;
    this.dataSource.push({
      nombre: 'Ingreso mínimo obtenido',
      valor: flujo.IngresoPromedio,
      observacion: flujo.IngresoPromedio > this.sensibilidad.IngresoPromedio ? 'Se cumple ingreso mínimo esperado' : 'No se cumple ingreso mínimo esperado'
    });
    this.dataSource.push({
      nombre: 'TIR',
      valor: flujo.TIR,
      observacion: flujo.TIR > flujo.DTF ? 'Cumple rentabilidad mínima esperada' : 'No cumple con condiciones mínimas de rentabilidad'
    });
    this.dataSource.push({
      nombre: 'VPN',
      valor: flujo.VPN,
      observacion: ''
    });
    this.dataSource.push({
      nombre: 'DTF',
      valor: flujo.DTF,
      observacion: ''
    });
  }

  calcularRegresiones() {

    const mfs = this.mfs;
    const x: number[] = [0.5, 0.7, 1, 1.2, 1.4, 1.6, 1.8, 2.5, 3, 3.5, 4, 5, 6, 7, 8, 9];
    const y: number[] = [];
    const z: number[] = [];
    x.forEach((areaSensibilidad) => {
      mfs.Sensibilidad.Area = areaSensibilidad;
      const flujo = BLFlujoCaja.obtenerFlujoCaja(mfs);
      y.push(flujo.TIR);
      z.push(flujo.IngresoPromedio);
    });
    this.sensibilidadTIR.regression = new ExponentialRegression(y, x);
    const {a, b, c} = ExpReg.solve(y, x);
    this.sensibilidadTIR.regression2.predict = (n: number) => a + b * Math.exp(c * n);
    this.sensibilidadIngresoPromedio.regression = new SimpleLinearRegression(x, z);
  }

  estimarSensibilidadTIR() {
    const tir = this.formulario.get('fcontrolTirEsperada')?.value;
    if (tir > 0) {
      const mfs = this.mfs;
      if (!this.sensibilidadTIR.regression) {
        this.calcularRegresiones();
      }
      this.sensibilidadTIR.tirEsperada = tir / 100;
      const s1 = (this.sensibilidadTIR.regression as ExponentialRegression)?.predict(this.sensibilidadTIR.tirEsperada);
      const s2 = this.sensibilidadTIR.regression2.predict(this.sensibilidadTIR.tirEsperada);
      mfs.Sensibilidad.Area = s1;
      const t1 = BLFlujoCaja.obtenerFlujoCaja(mfs).TIR;
      mfs.Sensibilidad.Area = s2;
      const t2 = BLFlujoCaja.obtenerFlujoCaja(mfs).TIR;
      if (Math.abs(this.sensibilidadTIR.tirEsperada - t1) < Math.abs(this.sensibilidadTIR.tirEsperada - t2)) {
        this.sensibilidadTIR.tirEstimada = t1;
        this.sensibilidadTIR.sensibilidadArea = s1;
      } else {
        this.sensibilidadTIR.tirEstimada = t2;
        this.sensibilidadTIR.sensibilidadArea = s2;
      }
      this.sensibilidadTIR.area = this.sensibilidadTIR.sensibilidadArea * mfs.InformacionGeneral.AreaBosque;
    }
  }

  estimarSensibilidadIngresoPromedio() {
    const ingreso = (this.formulario.get('fcontrolIngresoEsperado')?.value || 0) * (this.mfs.InformacionGeneral.Familias > 0 ? this.mfs.InformacionGeneral.Familias : 1);
    if (ingreso > 0) {
      const mfs = this.mfs;
      if (!this.sensibilidadIngresoPromedio.regression) {
        this.calcularRegresiones();
      }
      this.sensibilidadIngresoPromedio.ingresoEsperado = ingreso;
      this.sensibilidadIngresoPromedio.sensibilidadArea = (this.sensibilidadIngresoPromedio.regression as SimpleLinearRegression)?.computeX(ingreso);
      this.sensibilidadIngresoPromedio.area = this.sensibilidadIngresoPromedio.sensibilidadArea * mfs.InformacionGeneral.AreaBosque;
      mfs.Sensibilidad.Area = this.sensibilidadIngresoPromedio.sensibilidadArea;
      this.sensibilidadIngresoPromedio.ingresoEstimado = BLFlujoCaja.obtenerFlujoCaja(mfs).IngresoPromedio;
    }
  }

  guardar() {
    this.actualizarSensibilidad();
    this.eventoSensibilidad.emit(this.sensibilidad);
  }

}
