import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Material} from "../../../core/entities/MFS/Material";
import {SistemaFijo} from "../../../core/entities/MFS/SistemaFijo";
import {Rendimiento} from "../../../core/entities/MFS/Rendimiento";
import {Sistema} from "../../../core/entities/MFS/Sistema";
import {SistemaModelo} from "../../../core/entities/MFS/SistemaModelo";
import {CostoSistema} from "../../../core/entities/MFS/Costos/CostoSistema";
import {BLSistemas} from "../../../core/business/MFS/BLSistemas";
import {FactorRendimiento} from "../../../core/entities/MFS/FactorRendimiento";
import {Madera} from "../../../core/entities/MFS/Madera";
import {PFNM} from "../../../core/entities/MFS/PFNM";
import {Observable} from "rxjs";
import {map, startWith} from "rxjs/operators";

@Component({
  selector: 'mfs-sistema',
  templateUrl: './sistema.component.html',
  styleUrls: ['./sistema.component.scss']
})

export class SistemaComponent implements OnInit {
  @Input() listaNombres?: string[];
  @Input() materiales: Material[] = [];
  @Input() sistema?: Sistema;
  @Input() nombre?: string;
  @Output() guardarSistema = new EventEmitter<Sistema>();
  @Output() cancelarSistema = new EventEmitter<Sistema | undefined>();

  verCancelar = false;
  placeholder = '';

  formGroupFields = new FormGroup({
    fcontrolNombre: new FormControl(''),
    fcontrolTipo: new FormControl(''),
  });

  formGroupSistemaFijo = new FormGroup({});
  formGroupSistemaDetallado = new FormGroup({});

  // Form data
  tipoSistema = '';
  costosSistemaDetallado: CostoSistema[] = [];

  // @ts-ignore
  nombresFiltrados: Observable<string[]>;

  maderas: Madera[] = [];
  pfnm: PFNM[] = [];

  ngOnInit(): void {
    this.actualizar();
    if (this.listaNombres) {
      this.nombresFiltrados = this.formGroupFields.controls.fcontrolNombre.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value)),
      );
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.materiales) {
      this.materiales = changes.materiales.currentValue;
      this.actualizar();
    }
  }

  private _filter(value: string = ''): string[] {
    const filterValue = value?.toLowerCase() || '';

    return this.listaNombres?.filter(option => option.toLowerCase().includes(filterValue)) || [];
  }

  actualizarPlaceholder(option: string) {
    if (option === 'Otro') {
      this.formGroupFields.get('fcontrolNombre')?.setValue('')
      this.placeholder = 'Digite el tipo de sistema';
    } else {
      this.placeholder = '';
    }
  }

  actualizar() {
    const maderas: Madera[] = [];
    const pfnm: PFNM[] = [];
    this.materiales.forEach((material: Material) => {
      this.formGroupSistemaFijo.addControl(material.Nombre, new FormControl(''));
      this.formGroupSistemaDetallado.addControl(material.Nombre, new FormControl(''));
      if (material instanceof Madera) {
        maderas.push(material);
      } else if (material instanceof PFNM) {
        pfnm.push(material);
      }
    });
    this.maderas = maderas;
    this.pfnm = pfnm;
    this.verCancelar = this.cancelarSistema.observers.length > 0;
    this.actualizarForm();
  }

  actualizarForm() {
    if (this.sistema) {
      if (this.sistema instanceof SistemaModelo) {
        this.tipoSistema = 'detallado';
        this.formGroupFields.reset({
          fcontrolNombre: this.sistema.Nombre,
          fcontrolTipo: this.tipoSistema
        });
        this.formGroupSistemaDetallado.reset({});
        this.sistema.FactoresRendimiento.forEach(rendimiento => {
          this.formGroupSistemaDetallado.patchValue({[rendimiento.Material.Nombre]: rendimiento.FactorRendimiento});
        });
        this.costosSistemaDetallado = [...this.sistema.Costos];
      } else {
        this.tipoSistema = 'fijo';
        this.formGroupFields.reset({
          fcontrolNombre: this.sistema.Nombre,
          fcontrolTipo: this.tipoSistema
        });
        this.formGroupSistemaFijo.reset({});
        this.sistema.Rendimientos.forEach(rendimiento => {
          this.formGroupSistemaFijo.patchValue({[rendimiento.Material.Nombre]: rendimiento.Rendimiento});
        });
      }
    }
  }

  cambioTipo() {
    this.tipoSistema = this.formGroupFields.get('fcontrolTipo')?.value;
  }

  actualizarCostosSistema(costos: CostoSistema[]) {
    this.costosSistemaDetallado = [...costos];
  }

  guardar() {
    if (this.tipoSistema === 'fijo') {
      const sistema = new SistemaFijo();
      sistema.Nombre = this.nombre || this.formGroupFields.get('fcontrolNombre')?.value;
      sistema.Rendimientos = this.materiales.map((material: Material) => new Rendimiento(material, this.formGroupSistemaFijo.get(material.Nombre)?.value));
      this.guardarSistema.emit(sistema);
    } else if (this.tipoSistema === 'detallado') {
      const sistema = new SistemaModelo();
      sistema.Nombre = this.nombre || this.formGroupFields.get('fcontrolNombre')?.value;
      sistema.FactoresRendimiento = this.materiales.map((material: Material) => new FactorRendimiento(material, this.formGroupSistemaDetallado.get(material.Nombre)?.value));
      sistema.Costos = this.costosSistemaDetallado;
      BLSistemas.CalcularRendimientos(sistema);
      this.guardarSistema.emit(sistema);
    }
  }

  cancelar() {
    this.reset();
    this.costosSistemaDetallado = [];
    this.sistema = undefined;
    this.cancelarSistema.emit(this.sistema);
  }

  reset() {
    this.tipoSistema = '';
    this.formGroupFields.reset();
    this.formGroupSistemaFijo.reset();
    this.formGroupSistemaDetallado.reset();
  }
}


