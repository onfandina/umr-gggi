import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {CostoSistema} from "../../../../../core/entities/MFS/Costos/CostoSistema";
import {MatTableDataSource} from "@angular/material/table";
import {Especie} from "../../../../../core/entities/MFS/Especie";

@Component({
  selector: 'mfs-sistema-costos',
  templateUrl: './sistema-costos.component.html',
  styleUrls: ['./sistema-costos.component.scss']
})

export class SistemaCostosComponent implements OnInit {
  @Input() costos: CostoSistema[] = [];
  @Output() guardarCostos = new EventEmitter<CostoSistema[]>();

  formGroupFields = new FormGroup({
    nombre: new FormControl(''),
    unidad: new FormControl(''),
    cantidad: new FormControl(''),
    valor: new FormControl(''),
  });

  actualCosto = -1;
  verFormulario = false;
  listaCostos: CostoSistema[] = [];

  tablaCostos: string[] = [
    'nombre',
    'unidad',
    'cantidad',
    'valor',
    'operaciones',
  ];
  dataSourceCostos = new MatTableDataSource<any>();


  ngOnInit(): void {
    this.listaCostos = [...this.costos];
    this.actualizarTabla();
  }

  agregarCosto() {
    this.verFormulario = true;
  }

  crearCosto() {
    let data = this.formGroupFields.value;
    const costo = new CostoSistema(data.nombre, data.valor, data.cantidad, data.unidad);

    if (this.actualCosto < 0) {
      this.listaCostos.push(costo);
    } else {
      this.listaCostos[this.actualCosto] = costo;
    }
    this.actualizarTabla();
    this.reiniciar();
    this.guardarCostos.emit(this.listaCostos);
  }

  editarCosto(index: number) {
    const costo = this.listaCostos[index];
    this.actualCosto = index;
    this.formGroupFields.reset({
      nombre: costo.Nombre,
      unidad: costo.Unidad,
      cantidad: costo.Cantidad,
      valor: costo.Valor,
    });

    this.verFormulario = true;
    this.guardarCostos.emit(this.listaCostos);
  }

  eliminarCosto(index: number) {
    if (confirm("Está seguro de eliminar el costo: " + this.listaCostos[index].Nombre + "?")) {

      this.listaCostos.splice(index, 1);
      this.actualizarTabla();
      this.guardarCostos.emit(this.listaCostos);
    }
  }

  actualizarTabla() {
    this.dataSourceCostos.data = this.listaCostos.map(costo => ({
      nombre: costo.Nombre,
      unidad: costo.Unidad,
      cantidad: costo.Cantidad,
      valor: costo.Valor,
    }));
  }

  reiniciar() {
    this.formGroupFields.reset();
    this.verFormulario = false;
    this.actualCosto = -1;
  }

}


