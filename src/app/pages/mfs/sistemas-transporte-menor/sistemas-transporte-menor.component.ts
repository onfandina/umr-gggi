import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Material} from "../../../core/entities/MFS/Material";
import {Sistema} from "../../../core/entities/MFS/Sistema";
import {mfsSistemasTransporteMenor} from "../../../models/mfsSistemas";

@Component({
  selector: 'mfs-sistemas-transporte-menor',
  templateUrl: './sistemas-transporte-menor.component.html',
  styleUrls: ['./sistemas-transporte-menor.component.scss']
})

export class SistemasTransporteMenorComponent implements OnInit {
  @Input() default: Sistema[] = [];
  @Input() materiales: Material[] = [];
  @Output() guardarSistemas = new EventEmitter<Sistema[]>();

  verFormularioSistema = false;
  listaSistemas: Sistema[] = [];
  listaNombres = mfsSistemasTransporteMenor;
  actualSistemaId: number = -1;

  dataSourceSistemas = new MatTableDataSource<any>();
  tablaSistemas: string[] = [
    'nombreSistema',
  ];

  // Form data
  tipoSistema = '';

  ngOnInit(): void {
    this.materiales.forEach(material => {
      this.tablaSistemas.push(material.Nombre);
    });
    this.tablaSistemas.push('operaciones');
    if (this.default) {
      this.listaSistemas = this.default;
    }
    this.actualizarTablaSistemas();
  }

  agregarSistema() {
    this.verFormularioSistema = true;
  }

  actualizarTablaSistemas() {
    this.dataSourceSistemas.data = this.listaSistemas.map(sistema => {
      const data: any = {};
      data.nombreSistema = sistema.Nombre;
      sistema.Rendimientos.forEach(rendimiento => {
        data[rendimiento.Material.Nombre] = rendimiento.Rendimiento.toFixed(2);
      });
      return data;
    });
  }

  guardarSistema(sistema: Sistema) {
    if (this.actualSistemaId > -1) {
      this.listaSistemas[this.actualSistemaId] = sistema;
    } else {
      this.listaSistemas.push(sistema);
    }
    this.verFormularioSistema = false;
    this.actualSistemaId = -1;
    this.actualizarTablaSistemas();
    this.guardarSistemas.emit(this.listaSistemas);
  }

  guardar() {
    this.guardarSistemas.emit(this.listaSistemas);
  }

  cancelarSistema(sistema?: Sistema) {
    this.actualizarTablaSistemas();
    this.verFormularioSistema = false;
    this.actualSistemaId = -1;
  }

  editarSistema(sistemaId: number) {
    this.actualSistemaId = sistemaId;
    this.verFormularioSistema = true;
  }

  eliminarSistema(sistemaId: number) {
    const sistema = this.listaSistemas[sistemaId];
    if (confirm("Está seguro de eliminar el transporte menor: " + sistema.Nombre + "?")) {
      this.listaSistemas.splice(sistemaId, 1);
      this.actualizarTablaSistemas();
      this.guardarSistemas.emit(this.listaSistemas);
    }
  }

}


