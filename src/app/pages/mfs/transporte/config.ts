export const transporteValues = [
  { label: 'Transporte Menor', value: 1 },
  { label: 'Transporte Mayor', value: 2 },
];

export const transporteMenorValues = [
  { label: 'Mecanizado', value: 1 },
  { label: 'Animal', value: 2 },
];

export const transporteMayorValues = [
  { label: 'Terrestre', value: 1 },
  { label: 'Fluvial', value: 2 },
];