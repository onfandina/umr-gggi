import { Component, OnInit } from '@angular/core';
import {
  transporteValues,
  transporteMayorValues,
  transporteMenorValues,
} from './config';

@Component({
  selector: 'mfs-transporte',
  templateUrl: './transporte.component.html',
  styleUrls: ['./transporte.component.scss'],
})
export class TransporteComponent implements OnInit {
  transporteQt: string[] = [];

  // Ranges
  transporteValues = transporteValues;
  transporteMenorValues = transporteMenorValues;
  transporteMayorValues = transporteMayorValues;

  // Form values
  tranposrte_menor = '';
  transporte_submenor = '';
  transporte_menor_distancia = '';
  transporte_menor_valor = '';

  tranposrte_mayor = '';
  transporte_submayor = '';
  transporte_mayor_distancia = '';
  transporte_mayor_valor = '';

  constructor() {}

  ngOnInit(): void {}
}
