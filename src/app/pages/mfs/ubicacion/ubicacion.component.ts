import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MfsInformacionGeneral} from "../../../core/entities/MFS/MfsInformacionGeneral";
import {locations} from "../../../models/departamentos";
import {rangoAlturaValues} from "../../../models/rangoAltura";
import {autoridadesAmbientales} from "../../../models/autoridadesAmbientales";
import {Ubicacion} from "../../../core/entities/MFS/Ubicacion";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'mfs-ubicacion',
  templateUrl: './ubicacion.component.html',
  styleUrls: ['./ubicacion.component.scss']
})

export class UbicacionComponent implements OnInit {
  @Input() default?: Ubicacion;
  @Output() eventoUbicacion = new EventEmitter<any>();

  //Form controls
  formUbicacion = new FormGroup({
    fcontrolDepartamento: new FormControl(''),
    fcontrolMunicipio: new FormControl(''),
    fcontrolAltura: new FormControl(''),
    fcontrolAutoridad: new FormControl(''),
    fcontrolPendiente: new FormControl(false),
    fcontrolPendienteArea: new FormControl(false),
    fcontrolPof: new FormControl(false),
    fcontrolPmf: new FormControl(false),
  });

  completo = false;

  ubicacion: Ubicacion = new Ubicacion();

  departamentos = Array.from(new Set(locations.map((key) => key.departamento))).sort();
  municipios: string[] = [];
  alturas = rangoAlturaValues;
  autoridades = autoridadesAmbientales;
  verPendienteArea = false;

  constructor(private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    if (this.default?.Departamento) {
      this.formUbicacion.get('fcontrolDepartamento')?.setValue(this.default.Departamento);
      this.formUbicacion.get('fcontrolMunicipio')?.setValue(this.default.Municipio);
      this.formUbicacion.get('fcontrolAltura')?.setValue(this.default.Altura);
      this.formUbicacion.get('fcontrolAutoridad')?.setValue(this.default.Autoridad);
      this.formUbicacion.get('fcontrolPendiente')?.setValue(this.default.Pendiente);
      this.formUbicacion.get('fcontrolPendienteArea')?.setValue(this.default.PendienteArea);
      this.formUbicacion.get('fcontrolPof')?.setValue(this.default.POF);
      this.formUbicacion.get('fcontrolPmf')?.setValue(this.default.PMF);
    }

  }

  cambioDepartamento() {
    this.municipios = Array.from(new Set(locations.filter(u => u.departamento === this.formUbicacion.get('fcontrolDepartamento')?.value).map((key) => key.municipio))).sort();
  }

  cambioPendiente() {
    this.verPendienteArea = this.formUbicacion.get('fcontrolPendiente')?.value;
    this.validar();
  }

  validar() {
    if (this.formUbicacion.get('fcontrolPendiente')?.value && this.formUbicacion.get('fcontrolPendienteArea')?.value) {
      this.completo = false;
      this._snackBar.open('Su proyecto no es viable', 'cerrar');
      return;
    }
    if (!this.formUbicacion.get('fcontrolPmf')?.value && !this.formUbicacion.get('fcontrolPof')?.value) {
      this.completo = false;
      this._snackBar.open('Su proyecto no es viable', 'cerrar');
      return;
    }
    this._snackBar.dismiss();
    this.completo = true;
  }

  guardar() {
    this.ubicacion.Departamento = this.formUbicacion.get('fcontrolDepartamento')?.value;
    this.ubicacion.Municipio = this.formUbicacion.get('fcontrolMunicipio')?.value;
    this.ubicacion.Altura = this.formUbicacion.get('fcontrolAltura')?.value;
    this.ubicacion.Autoridad = this.formUbicacion.get('fcontrolAutoridad')?.value;
    this.ubicacion.Pendiente = this.formUbicacion.get('fcontrolPendiente')?.value;
    this.ubicacion.PendienteArea = this.formUbicacion.get('fcontrolPendienteArea')?.value;
    this.ubicacion.POF = this.formUbicacion.get('fcontrolPof')?.value;
    this.ubicacion.PMF = this.formUbicacion.get('fcontrolPmf')?.value;
    this.eventoUbicacion.emit(this.ubicacion);
  }

}
