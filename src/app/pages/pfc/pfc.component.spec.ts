import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PFCComponent } from './pfc.component';

describe('PFCComponent', () => {
  let component: PFCComponent;
  let fixture: ComponentFixture<PFCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PFCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PFCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
