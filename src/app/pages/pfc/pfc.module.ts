import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UbicacionComponent } from '../../components/componentes-flujo/ubicacion/ubicacion.component';
import { EspecieComponent } from '../../components/componentes-flujo/especie/especie.component';
import { ResultadosComponent } from '../../components/componentes-flujo/resultados/resultados.component';
import { CommonLibsModule } from 'src/app/components/common.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { ModeloInversionComponent } from '../../components/componentes-flujo/modelo-inversion/modelo-inversion.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { ProyectoComponent } from '../../components/componentes-flujo/proyecto/proyecto.component';
import { CostosComponent } from '../../components/componentes-flujo/costos/costos.component';
import { IngresosComponent } from '../../components/componentes-flujo/ingresos/ingresos.component';
import { SensibilidadComponent } from '../../components/componentes-flujo/sensibilidad/sensibilidad.component';
import { EspecificacionCostoDialogComponent } from '../../components/dialogos/especificacion-costo-dialog/especificacion-costo-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CostoEstablecimientoDialogComponent } from '../../components/dialogos/costo-establecimiento-dialog/costo-establecimiento-dialog.component';
import { CostoMantenimientoDialogComponent } from '../../components/dialogos/costo-mantenimiento-dialog/costo-mantenimiento-dialog.component';
import { CostoAprovechamientoDialogComponent } from '../../components/dialogos/costo-aprovechamiento-dialog/costo-aprovechamiento-dialog.component';
import { CostoTransporteMenorDialogComponent } from '../../components/dialogos/costo-transporte-menor-dialog/costo-transporte-menor-dialog.component';

@NgModule({
  declarations: [
    UbicacionComponent,
    EspecieComponent,
    ResultadosComponent,
    ModeloInversionComponent,
    ProyectoComponent,
    CostosComponent,
    IngresosComponent,
    SensibilidadComponent,
    EspecificacionCostoDialogComponent,
    CostoEstablecimientoDialogComponent,
    CostoMantenimientoDialogComponent,
    CostoAprovechamientoDialogComponent,
    CostoTransporteMenorDialogComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    CommonLibsModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatCardModule,
  ],
  exports: [
    UbicacionComponent,
    EspecieComponent,
    ResultadosComponent,
    ModeloInversionComponent,
    ProyectoComponent,
    CostosComponent,
    IngresosComponent,
    SensibilidadComponent
  ],
})
export class PfcModule {}
