import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { StepperOrientation } from '@angular/material/stepper';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UbicacionComponent } from 'src/app/components/componentes-flujo/ubicacion/ubicacion.component';
import { ModeloInversionComponent } from 'src/app/components/componentes-flujo/modelo-inversion/modelo-inversion.component';
import { ProyectoComponent } from 'src/app/components/componentes-flujo/proyecto/proyecto.component';
import { EspecieComponent } from 'src/app/components/componentes-flujo/especie/especie.component';
import { CostosComponent } from 'src/app/components/componentes-flujo/costos/costos.component';
import { IngresosComponent } from 'src/app/components/componentes-flujo/ingresos/ingresos.component';
import { SensibilidadComponent } from 'src/app/components/componentes-flujo/sensibilidad/sensibilidad.component';
import { ResultadosComponent } from 'src/app/components/componentes-flujo/resultados/resultados.component';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-restauracion',
  templateUrl: './restauracion.component.html',
  styleUrls: ['./restauracion.component.scss'],
})
export class RestauracionComponent implements OnInit {
  
  @ViewChild(UbicacionComponent) ubicacion!: UbicacionComponent;
  @ViewChild(ModeloInversionComponent) modeloInversion!: ModeloInversionComponent;
  @ViewChild(ProyectoComponent) infoProyecto!: ProyectoComponent;
  @ViewChild(EspecieComponent) especies!: EspecieComponent;
  @ViewChild(CostosComponent) costos!: CostosComponent;
  @ViewChild(IngresosComponent) ingresos!: IngresosComponent;
  @ViewChild(SensibilidadComponent) sensibilidad!: SensibilidadComponent;
  @ViewChild(ResultadosComponent) resultados!: ResultadosComponent;

  
  stepperOrientation: Observable<StepperOrientation>;

  constructor(breakpointObserver: BreakpointObserver) {
    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 800px)')
      .pipe(map(({ matches }) => (matches ? 'horizontal' : 'vertical')));

      sessionStorage.clear();
      sessionStorage.setItem('flujo', 'restauracion');
  }

  ngOnInit(): void {
    
  }

  //Obtienen los formGroups de los componentes
  get frmUbicacion(){
    return this.ubicacion ? this.ubicacion.fgroupUbicacion : new FormGroup({});
  }

  get frmModeloInversion(){
    return this.modeloInversion ? this.modeloInversion.formgModeloInversion : new FormGroup({});
  }
  
  get frmInfoProyecto() {
    return this.infoProyecto ? this.infoProyecto.formProyecto : new FormGroup({});
  }

  get frmEspecies(){
    return this.especies ? this.especies.fgroupEspecie : new FormGroup({});
  }

  get frmCostos(){
    return this.costos ? this.costos.fgroupCostos : new FormGroup({});
  }
  
  get frmIngresos(){
    return this.ingresos ? this.ingresos.fgroupIngresos : new FormGroup({});
  }

  get frmSensibilidad(){
    return this.sensibilidad ? this.sensibilidad.fgroupSensibilidad : new FormGroup({});
  }

  get frmResultados() {
    return this.resultados ? this.resultados.fgroupResultados : new FormGroup({});
  }

  //cambio de step
  guardarUbicacion(){
    this.ubicacion.guardarUbicacion();
  }

  guardarModeloInversion(){
    this.modeloInversion.guardarModeloInversion();
  }

  guardarInfoProyecto(){
    this.infoProyecto.guardarInfoProyecto();
  }

  guardarEspecies(){
    this.especies.guardarEspecies();
  }

  guardarCostos(){
    this.costos.guardarCostos();
  }

  guardarIngresos(){
    this.ingresos.guardarIngresos();
  }

  guardarSensibilidad(){
    this.sensibilidad?.guardarSensibilidad();
  }


  selectionChange(event: any){
    console.log('***cambió selección stepper: índice seleccionado '+ event.selectedIndex + ' Índice previamente seleccionado: ' + event.previouslySelectedIndex +' ***');
    switch (event.previouslySelectedIndex) {
      case 0:
        this.guardarUbicacion();
        break;
      case 1: 
        this.guardarModeloInversion();
        break;
      case 2:
        this.guardarInfoProyecto();
        break;
      case 3: 
        this.guardarEspecies();
        break;
      case 4:
        this.guardarCostos();
        break;
      case 5: 
        this.guardarIngresos();
        break;
      case 7:
        this.guardarSensibilidad();
        break;  
      default:
        break;
    }

    switch (event.selectedIndex) {
      case 6:
        this.resultados.calcularFlujoCaja();
        this.resultados.obtenerResultados();
        console.log("se calculó el flujo de caja");
        break;
    
      default:
        break;
    }
  }
}
