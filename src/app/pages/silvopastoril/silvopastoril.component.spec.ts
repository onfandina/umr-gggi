import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SilvopastorilComponent } from './silvopastoril.component';

describe('SilvopastorilComponent', () => {
  let component: SilvopastorilComponent;
  let fixture: ComponentFixture<SilvopastorilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SilvopastorilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SilvopastorilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
